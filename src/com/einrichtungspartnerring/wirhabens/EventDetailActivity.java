package com.einrichtungspartnerring.wirhabens;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Locale;


import com.einrichtungspartnerring.wirhabens.activityhelper.events.OnTaskLoginCompleteEvent;
import com.einrichtungspartnerring.wirhabens.helper.AirbrakeHelper;
import com.einrichtungspartnerring.wirhabens.helper.MeinERPDialogBuilder;
import com.einrichtungspartnerring.wirhabens.manager.EventsManager;
import com.einrichtungspartnerring.wirhabens.manager.ManagerFactory;
import com.einrichtungspartnerring.wirhabens.manager.NetworkManager;
import com.einrichtungspartnerring.wirhabens.parser.MeinERPParserException;
import com.einrichtungspartnerring.wirhabens.parser.Parser;
import com.einrichtungspartnerring.wirhabens.parser.ParserVO;
import com.einrichtungspartnerring.wirhabens.parser.builder.BookingBuilder;
import com.einrichtungspartnerring.wirhabens.vo.BookingVO;
import com.einrichtungspartnerring.wirhabens.vo.EventVO;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebSettings;
import android.webkit.WebView;




public class EventDetailActivity extends Activity implements OnTaskLoginCompleteEvent {
	private static final String TAG = "EventDetailActivity";
	
	private EventsManager eventManager;
	private EventVO myEvent;
	private Context mContext;
	private ProgressDialog dialog;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		mContext = this;
		try{
			setContentView(R.layout.activity_news_detail);
			if (android.os.Build.VERSION.SDK_INT <= Build.VERSION_CODES.GINGERBREAD_MR1){
				//TODO Android 2.3.3. Action bar coloring.
			}
			else{
				MainActivity.customActionBar(getActionBar(), this);

			}

			Bundle extras = getIntent().getExtras();
            if (extras == null)
                finish();

			int eventItemPos = extras.getInt("eventitempos");
			eventManager = ManagerFactory.buildEventsManager(this);

			myEvent = eventManager.getItemByPosition(eventItemPos, true);

			setEventData(myEvent);
			
			((StaffsaleApplication) getApplication()).trackEvent(getString(R.string.ga_category_news),getString(R.string.ga_action_read),myEvent.getId());	
		}catch(Exception e){
			String eMessage = "Unknown Exception";
			AirbrakeHelper.sendException(e, eMessage);
		}
 
	}
	/**
	 * Menu button
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.events_detail, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {	
		NetworkManager netManager = ManagerFactory.buildNetworkManager(mContext);
		if(netManager.isAuthenticated()) {
			new BookEventTask().execute(myEvent.getUrl());
		} else {
			Dialog loginDialog = MeinERPDialogBuilder.buildAuthenticatDialog(this, netManager, this);
			loginDialog.show();
		}
		return super.onOptionsItemSelected(item);
	}
	
	public class BookEventTask extends AsyncTask<String, Integer, BookingVO> {
		
		@Override
		protected void onPreExecute() {
			dialog = new ProgressDialog (mContext);
			dialog.setTitle(R.string.dialog_loading_title);
			dialog.setMessage(getString(R.string.dialog_loading_text));
			dialog.show();
			super.onPreExecute();
		}
		
		@Override
		protected BookingVO doInBackground(String... params) {
	        InputStream iStream = null;
	        ParserVO firstElement = null;
	        BookingVO booking = null;
	        HttpURLConnection urlConnection = null;
	        try {
	        	NetworkManager netManager = ManagerFactory.buildNetworkManager(mContext);
	        	URL url = new URL(params[0]);
	        	urlConnection = netManager.addAuthDataToConnection((HttpURLConnection) url.openConnection());
	            iStream = new BufferedInputStream(urlConnection.getInputStream());
				Parser parser = new Parser();
				firstElement = parser.parse(iStream);
				
				booking = BookingBuilder.buildBooking(firstElement);
				
	        } catch (MalformedURLException e) {
	            String eMessage = "url is wrong";
	            AirbrakeHelper.sendException(e, eMessage);
	        } catch (IOException e) { 
	            String eMessage = "loading content";
	            AirbrakeHelper.sendException(e, eMessage);
	        } catch (MeinERPParserException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
			     urlConnection.disconnect();  
			}
	        
	        return booking;
	     }

	     protected void onPostExecute(BookingVO result) {
	         
	    	 dialog.cancel();
	    	 AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);
	  
	 			// set title
	 			alertDialogBuilder.setTitle(myEvent.getTitle());
	  
	 			String message = getString(R.string.event_book_event_success);
	 			if(result == null || result.getStatus().equalsIgnoreCase("Error")) {
	 				message = getString(R.string.event_book_event_failure);
	 			}
	 			
	 			// set dialog message
	 			alertDialogBuilder
	 				.setMessage(message)
	 				.setCancelable(true)
	 				.setPositiveButton("OK",new DialogInterface.OnClickListener() {
	 					public void onClick(DialogInterface dialog,int id) {
	 					}
	 				  });
	  
	 				// create alert dialog
	 				AlertDialog alertDialog = alertDialogBuilder.create();
	  
	 				// show it
	 				alertDialog.show();
	 		}
	}
	
	public void setEventData(EventVO vo) {
		
        try {
            if (vo != null) {
         	
            	String templateString = ((StaffsaleApplication) getApplication()).getHtmlTemplateForFile("newsDetails.html");        	
                
            	templateString = templateString.replace("##META##","<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, maximum-scale=1.0\">");
            	templateString = templateString.replace("##CATEGORY##", "");
            	templateString = templateString.replace("##TITLE##", vo.getTitle());                	

                SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy", Locale.GERMANY);
                templateString = templateString.replace("##PUBLISHED##",df.format(vo.getDate()));
                templateString = templateString.replace("##SOURCE##","");
                templateString = templateString.replace("##CONTENT##",vo.getDescription());
            	
                WebView text = (WebView) findViewById(R.id.details_content);
                WebSettings settings = text.getSettings();
                settings.setDefaultFontSize(20);
                settings.setDefaultTextEncodingName("utf-8");
                text.loadData(templateString, "text/html; charset=utf-8", "UTF-8");
            }
            else {
                Log.w(TAG, "A deleted news was called.");
                finish();
            }
        } catch (Exception e) {
            String eMessage = "Unknown Exception";
            AirbrakeHelper.sendException(e, eMessage);
        }    
        vo.setReaded(true);
	}
	
	@Override
	public void onTaskLoginCompleteEvent() {
		new BookEventTask().execute(myEvent.getUrl());
	}
}

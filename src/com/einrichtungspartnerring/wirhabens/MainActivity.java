package com.einrichtungspartnerring.wirhabens;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.einrichtungspartnerring.wirhabens.activityhelper.InfoLoadAT;
import com.einrichtungspartnerring.wirhabens.activityhelper.events.OnInfoLoadCompleteEvent;
import com.einrichtungspartnerring.wirhabens.activityhelper.events.OnTaskLoginCompleteEvent;
import com.einrichtungspartnerring.wirhabens.fragments.StaffdealListFragment;
import com.einrichtungspartnerring.wirhabens.fragments.dealsFragment;
import com.einrichtungspartnerring.wirhabens.fragments.eventsFragment;
import com.einrichtungspartnerring.wirhabens.fragments.ideasFragment;
import com.einrichtungspartnerring.wirhabens.fragments.mediaFragment;
import com.einrichtungspartnerring.wirhabens.fragments.newsFragment;
import com.einrichtungspartnerring.wirhabens.gui.listener.MediaButtonListener;
import com.einrichtungspartnerring.wirhabens.helper.AirbrakeHelper;
import com.einrichtungspartnerring.wirhabens.helper.AndroidInformationHelper;
import com.einrichtungspartnerring.wirhabens.helper.MeinERPDialogBuilder;
import com.einrichtungspartnerring.wirhabens.helper.Rights;
import com.einrichtungspartnerring.wirhabens.imagedownloader.ImageDownloader;
import com.einrichtungspartnerring.wirhabens.manager.CompetitionIdeasManager;
import com.einrichtungspartnerring.wirhabens.manager.DealsManager;
import com.einrichtungspartnerring.wirhabens.manager.ImageManager;
import com.einrichtungspartnerring.wirhabens.manager.ManagerFactory;
import com.einrichtungspartnerring.wirhabens.manager.NetworkManager;
import com.einrichtungspartnerring.wirhabens.manager.NewsManager;
import com.einrichtungspartnerring.wirhabens.manager.SystemPaths;
import com.einrichtungspartnerring.wirhabens.properties.MeinERPProperties;
import com.einrichtungspartnerring.wirhabens.properties.MeinERPPropertiesFactory;
import com.einrichtungspartnerring.wirhabens.properties.PropertiesList;
import com.einrichtungspartnerring.wirhabens.vo.InfoVO;

import net.hockeyapp.android.CrashManager;
import net.hockeyapp.android.UpdateManager;

/**
 * Main activity with the tab view.
 * 
 * @author Thomas Möller
 * 
 */
public class MainActivity extends FragmentActivity implements PropertiesList,
OnInfoLoadCompleteEvent, ActionBar.TabListener  {

	public static boolean wasStarted = false;

	private static final String TAG = "MainActivity";

	private ViewPager mViewPager;
	StaffDealPagerAdapter mPagerAdapter;
	Activity myActivity;

	private final int NUM_TABS = 5;

	/** 
	 * Five minutes.
	 */
	private static final long FIVE_MINUTE = 300000;

	private newsFragment 	mNewsFragment;
	private ideasFragment 	mIdeasFragment;
	private dealsFragment 	mDealsFragment;
	private eventsFragment	mEventsFragment;
	private mediaFragment 	mMediaFragment;
	private StaffdealListFragment[] mFragmentList;
	private long[] mUpdatedTimes;
	private long mLastDealAdShow;
	private int mCurrentTab;

	private int mLoginPositionItem;

	/**
	 * System properties for this app.
	 */
	private MeinERPProperties properties;

	private ImageDownloader iDown;


	protected int currentTab() {
		try {
			return Integer.valueOf((String) this.properties.get(LAST_TAB_PROP));
		} catch (NumberFormatException e) {
			Log.e(TAG, "Problem with the properties", e);
		}
		return 0;
	}

	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	public static void customActionBar(final ActionBar actionbar,
			Context context) {
		actionbar.setBackgroundDrawable(context.getResources().getDrawable(
				R.color.staffdeal_orange));
	}

	public static void customActionBar() {
	}

	/**
	 * Initialization of the impotent content. First version here will create
	 * all needed folders.
	 */
	protected void init() {
		myActivity = this;
		mFragmentList = new StaffdealListFragment[NUM_TABS];

		//When the app inits set all reload timers to 0 so that all tabs upload
		mUpdatedTimes = new long[NUM_TABS];

		for(int i=0;i<NUM_TABS;i++) {
			mUpdatedTimes[i] = 0;
		}
		mLastDealAdShow = 0;

		this.properties = loadProperties();
		AndroidInformationHelper.load(this);
		AirbrakeHelper.load(this);
		File root = this.getFilesDir();
		String[] filelist = root.list();

		boolean imgDirNotExist = true;
		boolean xmlDirNotExist = true;
		boolean readedItemsDirNotExist = true;
		boolean barcodeDirNotExist = true;

		for (String dir : filelist) {
			if (dir.equals(SystemPaths.IMAGE_Folder)) {
				imgDirNotExist = false;
			} else if (dir.equals(SystemPaths.XML_Folder)) {
				xmlDirNotExist = false;
			} else if (dir.equals(SystemPaths.READED_FOLDER))
				readedItemsDirNotExist = false;
			else if (dir.equals(SystemPaths.BARCODE_FOLDER))
				barcodeDirNotExist = false;
		}
		if (imgDirNotExist) {
			File dir = new File(root.getPath() + SystemPaths.IMAGE_Folder);
			dir.mkdir();
		}
		if (xmlDirNotExist) {
			File dir = new File(root.getPath() + SystemPaths.XML_Folder);
			dir.mkdir();
		}
		if (readedItemsDirNotExist) {
			File dir = new File(root.getPath() + SystemPaths.BARCODE_FOLDER);
			dir.mkdir();
		}
		if (barcodeDirNotExist) {
			File dir = new File(root.getPath() + SystemPaths.READED_FOLDER);
			dir.mkdir();
		}
	}



	/**
	 * Load the default app system properties.
	 * 
	 * @return the loaded properties.
	 */
	private MeinERPProperties loadProperties() {
		MeinERPProperties properties = MeinERPPropertiesFactory.getProperties(this);
		return properties;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		wasStarted = true;
		init();
		setmCurrentTab(0);
		try {

			super.onCreate(savedInstanceState);
			if (android.os.Build.VERSION.SDK_INT <= Build.VERSION_CODES.GINGERBREAD_MR1) {
				customActionBar();
			} else {
				customActionBar(getActionBar(), this);
			}

			setContentView(R.layout.activity_main);

			// ViewPager and its adapters use support library
			// fragments, so use getSupportFragmentManager.
			mPagerAdapter = new StaffDealPagerAdapter(
					getSupportFragmentManager());
			mViewPager = (ViewPager) findViewById(R.id.pager);
			mViewPager.setAdapter(mPagerAdapter);
			mViewPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
				@Override
				public void onPageSelected(int position) {
					// When swiping between pages, select the
					// corresponding tab.
					setmCurrentTab(position);
					getActionBar().setSelectedNavigationItem(position);
					Log.d("FRAGMENT","Change to tab " + position);
					if(mFragmentList[position] != null && checkIfUpdateNeeded(position)) {
						mFragmentList[position].reloadData();
						Log.d("FRAGMENT","Reloading tab " + position);
					}
					if(!hasRights(position)) { 
						mLoginPositionItem = position;
						showLoginDiaglog();
					}
					if(position == getResources().getInteger(R.integer.deals_tab_position) 
							&& (System.currentTimeMillis() - FIVE_MINUTE > mLastDealAdShow)) {
						((dealsFragment)mFragmentList[position]).checkAdDeals();
						mLastDealAdShow = System.currentTimeMillis();
					}
				}
			});

			mViewPager.setOffscreenPageLimit(5);

			NetworkManager netManager = ManagerFactory
					.buildNetworkManager(this);
			if (this.properties.get(GMC_ACTIVE) != null) {
				try {
					Log.i(TAG, "registry on GCM");
					netManager.registGCM(this, this.properties);
				} catch (UnsupportedOperationException e) {
					Log.w(TAG, "This device didn't support GCM.");
				}

			}

			if (netManager.getInfo() == null) {
				new InfoLoadAT(this, netManager, this).execute();
			} else {
				onInfoLoadComplete();
			}

			tabViewSetup();
		} catch (Exception e) {
			String eMessage = "Exception on start";
			Log.e(TAG, eMessage, e);
			AirbrakeHelper.sendException(e, eMessage);
		}
		//HockeyApp code
		checkForUpdates();
	}

	private void checkForCrashes() {
	    CrashManager.register(this, "ea0e7e196d554ddb8e5f9f37a1fe0ef2");
	}

	private void checkForUpdates() {
	    // Remove this for store / production builds!
	    UpdateManager.register(this, "ea0e7e196d554ddb8e5f9f37a1fe0ef2");
	}
	  
	@Override
	protected void onResume() {
	    super.onResume();
	    checkForCrashes();
	}

	@Override
	public void onInfoLoadComplete(Object... objects) {

		InfoVO info = ManagerFactory.buildNetworkManager(this).getInfo();

		try {
			if (info != null
					&& info.getLatestVersion() != 0
					&& info.getUpdateText() != null
					&& AndroidInformationHelper.getAPKVersionCode(this) < info
					.getLatestVersion()) {

				AlertDialog.Builder builder = new AlertDialog.Builder(this);
				builder.setMessage(info.getUpdateText())
				.setPositiveButton("Ok",
						new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,
							int id) {
						// FIRE ZE MISSILES!
					}
				}).setTitle("Update verfügbar");
				// Create the AlertDialog object and return it
				builder.create().show();
			}
		} catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public StaffdealListFragment getCurrentFragment() {
		return mFragmentList[mViewPager.getCurrentItem()];	
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		Intent intent = null;
		int itemId = item.getItemId();
		if (itemId == R.id.main_menu_settings) {
			intent = new Intent(this, SettingsActivity.class);
			this.startActivity(intent);
		} else if (itemId == R.id.main_menu_about) {
			intent = new Intent(this, AboutActivity.class);
			this.startActivity(intent);
		} else if (itemId == R.id.main_menu_filter) {
			showFilterDialog(getActionBar().getSelectedTab().getPosition());
		} else {
			return super.onOptionsItemSelected(item);
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onPause() {
		try {
			this.properties.store(this);
		} catch (IOException e) {
			String eMessage = "Exception by saving properties";
			Log.e(TAG, eMessage);
			AirbrakeHelper.sendException(e, eMessage);
		}
		super.onPause();
		UpdateManager.unregister();
	}

	public boolean checkIfUpdateNeeded(int position) {

		boolean refresh = false;

		if (System.currentTimeMillis() - FIVE_MINUTE > mUpdatedTimes[position]) {
			refresh = true;
			mUpdatedTimes[position] = System.currentTimeMillis();
			Log.d("RELOAD","Position " + position + " time " + mUpdatedTimes[position]);
		}

		return refresh;
	}

	public void setUpdateTimeOfPosition(int position) {		
		mUpdatedTimes[position] = System.currentTimeMillis();
	}

	@Override
	protected void onStart() {
		Log.d(TAG, "Call OnStart");
		ImageManager iManager = ManagerFactory.buildImageManager(this);
		if (iDown == null) {
			iDown = new ImageDownloader(this, iManager);
		}
		super.onStart();
	}

	private void showFilterDialog(int tabview) {
		Dialog dialog = null;
		((StaffsaleApplication) getApplication())
		.trackScreen(getString(R.string.ga_screen_filter));
		switch (tabview) {
		case 0:
			NewsManager newsManager = ManagerFactory.buildNewsManager(this);
			dialog = MeinERPDialogBuilder.buildFilterDialog(this, newsManager,
					tabview);
			if (dialog != null) {
				dialog.show();
			}
			break;
		case 1:
			CompetitionIdeasManager comManager = ManagerFactory
			.buildCompetitionIdeasManager(this);
			dialog = MeinERPDialogBuilder.buildFilterDialog(this, comManager,
					tabview);
			if (dialog != null) {
				dialog.show();
			}
			break;
		case 2:
			DealsManager dealManager = ManagerFactory.buildDealsManager(this);
			dialog = MeinERPDialogBuilder.buildFilterDialog(this, dealManager,
					tabview);
			if (dialog != null) {
				dialog.show();
			}
			break;
		default:
			break;
		}
	}

	public boolean hasRights(int position) {

		boolean retVal = false;
		String searchedProp = null;
		switch (position) {
		case 0:
			searchedProp = NEWS_RIGHTS;
			break;
		case 1:
			searchedProp = IDEAS_RIGHTS;
			break;
		case 2:
			searchedProp = DEALS_RIGHTS;
			break;
		case 3:
			searchedProp = EVENTS_RIGHTS;
			break;
		default:
			searchedProp = null;
		}

		if (Rights.check((String) properties.get(searchedProp),(String) properties.get(USER_RIGHT))) {
			properties.set(LAST_TAB_PROP, String.valueOf(position));
			retVal = true;
		}

		return retVal;
	}

	public void showLoginDiaglog() {		
		AlertDialog dialog = MeinERPDialogBuilder.buildAuthenticatDialog(this, ManagerFactory.buildNetworkManager(this), new OnTaskLoginCompleteEvent() {

			@Override
			public void onTaskLoginCompleteEvent() {
				// TODO Auto-generated method stub
				properties.set(LAST_TAB_PROP, String.valueOf(mViewPager.getCurrentItem()));
				if (mFragmentList[mLoginPositionItem] != null) {
					mFragmentList[mLoginPositionItem].logedIn();
				}
			}
		});
		dialog.show();		
	}

	/**
	 * Build the tab view on the main side.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	protected void tabViewSetup() {

		final ActionBar actionBar = getActionBar();

		// Specify that tabs should be displayed in the action bar.
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

		ArrayList<String> tabNames = new ArrayList<String>();

		tabNames.add(getString(R.string.tab_news));
		tabNames.add(getString(R.string.tab_ideas));
		tabNames.add(getString(R.string.tab_deals));
		tabNames.add(getString(R.string.tab_events));
		tabNames.add(getString(R.string.tab_media));

		for (int i = 0; i < tabNames.size(); i++) {
			if(i<4) {
				actionBar.addTab(actionBar.newTab().setText(tabNames.get(i)).setTabListener(this));		
			} else {
				MediaButtonListener mediaButton = new MediaButtonListener(properties,myActivity);
				actionBar.addTab(actionBar.newTab().setText(tabNames.get(i))
						.setTabListener(mediaButton));
			}
		}
	}

	public void changeTab(int position) {
		getActionBar().setSelectedNavigationItem(position);
		mViewPager.setCurrentItem(position);
	}
	public class StaffDealPagerAdapter extends FragmentStatePagerAdapter {

		public StaffDealPagerAdapter(FragmentManager supportFragmentManager) {
			// TODO Auto-generated constructor stub
			super(supportFragmentManager);
		}

		@Override
		public Fragment getItem(int i) {

			switch (i) {
			case 0: {
				if (mNewsFragment == null) {
					Log.d("","News fragment created new");
					mNewsFragment = new newsFragment();
					ImageManager iManager = ManagerFactory.buildImageManager(myActivity);
					mNewsFragment.setIDown(new ImageDownloader(myActivity, iManager));
					ManagerFactory.buildNetworkManager(myActivity).addLogInListener(mNewsFragment);
					mFragmentList[i] = mNewsFragment;
				}

				break;
			}
			case 1: {
				if (mIdeasFragment == null) {
					mIdeasFragment = new ideasFragment();
					ImageManager iManager = ManagerFactory.buildImageManager(myActivity);
					mIdeasFragment.setIDown(new ImageDownloader(myActivity, iManager));
					ManagerFactory.buildNetworkManager(myActivity).addLogInListener(mIdeasFragment);
					mFragmentList[i] = mIdeasFragment;
				}					
				break;
			}
			case 2: {
				if (mDealsFragment == null) {
					mDealsFragment = new dealsFragment();
					ImageManager iManager = ManagerFactory.buildImageManager(myActivity);
					mDealsFragment.setIDown(new ImageDownloader(myActivity, iManager));
					ManagerFactory.buildNetworkManager(myActivity).addLogInListener(mDealsFragment);
					mFragmentList[i] = mDealsFragment;
				}					
				break;
			}			
			case 3: {
				if (mEventsFragment == null) {
					mEventsFragment = new eventsFragment();
					ImageManager iManager = ManagerFactory.buildImageManager(myActivity);
					mEventsFragment.setIDown(new ImageDownloader(myActivity, iManager));
					ManagerFactory.buildNetworkManager(myActivity).addLogInListener(mEventsFragment);
					mFragmentList[i] = mEventsFragment;
				}

				break;
			}
			case 4: {
				if (mMediaFragment == null) {
					mMediaFragment = new mediaFragment();
					mFragmentList[i] = mMediaFragment;
				}					
				break;
			}
			default:{
				return null;
			}
			}

			return mFragmentList[i];
		}

		@Override
		public int getCount() {
			return 4;
		}

		@Override
		public CharSequence getPageTitle(int position) {
			switch (position) {
			case 0:
				return getString(R.string.tab_news);
			case 1:
				return getString(R.string.tab_ideas);
			case 2:
				return getString(R.string.tab_deals);
			case 3:
				return getString(R.string.tab_events);
			}
			return null;
		}
	}

	public void reload(int reloadTab, boolean b) {
		if(mFragmentList[reloadTab] !=  null) {
			mFragmentList[reloadTab].setAdapter();
		}
	}

	@Override
	public void onTabReselected(Tab tab, FragmentTransaction ft) {
		// When the tab is selected, switch to the
		// corresponding page in the ViewPager.

	}

	@Override
	public void onTabSelected(Tab tab, FragmentTransaction ft) {
		// TODO Auto-generated method stub
		mViewPager.setCurrentItem(tab.getPosition());
	}

	@Override
	public void onTabUnselected(Tab tab, FragmentTransaction ft) {
		// TODO Auto-generated method stub

	}

	public int getmCurrentTab() {
		return mCurrentTab;
	}

	public void setmCurrentTab(int mCurrentTab) {
		this.mCurrentTab = mCurrentTab;
	}
}

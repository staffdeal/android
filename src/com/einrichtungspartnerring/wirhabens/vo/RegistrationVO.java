package com.einrichtungspartnerring.wirhabens.vo;

/**
 * Registration information from the info request
 * 
 * @author Thomas Möller
 * 
 */
public class RegistrationVO {

	private String privacyPolicy;
	private String termsOfUse;
	private String hint;

	public String getHint() {
		return hint;
	}

	public String getPrivacyPolicy() {
		return privacyPolicy;
	}

	public String getTermsOfUse() {
		return termsOfUse;
	}

	public void setHint(String hint) {
		this.hint = hint;
	}

	public void setPrivacyPolicy(String privacyPolicy) {
		this.privacyPolicy = privacyPolicy;
	}

	public void setTermsOfUse(String termsOfUse) {
		this.termsOfUse = termsOfUse;
	}

}

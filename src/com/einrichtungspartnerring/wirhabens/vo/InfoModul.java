package com.einrichtungspartnerring.wirhabens.vo;

import java.util.ArrayList;

import com.einrichtungspartnerring.wirhabens.helper.Rights;

/**
 * Modules of the information class.
 * 
 * @author Thomas Möller
 * @date 06.04.2013
 * @TestClasses
 * 
 */
public class InfoModul {

	private String id;
	private String title;
	private String path;
	private Rights.Level authLevel;
	private ArrayList<TemplateVO> templates;
	
	public Rights.Level getAuthLevel() {
		return authLevel;
	}

	public String getId() {
		return id;
	}

	public String getPath() {
		return path;
	}

	public String getTitle() {
		return title;
	}

	public void setAuthLevel(Rights.Level authLevel) {
		this.authLevel = authLevel;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public ArrayList<TemplateVO> getTemplates() {
		return templates;
	}

	public void setTemplates(ArrayList<TemplateVO> templates) {
		this.templates = templates;
	}
}

package com.einrichtungspartnerring.wirhabens.vo;

import com.einrichtungspartnerring.wirhabens.manager.CategoryFilter;

/**
 * Value Object for the category of competitions.
 * 
 * @author Thomas Möller
 * 
 */
public class CompetitionCategoryVO implements CategoryFilter {

	private String id;
	private String title;
	private String image;
	private boolean visible = true;

	public String getId() {
		return id;
	}

	public String getImage() {
		return image;
	}

	public String getTitle() {
		return title;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@Override
	public boolean isVisible() {
		return visible;
	}

	@Override
	public void setVisible(boolean visible) {
		this.visible = visible;
	}

    @Override
    public boolean isSticky() {
        return false;
    }


}

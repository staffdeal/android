package com.einrichtungspartnerring.wirhabens.vo;

/**
 * Auth object.
 * 
 * @author Thomas Möller
 * 
 */
public class AuthVO {

	private UserVO user;
	private String statusMessag;
	private String token;

	public String getStatusMessag() {
		return statusMessag;
	}

	public String getToken() {
		return token;
	}

	public UserVO getUser() {
		return user;
	}

	public void setStatusMessag(String statusMessag) {
		this.statusMessag = statusMessag;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public void setUser(UserVO user) {
		this.user = user;
	}

}

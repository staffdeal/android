package com.einrichtungspartnerring.wirhabens.vo;

import java.util.ArrayList;
import java.util.List;

/**
 * Information VO for the API. See XML definition for the attributes
 * 
 * @author Thomas Möller
 * @date 06.04.2013
 * @TestClasses
 * 
 */
public class InfoVO {

	/**
	 * Latest available version for download
	 */
	private int latestVersion;

	/**
	 * Update text to be shown in case of new version available
	 */
	private String updateText;
	
	/**
	 * Name tag in the client tag
	 */
	private String clientName;
	/**
	 * EMail tag in the client tag
	 */
	private String clientEMail;
	/**
	 * About tag in the client tag. Showing in the about menu
	 */
	private String clientAbout;
	/**
	 * Impress tag in the client tag
	 */
	private String clientImpressum;
	/**
	 * Media URL from the backen
	 * MediaURL tag  in the client tag
	 */
	private String youTubeMediaUrl;

    private String facebookMediaUrl;

	private String userName;
	private String userEMail;

	private UserVO user;

	private List<InfoModul> modules;

	private RegistrationVO registration;

    private List<InfoFieldVO> fields = new ArrayList<InfoFieldVO>();
    
    private String googleAnalyticsID;


    public String getGoogleAnalyticsID() {
		return googleAnalyticsID;
	}

	public void setGoogleAnalyticsID(String googleAnalyticsID) {
		this.googleAnalyticsID = googleAnalyticsID;
	}

	public List<InfoFieldVO> getFields() {
        return fields;
    }

    public void setFields(List<InfoFieldVO> fields) {
        this.fields = fields;
    }

    public String getClientEMail() {
		return clientEMail;
	}

	public String getClientName() {
		return clientName;
	}

	public List<InfoModul> getModules() {
		return modules;
	}

	public RegistrationVO getRegistration() {
		return registration;
	}

	public UserVO getUser() {
		return user;
	}

	public String getUserEMail() {
		return userEMail;
	}

	public String getUserName() {
		return userName;
	}

	public void setClientEMail(String clientEMail) {
		this.clientEMail = clientEMail;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public void setModules(List<InfoModul> modules) {
		this.modules = modules;
	}

	public void setRegistration(RegistrationVO registration) {
		this.registration = registration;
	}

	public void setUser(UserVO user) {
		this.user = user;
	}

	public void setUserEMail(String userEMail) {
		this.userEMail = userEMail;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getClientAbout() {
		return clientAbout;
	}

	public void setClientAbout(String clientAbout) {
		this.clientAbout = clientAbout;
	}

	public String getClientImpressum() {
		return clientImpressum;
	}

	public void setClientImpressum(String clientImpressum) {
		this.clientImpressum = clientImpressum;
	}

	public String getYouTubeMediaUrl() {
		return youTubeMediaUrl;
	}

	public void setYouTubeMediaUrl(String youTubeMediaUrl) {
		this.youTubeMediaUrl = youTubeMediaUrl;
	}

    public String getFacebookMediaUrl() {
        return facebookMediaUrl;
    }

    public void setFacebookMediaUrl(String facebookMediaUrl) {
        this.facebookMediaUrl = facebookMediaUrl;
    }

	public int getLatestVersion() {
		return latestVersion;
	}

	public void setLatestVersion(int latestVersion) {
		this.latestVersion = latestVersion;
	}

	public String getUpdateText() {
		return updateText;
	}

	public void setUpdateText(String updateText) {
		this.updateText = updateText;
	}

}

package com.einrichtungspartnerring.wirhabens.vo;

import java.util.Date;

import com.einrichtungspartnerring.wirhabens.gui.adapter.PicAdapterItem;

import android.graphics.Color;

/**
 * Value Object for the deals. Definition for the attributes see xml definition.
 * 
 * @author Thomas Möller
 * 
 */
public class DealsItemVO implements PicAdapterItem {
	
	private static final int MILLI_SEC_TO_DAY = 86400000;

	private String id;

	private String provider;
	private String title;
	private String teaser;
	private String titleImage;
	private Date startDate;
	private Date endDate;
	private String text;
    private String price;
    private String oldPrice;

    private boolean featured = false;

	private String passOnlineUrl;
	private boolean passOnlineAvaible;
    private String onlineDescription;
    private String onlineUrl;
	private String passOnSiteUrl;
    private boolean passOnSiteAvaible;
	private boolean readed;
	private DealsCategory category;
    private boolean onSiteSaved = false;
    private boolean onlineSaved = false;
    private boolean soldOut = false;
    private String passOnlineCode;
    private boolean isAdvertising;
    private boolean isReminded;

    public boolean isAdvertising() {
        return isAdvertising;
    }

    public void setAdvertising(boolean isAdvertising) {
        this.isAdvertising = isAdvertising;
    }

    public boolean isReminded() {
        return isReminded;
    }

    public void setReminded(boolean isReminded) {
        this.isReminded = isReminded;
    }

    public boolean isSoldOut() {
        return soldOut;
    }

    public void setSoldOut(boolean soldOut) {
        this.soldOut = soldOut;
    }

    public boolean isOnSiteSaved() {
        return onSiteSaved;
    }

    public void setOnSiteSaved(boolean onSiteSaved) {
        this.onSiteSaved = onSiteSaved;
    }

    public boolean isOnlineSaved() {
        return onlineSaved;
    }

    public void setOnlineSaved(boolean onlineSaved) {
        this.onlineSaved = onlineSaved;
    }

    public DealsItemVO() {
	}

    public String getOnlineDescription() {
        return onlineDescription;
    }

    public void setOnlineDescription(String onlineDescription) {
        this.onlineDescription = onlineDescription;
    }


    public String getOnlineUrl() {
        return onlineUrl;
    }

    public void setOnlineUrl(String onlineUrl) {
        this.onlineUrl = onlineUrl;
    }

    @Override
	public int getAdapterColor() {
		return Color.BLUE;
	}

	@Override
	public String getAdapterDate() {
		Date endDate = getEndDate();
		if (endDate != null){
			Date current = new Date();
			long dayInMilli = endDate.getTime() - current.getTime();
			if (dayInMilli >= 0)
				return "in " + dayInMilli/MILLI_SEC_TO_DAY +" Tagen";
			else
				return dayInMilli/MILLI_SEC_TO_DAY*-1 + " Tage her";
		}	
		else
			return "";
	}

	@Override
	public String getAdapterHead() {
		return getProvider();
	}

	@Override
	public String getAdapterImagePath() {
		return getTitleImage();
	}

	@Override
	public String getAdapterTitle() {
		return getTitle();
	}

	public Date getEndDate() {
		return endDate;
	}

	public String getId() {
		return id;
	}

	public String getPassOnlineUrl() {
		return passOnlineUrl;
	}


	public String getPassOnSiteUrl() {
		return passOnSiteUrl;
	}

	public String getProvider() {
		return provider;
	}

	public Date getStartDate() {
		return startDate;
	}

	public String getTeaser() {
		return teaser;
	}

	public String getText() {
		return text;
	}

	public String getTitle() {
		return title;
	}

	public String getTitleImage() {
		return titleImage;
	}

	@Override
	public boolean isReaded() {
		return readed;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setPassOnlineUrl(String passOnlineUrl) {
		this.passOnlineUrl = passOnlineUrl;
	}


	public void setPassOnSiteUrl(String passOnSiteUrl) {
		this.passOnSiteUrl = passOnSiteUrl;
	}

	public void setProvider(String provider) {
		this.provider = provider;
	}

	public void setReaded(boolean readed) {
		this.readed = readed;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public void setTeaser(String teaser) {
		this.teaser = teaser;
	}

	public void setText(String text) {
		this.text = text;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setTitleImage(String titleImage) {
		this.titleImage = titleImage;
	}

	@Override
	public String getCategoryId() {
		if (category != null)
			return this.category.getId();
		return null;
	}

	public DealsCategory getCategory() {
		return category;
	}

	public void setCategory(DealsCategory category) {
		this.category = category;
	}

	
	@Override
	public int compareTo(PicAdapterItem another) {
		return this.getSortingValue().compareTo(another.getSortingValue());
	}

	@Override
	public Long getSortingValue() {
		return this.endDate.getTime();
	}


    public boolean isFeatured() {
        return featured;
    }

    public void setFeatured(boolean featured) {
        this.featured = featured;
    }

    public boolean isPassOnlineAvaible() {
        return passOnlineAvaible;
    }

    public void setPassOnlineAvaible(boolean passOnlineAvaible) {
        this.passOnlineAvaible = passOnlineAvaible;
    }

    public boolean isPassOnSiteAvaible() {
        return passOnSiteAvaible;
    }

    public void setPassOnSiteAvaible(boolean passOnSiteAvaible) {
        this.passOnSiteAvaible = passOnSiteAvaible;
    }

    public String getPassOnlineCode() {
        return passOnlineCode;
    }

    public void setPassOnlineCode(String passOnlineCode) {
        this.passOnlineCode = passOnlineCode;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getOldPrice() {
        return oldPrice;
    }

    public void setOldPrice(String oldPrice) {
        this.oldPrice = oldPrice;
    }

	@Override
	public int getType() {
		// TODO Auto-generated method stub
		return 2;
	}
}

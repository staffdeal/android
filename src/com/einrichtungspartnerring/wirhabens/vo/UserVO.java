package com.einrichtungspartnerring.wirhabens.vo;

/**
 * User object.
 * 
 * @author Thomas Möller
 * 
 */
public class UserVO {

	private String id;
	private String firstname;
	private String lastname;
	private String email;

	public String getEmail() {
		return email;
	}

	public String getFirstname() {
		return firstname;
	}

	public String getId() {
		return id;
	}

	public String getLastname() {
		return lastname;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

}

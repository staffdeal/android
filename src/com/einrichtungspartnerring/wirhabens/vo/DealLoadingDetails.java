package com.einrichtungspartnerring.wirhabens.vo;

/**
 * Created by loc on 01.08.14.
 */
public class DealLoadingDetails {

    private String barcodeUrl;
    private String ean;
    private String code;
    private String url;

    public String getBarcodeUrl() {
        return barcodeUrl;
    }

    public void setBarcodeUrl(String barcodeUrl) {
        this.barcodeUrl = barcodeUrl;
    }

    public String getEan() {
        return ean;
    }

    public void setEan(String ean) {
        this.ean = ean;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}

package com.einrichtungspartnerring.wirhabens.vo;

import com.einrichtungspartnerring.wirhabens.manager.CategoryFilter;

public class NewsCategoryVO implements CategoryFilter {

	private String id;

	private String title;
	private String slug;
	private String color;
	private boolean visible = true;

    public void setSticky(boolean sticky) {
        this.sticky = sticky;
    }

    public boolean isSticky() {
        return sticky;
    }

    private boolean sticky = false;

	public NewsCategoryVO() {
		super();
	}

	public String getColor() {
		return color;
	}

	public String getId() {
		return id;
	}

	public String getSlug() {
		return slug;
	}

	public String getTitle() {
		return title;
	}

	@Override
	public boolean isVisible() {
        if (sticky)
            return true;
		return visible;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setSlug(String slug) {
		this.slug = slug;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@Override
	public void setVisible(boolean visible) {
		this.visible = visible;
	}

}

package com.einrichtungspartnerring.wirhabens.vo;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import android.graphics.Color;

import com.einrichtungspartnerring.wirhabens.gui.adapter.PicAdapterItem;

public class EventVO implements PicAdapterItem {

	private String id;
	private String title;
	private Date date;
	private String titleImage;
	private String description;
	private String url;
	
	private boolean readed;
	
	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getTitleImage() {
		return titleImage;
	}

	public void setTitleImage(String titleImage) {
		this.titleImage = titleImage;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	
	@Override
	public int compareTo(PicAdapterItem another) {
		return -getSortingValue().compareTo(another.getSortingValue());
	}

	@Override
	public int getAdapterColor() {
		return Color.GRAY;
	}

	@Override
	public String getAdapterDate() {
		SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy", Locale.GERMANY);
		Date date = getDate();
		if(date != null) {
			return df.format(date);
		} else {
			return "";
		}
	}

	@Override
	public String getAdapterHead() {
		return "";
	}

	@Override
	public String getAdapterImagePath() {
		return getTitleImage();
	}

	@Override
	public String getAdapterTitle() {
		return getTitle();
	}


	@Override
	public boolean isReaded() {
		// TODO Auto-generated method stub
		return readed;
	}

	@Override
	public void setReaded(boolean readed) {
		this.readed = readed;
	}
	
	@Override
	public String getCategoryId() {
		return "";
	}

	@Override
	public Long getSortingValue() {
		if(this.date != null) {
			return this.date.getTime();
		} else {
			return (long) 0;
		}
		
	}

	@Override
	public boolean isFeatured() {
		// TODO Auto-generated method stub
		return false;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	@Override
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}

	@Override
	public int getType() {
		// TODO Auto-generated method stub
		return 3;
	}



}

package com.einrichtungspartnerring.wirhabens.vo;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import com.einrichtungspartnerring.wirhabens.gui.adapter.PicAdapterItem;

import android.graphics.Color;

/**
 * Value Object for the ideas.
 * 
 * @author Thomas Möller
 * 
 */
public class IdeaVO implements PicAdapterItem {

	private String id;
	private String text;
	private Date submissionDate;
	private String url;
	private String userFirstName;
	private String userLastName;
	private List<ImageVO> images;
	private boolean readed;

	@Override
	public int getAdapterColor() {
		return Color.WHITE;
	}

	@Override
	public String getAdapterDate() {
		SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy", Locale.GERMANY);
		return df.format(getSubmissionDate());
	}

	@Override
	public String getAdapterHead() {
		return getUserFirstName() + " " + getUserLastName();
	}

	@Override
	public String getAdapterImagePath() {
		List<ImageVO> images = getImages();
		if (images != null && images.size() > 0)
			return images.get(0).getPfad();
		return null;
	}

	@Override
	public String getAdapterTitle() {
		return getText();
	}

	public String getId() {
		return id;
	}

	public List<ImageVO> getImages() {
		return images;
	}

	public Date getSubmissionDate() {
		return submissionDate;
	}

	public String getText() {
		return text;
	}

	public String getUrl() {
		return url;
	}

	public String getUserFirstName() {
		return userFirstName;
	}

	public String getUserLastName() {
		return userLastName;
	}

	@Override
	public boolean isReaded() {
		return readed;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setImages(List<ImageVO> images) {
		this.images = images;
	}

	public void setReaded(boolean readed) {
		this.readed = readed;
	}

	public void setSubmissionDate(Date submissionDate) {
		this.submissionDate = submissionDate;
	}

	public void setText(String text) {
		this.text = text;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public void setUserFirstName(String userFirstName) {
		this.userFirstName = userFirstName;
	}

	public void setUserLastName(String userLastName) {
		this.userLastName = userLastName;
	}

	@Override
	public String getCategoryId() {
		return null;
	}
	
	@Override
	public int compareTo(PicAdapterItem another) {
		return getSortingValue().compareTo(another.getSortingValue());
	}

	@Override
	public Long getSortingValue() {
		return submissionDate.getTime();
	}

    @Override
    public boolean isFeatured() {
        return false;
    }

	@Override
	public int getType() {
		// TODO Auto-generated method stub
		return 5;
	}
}

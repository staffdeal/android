package com.einrichtungspartnerring.wirhabens.vo;

/**
 * Created by loc on 28.07.14.
 */
public class SyncedDealsVO {

    private String id;
    private String online;
    private String onSite;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOnline() {
        return online;
    }

    public void setOnline(String online) {
        this.online = online;
    }

    public String getOnSite() {
        return onSite;
    }

    public void setOnSite(String onSite) {
        this.onSite = onSite;
    }
}

package com.einrichtungspartnerring.wirhabens.vo;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import com.einrichtungspartnerring.wirhabens.gui.adapter.PicAdapterItem;

import android.graphics.Color;

/**
 * Representation of a new item. For information about the tag see the xml
 * definition in the documentation.
 * 
 * @author Thomas Möller
 * 
 */
public class NewsItemVO implements PicAdapterItem {

	private String id;

	private NewsCategoryVO category;

	private String title;

	private String url;

	private Date published;

	/**
	 * URL to the image
	 */
	private String titleImage;

	private boolean readed;

	private String text;
	
	private String source;

	public NewsItemVO() {

	}

	@Override
	public int getAdapterColor() {
		int color = Color.GRAY;
		if (category != null && category.getColor() != null
				&& !category.getColor().isEmpty())
			color = Color.parseColor(category.getColor());
		return color;
	}

	@Override
	public String getAdapterDate() {
		SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy", Locale.GERMANY);
		return df.format(getPublished());
	}

	@Override
	public String getAdapterHead() {
		String head = "";
		if (category != null && category.getTitle() != null)
			head = category.getTitle();
		return head;
	}

	@Override
	public String getAdapterImagePath() {
		return getTitleImage();
	}

	@Override
	public String getAdapterTitle() {
		return getTitle();
	}

	public NewsCategoryVO getCategory() {
		return category;
	}

	public String getId() {
		return id;
	}

	public Date getPublished() {
		return published;
	}

	public String getText() {
		return text;
	}

	public String getTitle() {
		return title;
	}

	public String getTitleImage() {
		return titleImage;
	}

	public String getUrl() {
		return url;
	}

	@Override
	public boolean isReaded() {
		return readed;
	}

	public void setCategory(NewsCategoryVO category) {
		this.category = category;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setPublished(Date published) {
		this.published = published;
	}

	public void setReaded(boolean readed) {
		this.readed = readed;
	}

	public void setText(String text) {
		this.text = text;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setTitleImage(String titleImage) {
		this.titleImage = titleImage;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@Override
	public String getCategoryId() {
		if (category != null)
			return category.getId();
		return null;
	}
	
	@Override
	public int compareTo(PicAdapterItem another) {
		return this.getSortingValue().compareTo(another.getSortingValue());
	}

	@Override
	public Long getSortingValue() {
		return this.published.getTime();
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

    @Override
    public boolean isFeatured() {
        return false;
    }

	@Override
	public int getType() {
		// TODO Auto-generated method stub
		return 0;
	}
}

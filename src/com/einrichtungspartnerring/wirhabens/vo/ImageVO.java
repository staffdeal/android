package com.einrichtungspartnerring.wirhabens.vo;

public class ImageVO {

	private String id;
	private String pfad;

	public String getId() {
		return id;
	}

	public String getPfad() {
		return pfad;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setPfad(String pfad) {
		this.pfad = pfad;
	}

}

package com.einrichtungspartnerring.wirhabens.vo;

public class TemplateVO {

	private String url;
	private String hash;
	private String useFor;
	

	public TemplateVO(String url,String hash,String useFor) {
		
		this.setUrl(url);
		this.setHash(hash);
		this.setUseFor(useFor);
	
	}

	public String getUrl() {
		return url;
	}


	public void setUrl(String url) {
		this.url = url;
	}


	public String getHash() {
		return hash;
	}


	public void setHash(String hash) {
		this.hash = hash;
	}


	public String getUseFor() {
		return useFor;
	}


	public void setUseFor(String useFor) {
		this.useFor = useFor;
	}
	
}

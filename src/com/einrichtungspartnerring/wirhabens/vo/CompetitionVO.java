package com.einrichtungspartnerring.wirhabens.vo;

import java.util.Date;
import java.util.List;

import com.einrichtungspartnerring.wirhabens.gui.adapter.PicAdapterItem;

import android.graphics.Color;

/**
 * Value Object for the content of competitions. Description for variables see
 * xml definition.
 * 
 * @author Thomas Möller
 * 
 */
public class CompetitionVO implements PicAdapterItem {

	private static final int MILLI_SEC_TO_DAY = 86400000;
	
	private String id;
	private String title;
	private String titleImage;
	private String company;
	private String teasertext;
	private String description;
	private String rules;
	private Date startDate;
	private Date endDate;
	private CompetitionCategoryVO category;
	private List<IdeaVO> ideas;
	private boolean readed;
    private int serverOrder;
    private String endText;

	@Override
	public int getAdapterColor() {
		return Color.GRAY;
	}

	@Override
	public String getAdapterDate() {
		Date endDate = getEndDate();
		if (endDate != null){
			Date current = new Date();
			long dayInMilli = endDate.getTime() - current.getTime();
			if (dayInMilli >= 0)
				return "in " + dayInMilli/MILLI_SEC_TO_DAY +" Tagen";
			else
				return dayInMilli/MILLI_SEC_TO_DAY*-1 + " Tage her";
		}	
		else
			return "";
	}

	@Override
	public String getAdapterHead() {
		CompetitionCategoryVO category = getCategory();
		if (category != null && category.getTitle() != null)
			return category.getTitle();
		return "";
	}

	@Override
	public String getAdapterImagePath() {
		String titleImage = getTitleImage();
		CompetitionCategoryVO category = getCategory();
		if (titleImage == null && category != null
				&& category.getImage() != null)
			titleImage = category.getImage();
		return getTitleImage();
	}

    public String getEndText() {
        return endText;
    }

    public void setEndText(String endText) {
        this.endText = endText;
    }

    @Override
	public String getAdapterTitle() {
		return getTitle();
	}

	public CompetitionCategoryVO getCategory() {
		return category;
	}

	public String getCompany() {
		return company;
	}

	public String getDescription() {
		return description;
	}

	public Date getEndDate() {
		return endDate;
	}

	/**
	 * Getter and Setter + implementation of PicAdapterItem
	 */
	@Override
	public String getId() {
		return id;
	}

	public List<IdeaVO> getIdeas() {
		return ideas;
	}

	public List<PicAdapterItem> getIdeasAtPicAdapterItem() {
		@SuppressWarnings("unchecked")
		List<PicAdapterItem> items = (List<PicAdapterItem>) (List<?>) getIdeas();
		return items;
	}

	public String getRules() {
		return rules;
	}

	public Date getStartDate() {
		return startDate;
	}

	public String getTeasertext() {
		return teasertext;
	}

	public String getTitle() {
		return title;
	}

	public String getTitleImage() {
		return titleImage;
	}

	@Override
	public boolean isReaded() {
		return this.readed;
	}

	public void setCategory(CompetitionCategoryVO category) {
		this.category = category;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setIdeas(List<IdeaVO> ideas) {
		this.ideas = ideas;
	}

	public void setReaded(boolean readed) {
		this.readed = readed;
	}

	public void setRules(String rules) {
		this.rules = rules;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public void setTeasertext(String teasertext) {
		this.teasertext = teasertext;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setTitleImage(String titleImage) {
		this.titleImage = titleImage;
	}

	@Override
	public String getCategoryId() {
		if (category != null)
			return category.getId();
		return null;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((category == null) ? 0 : category.hashCode());
		result = prime * result + ((company == null) ? 0 : company.hashCode());
		result = prime * result
				+ ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((endDate == null) ? 0 : endDate.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((ideas == null) ? 0 : ideas.hashCode());
		result = prime * result + (readed ? 1231 : 1237);
		result = prime * result + ((rules == null) ? 0 : rules.hashCode());
		result = prime * result
				+ ((startDate == null) ? 0 : startDate.hashCode());
		result = prime * result
				+ ((teasertext == null) ? 0 : teasertext.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		result = prime * result
				+ ((titleImage == null) ? 0 : titleImage.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (this.getClass() != obj.getClass())
			return false;
		CompetitionVO other = (CompetitionVO) obj;
		if (category == null) {
			if (other.category != null)
				return false;
		} else if (!category.equals(other.category))
			return false;
		if (company == null) {
			if (other.company != null)
				return false;
		} else if (!company.equals(other.company))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (endDate == null) {
			if (other.endDate != null)
				return false;
		} else if (!endDate.equals(other.endDate))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (ideas == null) {
			if (other.ideas != null)
				return false;
		} else if (!ideas.equals(other.ideas))
			return false;
		if (readed != other.readed)
			return false;
		if (rules == null) {
			if (other.rules != null)
				return false;
		} else if (!rules.equals(other.rules))
			return false;
		if (startDate == null) {
			if (other.startDate != null)
				return false;
		} else if (!startDate.equals(other.startDate))
			return false;
		if (teasertext == null) {
			if (other.teasertext != null)
				return false;
		} else if (!teasertext.equals(other.teasertext))
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		if (titleImage == null) {
			if (other.titleImage != null)
				return false;
		} else if (!titleImage.equals(other.titleImage))
			return false;
		return true;
	}

	@Override
	public int compareTo(PicAdapterItem another) {
		return this.getSortingValue().compareTo(another.getSortingValue());
	}

    public void setServerOrder(int serverOrder) {
        this.serverOrder = serverOrder;
    }

    @Override
	public Long getSortingValue() {
		return this.serverOrder * -1l;

	}

    @Override
    public boolean isFeatured() {
        return false;
    }

	@Override
	public int getType() {
		// TODO Auto-generated method stub
		return 1;
	}
}

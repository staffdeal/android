package com.einrichtungspartnerring.wirhabens.vo;

import com.einrichtungspartnerring.wirhabens.manager.CategoryFilter;

public class DealsCategory implements CategoryFilter{

	public enum IDs {
		special_unsaved, special_saved
	}
	
	private IDs id;
	private boolean visible = true;
	
	public DealsCategory( IDs id) {
		this.id = id;
	}

	@Override
	public String getId() {
		return this.id.name();
	}

	@Override
	public String getTitle() {
		// TODO String herbekommen
		if (id.equals(IDs.special_saved))
			return "gesicherte Deals";
		else
			return "neue Deals";
	}

	@Override
	public boolean isVisible() {
		return visible;
	}

	@Override
	public void setVisible(boolean visible) {
		this.visible = visible;
	}


    @Override
    public boolean isSticky() {
        return false;
    }
}

package com.einrichtungspartnerring.wirhabens.parser;

import java.util.Arrays;
import java.util.Stack;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 * ParserHanlder for a universal sax parser.
 * 
 * @author Thomas Möller
 * @date 23.08.2011
 * @TestClasses
 * 
 */
public class ParserHandler extends DefaultHandler {

	/**
	 * Stack for the level of analysed tags
	 */
	private Stack<ParserVO> parserStack = new Stack<ParserVO>();

	/**
	 * First XML element
	 */
	private ParserVO first;

	private StringBuffer buffer;

	@Override
	public void characters(char[] ch, int start, int length)
			throws SAXException {
		if (buffer != null)
			buffer.append(new String(Arrays.copyOfRange(ch, start, start
					+ length)));
		super.characters(ch, start, length);
	}

	@Override
	public void endDocument() throws SAXException {
		super.endDocument();
	}

	@Override
	public void endElement(String uri, String localName, String qName)
			throws SAXException {
		ParserVO stackVO = parserStack.pop();
		if (buffer != null) {
			stackVO.setValue(buffer.toString());
			buffer = null;
		}
		super.endElement(uri, localName, qName);
	}

	public ParserVO getFirst() {
		return first;
	}

	public Stack<ParserVO> getParserStack() {
		return parserStack;
	}

	public void setFirst(ParserVO first) {
		this.first = first;
	}

	public void setParserStack(Stack<ParserVO> parserStack) {
		this.parserStack = parserStack;
	}

	@Override
	public void startDocument() throws SAXException {
		super.startDocument();
	}

	@Override
	public void startElement(String uri, String localName, String qName,
			Attributes attributes) throws SAXException {
		ParserVO parserVO = new ParserVO(qName, attributes);
		if (first == null)
			first = parserVO;
		if (!parserStack.isEmpty()) {
			ParserVO stackVO = parserStack.pop();
			stackVO.getVos().add(parserVO);
			parserStack.push(stackVO);
		}
		parserStack.push(parserVO);
		buffer = new StringBuffer();
		super.startElement(uri, localName, qName, attributes);
	}

}
package com.einrichtungspartnerring.wirhabens.parser;

public class MeinERPParserException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7192074915579731266L;

	public MeinERPParserException() {
	}

	public MeinERPParserException(String message) {
		super(message);
	}

	public MeinERPParserException(String message, Throwable cause) {
		super(message, cause);
	}

	public MeinERPParserException(Throwable cause) {
		super(cause);
	}

}

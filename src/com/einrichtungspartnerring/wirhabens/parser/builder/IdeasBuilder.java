package com.einrichtungspartnerring.wirhabens.parser.builder;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import com.einrichtungspartnerring.wirhabens.helper.AirbrakeHelper;
import com.einrichtungspartnerring.wirhabens.parser.ParserVO;
import com.einrichtungspartnerring.wirhabens.vo.IdeaVO;
import com.einrichtungspartnerring.wirhabens.vo.ImageVO;

import android.util.Log;


/**
 * 
 * @author Thomas Möller
 * 
 */
public class IdeasBuilder implements TagNames {

	private static final String TAG = "IdeasBuilder";

	/**
	 * Add the content of user to the given idea.
	 * 
	 * @param rawUser
	 *            - raw information of the user.
	 * @param idea
	 *            - the item you want to add the information.
	 */
	private static void addUserBuild(ParserVO rawUser, IdeaVO idea) {
		for (ParserVO userContent : rawUser.getVos()) {
			if (userContent.getName().equals(FIRST_NAME_TAG_NAME)) {
				idea.setUserFirstName(userContent.getValue());
			} else if (userContent.getName().equals(LAST_NAME_TAG_NAME)) {
				idea.setUserLastName(userContent.getValue());
			}
		}
	}

	/**
	 * Build action for one idea.
	 * 
	 * @param staffsale
	 *            - raw tag
	 * @return a builded list of images.
	 */
	public static List<IdeaVO> buildideas(ParserVO staffsale) {
		List<IdeaVO> ideas = new ArrayList<IdeaVO>();
		ParserVO rawIdeas = staffsale.getVos().get(0);
		IdeaVO idea = null;
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd",
				Locale.GERMANY);
		for (ParserVO rawIdea : rawIdeas.getVos()) {
			idea = new IdeaVO();
			idea.setId(rawIdea.getAttributeByName(ID_ATTRIBUTE));
			for (ParserVO att : rawIdea.getVos()) {
				if (att.getName().equals(TEXT_TAG_NAME)) {
					idea.setText(att.getValue());
				} else if (att.getName().equals(SUB_MISSION_DATE_TAG_NAME)) {
					try {
						idea.setSubmissionDate(dateFormat.parse(att.getValue()));
					} catch (ParseException e) {
						String eMessage = "Formate of the dates has change";
						Log.e(TAG, eMessage, e);
						AirbrakeHelper.sendException(e, eMessage);
					}
				} else if (att.getName().equals(USER_TAG_NAME)) {
					addUserBuild(att, idea);
				} else if (att.getName().equals(IMAGES_TAG_NAME)) {
					List<ImageVO> images = ImagesBuilder.buildImages(att);
					idea.setImages(images);
				} else if (att.getName().equals(URL_TAG_NAME)) {
					idea.setUrl(att.getValue());
				}
			}
			ideas.add(idea);
		}

		return ideas;
	}
}

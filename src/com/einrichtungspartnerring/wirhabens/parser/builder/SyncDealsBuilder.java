package com.einrichtungspartnerring.wirhabens.parser.builder;

import com.einrichtungspartnerring.wirhabens.parser.Parser;
import com.einrichtungspartnerring.wirhabens.parser.ParserVO;
import com.einrichtungspartnerring.wirhabens.vo.SyncedDealsVO;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by loc on 02.08.14.
 */
public class SyncDealsBuilder implements  TagNames {

    public static List<SyncedDealsVO> buildSyncDeals(ParserVO firstElement){
        List<SyncedDealsVO> syncs = new ArrayList<SyncedDealsVO>();
        if (firstElement != null && firstElement.getVos() != null && firstElement.getVos().size() > 0){
            ParserVO syncDeals = firstElement.getVos().get(0);
            if (syncDeals.getVos() != null) {
                for (ParserVO syncDeal : syncDeals.getVos()){

                    if (syncDeal.getName().equals(DEAL_TAG_NAME)){
                        SyncedDealsVO sync = new SyncedDealsVO();
                        sync.setId(syncDeal.getAttributeByName(ID_ATTRIBUTE));
                        if (syncDeal.getVos() != null) {
                            for (ParserVO desc : syncDeal.getVos()) {
                                if(desc.getName().equals(PASS_ONLINE_TAG_NAME))
                                    sync.setOnline(desc.getValue());
                                else if(desc.getName().equals(PASS_ONSITE_TAG_NAME))
                                    sync.setOnSite(desc.getValue());
                            }
                        }
                        syncs.add(sync);
                    }
                }
            }
        }
        return  syncs;
    }
}

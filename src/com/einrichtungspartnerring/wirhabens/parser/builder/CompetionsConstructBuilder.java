package com.einrichtungspartnerring.wirhabens.parser.builder;

import java.util.List;

import com.einrichtungspartnerring.wirhabens.parser.ParserVO;
import com.einrichtungspartnerring.wirhabens.vo.CompetitionCategoryVO;
import com.einrichtungspartnerring.wirhabens.vo.CompetitionVO;


/**
 * Build the Competitions with the categories.
 * 
 * @author Thomas Möller
 * 
 */
public class CompetionsConstructBuilder implements TagNames {

	/**
	 * Build the list of CompetitionVOs for the system.
	 * 
	 * @param staffsale
	 *            - root element of the xml file.
	 * @return a list of the builded competitions.
	 */
	public static List<CompetitionVO> buildCategories(ParserVO staffsale, List<CompetitionCategoryVO> competionsCategory) {
		List<CompetitionVO> competions = null;
		for (ParserVO groups : staffsale.getVos()) {
			if (groups.getName().equals(CATEGORY_GROUP_TAG_NAME))
				competionsCategory = CompetitionCategoryBuilder.buildCategories(groups, competionsCategory);
			else if (groups.getName().equals(COMPETITION_GROUP_TAG_NAME))
				competions = CompetitionBuilder.buildCompetions(groups,competionsCategory);
		}
		return competions;
	}
}

package com.einrichtungspartnerring.wirhabens.parser.builder;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import com.einrichtungspartnerring.wirhabens.helper.AirbrakeHelper;
import com.einrichtungspartnerring.wirhabens.parser.ParserVO;
import com.einrichtungspartnerring.wirhabens.vo.DealsItemVO;

import android.util.Log;


/**
 * Builder for the deals items.
 * 
 * @author Thomas Möller
 * 
 */
public class DealsBuilder implements TagNames {

	private static final String TAG = "DealsBuilder";

	/**
	 * Set all information about the bar codes from the parsed given passAtt
	 * into the given deal.
	 * 
	 * @param passAtt
	 *            - parsed information about the bar code.
	 * @param deal
	 *            - where you want to set the information.
	 */
	protected static void addPassAtt(ParserVO passAtt, DealsItemVO deal) {
		for (ParserVO typeAtt : passAtt.getVos()) {
			if (typeAtt.getName().equals(PASS_ONLINE_TAG_NAME)) {
                deal.setPassOnlineUrl(typeAtt.getValue());
                if (deal.getPassOnlineUrl() == null || deal.getPassOnlineUrl().isEmpty())
                    deal.setPassOnlineUrl(TagNames.NO_CODE);
                String avail = typeAtt.getAttributeByName(AVAILABLE_ATTRIBUTE_NAME);
                if (avail != null && !avail.equals(""));
                    deal.setPassOnlineAvaible(Boolean.parseBoolean(avail));
            } else if (typeAtt.getName().equals(PASS_ONSITE_TAG_NAME)) {
                deal.setPassOnSiteUrl(typeAtt.getValue());
                String avail = typeAtt.getAttributeByName(AVAILABLE_ATTRIBUTE_NAME);
                if (avail != null && !avail.equals(""));
                    deal.setPassOnSiteAvaible(Boolean.parseBoolean(avail));
            }
		}
	}

    protected static void addOnlineAtt(ParserVO online, DealsItemVO deal){
        for (ParserVO typeAtt : online.getVos()) {
            if (typeAtt.getName().toLowerCase().equals(URL_TAG_NAME)) {
                deal.setOnlineUrl(typeAtt.getValue());
            }
            else if(typeAtt.getName().equals(DESCRIPTION_TAG_NAME)){
                deal.setOnlineDescription(typeAtt.getValue());
            }
        }
    }

	/**
	 * Build the deals from the parsed tags.
	 * 
	 * @param staffsaleTag
	 *            - raw tag with the staff-sale tag at first ParserVO.
	 * @return a list of parsed deal items.
	 */
	public static List<DealsItemVO> buildDeals(ParserVO staffsaleTag) {

		ParserVO dealstag = staffsaleTag.getVos().get(0);
		List<DealsItemVO> deals = new ArrayList<DealsItemVO>();
		DealsItemVO deal;

		for (ParserVO rawDeal : dealstag.getVos()) {
			deal = new DealsItemVO();
			deal.setId(rawDeal.getAttributeByName(ID_ATTRIBUTE));
            String featuredRaw = rawDeal.getAttributeByName(FEATURED_ATTRIBUTE);
            if (featuredRaw != null &&  ( featuredRaw.toUpperCase().equals("YES") || Boolean.valueOf(featuredRaw) ))
                    deal.setFeatured(true);

			for (ParserVO attribute : rawDeal.getVos()) {
				SimpleDateFormat dateFormat = new SimpleDateFormat(
						"yyyy-MM-dd", Locale.GERMANY);
				if (attribute.getName().equals(PROVIDER_TAG_NAME))
					deal.setProvider(attribute.getValue());
				else if (attribute.getName().equals(TITLE_TAG_NAME))
					deal.setTitle(attribute.getValue());
				else if (attribute.getName().equals(TEASER_TAG_NAME))
					deal.setTeaser(attribute.getValue());
				else if (attribute.getName().equals(START_DATE_TAG_NAME)) {
					try {
						deal.setStartDate(dateFormat.parse(attribute.getValue()));
					} catch (ParseException e) {
						String eMessage = "Formate of the dates has change";
						Log.e(TAG, eMessage, e);
						AirbrakeHelper.sendException(e, eMessage);
					}
				} else if (attribute.getName().equals(END_DATE_TAG_NAME)) {
					try {
						deal.setEndDate(dateFormat.parse(attribute.getValue()));
					} catch (ParseException e) {
						String eMessage = "Formate of the dates has change";
						Log.e(TAG, eMessage, e);
						AirbrakeHelper.sendException(e, eMessage);
					}
				} else if (attribute.getName().equals(TEXT_TAG_NAME))
					deal.setText(attribute.getValue());
				else if (attribute.getName().equals(TITLE_IMAGE_TAG_NAME))
					deal.setTitleImage(attribute.getValue());
				else if (attribute.getName().equals(PASS_TAG_NAME))
					addPassAtt(attribute, deal);
                else if (attribute.getName().equals(PASS_ONLINE_TAG_NAME))
                    addOnlineAtt(attribute, deal);
                else if (attribute.getName().equals(PRICE_TAG_NAME))
                    deal.setPrice(attribute.getValue());
                else if (attribute.getName().equals(OLD_PRICE_TAG_NAME))
                    deal.setOldPrice(attribute.getValue());
			}

			deals.add(deal);
		}

		return deals;
	}
}

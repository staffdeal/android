package com.einrichtungspartnerring.wirhabens.parser.builder;

import com.einrichtungspartnerring.wirhabens.helper.AirbrakeHelper;
import com.einrichtungspartnerring.wirhabens.manager.ServerResponse;
import com.einrichtungspartnerring.wirhabens.manager.serverresponse.ResetPasswordServerResponse;
import com.einrichtungspartnerring.wirhabens.parser.ParserVO;

import android.util.Log;

/**
 * Build the server response status and return it.
 * @author Thomas Möller
 *
 */
public class ServerResponseBuilder implements TagNames{
	
	private static final String TAG = "ServerResponseBuilder";

	/**
	 * Build the server response for authentication.
	 * @param staffsale - tag tree of the API.
	 * @return the parsed server response.
	 */
	public static ServerResponse buildAuthenticationResponse(ParserVO staffsale){
		ServerResponse response = ServerResponse.unknown_error; 
		int code = extractInformationFromError(staffsale, SUB_CODE);
		response = ServerResponse.mappingResponse(code);
		return response;
	}
	
	/**
	 * Extract out of the normal server response tree the message code.
	 * @param staffsale - normal response xml tree you get from the server.
	 * @return the code id.
	 */
	protected static int extractInformationFromError(ParserVO staffsale, String tagname){
		int code = -1;
		if (staffsale != null && staffsale.getVos() != null && staffsale.getVos().size() > 0){
			ParserVO error = staffsale.getVos().get(0);
			for (ParserVO errorAttr : error.getVos()){
				if (errorAttr.getName().equals(tagname)){
					try{
						code = Integer.valueOf(errorAttr.getValue());
					}catch(NumberFormatException e){
						String eMessage = "extracting information code of API response failed";
						Log.e(TAG, eMessage, e);
						AirbrakeHelper.sendException(e, eMessage);
					}
				}
			}
		}
		return code;
	}
	
	/**
	 * Build out of the given tag-tree the server response.
	 * @param staffsale - tag tree of the API.
	 * @return the parsed server response.
	 */
	public static ResetPasswordServerResponse buildResetPasswordResponse(ParserVO staffsale){
		ResetPasswordServerResponse response = ResetPasswordServerResponse.unknown_error;
		int code = extractInformationFromError(staffsale, CODE_TAG_NAME);
		response = ResetPasswordServerResponse.mappingServerResponseCode(code);
		return response;
	}
	
	/**
	 * Build out of the given staffsale the token for unsubscribe and return it. 
	 * @param staffsale object where the response is in it.
	 * @return the extracted token id to unsubscribe. Null if no token could extract.
	 */
	public static String buildSubScripteNotificationResponse(ParserVO staffsale){
		String token = null;
		if (staffsale != null && staffsale.getVos() != null && staffsale.getVos().size() > 0){
			ParserVO subscription = staffsale.getVos().get(0);
			for (ParserVO subscripteChilds : subscription.getVos()){
				if (subscripteChilds.getName().equals(TOKEN_TAG_NAME)){
					token = subscripteChilds.getValue();
					break;
				}
			}
		}
		return token;
	}
	
	/**
	 * Build out of the given staffsale the token for unsubscribe and return it. 
	 * @param staffsale object where the response is in it.
	 * @return the extracted token id to unsubscribe. Null if no token could extract.
	 */
	public static ServerResponse buildChangePasswordResponse(ParserVO staffsale){
		ServerResponse response = ServerResponse.unknown_error; 
		int code = extractInformationFromError(staffsale, CODE_TAG_NAME);
		response = ServerResponse.mappingResponse(Integer.valueOf(code));
		return response;
	}

}

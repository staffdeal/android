package com.einrichtungspartnerring.wirhabens.parser.builder;

import com.einrichtungspartnerring.wirhabens.parser.ParserVO;
import com.einrichtungspartnerring.wirhabens.vo.DealLoadingDetails;

/**
 * Created by loc on 01.08.14.
 */
public class DealsLoadingDetailsBuilder implements  TagNames{

    public static DealLoadingDetails buildLoadingDetails(ParserVO vo){
        DealLoadingDetails dealDetail = new DealLoadingDetails();

        if (vo != null && vo.getVos() != null && vo.getVos().size() > 0){
            ParserVO redeem = vo.getVos().get(0);
            if (redeem != null && vo.getVos() != null){
                for (ParserVO att : redeem.getVos()){
                    if (att.getName().equals(EAN_TAG_NAME)) {
                        dealDetail.setEan(att.getValue());
                    }else if (att.getName().equals(BARCODE_TAG_NAME)) {
                        dealDetail.setBarcodeUrl(att.getValue());
                    }else if (att.getName().equals(CODE_TAG_NAME)) {
                        dealDetail.setCode(att.getValue());
                    }else if (att.getName().equals(URL_TAG_NAME)) {
                        dealDetail.setUrl(att.getValue());
                    }
                }
            }
        }

        return dealDetail;
    }
}

package com.einrichtungspartnerring.wirhabens.parser.builder;

public interface TagNames {

	String ID_ATTRIBUTE = "id";
    String FEATURED_ATTRIBUTE = "featured";

	String CATEGORY_TAG_NAME = "category";
	String NEWS_ITEM_TAG_NAME = "newsItem";
	String TITLE_TAG_NAME = "title";
	String SLUG_TAG_NAME = "slug";
	String COLOR_TAG_NAME = "color";
	String URL_TAG_NAME = "url";
	String PUBLISHED_TAG_NAME = "published";
	String TEXT_TAG_NAME = "text";
	String IMAGES_TAG_NAME = "images";
    String EAN_TAG_NAME = "ean";
    String BARCODE_TAG_NAME = "barcode";
    String DEAL_TAG_NAME = "deal";
    String FIELD_TAG_NAME = "field";
    String FIELDS_TAG_NAME = "fields";
    String PRICE_TAG_NAME = "price";
    String OLD_PRICE_TAG_NAME = "oldPrice";
    String EVENT_TAG_NAME = "event";

    String BOOKING_TAG_NAME = "booking";
    String DATE_TAG_NAME = "date";
	String IMAGE_TAG_NAME = "image";
	String PROVIDER_TAG_NAME = "provider";
	String TEASER_TAG_NAME = "teaser";
	String START_DATE_TAG_NAME = "startDate";
	String END_DATE_TAG_NAME = "endDate";
	String TITLE_IMAGE_TAG_NAME = "titleImage";
	String COMPANY_TAG_NAME = "company";
	String DESCRIPTION_TAG_NAME = "description";
	String RULES_TAG_NAME = "rules";
	String PARENT_TAG_NAME = "parent";
	String SUB_MISSION_DATE_TAG_NAME = "submissionDate";
	String USER_TAG_NAME = "user";
	String ANALYTICS_TAG_NAME = "analytics";
	String FIRST_NAME_TAG_NAME = "firstname";
	String LAST_NAME_TAG_NAME = "lastname";
	String PASS_TAG_NAME = "pass";
	String PASS_ONLINE_TAG_NAME = "online";
	String PASS_ONSITE_TAG_NAME = "onsite";
	String STATIC_TAG_NAME = "static";
	String STATUS_TAG_NAME = "status";
	String TOKEN_TAG_NAME = "token";
	String EMAIL_TAG_NAME = "email";
	String APPS_TAG_NAME = "apps";
	String CLIENT_TAG_NAME = "client";
	String MODULES_TAG_NAME = "modules";
	String MODULE_TAG_NAME = "module";
	String REGISTRATION_TAG_NAME = "registration";
	String NAME_TAG_NAME = "name";
	String UPDATETEXT_TAG_NAME = "updateText";
	String VERSION_TAG_NAME = "version";
	String ANDROID_TAG_NAME = "Android";
	String PRIVACY_POLICE_TAG_NAME = "privacyPolicy";
	String TERMS_OF_USE_TAG_NAME = "termsOfUse";
	String HINT_TAG_NAME = "hint";
	String PATH_TAG_NAME = "path";
	String AUTH_LEVEL_TAG_NAME = "authLevel";
	String TEMPLATE_TAG_NAME = "templates";
	String ADDRESS_TAG_NAME = "address";
	String STREET_TAG_NAME = "street";
	String ZIP_TAG_NAME = "zip";
	String CITY_TAG_NAME = "city";
	String COUNTRY_TAG_NAME = "country";
	String LOCATION_TAG_NAME = "location";
	String LATITUDE_TAG_NAME = "lat";
	String LONGTITUDE_TAG_NAME = "lon";
	String CODE_TAG_NAME = "code";
	String ABOUT_TAG_NAME = "about";
	String IMPRESS_TAG_NAME = "impress";
	String YOUTUBE_MEDIAL_URL_TAG_NAME = "mediaURL";
    String FACEBOOK_MEDIAL_URL_TAG_NAME = "facebookPageURL";
	String SOURCE_TAG_NAME = "source";
    String END_TEXT = "endText";

	String CATEGORY_GROUP_TAG_NAME = "categories";
	String COMPETITION_GROUP_TAG_NAME = "competitions";
    String SUB_CODE = "subCode";

    String AVAILABLE_ATTRIBUTE_NAME = "available";

    String STICKY_ATTRIBUTE = "sticky";
    String NAME_ATTRIBUTE = "name";
    String TYPE_ATTRIBUTE = "type";
    String REQUIRED_ATTRIBUTE = "required";

    String NO_CODE = "nocode";
    
    String TEMPLATES_USEFOR_KEY = "useFor";
    String TEMPLATES_HASH_KEY = "hash";
    String TEMPLATES_PLATFORM_KEY = "platform";
    
    String GOOGLE_TAG_NAME = "google";

}

package com.einrichtungspartnerring.wirhabens.parser.builder;


import com.einrichtungspartnerring.wirhabens.parser.ParserVO;
import com.einrichtungspartnerring.wirhabens.vo.BookingVO;


public class BookingBuilder implements TagNames {

	private static final String TAG = "BookingBuilder";

	public static BookingVO buildBooking(ParserVO rawBookingItem) {

		BookingVO booking = new BookingVO();
		
		for (ParserVO current : rawBookingItem.getVos()) {
			
			if (current.getName().equals(BOOKING_TAG_NAME)) {
				
				for (ParserVO subCurrent : current.getVos()) {
					if (subCurrent.getName().equals(STATUS_TAG_NAME))
						booking.setStatus(subCurrent.getValue());
					else if (subCurrent.getName().equals(EVENT_TAG_NAME)) {
						booking.setId(subCurrent.getAttributeByName(ID_ATTRIBUTE));
					}
				}
			}
		}
		
		return booking;
	}
	
}

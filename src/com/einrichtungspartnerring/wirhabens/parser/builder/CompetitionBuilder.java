package com.einrichtungspartnerring.wirhabens.parser.builder;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import com.einrichtungspartnerring.wirhabens.helper.AirbrakeHelper;
import com.einrichtungspartnerring.wirhabens.parser.ParserVO;
import com.einrichtungspartnerring.wirhabens.vo.CompetitionCategoryVO;
import com.einrichtungspartnerring.wirhabens.vo.CompetitionVO;

import android.util.Log;


/**
 * Build the competitions items. Elements of the ideas.
 * 
 * @author Thomas Möller
 * 
 */
class CompetitionBuilder implements TagNames {

	private static final String TAG = "CompetitionBuilder";

	/**
	 * Build the items for the competitions.
	 * 
	 * @param rawCompetions
	 *            - xml representation of the competitions.
	 * @param categories
	 *            for the competitions.
	 * @return a list of builded competitions.
	 */
	protected static List<CompetitionVO> buildCompetions(
			ParserVO rawCompetions, List<CompetitionCategoryVO> categories) {
        int i = 0;
		List<CompetitionVO> competitions = new ArrayList<CompetitionVO>();
		CompetitionVO competition;
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd",
				Locale.GERMANY);
		for (ParserVO rawCompetion : rawCompetions.getVos()) {
            i++;
			competition = new CompetitionVO();
			competition.setId(rawCompetion.getAttributeByName(ID_ATTRIBUTE));
            competition.setServerOrder(i);
			for (ParserVO attribute : rawCompetion.getVos()) {
				if (attribute.getName().equals(TITLE_TAG_NAME))
					competition.setTitle(attribute.getValue());
				else if (attribute.getName().equals(TITLE_IMAGE_TAG_NAME))
					competition.setTitleImage(attribute.getValue());
				else if (attribute.getName().equals(CATEGORY_TAG_NAME))
					competition.setCategory(searchCategory(
							attribute.getValue(), categories));
				else if (attribute.getName().equals(COMPANY_TAG_NAME))
					competition.setCompany(attribute.getValue());
				else if (attribute.getName().equals(TEASER_TAG_NAME))
					competition.setTeasertext(attribute.getValue());
				else if (attribute.getName().equals(DESCRIPTION_TAG_NAME))
					competition.setDescription(attribute.getValue());
				else if (attribute.getName().equals(RULES_TAG_NAME))
					competition.setRules(attribute.getValue());
                else if (attribute.getName().equals(END_TEXT))
                    competition.setEndText(attribute.getValue());
				else if (attribute.getName().equals(START_DATE_TAG_NAME)) {
					try {
						competition.setStartDate(dateFormat.parse(attribute
								.getValue()));
					} catch (ParseException e) {
						e.printStackTrace();
					}
				} else if (attribute.getName().equals(END_DATE_TAG_NAME)) {
					try {
						competition.setEndDate(dateFormat.parse(attribute.getValue()));
					} catch (ParseException e) {
						String eMessage = "Formate of the dates has change";
						Log.e(TAG, eMessage, e);
						AirbrakeHelper.sendException(e, eMessage);
					}
				}
			}
			competitions.add(competition);
		}

		return competitions;
	}

	/**
	 * Search out of the given categories the category which id is equal of the
	 * given value.
	 * 
	 * @param value
	 *            - you searched category id.
	 * @param categories
	 *            - categories where should searched.
	 * @return the searched category or null.
	 */
	private static CompetitionCategoryVO searchCategory(String value,
			List<CompetitionCategoryVO> categories) {
		for (CompetitionCategoryVO category : categories)
			if (category.getId().equals(value))
				return category;
		return null;
	}
}

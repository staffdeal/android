package com.einrichtungspartnerring.wirhabens.parser.builder;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import android.util.Log;

import com.einrichtungspartnerring.wirhabens.helper.AirbrakeHelper;
import com.einrichtungspartnerring.wirhabens.parser.ParserVO;
import com.einrichtungspartnerring.wirhabens.vo.EventVO;

public class EventBuilder implements TagNames {

	private static final String TAG = "EventsBuilder";

	/**
	 * Build action for one idea.
	 * 
	 * @param staffsale
	 *            - raw tag
	 * @return a builded list of images.
	 */
	public static List<EventVO> buildevents(ParserVO staffsale) {
		List<EventVO> events = new ArrayList<EventVO>();
		if (staffsale.getVos().size() > 0) {
			ParserVO rawEvents = staffsale.getVos().get(0);
			EventVO event = null;

			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd",
					Locale.GERMANY);
			for (ParserVO rawEvent : rawEvents.getVos()) {
				event = new EventVO();
				event.setId(rawEvent.getAttributeByName(ID_ATTRIBUTE));
				for (ParserVO att : rawEvent.getVos()) {
					if (att.getName().equals(TITLE_TAG_NAME)) {
						event.setTitle(att.getValue());
					} else if (att.getName().equals(DATE_TAG_NAME)) {
						try {
							event.setDate(dateFormat.parse(att.getValue()));
						} catch (ParseException e) {
							String eMessage = "Formate of the dates has change";
							Log.e(TAG, eMessage, e);
							AirbrakeHelper.sendException(e, eMessage);
						}
					} else if (att.getName().equals(TITLE_IMAGE_TAG_NAME)) {
						event.setTitleImage(att.getValue());
					} else if (att.getName().equals(DESCRIPTION_TAG_NAME)) {
						event.setDescription(att.getValue());
					} else if (att.getName().equals(BOOKING_TAG_NAME)) {
						event.setUrl(att.getAttributes().get("url"));
					}
				}
				events.add(event);
			}
		}
		return events;
	}
	
}

package com.einrichtungspartnerring.wirhabens.parser.builder;

import java.util.List;

import com.einrichtungspartnerring.wirhabens.parser.ParserVO;
import com.einrichtungspartnerring.wirhabens.vo.CompetitionCategoryVO;


/**
 * Build the categories for the competitions.
 * 
 * @author Thomas Möller
 * 
 */
class CompetitionCategoryBuilder implements TagNames {

	/**
	 * Build by the given parser object the categories for competitions.
	 * 
	 * @param rawCategories
	 *            - raw parser VO which represent categories.
	 * @return list of builded categories.
	 */
	protected static List<CompetitionCategoryVO> buildCategories(ParserVO rawCategories, List<CompetitionCategoryVO> categories) {
		CompetitionCategoryVO category;
		for (ParserVO rawCategory : rawCategories.getVos()) {
			category = new CompetitionCategoryVO();
			category.setId(rawCategory.getAttributeByName(ID_ATTRIBUTE));
			for (ParserVO attribute : rawCategory.getVos()) {
				if (attribute.getName().equals(TITLE_TAG_NAME))
					category.setTitle(attribute.getValue());
				else if (attribute.getName().equals(IMAGE_TAG_NAME))
					category.setImage(attribute.getValue());
			}
			categories.add(category);
		}

		return categories;
	}
}

package com.einrichtungspartnerring.wirhabens.parser.builder;

import java.util.ArrayList;
import java.util.List;

import com.einrichtungspartnerring.wirhabens.parser.ParserVO;
import com.einrichtungspartnerring.wirhabens.vo.ImageVO;


/**
 * Build the images tags.
 * 
 * @author Thomas Möller
 * 
 */
public class ImagesBuilder implements TagNames {

	/**
	 * Build single image.
	 * 
	 * @param rawImage
	 *            - single raw image.
	 * @return the builded new image.
	 */
	private static ImageVO buildImage(ParserVO rawImage) {
		ImageVO vo = new ImageVO();
		vo.setId(rawImage.getAttributes().get(ID_ATTRIBUTE));
		vo.setPfad(rawImage.getValue());
		return vo;
	}

	/**
	 * Build action for images
	 * 
	 * @param rawImages
	 *            - raw image.
	 * @return a builded list of images.
	 */
	public static List<ImageVO> buildImages(ParserVO rawImages) {
		List<ImageVO> images = new ArrayList<ImageVO>();
		for (ParserVO rawImage : rawImages.getVos())
			images.add(buildImage(rawImage));
		return images;
	}

}

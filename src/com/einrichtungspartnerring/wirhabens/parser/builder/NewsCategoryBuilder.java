package com.einrichtungspartnerring.wirhabens.parser.builder;

import java.util.List;

import com.einrichtungspartnerring.wirhabens.parser.ParserVO;
import com.einrichtungspartnerring.wirhabens.vo.NewsCategoryVO;


/**
 * Build the categories for the news.
 * 
 * @author Thomas Möller
 * 
 */
class NewsCategoryBuilder implements TagNames {

	/**
	 * Build a NewsCategoryVO out of the given rawCategory.
	 * 
	 * @param rawCategory
	 *            - current row element you want build a NewsCategory.
	 * @param possibleParents
	 *            - all NewsCategoryVO you have in the system.
	 * @return a new builded NewsCategoryVO.
	 */
	protected static NewsCategoryVO buildCategory(ParserVO rawCategory,
			List<NewsCategoryVO> possibleParents) {
		NewsCategoryVO vo = new NewsCategoryVO();
		vo.setId(rawCategory.getAttributeByName(ID_ATTRIBUTE));
        vo.setSticky( Boolean.parseBoolean(rawCategory.getAttributeByName(STICKY_ATTRIBUTE)) );
		for (ParserVO current : rawCategory.getVos()) {
			if (current.getName().equals(TITLE_TAG_NAME))
				vo.setTitle(current.getValue());
			else if (current.getName().equals(SLUG_TAG_NAME))
				vo.setSlug(current.getValue());
			else if (current.getName().equals(COLOR_TAG_NAME))
				vo.setColor(current.getValue());
			else if (current.getName().equals(PARENT_TAG_NAME)) {
				setParentInformation(current.getValue(), vo, possibleParents);
			} else
				throw new RuntimeException(
						"You used unknown tag elements, Version-Update");
		}
		return vo;
	}

	/**
	 * Search in the given list after the parent of the current element. Set all
	 * information from the parent element into the given current element
	 * 
	 * @param parentId
	 *            - of from the parent of the current element.
	 * @param current
	 *            element you want set information.
	 * @param possibleParents
	 *            - list of all parents in the system.
	 */
	protected static void setParentInformation(String parentId,
			NewsCategoryVO current, List<NewsCategoryVO> possibleParents) {
		NewsCategoryVO parent = null;
		for (NewsCategoryVO possibleParent : possibleParents) {
			if (possibleParent.getId().equals(parentId)) {
				parent = possibleParent;
				break;
			}
		}
		if (parent != null) {
			current.setColor(parent.getColor());
			current.setSlug(parent.getSlug());
			current.setTitle(parent.getTitle());
		}
	}

}

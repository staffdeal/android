package com.einrichtungspartnerring.wirhabens.parser.builder;

import java.util.List;

import com.einrichtungspartnerring.wirhabens.parser.ParserVO;
import com.einrichtungspartnerring.wirhabens.vo.NewsCategoryVO;
import com.einrichtungspartnerring.wirhabens.vo.NewsItemVO;


public class NewsListBuilder implements TagNames {

    /**
     *
     * @param newsList
     * @param categorys
     * @param firstElement
     * @return the url to the next page;
     */
	public static String buildNews(List<NewsItemVO> newsList,
			List<NewsCategoryVO> categorys, ParserVO firstElement) {
        String nextUrl = null;
        if (firstElement != null && firstElement.getVos() != null){
            for (ParserVO element : firstElement.getVos()){
                if (element.getName().equals("news")){
                    ParserVO newsElement = element;
                    for (ParserVO currentElement : newsElement.getVos()) {
                        if (currentElement.getName().equals(CATEGORY_TAG_NAME))
                            categorys.add(NewsCategoryBuilder.buildCategory(currentElement,
                                    categorys));
                        else if (currentElement.getName().equals(NEWS_ITEM_TAG_NAME)){
                            NewsItemVO item = NewsBuilder.buildCategory(currentElement,categorys);
                            newsList.add(item);
                        }
                    }
                } else if (element.getName().equals("links"))
                {
                    if (element.getVos() != null) {
                        for (ParserVO link : element.getVos()) {
                            if (link.getName().equals("next"))
                                nextUrl = link.getValue();
                        }
                    }
                }
            }
        }
        return  nextUrl;

	}

}

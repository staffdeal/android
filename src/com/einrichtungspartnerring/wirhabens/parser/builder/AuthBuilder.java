package com.einrichtungspartnerring.wirhabens.parser.builder;

import com.einrichtungspartnerring.wirhabens.parser.ParserVO;
import com.einrichtungspartnerring.wirhabens.vo.AuthVO;
import com.einrichtungspartnerring.wirhabens.vo.UserVO;

/**
 * Build the authentication object.
 * 
 * @author Thomas Möller
 * 
 */
public class AuthBuilder implements TagNames {

	public static AuthVO buildAuthentication(ParserVO staffSale) {
		AuthVO auth = null;
		if (staffSale.getVos().size() > 0) {
			auth = new AuthVO();
			ParserVO authRaw = staffSale.getVos().get(0);
			for (ParserVO element : authRaw.getVos()) {
				if (element.getName().equals(STATUS_TAG_NAME)) {
					auth.setStatusMessag(element.getValue());
				} else if (element.getName().equals(TOKEN_TAG_NAME)) {
					auth.setToken(element.getValue());
				} else if (element.getName().equals(USER_TAG_NAME)) {
					auth.setUser(getUserInfo(element));
				}
			}
		}

		return auth;
	}

	public static UserVO getUserInfo(ParserVO userRaw) {
		UserVO user = new UserVO();
		user.setId(userRaw.getAttributeByName(ID_ATTRIBUTE));
		for (ParserVO att : userRaw.getVos()) {
			if (att.getName().equals(FIRST_NAME_TAG_NAME)) {
				user.setFirstname(att.getValue());
			} else if (att.getName().equals(LAST_NAME_TAG_NAME)) {
				user.setLastname(att.getValue());
			} else if (att.getName().equals(EMAIL_TAG_NAME)) {
				user.setEmail(att.getValue());
			}
		}

		return user;
	}
}

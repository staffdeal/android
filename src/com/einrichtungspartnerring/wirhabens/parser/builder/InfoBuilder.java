package com.einrichtungspartnerring.wirhabens.parser.builder;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;

import com.einrichtungspartnerring.wirhabens.StaffsaleApplication;
import com.einrichtungspartnerring.wirhabens.helper.Rights.Level;
import com.einrichtungspartnerring.wirhabens.parser.ParserVO;
import com.einrichtungspartnerring.wirhabens.vo.InfoFieldVO;
import com.einrichtungspartnerring.wirhabens.vo.InfoModul;
import com.einrichtungspartnerring.wirhabens.vo.InfoVO;
import com.einrichtungspartnerring.wirhabens.vo.RegistrationVO;
import com.einrichtungspartnerring.wirhabens.vo.TemplateVO;
import com.einrichtungspartnerring.wirhabens.vo.UserVO;


public class InfoBuilder implements TagNames {


    protected  static void addFieldInformations(ParserVO fields, InfoVO vo){
        if(fields.getVos() != null){
            for(ParserVO field : fields.getVos()){
                if(field.getName().equals(FIELD_TAG_NAME)){
                    InfoFieldVO fieldVo = new InfoFieldVO();
                    fieldVo.setName(field.getAttributeByName(NAME_ATTRIBUTE));
                    fieldVo.setRequired(Boolean.valueOf(field.getAttributeByName(REQUIRED_ATTRIBUTE)));
                    fieldVo.setType(field.getAttributeByName(TYPE_ATTRIBUTE));
                    fieldVo.setUrl(field.getAttributeByName(URL_TAG_NAME));
                    fieldVo.setText(field.getValue());
                    vo.getFields().add(fieldVo);
                }

            }
        }
    }

    /**
	 * Add information about the tag "App" into the given info object
	 * 
	 * @param appRaw
	 * @param info
	 */
	protected static void addAppInformation(ParserVO appRaw, InfoVO info) {
		for (ParserVO appInfoRaw : appRaw.getVos()) {
			if (appInfoRaw.getName().equals(ANDROID_TAG_NAME)) {
				for (ParserVO androidSub : appInfoRaw.getVos()) {
					if (androidSub.getName().equals(VERSION_TAG_NAME)) {
						info.setLatestVersion(Integer.parseInt(androidSub.getValue()));
					}
				}
			} else if(appInfoRaw.getName().equals(UPDATETEXT_TAG_NAME)){
				info.setUpdateText(appInfoRaw.getValue());
			} 
		}
	}
	
	/**
	 * Add information about the tag "Client" into the given info object
	 * 
	 * @param clientRaw
	 * @param info
	 */
	protected static void addClientInformation(ParserVO clientRaw, InfoVO info) {
		for (ParserVO clientInfoRaw : clientRaw.getVos()) {
			if (clientInfoRaw.getName().equals(NAME_TAG_NAME))
				info.setClientName(clientInfoRaw.getValue());
			else if (clientInfoRaw.getName().equals(EMAIL_TAG_NAME))
				info.setClientEMail(clientInfoRaw.getValue());			
			else if (clientInfoRaw.getName().equals(ABOUT_TAG_NAME))
				info.setClientAbout(clientInfoRaw.getValue());
			else if (clientInfoRaw.getName().equals(IMPRESS_TAG_NAME))
				info.setClientImpressum(clientInfoRaw.getValue());
			else if (clientInfoRaw.getName().equals(YOUTUBE_MEDIAL_URL_TAG_NAME))
				info.setYouTubeMediaUrl(clientInfoRaw.getValue());
            else if (clientInfoRaw.getName().equals(FACEBOOK_MEDIAL_URL_TAG_NAME))
                info.setFacebookMediaUrl(clientInfoRaw.getValue());
		}
	}

	/**
	 * Add information about the tag "modules" into the given info object
	 * 
	 * @param modulesRaw
	 * @param info
	 */
	protected static void addModulesInformation(ParserVO modulesRaw, InfoVO info) {
		List<InfoModul> modules = new ArrayList<InfoModul>();
		InfoModul modul = null;
		for (ParserVO moduleRaw : modulesRaw.getVos()) {
			modul = new InfoModul();
			modul.setId(moduleRaw.getAttributeByName("id"));
			for (ParserVO modulAtt : moduleRaw.getVos()) {
				if (modulAtt.getName().equals(TITLE_TAG_NAME)) {
					modul.setTitle(modulAtt.getValue());
				} else if (modulAtt.getName().equals(PATH_TAG_NAME)) {
					modul.setPath(modulAtt.getValue());
				} else if (modulAtt.getName().equals(AUTH_LEVEL_TAG_NAME)) {
					modul.setAuthLevel(Level.valueOf(modulAtt.getValue()));
				} else if (modulAtt.getName().equals(TEMPLATE_TAG_NAME)) {
					ArrayList<TemplateVO> templates = new ArrayList<TemplateVO>();
					
					for(ParserVO subTemplate : modulAtt.getVos()) {
						
						if(subTemplate.getAttributes().get(TEMPLATES_PLATFORM_KEY).equals("android")) {
							String hashkey = subTemplate.getAttributes().get(TEMPLATES_HASH_KEY);
							String useFor = subTemplate.getAttributes().get(TEMPLATES_USEFOR_KEY);
							templates.add(new TemplateVO(subTemplate.getValue(),hashkey,useFor));	
						}
					
					}
					
					modul.setTemplates(templates);
				}
			}
			modules.add(modul);
		}
		info.setModules(modules);
	}

	/**
	 * Add information about the tag "registration" into the given info object
	 * 
	 * @param registrationRaw
	 * @param info
	 */
	protected static void addRegistrationInformation(ParserVO registrationRaw,
			InfoVO info) {
		RegistrationVO reg = new RegistrationVO();
		for (ParserVO regInfoRaw : registrationRaw.getVos()) {
			if (regInfoRaw.getName().equals(PRIVACY_POLICE_TAG_NAME))
				reg.setPrivacyPolicy(regInfoRaw.getValue());
			else if (regInfoRaw.getName().equals(TERMS_OF_USE_TAG_NAME))
				reg.setTermsOfUse(regInfoRaw.getValue());
			else if (regInfoRaw.getName().equals(HINT_TAG_NAME))
				reg.setHint(regInfoRaw.getValue());
            else if(regInfoRaw.getName().equals(FIELDS_TAG_NAME))
                addFieldInformations(regInfoRaw, info);
		}
		info.setRegistration(reg);
	}

	/**
	 * Build the Info VO object and return it.
	 * 
	 * @param staffsaleRaw
	 *            - raw TAG with the information about the info request.
	 * @return the builded VO, or null if something wrong.
	 */
	public static InfoVO buildInfo(ParserVO staffsaleRaw, Context ctx) {
		InfoVO info = null;
		if (!(staffsaleRaw.getVos().size() > 0))
			return info;
		ParserVO infoRaw = staffsaleRaw.getVos().get(0);
		info = new InfoVO();
		for (ParserVO infomationsRaw : infoRaw.getVos()) {
			if(infomationsRaw.getName().equals(APPS_TAG_NAME)) {
				addAppInformation(infomationsRaw,info);
			} else if (infomationsRaw.getName().equals(CLIENT_TAG_NAME)) {
				addClientInformation(infomationsRaw, info);
			} else if (infomationsRaw.getName().equals(MODULES_TAG_NAME)) {
				addModulesInformation(infomationsRaw, info);
			} else if (infomationsRaw.getName().equals(REGISTRATION_TAG_NAME)) {
				addRegistrationInformation(infomationsRaw, info);
			} else if (infomationsRaw.getName().equals(USER_TAG_NAME)) {
				UserVO user = AuthBuilder.getUserInfo(infomationsRaw);
				info.setUser(user);
			} else if (infomationsRaw.getName().equals(ANALYTICS_TAG_NAME)) {
				for (ParserVO analyticsRaw : infomationsRaw.getVos()) {
					if (analyticsRaw.getName().equals(GOOGLE_TAG_NAME)) {
						String gaID = analyticsRaw.getAttributeByName(ID_ATTRIBUTE);
						if(gaID != null) {
							info.setGoogleAnalyticsID(gaID);
							((StaffsaleApplication) ((Activity) ctx).getApplication()).setTrackingID(gaID);
						}
					}
				}
			}
		}
		return info;
	}

}

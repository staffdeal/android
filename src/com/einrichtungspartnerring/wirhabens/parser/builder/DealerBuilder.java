package com.einrichtungspartnerring.wirhabens.parser.builder;

import java.util.ArrayList;
import java.util.List;

import com.einrichtungspartnerring.wirhabens.parser.ParserVO;
import com.einrichtungspartnerring.wirhabens.vo.DealerVO;


/**
 * Builder for the dealers object.
 * @author Thomas Möller
 *
 */
public class DealerBuilder implements TagNames{

	public static List<DealerVO> buildDealers(ParserVO staffsale){
		List<DealerVO> dealers = null;
		if (staffsale.getVos() != null && staffsale.getVos().size() > 0 && staffsale.getVos().get(0).getVos() != null){
			dealers = new ArrayList<DealerVO>();
			DealerVO dealer = null;
			for (ParserVO dealerRaw : staffsale.getVos().get(0).getVos()){
				dealer = new DealerVO();
				dealer.setId(dealerRaw.getAttributeByName(ID_ATTRIBUTE));
				for (ParserVO dealerAtt : dealerRaw.getVos()){
					if (dealerAtt.getName().equals(NAME_TAG_NAME)){
						dealer.setName(dealerAtt.getValue());
					}
					else if (dealerAtt.getName().equals(ADDRESS_TAG_NAME)){
						addAddressInformations(dealerAtt, dealer);
					}
					else if (dealerAtt.getName().equals(LOCATION_TAG_NAME)){
						addLocationInformations(dealerAtt, dealer);
					}
				}
				dealers.add(dealer);
			}
		}
		
		return dealers;
	}
	
	protected static void addAddressInformations (ParserVO addressRaw, DealerVO vo){
		for (ParserVO addressInfo : addressRaw.getVos()){
			if (addressInfo.getName().equals(STREET_TAG_NAME))
				vo.setStreet(addressInfo.getValue());
			else if (addressInfo.getName().equals(ZIP_TAG_NAME))
				vo.setZip(addressInfo.getValue());
			else if (addressInfo.getName().equals(CITY_TAG_NAME))
				vo.setCity(addressInfo.getValue());
			else if (addressInfo.getName().equals(COUNTRY_TAG_NAME))
				vo.setCountry(addressInfo.getValue());
		}
	}
	
	protected static void addLocationInformations (ParserVO locationRaw, DealerVO vo){
		for (ParserVO locationInfo : locationRaw.getVos()){
			if (locationInfo.getName().equals(LATITUDE_TAG_NAME))
				vo.setLocationLat(locationInfo.getValue());
			else if (locationInfo.getName().equals(LONGTITUDE_TAG_NAME))
				vo.setLocationLon(locationInfo.getValue());
		}
	}
}

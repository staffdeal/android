package com.einrichtungspartnerring.wirhabens.parser.builder;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import com.einrichtungspartnerring.wirhabens.helper.AirbrakeHelper;
import com.einrichtungspartnerring.wirhabens.parser.ParserVO;
import com.einrichtungspartnerring.wirhabens.vo.NewsCategoryVO;
import com.einrichtungspartnerring.wirhabens.vo.NewsItemVO;

import android.util.Log;


/**
 * Builder for the news items.
 * 
 * @author Thomas Möller
 * 
 */
class NewsBuilder implements TagNames {

	private static final String TAG = "NewsBuilder";

	/**
	 * Build a single news item out of the parser VO which should represent it.
	 * 
	 * @param rawNewsItem
	 *            - raw also parser VO of the news item.
	 * @param categorys
	 *            - all categories of the system, so that the news can be
	 *            categorized.
	 * @return the builded news item.
	 */
	protected static NewsItemVO buildCategory(ParserVO rawNewsItem,
			List<NewsCategoryVO> categorys) {

		NewsItemVO vo = new NewsItemVO();
		vo.setId(rawNewsItem.getAttributeByName(ID_ATTRIBUTE));
		for (ParserVO current : rawNewsItem.getVos()) {
			if (current.getName().equals(TITLE_TAG_NAME))
				vo.setTitle(current.getValue());
			else if (current.getName().equals(CATEGORY_TAG_NAME)) {
				for (NewsCategoryVO category : categorys) {
					if (category.getId().equals(current.getValue())) {
						vo.setCategory(category);
						break;
					}

				}
			} else if (current.getName().equals(URL_TAG_NAME)) {
				vo.setUrl(current.getValue());
			} else if (current.getName().equals(PUBLISHED_TAG_NAME)) {
				SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.GERMANY);
				try {
					vo.setPublished(dateFormat.parse(current.getValue()));
				} catch (ParseException e) {
					String eMessage = "Formate of the dates has change";
					Log.e(TAG, eMessage, e);
					AirbrakeHelper.sendException(e, eMessage);
				}
			} else if (current.getName().equals(TEXT_TAG_NAME)) {
				vo.setText(current.getValue());
			} else if (current.getName().equals(TITLE_IMAGE_TAG_NAME)) {
				vo.setTitleImage(current.getValue());
			} else if (current.getName().equals(SOURCE_TAG_NAME)) {
				vo.setSource(current.getValue());
			}
		}
		return vo;
	}

}

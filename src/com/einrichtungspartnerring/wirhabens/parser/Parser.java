package com.einrichtungspartnerring.wirhabens.parser;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;

import javax.xml.parsers.FactoryConfigurationError;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 * Parser which use an universal handler and vos for representation.
 * 
 * @author Thomas Möller
 * @date 23.08.2011
 * @TestClasses
 * 
 */
public class Parser {

	/**
	 * Parse a xml file by given Name and return the xml struct with attributes
	 * in the universal ParserVO struct.
	 * 
	 * TODO Error-Handling
	 * 
	 * @param appPath
	 * 
	 * @param fileName
	 *            path to the xml file which should parsed.
	 * @return a ParserVO which the struct of xml.
	 * @throws MerpParserException
	 * 
	 */
	public ParserVO parse(File xmlFile) throws MeinERPParserException {
		ParserVO first = null;
		try {
			ParserHandler viewHandler = new ParserHandler();
			SAXParserFactory factory = SAXParserFactory.newInstance();
			factory.setNamespaceAware(false);

			SAXParser parser = factory.newSAXParser();
			parser.parse(xmlFile, viewHandler);

			first = viewHandler.getFirst();
		} catch (ParserConfigurationException e) {
			throw (new MeinERPParserException(e));
		} catch (FactoryConfigurationError e) {
			throw (new MeinERPParserException(e));
		} catch (SAXException e) {
			throw (new MeinERPParserException(e));
		} catch (IOException e) {
			throw (new MeinERPParserException(e));
		}
		return first;
	}

	/**
	 * Parse a xml file by given Name and return the xml struct with attributes
	 * in the universal ParserVO struct.
	 * 
	 * TODO Error-Handling
	 * 
	 * @param appPath
	 * 
	 * @param iStream
	 *            path to the xml file which should parsed.
	 * @return a ParserVO which the struct of xml.
	 * @throws MerpParserException
	 * 
	 */
	public ParserVO parse(InputStream iStream) throws MeinERPParserException {
		ParserVO first = null;
		try {
			ParserHandler viewHandler = new ParserHandler();
			SAXParserFactory factory = SAXParserFactory.newInstance();
			factory.setNamespaceAware(false);

			SAXParser parser = factory.newSAXParser();
			parser.parse(iStream, viewHandler);

			first = viewHandler.getFirst();
		} catch (ParserConfigurationException e) {
			throw (new MeinERPParserException(e));
		} catch (FactoryConfigurationError e) {
			throw (new MeinERPParserException(e));
		} catch (SAXException e) {
			throw (new MeinERPParserException(e));
		} catch (IOException e) {
			throw (new MeinERPParserException(e));
		}
		return first;
	}

	/**
	 * Parse a xml file by given Name and return the xml struct with attributes
	 * in the universal ParserVO struct.
	 * 
	 * TODO Error-Handling
	 * 
	 * @param appPath
	 * 
	 * @param iStream
	 *            path to the xml file which should parsed.
	 * @return a ParserVO which the struct of xml.
	 * @throws MerpParserException
	 * 
	 */
	public ParserVO parseContent(String string) throws MeinERPParserException {
		InputSource source = new InputSource(new StringReader(string));
		ParserVO first = null;
		try {
			ParserHandler viewHandler = new ParserHandler();
			SAXParserFactory factory = SAXParserFactory.newInstance();
			factory.setNamespaceAware(false);

			SAXParser parser = factory.newSAXParser();
			parser.parse(source, viewHandler);

			first = viewHandler.getFirst();
		} catch (ParserConfigurationException e) {
			throw (new MeinERPParserException(e));
		} catch (FactoryConfigurationError e) {
			throw (new MeinERPParserException(e));
		} catch (SAXException e) {
			throw (new MeinERPParserException(e));
		} catch (IOException e) {
			throw (new MeinERPParserException(e));
		}
		return first;
	}

}
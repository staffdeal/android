package com.einrichtungspartnerring.wirhabens.parser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.xml.sax.Attributes;

/**
 * Universal Object for the Parser data.
 * 
 * @author Thomas Möller
 * @date 23.08.2011
 * @TestClasses
 * 
 */
public class ParserVO {

	/**
	 * name of the tag, sometimtes qname named.
	 */
	private String name;
	/**
	 * A map of attributes from the saxparser
	 */
	private Map<String, String> attributes;

	/**
	 * Struct of under elements in the xml file.
	 */
	private List<ParserVO> vos;

	/**
	 * The value between <tagname> </tagname>.
	 */
	private String value;

	/**
	 * Default Parser
	 */
	public ParserVO() {
		vos = new ArrayList<ParserVO>();
	}

	/**
	 * Parser which set the the minimum values.
	 * 
	 * @param name
	 * @param attributes
	 */
	public ParserVO(String name, Attributes attributes) {
		super();
		this.name = name;
		this.attributes = new HashMap<String, String>();
		vos = new ArrayList<ParserVO>();
		buildMap(attributes);
	}

	protected void buildMap(Attributes attributes) {
		int attributeSize = attributes.getLength();
		for (int i = 0; i < attributeSize; i++)
			this.attributes.put(attributes.getQName(i), attributes.getValue(i));
	}

	/**
	 * Returns the String with the given name.
	 * 
	 * @param name
	 *            The name under which the desired string is saved.
	 * @return the String or null, if the name was not found.
	 */
	public String getAttributeByName(String name) {
		return attributes.get(name);
	}

	public Map<String, String> getAttributes() {
		return attributes;
	}

	public String getName() {
		return name;
	}

	public String getValue() {
		return value;
	}

	public List<ParserVO> getVos() {
		return vos;
	}

	public void setAttributes(Map<String, String> attributes) {
		this.attributes = attributes;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public void setVos(List<ParserVO> vos) {
		this.vos = vos;
	}

	/**
	 * Create a terminal output in the xml struction.
	 * 
	 * @param step
	 *            tab step
	 */
	public void toString(int step) {
		for (int i = 0; i < step; i++)
			System.out.print("\t");
		System.out.println(name);
		for (int i = 0; i < vos.size(); i++) {
			vos.get(i).toString(step + 1);
		}

	}

}
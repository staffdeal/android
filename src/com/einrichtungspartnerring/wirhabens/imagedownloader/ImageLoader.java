package com.einrichtungspartnerring.wirhabens.imagedownloader;

import java.io.File;

import com.einrichtungspartnerring.wirhabens.activityhelper.events.ReloadedInformation;
import com.einrichtungspartnerring.wirhabens.helper.ImageHelper;
import com.einrichtungspartnerring.wirhabens.manager.ImageManager;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Handler;

public class ImageLoader implements Runnable, ReloadedInformation {

	private ImageViewVO vo;
	private ImageManager iManager;
	private Context context;
	private Handler handler;
    private boolean fileIsReloaded;

	public ImageLoader(ImageViewVO vo, ImageManager iManager, Context context) {
		this.vo = vo;
		this.iManager = iManager;
		this.context = context;
		handler = new Handler();
        fileIsReloaded = true;
	}

	@Override
	public void run() {

		File file = iManager.get(context, vo.id, vo.url);
        Bitmap map = null;
        if (fileIsReloaded)
		    map = ImageHelper.decodeAndScaleFile(file, 100, 100, false);
        else
            map = ImageHelper.decodeAndScaleFile(file, 100, 100);

		ImageDisplayer id = new ImageDisplayer(vo, map);
		handler.post(id);
	}

    public void wasReloaded(boolean status){
        fileIsReloaded = status;
    }

}

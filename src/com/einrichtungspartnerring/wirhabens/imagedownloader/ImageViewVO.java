package com.einrichtungspartnerring.wirhabens.imagedownloader;

import com.einrichtungspartnerring.wirhabens.gui.adapter.PicTextAdapter.ViewHolder;

public class ImageViewVO {

	public ViewHolder iView;

	public String id;

	public String url;

	/**
	 * Package visible.
	 */
	protected ImageViewVO() {

	}

	/**
	 * Package visible with initialization.
	 * 
	 * @param iView
	 * @param id
	 * @param url
	 */
	protected ImageViewVO(ViewHolder iView, String id, String url) {
		super();
		this.iView = iView;
		this.id = id;
		this.url = url;
	}
}

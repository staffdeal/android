package com.einrichtungspartnerring.wirhabens.imagedownloader;

import android.graphics.Bitmap;

class ImageDisplayer implements Runnable {

	private ImageViewVO vo;
	private Bitmap bitmap;

	protected ImageDisplayer(ImageViewVO vo, Bitmap bitmap) {
		super();
		this.vo = vo;
		this.bitmap = bitmap;
	}

	@Override
	public void run() {
		if(vo.iView.id == vo.id) {
			vo.iView.image.setImageBitmap(bitmap);
		}
	}

}

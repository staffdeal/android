package com.einrichtungspartnerring.wirhabens.imagedownloader;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import android.content.Context;
import android.widget.ImageView;

import com.einrichtungspartnerring.wirhabens.gui.adapter.PicTextAdapter.ViewHolder;
import com.einrichtungspartnerring.wirhabens.manager.ImageManager;

/**
 * Download all given Images, which are added in the image downloader.
 * 
 * @Source https://github.com/thest1/LazyList
 * @author Thomas Möller
 * 
 */
public class ImageDownloader {

	private Context context;

	private ImageManager iManager;

	private ExecutorService executor;
	private List<ImageView> imageCache = Collections.synchronizedList(new ArrayList<ImageView>());
    private Map<ImageView, String> imageUrl = new HashMap<ImageView, String>();

	private ImageDownloader() {
		executor = Executors.newFixedThreadPool(3);
	}

	public ImageDownloader(Context context, ImageManager iManager) {
		this();
		this.context = context;
		this.iManager = iManager;
	}

	public void add(ViewHolder iView, String id, String url) {
		//if (isCached(iView, url) == false) {
            imageUrl.put(iView.image, url);
			imageCache.add(iView.image);
			ImageViewVO vo = new ImageViewVO(iView, id, url);
			executor.submit(new ImageLoader(vo, iManager, context));
		//}
	}

	public synchronized boolean isCached(ImageView iView, String url) {
		for (ImageView view : imageCache) {
			if (iView.equals(view) && imageUrl.get(iView) != null && imageUrl.get(iView) == url)
				return true;
		}
		return false;
	}

}

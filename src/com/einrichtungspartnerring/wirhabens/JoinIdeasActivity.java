package com.einrichtungspartnerring.wirhabens;

import java.io.File;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.einrichtungspartnerring.wirhabens.gui.listener.joinideas.AddGalleryPicListener;
import com.einrichtungspartnerring.wirhabens.gui.listener.joinideas.SubmitButtonListener;
import com.einrichtungspartnerring.wirhabens.helper.AirbrakeHelper;
import com.einrichtungspartnerring.wirhabens.manager.CompetitionIdeasManager;
import com.einrichtungspartnerring.wirhabens.manager.ManagerFactory;
import com.einrichtungspartnerring.wirhabens.manager.NetworkManager;
import com.einrichtungspartnerring.wirhabens.vo.CompetitionVO;

public class JoinIdeasActivity extends Activity {

	private static final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 100;
	private static final int SELECT_PICTURE = 200;
	public static final String COMPETITON_ID = "com id";

	private  SubmitButtonListener submitButtonL;
	private ImageView ImagePreView ;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		try{
			if (!MainActivity.wasStarted){
				Intent i = new Intent(getBaseContext(), MainActivity.class);
				i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(i);
			}
			if (android.os.Build.VERSION.SDK_INT <= Build.VERSION_CODES.GINGERBREAD_MR1){
				MainActivity.customActionBar();
			}
			else{
				MainActivity.customActionBar(getActionBar(), this);
			}
			setContentView(R.layout.activity_join_ideas);

			Bundle extras = getIntent().getExtras();
            if (extras == null)
                finish();
            String comId =  extras.getString(COMPETITON_ID);

			CompetitionIdeasManager comManager = ManagerFactory.buildCompetitionIdeasManager(this);
			CompetitionVO competitionVO = comManager.getItemById(comId);

			NetworkManager netManager = ManagerFactory.buildNetworkManager(this);

			EditText text = (EditText) findViewById(R.id.join_idea_submit_text);
			EditText link = (EditText) findViewById(R.id.join_idea_submit_link);

			ImagePreView = (ImageView) findViewById(R.id.join_ideas_preview_image);

			submitButtonL = new SubmitButtonListener(this, competitionVO, netManager, text, link);
			Button submitButton = (Button) findViewById(R.id.join_ideas_submit);
			submitButton.setOnClickListener(submitButtonL);

			Button galleryAddPicButton = (Button) findViewById(R.id.join_ideas_gallery_pic_addButton);
			galleryAddPicButton.setOnClickListener(new AddGalleryPicListener(SELECT_PICTURE, this));
		}catch(Exception e){
			String eMessage = "Unknown Exception";
			AirbrakeHelper.sendException(e, eMessage);
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE) {
			// TODO implement

		}
		if (requestCode == SELECT_PICTURE) {
			if (resultCode == RESULT_OK) {
				Uri selectedImageUri = data.getData();
				String imagePathString = getPath(selectedImageUri);
				System.out.println("!!!!!!!!!!!!IMAGE: " + selectedImageUri);
				File imgPath = new File(imagePathString);
				Bitmap myBitmap = BitmapFactory.decodeFile(imgPath.getAbsolutePath());
				ImagePreView.setImageBitmap(myBitmap);
				submitButtonL.setImagePath(imgPath);
			}
		}
	}

	public String getPath(Uri uri) {
		String[] projection = { MediaStore.Images.Media.DATA };
		Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
		//Cursor cursor = managedQuery(uri, projection, null, null, null);
		cursor.moveToFirst();
		int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
		cursor.moveToFirst();
		String path = cursor.getString(column_index);
		cursor.close();
		return path;
	}

}

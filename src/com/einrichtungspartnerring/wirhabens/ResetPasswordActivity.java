package com.einrichtungspartnerring.wirhabens;

import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;

import com.einrichtungspartnerring.wirhabens.gui.listener.reset.ResetButtonListener;

/**
 * Layout for resetting password.
 * @author Thomas Möller
 *
 */
public class ResetPasswordActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		if (android.os.Build.VERSION.SDK_INT <= Build.VERSION_CODES.GINGERBREAD_MR1){
			MainActivity.customActionBar();
		}
		else{
			MainActivity.customActionBar(getActionBar(), this);
		}
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_reset_password);


		EditText email = (EditText) findViewById(R.id.reset_password_email);

		Button b1 = (Button) findViewById(R.id.reset_password_start_button);
		b1.setOnClickListener(new ResetButtonListener(this, email));

	}

}

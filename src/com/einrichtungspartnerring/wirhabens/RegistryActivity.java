package com.einrichtungspartnerring.wirhabens;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.*;

import com.einrichtungspartnerring.wirhabens.activityhelper.InfoLoadAT;
import com.einrichtungspartnerring.wirhabens.activityhelper.RegistryDealerGrabberAT;
import com.einrichtungspartnerring.wirhabens.activityhelper.events.OnInfoLoadCompleteEvent;
import com.einrichtungspartnerring.wirhabens.gui.listener.registry.CheckBoxVerificationListener;
import com.einrichtungspartnerring.wirhabens.gui.listener.registry.RegistryButtonListener;
import com.einrichtungspartnerring.wirhabens.gui.listener.registry.RegistryLocationListener;
import com.einrichtungspartnerring.wirhabens.helper.AirbrakeHelper;
import com.einrichtungspartnerring.wirhabens.helper.GPSHelper;
import com.einrichtungspartnerring.wirhabens.helper.HTMLProjectHelper;
import com.einrichtungspartnerring.wirhabens.helper.MeinERPDialogBuilder;
import com.einrichtungspartnerring.wirhabens.manager.ManagerFactory;
import com.einrichtungspartnerring.wirhabens.manager.NetworkManager;
import com.einrichtungspartnerring.wirhabens.vo.RegistrationVO;

import java.util.ArrayList;
import java.util.List;

/**
 * Activity for registry
 * 
 * @author Thomas Möller
 * 
 */
public class RegistryActivity extends Activity implements OnInfoLoadCompleteEvent{

	private LocationManager locationManager;
	private NetworkManager netManager;
    private Location currentLocation;
    private RegistryLocationListener locationListener;

    private ProgressBar locationProgressBar;
    private ImageView locationChecked;
    private Button registryButton;

    public static int REQUEST_GPS_SETTING_CODE = 134;

    public void setCurrentLocation(Location location){
        this.currentLocation = location;
        if(currentLocation != null){
            registryButton.setEnabled(true);
            locationChecked.setVisibility(View.VISIBLE);
            locationProgressBar.setVisibility(View.GONE);
        }
    }

    public Location getCurrentLocation(){
        return this.currentLocation;
    }

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		try{
			if (android.os.Build.VERSION.SDK_INT <= Build.VERSION_CODES.GINGERBREAD_MR1){
				MainActivity.customActionBar();
			}
			else{
				MainActivity.customActionBar(getActionBar(), this);
			}
			setContentView(R.layout.activity_registry);
			registryButton = (Button) findViewById(R.id.registy_regist_button);
			this.netManager = ManagerFactory.buildNetworkManager(this);
			View view = findViewById(R.id.activity_registry_main_layout);
			locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

            locationProgressBar = (ProgressBar) findViewById(R.id.registy_gps_progressBar);
            locationChecked = (ImageView) findViewById(R.id.registy_gps_complete);
            locationChecked.setVisibility(View.GONE);


            Spinner dealerSpinner = (Spinner)findViewById(R.id.registy_dealer_spinner);
            dealerSpinner.setVisibility(View.GONE);
            ProgressBar bar = (ProgressBar) findViewById(R.id.registy_dealer_loading_progressBar);
            bar.setVisibility(View.VISIBLE);

            new RegistryDealerGrabberAT(this, dealerSpinner, bar, netManager).execute();

			registryButton.setOnClickListener(new RegistryButtonListener(view,
					netManager, this, locationManager));
			if (netManager.getInfo() == null){
				new InfoLoadAT(this, netManager, this).execute();
			}else{
				onInfoLoadComplete();
			}
            startGPS();
		}catch(Exception e){
			String eMessage = "Unknown Exception";
			AirbrakeHelper.sendException(e, eMessage);
		}

	}

    private void startGPS(){

        if (GPSHelper.isGPSOn(locationManager))
            startGPSLocalization();
        else  {
            AlertDialog dialog = MeinERPDialogBuilder.buildActivateGPSDialog(this);
            dialog.show();
        }

    }

    private void startGPSLocalization(){
        //Criteria criteria = new Criteria();
        //criteria.setAccuracy(Criteria.ACCURACY_FINE);
        //criteria.setCostAllowed(false);
        //String provider = locationManager.getBestProvider(criteria, true);
        locationListener = new RegistryLocationListener(this, locationManager);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0,locationListener);
    }


	@Override
	protected void onStop() {
        if (this.locationListener != null)
            this.locationManager.removeUpdates(this.locationListener);
		super.onStop();
	}

	@Override
	public void onInfoLoadComplete(Object... objects) {
		CheckBox privacyPolicyBox = (CheckBox) findViewById(R.id.registy_privacy_policy_checkbox);
		CheckBox termsOfUseBox = (CheckBox) findViewById(R.id.registy_terms_of_use_checkbox);
		
		
		
		WebView hint = (WebView) findViewById(R.id.registy_hint_content);

		RegistrationVO registrationInfo = null;
		if (netManager.getInfo() != null)
			registrationInfo = netManager.getInfo().getRegistration();

		String privacyPolicyText = getString(R.string.no_information);
		if (registrationInfo != null && registrationInfo.getPrivacyPolicy() != null)
			privacyPolicyText = HTMLProjectHelper.handlingURLEncoding(registrationInfo.getPrivacyPolicy());
		privacyPolicyBox.setOnClickListener(new CheckBoxVerificationListener(privacyPolicyBox, privacyPolicyText, this));
		
		String termsOfUseText = getString(R.string.no_information);
		if (registrationInfo != null && registrationInfo.getTermsOfUse() != null)
			termsOfUseText = HTMLProjectHelper.handlingURLEncoding(registrationInfo.getTermsOfUse());
		termsOfUseBox.setOnClickListener(new CheckBoxVerificationListener(termsOfUseBox, termsOfUseText, this));
		
		if (registrationInfo != null && registrationInfo.getHint() != null){
			String hintText = HTMLProjectHelper.handlingURLEncoding(registrationInfo.getHint());
			hint.loadData(hintText, "text/html; charset=utf-8", "UTF-8");
		}
		else
			hint.loadData(getString(R.string.no_information),"text/html", "utf-8");
		
	}

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_GPS_SETTING_CODE)
            startGPS();
    }
}

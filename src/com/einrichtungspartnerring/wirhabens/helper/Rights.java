package com.einrichtungspartnerring.wirhabens.helper;

/**
 * Rights in this system.
 * 
 * @author Thomas Möller
 * 
 */
public class Rights {

	/**
	 * Rights level for the systems.
	 * 
	 * @author Thomas Möller
	 * 
	 */
	public enum Level {
		guest, user
	}

	/**
	 * Check if the given currentLevel in minLevel.
	 * 
	 * @param minLevel
	 * @param currentLevel
	 * @return true if given current level in min level.
	 */
	public static boolean check(Level minLevelEnum, Level currentLevelEnum) {
		int min = minLevelEnum.ordinal();
		int current = currentLevelEnum.ordinal();
		if (current >= min)
			return true;
		return false;
	}

	/**
	 * Check if the given currentLevel in minLevel.
	 * 
	 * @param minLevel
	 * @param currentLevel
	 * @return true if given current level in min level.
	 */
	public static boolean check(String minLevel, String currentLevel) {
		Level minLevelEnum = Level.valueOf(minLevel);
		Level currentLevelEnum = Level.valueOf(currentLevel);
		return check(minLevelEnum, currentLevelEnum);

	}

}

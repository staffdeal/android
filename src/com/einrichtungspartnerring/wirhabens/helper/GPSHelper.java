package com.einrichtungspartnerring.wirhabens.helper;

import android.location.LocationManager;

/**
 * Created by loc on 18.01.14.
 */
public class GPSHelper {


    /**
     * Check if gps online
     * @param locationManager
     * @return
     */
    public static boolean isGPSOn(LocationManager locationManager){
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

    }
}

package com.einrichtungspartnerring.wirhabens.helper;

import java.util.HashMap;

import android.content.Context;
import android.content.pm.PackageManager.NameNotFoundException;

import com.einrichtungspartnerring.wirhabens.properties.MeinERPProperties;
import com.einrichtungspartnerring.wirhabens.properties.MeinERPPropertiesFactory;
import com.einrichtungspartnerring.wirhabens.properties.PropertiesList;

import de.forloc.airbrake.AirbrakeNotice;
import de.forloc.airbrake.AirbrakeNoticeBuilder;
import de.forloc.airbrake.AirbrakeNotifier;


/**
 * Helper for sending Airbrake messages.
 * @author Thomas Möller
 *
 */
public class AirbrakeHelper {

	private static MeinERPProperties prop; 

	public static void load(Context context){
		prop = MeinERPPropertiesFactory.getProperties(context);
	}

	public static void sendException(Exception e){
		if (prop == null)
			load(null);
		if (prop != null){
			AirbrakeNotice notice;
			try {
				notice = new AirbrakeNoticeBuilder(prop.get(PropertiesList.AIRBRAKE_API_KEY), e, AndroidInformationHelper.getAPKVersionName(null)).newNotice();
				AirbrakeNotifier notifier = new AirbrakeNotifier(prop.get(PropertiesList.AIRBRAKE_URL), notice);
				notifier.execute();
			} catch (NameNotFoundException e1) {
				e1.printStackTrace();
			}
		}
	}
	
	public static void sendException(Exception e, String message){
		if (prop == null)
			load(null);
		if (prop != null){
			AirbrakeNotice notice;
			HashMap<String, String> map = new HashMap<String, String>();
			map.put("message", message);
			try {
				notice = new AirbrakeNoticeBuilder(prop.get(PropertiesList.AIRBRAKE_API_KEY), e, AndroidInformationHelper.getAPKVersionName(null)).newNotice(map);
				AirbrakeNotifier notifier = new AirbrakeNotifier(prop.get(PropertiesList.AIRBRAKE_URL), notice);
				notifier.execute();
			} catch (NameNotFoundException e1) {
				e1.printStackTrace();
			}
		}
	}

}

package com.einrichtungspartnerring.wirhabens.helper;

/**
 * Helper for handling HTML content in this project
 * @author Thomas Möller
 *
 */
public class HTMLProjectHelper {

	public static String handlingURLEncoding(String content){
		return "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>"+
				"<html><head>"+
				"<meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\" />"+
				"<head><body>"+ content+ "</body></html>";
	}

}

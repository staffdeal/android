package com.einrichtungspartnerring.wirhabens.helper;

import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface.OnClickListener;
import android.graphics.Bitmap;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import com.einrichtungspartnerring.wirhabens.DealActivity;
import com.einrichtungspartnerring.wirhabens.MainActivity;
import com.einrichtungspartnerring.wirhabens.R;
import com.einrichtungspartnerring.wirhabens.activityhelper.events.OnTaskLoginCompleteEvent;
import com.einrichtungspartnerring.wirhabens.gui.adapter.CategoryFilterDialogAdapter;
import com.einrichtungspartnerring.wirhabens.gui.listener.dialogbuilder.DialogActivateGPSActiveButton;
import com.einrichtungspartnerring.wirhabens.gui.listener.dialogbuilder.DialogCancelButton;
import com.einrichtungspartnerring.wirhabens.gui.listener.dialogbuilder.DialogCategoryFilterItemListListener;
import com.einrichtungspartnerring.wirhabens.gui.listener.dialogbuilder.DialogCategoryFilterOKButtonListener;
import com.einrichtungspartnerring.wirhabens.gui.listener.dialogbuilder.DialogLoginLoginButton;
import com.einrichtungspartnerring.wirhabens.gui.listener.dialogbuilder.DialogLoginRegistryButton;
import com.einrichtungspartnerring.wirhabens.gui.listener.dialogbuilder.DialogLoginResetPassword;
import com.einrichtungspartnerring.wirhabens.gui.listener.registry.DialogRegistryCancelButton;
import com.einrichtungspartnerring.wirhabens.manager.MeinEPRSystemManager;
import com.einrichtungspartnerring.wirhabens.manager.NetworkManager;

/**
 * Builder for the dialogs.
 * 
 * @author Thomas Möller
 * 
 */
public class MeinERPDialogBuilder {

	private static final String TAG = "MeinERPDialogBuilder";


    /**
     * Build a progress dialog with string ids.
     * @param activity you want to show the dialog.
     * @param title of your dialog.
     * @return the built dialog.
     */
    public static ProgressDialog buildProgressDialog(Activity activity, int title){
        ProgressDialog dia = new ProgressDialog(activity);
        if (title > 0)
            dia.setTitle(title);
        return dia;
    }

    /**
     * Build a progress dialog with string ids.
     * @param activity you want to show the dialog.
     * @param title of your dialog.
     * @param body of your dialog.
     * @return the built dialog.
     */
    public static ProgressDialog buildProgressDialog(Activity activity, int title, int body){
        ProgressDialog dia = new ProgressDialog(activity);
        if (title > 0)
            dia.setTitle(title);
        if (body > 0){
            String message = activity.getString(body);
            dia.setMessage(message);
        }
        return dia;
    }

	/**
	 * Build the dialog for the activate GPS.
	 * 
	 * @param activity
	 * @return the builded dialog.
	 */
	public static AlertDialog buildActivateGPSDialog(Activity activity) {
		AlertDialog.Builder builder = new AlertDialog.Builder(activity);
		builder.setCancelable(false);
		builder.setMessage(R.string.dialog_gps_activate_message);
		builder.setTitle(R.string.dialog_gps_activate_title);
		builder.setPositiveButton(R.string.dialog_gps_activate_to_settings,
				new DialogActivateGPSActiveButton(activity));
		builder.setNegativeButton(R.string.dialog_button_cancel,
				new DialogRegistryCancelButton(activity));

		return builder.create();
	}

	/**
	 * Build the authentication dialog and return it.
	 * 
	 * @param activity
	 * @return the builded authentication dialog.
	 */
	public static AlertDialog buildAuthenticatDialog(final Activity activity,
			NetworkManager netManager, OnTaskLoginCompleteEvent event) {
		AlertDialog.Builder builder = new AlertDialog.Builder(activity);
		builder.setCancelable(false);
		
		LayoutInflater inflater = activity.getLayoutInflater();
		View dialogView = inflater.inflate(R.layout.dialog_login, null);
		TextView message = (TextView)dialogView.findViewById(R.id.dialog_login_message);
		
		builder.setView(dialogView);
		builder.setMessage(R.string.dialog_login_text)
				.setTitle(R.string.dialog_login_title)
				.setPositiveButton(R.string.dialog_login_button_login,
						new DialogLoginLoginButton(activity, netManager, event, message))
				.setNegativeButton(R.string.dialog_button_cancel,new DialogCancelButton())
				.setNeutralButton(R.string.dialog_login_button_regist,
						new DialogLoginRegistryButton(activity));
		TextView resetlink = (TextView) dialogView.findViewById(R.id.dialog_login_resetpassword);
		resetlink.setOnClickListener(new DialogLoginResetPassword(activity));
		return builder.create();
	}
 
	/**
	 * Build the dialog for the filters.
	 * 
	 * @param activity
	 *            where the filter should show
	 * @param systemManager
	 *            - instance of a MeinERPSystemManager.
	 * @return the builded dialog for filters.
	 */
	public static Dialog buildFilterDialog(MainActivity activity,
			MeinEPRSystemManager<?> systemManager, int currentTab) {
		Log.d(TAG, "build the filter for the dialog");
		if (systemManager.getCategories() == null || systemManager.getCategories().size() == 0) {
			return null;
		}
		AlertDialog.Builder builder = new AlertDialog.Builder(activity);
		builder.setCancelable(false);
		builder.setTitle(R.string.dialog_category_filter_title);
		builder.setAdapter(
				new CategoryFilterDialogAdapter(systemManager.getCategories(),
						activity), new DialogCategoryFilterItemListListener());

		builder.setPositiveButton(R.string.dialog_button_ok,
				new DialogCategoryFilterOKButtonListener(activity, systemManager, currentTab));
		builder.setNegativeButton(R.string.dialog_button_cancel,
				new DialogCancelButton());
		return builder.create();
	}

	/**
	 * Build a information dialog.
	 * 
	 * @param activity
	 * @return the builded dialog.
	 */
	public static AlertDialog buildInformationDialog(Activity activity,
			String title, String message) {
		AlertDialog.Builder builder = new AlertDialog.Builder(activity);
		builder.setCancelable(false);
		if (message != null)
			builder.setMessage(message);
		if (title != null)
			builder.setTitle(title);
		builder.setPositiveButton(R.string.dialog_button_ok,new DialogCancelButton());

		return builder.create();
	}
	
	/**
	 * Build a information dialog.
	 * 
	 * @param activity
	 * @return the builded dialog.
	 */
	public static AlertDialog buildInformationDialog(Activity activity,
			int title, int message) {
		AlertDialog.Builder builder = new AlertDialog.Builder(activity);
		builder.setCancelable(false);
		builder.setMessage(message);
		builder.setTitle(title);
		builder.setPositiveButton(R.string.dialog_button_ok,new DialogCancelButton());

		return builder.create();
	}


    /**
     * Build a information dialog.
     *
     * @param activity
     * @return the builded dialog.
     */
    public static AlertDialog buildInformationDialog(Activity activity,
                                                     int title, int message, List<String> errors) {


        String error = "";
        for (String oneerror : errors)
            error += oneerror;

        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setCancelable(false);
        builder.setMessage(error);
        builder.setTitle(title);
        builder.setPositiveButton(R.string.dialog_button_ok,new DialogCancelButton());

        return builder.create();
    }
	
	/**
	 * Build a information dialog.
	 * 
	 * @param activity
	 * @return the builded dialog.
	 */
	public static AlertDialog buildInformationDialog(Activity activity,
			String title, String message, OnClickListener listener) {
		AlertDialog.Builder builder = new AlertDialog.Builder(activity);
		builder.setCancelable(false);
		if (message != null)
			builder.setMessage(message);
		if (title != null)
			builder.setTitle(title);
		builder.setPositiveButton(R.string.dialog_button_ok,listener);

		return builder.create();
	}
	
	/**
	 * Build a dialog with a image.
	 * @param activity where the dialog should show.
	 * @param title of the dialog.
	 * @param message of the dialog.
	 * @param map picture of the dialog.
	 * @return the builded dialog.
	 */
	public static AlertDialog buildDialogWithImage(Activity activity,
			String title, String message, Bitmap map){
		AlertDialog.Builder builder = new AlertDialog.Builder(activity);
		LayoutInflater inflater = activity.getLayoutInflater();
		View view = inflater.inflate(R.layout.dialog_image, null);
		builder.setView(view);
		ImageView iView = (ImageView) view.findViewById(R.id.dialog_image_image);
		iView.setImageBitmap(map);
		if (message != null)
			builder.setMessage(message);
		if (title != null)
			builder.setTitle(title);
		return builder.create();
		
	}

	
	public static AlertDialog buildDialogGivenYesAction(Activity activity,
			String title, String message, OnClickListener yesButton, String nameYesButton){
		AlertDialog.Builder builder = new AlertDialog.Builder(activity);
		builder.setCancelable(false);
		if (message != null)
			builder.setMessage(message);
		if (title != null)
			builder.setTitle(title);
		builder.setNegativeButton(R.string.dialog_button_cancel,
				new DialogCancelButton());
		builder.setPositiveButton(nameYesButton, yesButton);
		return builder.create();
	}

    public static AlertDialog buildDialogYesNoCancelActionWithText(final Activity activity, int title, int message, int nameNoButton, int nameYesButton,
                                                                   OnClickListener yesButton, OnClickListener noButton){
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setCancelable(false);
        builder.setMessage(message);
        builder.setTitle(title);
        builder.setNegativeButton(R.string.dialog_button_cancel,new DialogCancelButton());
        builder.setPositiveButton(nameYesButton, yesButton);
        builder.setNeutralButton(nameNoButton, noButton);
        return builder.create();
    }
    
	public static AlertDialog buildDialogYesNoCancelActionWithText(
			final Activity activity, int title, int message, int nameNoButton,
			int nameYesButton, OnClickListener yesButton,
			OnClickListener noButton, OnClickListener cancelButton) {
		AlertDialog.Builder builder = new AlertDialog.Builder(activity);
		builder.setCancelable(false);
		builder.setMessage(message);
		builder.setTitle(title);
		builder.setNegativeButton(R.string.dialog_button_cancel, cancelButton);
		builder.setPositiveButton(nameYesButton, yesButton);
		builder.setNeutralButton(nameNoButton, noButton);
		return builder.create();
	}
    

	public static AlertDialog buildDialogGivenYesActionHTMLMessage(
			DealActivity activity, String title, String htmlMessage,
			android.content.DialogInterface.OnClickListener yesButton,
			String buttonName) {
		AlertDialog.Builder builder = new AlertDialog.Builder(activity);
		LayoutInflater inflater = activity.getLayoutInflater();
		View view = inflater.inflate(R.layout.dialog_html_message, null);
		WebView messageField = (WebView)view.findViewById(R.id.dialog_html_message_field);
		builder.setView(view);
		builder.setCancelable(false);
		if (htmlMessage != null) {
			Spanned spannedText = Html.fromHtml(htmlMessage);
			TextView textSp = new TextView(activity);
			textSp.setText(spannedText);
			messageField.loadData(textSp.getText().toString(), "text/html", "utf-8");
		} if (title != null)
			builder.setTitle(title);
		builder.setNegativeButton(R.string.dialog_button_cancel,
				new DialogCancelButton());
		builder.setPositiveButton(buttonName, yesButton);
		return builder.create();
	}
	
	
	public static AlertDialog buildDialogYesNoActionWithHTMLMessage(
			Activity activity, int title, String htmlMessage,
			OnClickListener yesButton, OnClickListener noButton,
			int yesButtonName) {
		AlertDialog.Builder builder = new AlertDialog.Builder(activity);
		LayoutInflater inflater = activity.getLayoutInflater();
		View view = inflater.inflate(R.layout.dialog_html_message, null);
		WebView messageField = (WebView)view.findViewById(R.id.dialog_html_message_field);
		
		WebSettings settings = messageField.getSettings();
		settings.setDefaultFontSize(10);
		
		if (htmlMessage != null) {
			Spanned spannedText = Html.fromHtml(htmlMessage);
			TextView textSp = new TextView(activity);
			textSp.setText(spannedText);
			messageField.loadData(textSp.getText().toString(), "text/html", "utf-8");
			
		}
		
		builder.setView(view);
		builder.setCancelable(false);

		builder.setTitle(title);
		builder.setNegativeButton(R.string.dialog_button_cancel, noButton);
		builder.setPositiveButton(yesButtonName, yesButton);
		return builder.create();
	}

    public static AlertDialog buildInformationDialogWithHTMLMessage(
            Activity activity, int title, String htmlMessage) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        LayoutInflater inflater = activity.getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_html_message, null);
        WebView messageField = (WebView)view.findViewById(R.id.dialog_html_message_field);
        builder.setView(view);
        builder.setCancelable(false);
        if (htmlMessage != null)
            messageField.loadData(htmlMessage, "text/html", "utf-8");
        builder.setTitle(title);
        builder.setPositiveButton(R.string.dialog_button_ok,new DialogCancelButton());
        return builder.create();
    }
    
    public static void changeTab(Activity activity) {
    	((MainActivity) activity).changeTab(((MainActivity) activity).getmCurrentTab());
    	Log.d("TAB","Changing tab");
    }
}

package com.einrichtungspartnerring.wirhabens.helper;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.io.Writer;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import android.util.Log;

/**
 * Helper for handling streams.
 * @author Thomas Möller
 *
 */
public class StreamHelper {

	private final static String TAG = "StreamHelper";

	/**
	 * Close the given InputStream.
	 * 
	 * @param iStream
	 *            - stream you want close.
	 */
	public static void close(InputStream iStream) {
		if (iStream != null) {
			try {
				iStream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Close the given OutputStream
	 * 
	 * @param oStream
	 *            - stream you want close.
	 */
	public static void close(OutputStream oStream) {
		if (oStream != null) {
			try {
				oStream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Close the given Writer
	 * 
	 * @param writer
	 *            - stream you want close.
	 */
	public static void close(Writer writer) {
		if (writer != null) {
			try {
				writer.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Copy the given input stream to the given output stream
	 * 
	 * @param input
	 *            where the information from.
	 * @param output
	 *            where the information should in
	 * @return number of copies byte.
	 * @throws IOException
	 */
	public static int copy(InputStream input, OutputStream output)
			throws IOException {
		byte[] buffer = new byte[500];
		int count = 0;
		int n = 0;
		while (-1 != (n = input.read(buffer))) {
			output.write(buffer, 0, n);
			count += n;
		}
		return count;
	}

	/**
	 * Close the given reader.
	 * @param reader
	 */
	public void close(Reader reader) {
		if (reader != null) {
			try {
				reader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Write the given lines into the given file.
	 * @param lines you want to write into the file.
	 * @param file you want to write the lines.
	 * @throws IOException - case something go wrong.
	 */
	public void writeLines(List<String> lines, File file) throws IOException{
		Writer writer = null;
		try {
			Log.d(TAG, "write into file : " + file.getAbsolutePath());
			writer = new FileWriter(file);
			for (String line : lines) {
				writer.write(line + "\n");
			}
		} catch (IOException e) {
			throw e;
		} finally {
			close(writer);
		}
	}
	/**
	 * Read all lines in the given file and return it.
	 * @param file where the lines should read out.
	 * @return the lines in the given file or null if the file not exist.
	 * @throws IOException - case something go wrong.
	 */
	public List<String> readLines(File file) throws IOException{
		List<String> lines = null;
		if (file.exists()) {
			lines = new ArrayList<String>();
			BufferedReader reader = null;
			try {
				reader = new BufferedReader(new FileReader(file));
				String line = "";
				while ((line = reader.readLine()) != null)
					lines.add(line);
			} catch (FileNotFoundException e) {
				throw e;
			} catch (IOException e) {
				throw e;
			} finally {
				close(reader);
			}

		}
		return lines;
	}
	
	/**
	 * Load the content of the given URL and return it.
	 * @param url where you want the content back.
	 * @return the content behind the given URL. Empty String is nothing behind it.
	 * @throws IOException - if problem with the url.
	 */
	public String loadContent(URL url) throws IOException{
		InputStreamReader iStream = new InputStreamReader(url.openStream(), "UTF-8");
		String content = loadContent(iStream);
		close(iStream);
		return content;
	}
	
	public String loadContent(InputStreamReader iStream) throws IOException {
		StringBuffer sBuffer = new StringBuffer();
		int read = 0;
		while ((read = iStream.read()) > 0) {
			sBuffer.append((char) read);
		}
		return sBuffer.toString();
	}
	
	

	/**
	 * Load the content of the given Stream and return it.
	 * @param iStream stream to the content
	 * @return the content behind the given stream. Empty String is nothing behind it.
	 * @throws IOException - if problem with the stream.
	 */
	public String loadContent(InputStream iStream) throws IOException{
		StringBuffer sBuffer = new StringBuffer();
        InputStreamReader isr = new InputStreamReader(iStream, "UTF8");
        Reader in = new BufferedReader(isr);
		int read = 0;
		while ((read = in.read()) >= 0) {
			sBuffer.append((char) read);
		}
		return sBuffer.toString();
	}

}

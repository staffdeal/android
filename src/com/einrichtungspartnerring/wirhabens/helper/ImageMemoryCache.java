package com.einrichtungspartnerring.wirhabens.helper;

import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

import android.graphics.Bitmap;
import android.util.Log;

/**
 * A Image Cache for faster loading images.
 * 
 * @author Thomas Möller
 * 
 */
public class ImageMemoryCache {

	/**
	 * Log tag name.
	 */
	private final String TAG = "ImageMemoryCache";
	/**
	 * Size if the cache.
	 */
	private long size;
	/**
	 * Limit of the cache.
	 */
	private long cache_limit;
	/**
	 * The image cache.
	 */
	private Map<String, Bitmap> imageCache = Collections
			.synchronizedMap(Collections
					.synchronizedMap(new LinkedHashMap<String, Bitmap>(10,
							1.5f, true)));

	/**
	 * Default Constructor. Use 1/3 max for this cache
	 */
	protected ImageMemoryCache() {
		setLimit(Runtime.getRuntime().maxMemory() / 3);
	}

	/**
	 * Clear the cache.
	 */
	public void clear() {
		if (imageCache != null) {
			imageCache.clear();
			size = 0;
		}
		Log.d(TAG, "Clean cache");
	}

	/**
	 * Check the size of the cache. If this size > cache limit the first images
	 * will delete until size <= cache limit
	 */
	private void controllSize() {
		if (size > cache_limit) {
			Iterator<Entry<String, Bitmap>> iter = imageCache.entrySet()
					.iterator();
			while (iter.hasNext()) {
				Entry<String, Bitmap> entry = iter.next();
				size -= getSizeInBytes(entry.getValue());
				iter.remove();
				if (size <= cache_limit)
					break;
			}

		}
	}

	/**
	 * Return you the the image under the given key. Or null if no image saved
	 * under this key.
	 * 
	 * @param id
	 *            under the image should saved.
	 * @return the image under the given key, or null.
	 */
	public Bitmap get(String id) {
		if (imageCache.containsKey(id))
			return imageCache.get(id);
		return null;
	}

	/**
	 * Return you the byte of the given image.
	 * 
	 * @param image
	 *            you want the size.
	 * @return size of the given image.
	 */
	private long getSizeInBytes(Bitmap image) {
		if (image == null)
			return 0;
		return image.getRowBytes() * image.getHeight();
	}

	/**
	 * Add a new image.
	 * 
	 * @param id
	 *            you want to save the image.
	 * @param image
	 *            you want to save.
	 */
	public void put(String id, Bitmap image) {
		if (imageCache.containsKey(id))
			size -= getSizeInBytes(imageCache.get(id));
		imageCache.put(id, image);
		size += getSizeInBytes(image);
		controllSize();
	}

	/**
	 * Set a new cache limit.
	 * 
	 * @param cache_limit
	 *            - new cache limit in byte.
	 */
	public void setLimit(long cache_limit) {
		this.cache_limit = cache_limit;
		Log.d(TAG, "ImageMemoryCache use " + this.cache_limit / 1024 / 1024
				+ "MB");
	}

}

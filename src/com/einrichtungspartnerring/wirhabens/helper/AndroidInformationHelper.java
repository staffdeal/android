package com.einrichtungspartnerring.wirhabens.helper;

import android.content.Context;
import android.content.pm.PackageManager.NameNotFoundException;
import android.util.Log;

/**
 * Helper for getting information about the android and APK versions.
 * @author Thomas Möller
 *
 */
public class AndroidInformationHelper {

	private static final String TAG = "AndroidInformationHelper";
	private static String apkVersionName = null;
	private static int apkVersionCode = -1;

	/**
	 * @return you the information about the version of the used device.
	 */
	public static String getAndroidReleaseVersion(){
		return android.os.Build.VERSION.RELEASE;
	}

	/**
	 * 
	 * @param context of the android app.
	 * @return you the name of the version set in the manifest.
	 * @throws NameNotFoundException if you have not set a version name in the manifest.
	 */
	public static String getAPKVersionName(Context context) throws NameNotFoundException{
		if (apkVersionName == null && context != null)
			apkVersionName = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
		return apkVersionName;
	}

	/**
	 * 
	 * @param context of the android app.
	 * @return you the name of the version set in the manifest.
	 * @throws NameNotFoundException if you have not set a version name in the manifest.
	 */
	public static int getAPKVersionCode(Context context) throws NameNotFoundException{
		if (apkVersionCode < 0 && context != null)
			apkVersionCode= context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode;
		return apkVersionCode;
	}

	public static void load(Context context) {
		try {
			getAPKVersionCode(context);
			getAPKVersionName(context);
		} catch (NameNotFoundException e) {
			Log.e(TAG, "Your API doen't work correct");
			e.printStackTrace();
		}
		
		
	}
}

package com.einrichtungspartnerring.wirhabens.helper;

import java.io.File;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

/**
 * Helper for handling image resize and loading.
 * 
 * @author Thomas Möller
 * 
 */
public class ImageHelper {

	private final static ImageMemoryCache imageCache = new ImageMemoryCache();

	/**
	 * Calculated the sample rate, how the image should sampled on loading so it
	 * is not to big.
	 * 
	 * @param options
	 *            - for sampling
	 * @param reqWidth
	 *            - width in pixel for the resized image.
	 * @param reqHeight
	 *            - height in pixel for the resized image.
	 * @return the calculated sampling rate.
	 */
	protected static int calculateInSampleSize(BitmapFactory.Options options,
			int reqWidth, int reqHeight) {
		// Raw height and width of image
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 1;

		if (height > reqHeight || width > reqWidth) {

			// Calculate ratios of height and width to requested height and
			// width
			final int heightRatio = Math.round((float) height
					/ (float) reqHeight);
			final int widthRatio = Math.round((float) width / (float) reqWidth);

			// Choose the smallest ratio as inSampleSize value, this will
			// guarantee
			// a final image with both dimensions larger than or equal to the
			// requested height and width.
			inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
		}

		return inSampleSize;
	}

	/**
	 * Decode and resize an image file by the given parameter.
	 * 
	 * @param image
	 *            file you want load and resize. #It will not check if file
	 *            exist.
	 * @param width
	 *            in pixel for the resized image.
	 * @param height
	 *            in pixel for the resized image.
	 * @return the loaded and resized image.
	 */
	public static Bitmap decodeAndScaleFile(File imageFile, int width,
			int height) {


		return decodeAndScaleFile(imageFile, width, height, true);
	}

    /**
     * Decode and resize an image file by the given parameter.
     *
     * @param image
     *            file you want load and resize. #It will not check if file
     *            exist.
     * @param width
     *            in pixel for the resized image.
     * @param height
     *            in pixel for the resized image.
     * @return the loaded and resized image.
     */
    public static Bitmap decodeAndScaleFile(File imageFile, int width, int height, boolean useCache) {

        String imageKey = imageMemoryCacheKeyGen(imageFile.getName(), width,
                height);
        Bitmap decodedImage = null;
        if (useCache)
            decodedImage = imageCache.get(imageKey);
        if (decodedImage == null) {
            BitmapFactory.Options bpo = new BitmapFactory.Options();
            bpo.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(imageFile.getAbsolutePath(), bpo);

            bpo.inSampleSize = calculateInSampleSize(bpo, width, height);
            bpo.inJustDecodeBounds = false;
            decodedImage = BitmapFactory.decodeFile(
                    imageFile.getAbsolutePath(), bpo);

            imageCache.put(imageKey, decodedImage);
        }
        return decodedImage;
    }

	private static String imageMemoryCacheKeyGen(String filename, int width,
			int height) {
		return filename + "|" + width + "|" + "|" + height + "|";
	}
}

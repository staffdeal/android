package com.einrichtungspartnerring.wirhabens.fragments;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Locale;

import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Paint;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.einrichtungspartnerring.wirhabens.DealActivity;
import com.einrichtungspartnerring.wirhabens.MainActivity;
import com.einrichtungspartnerring.wirhabens.R;
import com.einrichtungspartnerring.wirhabens.activityhelper.PicTextXMLDownloaderAT;
import com.einrichtungspartnerring.wirhabens.gui.adapter.PicTextAdapter;
import com.einrichtungspartnerring.wirhabens.gui.adapter.PicTextAdapter.ViewHolder;
import com.einrichtungspartnerring.wirhabens.gui.listener.ThirdTabOnItemClickListener;
import com.einrichtungspartnerring.wirhabens.gui.listener.ad.GoToDealButton;
import com.einrichtungspartnerring.wirhabens.helper.ImageHelper;
import com.einrichtungspartnerring.wirhabens.imagedownloader.ImageDownloader;
import com.einrichtungspartnerring.wirhabens.manager.DealsManager;
import com.einrichtungspartnerring.wirhabens.manager.ImageManager;
import com.einrichtungspartnerring.wirhabens.manager.ManagerFactory;
import com.einrichtungspartnerring.wirhabens.vo.DealsItemVO;

import eu.erikw.PullToRefreshListView;
import eu.erikw.PullToRefreshListView.OnRefreshListener;

public class dealsFragment extends StaffdealListFragment {

	private MainActivity mActivity;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View rootView = super.onCreateView(inflater, container, savedInstanceState);
		mFragmentList = (PullToRefreshListView) rootView.findViewById(R.id.main_tab_list);
		mFragmentList.setOnRefreshListener(new OnRefreshListener() {

		    @Override
		    public void onRefresh() {
		    	reloadData();
		    	mFragmentList.onRefreshComplete();
		    }
		});
		ThirdTabOnItemClickListener thirdListener = new ThirdTabOnItemClickListener(getActivity());
		mFragmentList.setOnItemClickListener(thirdListener);
		
		setAdapter();
		
		if(getActivity() != null && ((MainActivity)getActivity()).checkIfUpdateNeeded(getActivity().getResources().getInteger(R.integer.deals_tab_position))) {
			reloadData();
			((MainActivity)getActivity()).setUpdateTimeOfPosition((getActivity().getResources().getInteger((R.integer.deals_tab_position))));
		}
		
		logedIn();
		
		return rootView;
	}
	
	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		mActivity = (MainActivity)activity;
	}
	
	
	@Override
	public void reloadData() {
		if (mFragmentList != null) {
				super.reloadData();
			
				Log.d("RELOAD","Deals reload OK");
				
				DealsManager dealsManager = ManagerFactory.buildDealsManager(getActivity());

				new PicTextXMLDownloaderAT(mBaseAdapter,dealsManager,getActivity(), iDown).execute();
			
				//checkAdDeals(dealsManager);
		}
	}
	

	@Override 
	public void setAdapter() {
		// TODO Auto-generated method stub
		super.setAdapter();
		
		if (mFragmentList != null) {
			DealsManager dealsManager = ManagerFactory
					.buildDealsManager(getActivity());
			mBaseAdapter = new PicTextAdapter(getActivity(),dealsManager.loadOfflinePicAdapterItem(), iDown);
			mFragmentList.setAdapter(mBaseAdapter);
		}
	}
	
	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if(mBaseAdapter != null) {
			mBaseAdapter.notifyDataSetChanged();
		}
	}
	
	public void checkAdDeals() {
		
		DealsManager dealsManager = ManagerFactory.buildDealsManager(getActivity());
		Log.d("PENDIN"," DEAL 0");
		if (dealsManager.hasNexAdDeal()) {
			mAdView.setVisibility(View.VISIBLE);
			
			DealsItemVO vo = dealsManager.getNextAdDeal();

			TextView date = (TextView) mAdView.findViewById(R.id.ad_date);
            TextView title = (TextView) mAdView.findViewById(R.id.ad_title);
            TextView enterprise = (TextView) mAdView.findViewById(R.id.ad_enterprise);

            ImageView iView = (ImageView) mAdView.findViewById(R.id.ad_image);
            
            new ImageSetter(iView,vo).execute();            

            SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy", Locale.GERMANY);
            date.setText(getString(R.string.deal_date_head_text) + " "+ df.format(vo.getEndDate()));    //todo

            enterprise.setText(vo.getProvider());

            title.setText(vo.getTitle());

            Log.d("PENDIN"," DEAL 27");
            TextView teaser = (TextView) mAdView.findViewById(R.id.ad_teaser_text);
            teaser.setText(vo.getTeaser());

            TextView price =  (TextView) mAdView.findViewById(R.id.ad_price);
            if (vo.getPrice() != null && !vo.getPrice().isEmpty()){
                price.setVisibility(View.VISIBLE);
                price.setText(vo.getPrice());
            } else{
                price.setVisibility(View.GONE);
            }

            TextView oldPrice =  (TextView) mAdView.findViewById(R.id.ad_old_price);
            if (vo.getOldPrice() != null && !vo.getOldPrice().isEmpty()){
                oldPrice.setVisibility(View.VISIBLE);
                oldPrice.setText(vo.getOldPrice());
                oldPrice.setPaintFlags(oldPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            } else{
                oldPrice.setVisibility(View.GONE);
            }

            Log.d("PENDIN"," DEAL 37");
            Button exitButton = (Button) mAdView.findViewById(R.id.ad_exit_button) ;
            exitButton.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					mAdView.setVisibility(View.GONE);
				}
			}); 
            
            Log.d("PENDIN"," DEAL 47");
            Button goToDealButton = (Button) mAdView.findViewById(R.id.ad_go_to_deal_button) ;
            goToDealButton.setOnClickListener(new GoToDealButton(mAdView,getActivity(), vo, dealsManager));	
		
		} else if (dealsManager.hasNextReminderDeal()) {
			Log.d("PENDIN","PENDING DEAL");
			DealsItemVO vo = dealsManager.getNextReminderDeal();
			Intent intent = new Intent(getActivity(), DealActivity.class);
			intent.putExtra("item", dealsManager.getPositionByItem(vo));
			PendingIntent pIntent = PendingIntent.getActivity(getActivity(), 0, intent,PendingIntent.FLAG_UPDATE_CURRENT);

			NotificationManager notificationManager = (NotificationManager) getActivity()
					.getSystemService(getActivity().NOTIFICATION_SERVICE);
			NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(
					getActivity())
					.setContentTitle("Ein Deal läuft bald aus!")
					.setContentText(
							"Bitte klicken Sie hier, wenn Sie den Deal sehen wollen")
					.setSmallIcon(R.drawable.erp_icon).setAutoCancel(true)
					.setContentIntent(pIntent);

			notificationManager.notify(12344123,
					notificationBuilder.build());
		}	
	}
	
	public class ImageSetter extends AsyncTask<Void, Void, File> {

		private ImageView imageView;
		private DealsItemVO vo;
		
		public ImageSetter(ImageView iv,DealsItemVO vo) {
			imageView = iv;
			this.vo = vo;
		}
		
		@Override
		protected File doInBackground(Void... params) {
			
            ImageManager iManager = ManagerFactory.buildImageManager(getActivity());
            File imageFile = iManager.getIfExist(vo.getAdapterImagePath());         

			return imageFile;				
		}
		
	    @Override
	    protected void onPostExecute(File file) {	    	
            if (file != null && file.exists()) {
                Bitmap imageBitmap = ImageHelper.decodeAndScaleFile(file, 100, 100);
                imageView.setImageBitmap(imageBitmap);
            } else {
            	ImageManager iManager = ManagerFactory.buildImageManager(getActivity());
                ImageDownloader iDown = new ImageDownloader(getActivity(), iManager);
                if (iDown != null && vo.getAdapterImagePath() != null && !vo.getAdapterImagePath().isEmpty()) {
                	ViewHolder viewHolder = new ViewHolder();
                	viewHolder.image = imageView;
                	viewHolder.id = vo.getId();
                    iDown.add(viewHolder, vo.getId(), vo.getAdapterImagePath());
                }
            }
	    }
	    
	}

}

package com.einrichtungspartnerring.wirhabens.fragments;

import com.einrichtungspartnerring.wirhabens.MainActivity;
import com.einrichtungspartnerring.wirhabens.R;
import com.einrichtungspartnerring.wirhabens.activityhelper.PicTextXMLDownloaderAT;
import com.einrichtungspartnerring.wirhabens.gui.adapter.PicTextAdapter;
import com.einrichtungspartnerring.wirhabens.gui.listener.SecoundTabOnItemClickListener;
import com.einrichtungspartnerring.wirhabens.manager.CompetitionIdeasManager;
import com.einrichtungspartnerring.wirhabens.manager.ManagerFactory;

import eu.erikw.PullToRefreshListView;
import eu.erikw.PullToRefreshListView.OnRefreshListener;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class ideasFragment extends StaffdealListFragment {
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		
		
		View rootView = super.onCreateView(inflater, container, savedInstanceState);
		mFragmentList = (PullToRefreshListView) rootView.findViewById(R.id.main_tab_list);
		mFragmentList.setOnRefreshListener(new OnRefreshListener() {

		    @Override
		    public void onRefresh() {
		    	reloadData();
		    	mFragmentList.onRefreshComplete();
		    }
		});
		mFragmentList.setOnItemClickListener(new SecoundTabOnItemClickListener(getActivity()));
		
		setAdapter();
		
		if(getActivity() != null && ((MainActivity)getActivity()).checkIfUpdateNeeded(getActivity().getResources().getInteger(R.integer.ideas_tab_position))) {
			reloadData();
			((MainActivity)getActivity()).setUpdateTimeOfPosition((getActivity().getResources().getInteger((R.integer.ideas_tab_position))));
		}
		
		logedIn();
		
		return rootView;
	}
	
	@Override
	public void reloadData() {
		if (mFragmentList != null) {
			super.reloadData();
			
			Log.d("RELOAD","Ideas reload OK");
			
			CompetitionIdeasManager comManager = ManagerFactory.buildCompetitionIdeasManager(getActivity());
			new PicTextXMLDownloaderAT(mBaseAdapter,comManager,getActivity(), iDown).execute();
		}
	}
	
	@Override
	public void setAdapter() {
		// TODO Auto-generated method stub
		super.setAdapter();
		
		if (mFragmentList != null) {
			CompetitionIdeasManager comManager = ManagerFactory
					.buildCompetitionIdeasManager(getActivity());
			mBaseAdapter = new PicTextAdapter(getActivity(),comManager.loadOfflinePicAdapterItem(), iDown);
			mFragmentList.setAdapter(mBaseAdapter);
		}
	}
	
	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if(mBaseAdapter != null) {
			mBaseAdapter.notifyDataSetChanged();
		}
	}
}

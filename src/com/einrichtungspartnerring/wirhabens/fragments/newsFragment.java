package com.einrichtungspartnerring.wirhabens.fragments;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.einrichtungspartnerring.wirhabens.MainActivity;
import com.einrichtungspartnerring.wirhabens.R;
import com.einrichtungspartnerring.wirhabens.activityhelper.PicTextXMLDownloaderAT;
import com.einrichtungspartnerring.wirhabens.gui.adapter.EndlessScrollListener;
import com.einrichtungspartnerring.wirhabens.gui.adapter.PicTextAdapter;
import com.einrichtungspartnerring.wirhabens.gui.listener.MeinERPOnItemClickListener;
import com.einrichtungspartnerring.wirhabens.manager.ManagerFactory;
import com.einrichtungspartnerring.wirhabens.manager.NewsManager;

import eu.erikw.PullToRefreshListView;
import eu.erikw.PullToRefreshListView.OnRefreshListener;

public class newsFragment extends StaffdealListFragment {
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {

		View rootView = super.onCreateView(inflater, container, savedInstanceState);
		
		mFragmentList = (PullToRefreshListView) rootView.findViewById(R.id.main_tab_list);
		mFragmentList.setOnRefreshListener(new OnRefreshListener() {

		    @Override
		    public void onRefresh() {
		    	reloadData();
		    	mFragmentList.onRefreshComplete();
		    }
		});
		mFragmentList.setOnItemClickListener(new MeinERPOnItemClickListener(getActivity()));
		
		setAdapter();
		
		if(getActivity() != null && ((MainActivity)getActivity()).checkIfUpdateNeeded(getActivity().getResources().getInteger(R.integer.news_tab_position))) {
			reloadData();
			((MainActivity)getActivity()).setUpdateTimeOfPosition((getActivity().getResources().getInteger((R.integer.news_tab_position))));
		}
		
		logedIn();
		
		return rootView;
	}
	
	@Override 
	public void reloadData() {
		if (mFragmentList != null) {
			super.reloadData();		
			NewsManager newsManager = ManagerFactory.buildNewsManager(getActivity());
			new PicTextXMLDownloaderAT(mBaseAdapter, newsManager, getActivity(), iDown).execute();
		}
	}

	@Override
	public void setAdapter() {		
		if (mFragmentList != null) {
			NewsManager newsManager = ManagerFactory.buildNewsManager(getActivity()); 
			mBaseAdapter = new PicTextAdapter(getActivity(),newsManager.loadOfflinePicAdapterItem(), iDown);;
			mFragmentList.setAdapter(mBaseAdapter);
			mFragmentList.setOnScrollListener(new EndlessScrollListener(getActivity(), mBaseAdapter, newsManager));
		}		
	}
	
	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if(mBaseAdapter != null) {
			mBaseAdapter.notifyDataSetChanged();
		}
	}
}

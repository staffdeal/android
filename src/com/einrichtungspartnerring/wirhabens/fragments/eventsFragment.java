package com.einrichtungspartnerring.wirhabens.fragments;

import com.einrichtungspartnerring.wirhabens.MainActivity;
import com.einrichtungspartnerring.wirhabens.R;
import com.einrichtungspartnerring.wirhabens.activityhelper.PicTextXMLDownloaderAT;
import com.einrichtungspartnerring.wirhabens.gui.adapter.PicTextAdapter;
import com.einrichtungspartnerring.wirhabens.gui.listener.EventTabOnItemClickListener;
import com.einrichtungspartnerring.wirhabens.manager.EventsManager;
import com.einrichtungspartnerring.wirhabens.manager.ManagerFactory;

import eu.erikw.PullToRefreshListView;
import eu.erikw.PullToRefreshListView.OnRefreshListener;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class eventsFragment extends StaffdealListFragment {
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View rootView = super.onCreateView(inflater, container, savedInstanceState);
		mFragmentList = (PullToRefreshListView) rootView.findViewById(R.id.main_tab_list);
		mFragmentList.setOnRefreshListener(new OnRefreshListener() {

		    @Override
		    public void onRefresh() {
		    	reloadData();
		    	mFragmentList.onRefreshComplete();
		    }
		});
		EventTabOnItemClickListener eventsListener = new EventTabOnItemClickListener(getActivity());
		mFragmentList.setOnItemClickListener(eventsListener);

		setAdapter();
		
		if(getActivity() != null && ((MainActivity)getActivity()).checkIfUpdateNeeded(getActivity().getResources().getInteger(R.integer.events_tab_position))) {
			reloadData();
			((MainActivity)getActivity()).setUpdateTimeOfPosition((getActivity().getResources().getInteger((R.integer.events_tab_position))));
		}
		
		logedIn();
				
		return rootView;
	} 
	
	@Override
	public void logedIn() {
		// TODO Auto-generated method stub
		super.logedIn();
		if(!((MainActivity)getActivity()).hasRights(3)){
			mFragmentList.setVisibility(View.GONE);
		} else {
			mFragmentList.setVisibility(View.VISIBLE);
		}
		
		Log.d("LOG","NOTIFY LOGIN");
	}
	
	@Override
	public void reloadData() {
		// TODO Auto-generated method stub
		if(mFragmentList != null) {
			
			super.reloadData();
			
			EventsManager eventsManager = ManagerFactory.buildEventsManager(getActivity());
			new PicTextXMLDownloaderAT(mBaseAdapter, eventsManager,getActivity(), iDown).execute();
		}
		

	} 

	@Override
	public void setAdapter() {
		// TODO Auto-generated method stub
		super.setAdapter();
		
		if (mFragmentList != null) {
			EventsManager eventsManager = ManagerFactory
					.buildEventsManager(getActivity());
			mBaseAdapter = new PicTextAdapter(getActivity(),eventsManager.loadOfflinePicAdapterItem(), iDown);
			mFragmentList.setAdapter(mBaseAdapter);
		}
	} 
	
	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if(mBaseAdapter != null) {
			mBaseAdapter.notifyDataSetChanged();
		}
	}
}

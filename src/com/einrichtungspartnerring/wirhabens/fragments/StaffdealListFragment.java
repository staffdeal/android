package com.einrichtungspartnerring.wirhabens.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.einrichtungspartnerring.wirhabens.MainActivity;
import com.einrichtungspartnerring.wirhabens.R;
import com.einrichtungspartnerring.wirhabens.gui.adapter.PicTextAdapter;
import com.einrichtungspartnerring.wirhabens.imagedownloader.ImageDownloader;

import eu.erikw.PullToRefreshListView;



public class StaffdealListFragment extends Fragment {
	public static final String ARG_OBJECT = "object";
	protected ImageDownloader iDown;
	protected PullToRefreshListView mFragmentList;
	protected PicTextAdapter mBaseAdapter;
	protected View mAdView;

	
    @Override
    public View onCreateView(LayoutInflater inflater,ViewGroup container, Bundle savedInstanceState) {
        // The last two arguments ensure LayoutParams are inflated
        // properly.
    	final MainActivity myActivity;
    	myActivity = (MainActivity)getActivity();
    	
        View rootView = inflater.inflate(R.layout.fragment_staffdeal_list_fragment, container, false);

        mAdView = rootView.findViewById(R.id.addeal_layout);
        mAdView.setVisibility(View.GONE);
        Button loginButton = (Button) rootView.findViewById(R.id.login_button);
        loginButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				myActivity.showLoginDiaglog();
			}
		});
       
        return rootView;
    }
    
	public void setIDown(ImageDownloader iDown) {
		this.iDown = iDown;
	}
	
	public void reloadData() {
		if (getActivity() != null) {
			int duration = Toast.LENGTH_SHORT;
			Toast toast = Toast.makeText(getActivity(),
					R.string.toast_reloading, duration);
			toast.show();
		}
	}
	
	public void logedIn(){
		
	}
	
	public void setAdapter() {
		
	}
	
}

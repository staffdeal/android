package com.einrichtungspartnerring.wirhabens;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

public class PreviousMainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_previous_main);
		int currentapiVersion = android.os.Build.VERSION.SDK_INT;
		if (currentapiVersion > android.os.Build.VERSION_CODES.GINGERBREAD_MR1){
		    // Continue
			Intent intent = new Intent(this, MainActivity.class);
			finish();
			startActivity(intent);
		} else{
		    // do something for phones running an SDK before gingerbread
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setMessage(R.string.android_old_version)
			.setPositiveButton("Ok",
					new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,
						int id) {
					finish();
				}
			}).setTitle("Error");
			// Create the AlertDialog object and return it
			builder.create().show();
		}
	}
}

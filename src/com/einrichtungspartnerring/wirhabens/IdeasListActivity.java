package com.einrichtungspartnerring.wirhabens;

import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.widget.ListView;

import com.einrichtungspartnerring.wirhabens.activityhelper.IdeasItemATDownloader;
import com.einrichtungspartnerring.wirhabens.gui.adapter.PicAdapterItem;
import com.einrichtungspartnerring.wirhabens.gui.adapter.PicTextAdapter;
import com.einrichtungspartnerring.wirhabens.gui.listener.IdeasListOnItemClickListener;
import com.einrichtungspartnerring.wirhabens.helper.AirbrakeHelper;
import com.einrichtungspartnerring.wirhabens.imagedownloader.ImageDownloader;
import com.einrichtungspartnerring.wirhabens.manager.CompetitionIdeasManager;
import com.einrichtungspartnerring.wirhabens.manager.ImageManager;
import com.einrichtungspartnerring.wirhabens.manager.ManagerFactory;
import com.einrichtungspartnerring.wirhabens.vo.CompetitionVO;

/**
 * List view for all ideas.
 * 
 * @author Thomas Möller
 * 
 */
public class IdeasListActivity extends Activity {

	/**
	 * Bundle id.
	 */
	public final static String IDEAS_LIST_COMPETITION_ID = "id";

	private CompetitionVO comVO;
	private CompetitionIdeasManager comManager;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		try{
			if (!MainActivity.wasStarted){
				Intent i = new Intent(getBaseContext(), MainActivity.class);
				i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(i);
			}
			if (android.os.Build.VERSION.SDK_INT <= Build.VERSION_CODES.GINGERBREAD_MR1){
				MainActivity.customActionBar();
			}
			else{
				MainActivity.customActionBar(getActionBar(), this);
			}
			setContentView(R.layout.activity_ideas_list);

			Bundle extras = getIntent().getExtras();
            if (extras == null)
                finish();
			String competitionId = extras.getString(IDEAS_LIST_COMPETITION_ID);

			CompetitionIdeasManager comManager = ManagerFactory.buildCompetitionIdeasManager(this);
			CompetitionVO comVO = comManager.getItemById(competitionId);
			this.comVO = comVO; 
			this.comManager = comManager;
		}catch(Exception e){
			String eMessage = "Unknown Exception";
			AirbrakeHelper.sendException(e, eMessage);
		}

	}

	@Override
	protected void onStart() {
		super.onStart();
		try{
            if (comVO != null) {
                List<PicAdapterItem> ideas = comVO.getIdeasAtPicAdapterItem();

                ImageManager iManager = ManagerFactory.buildImageManager(this);
                ImageDownloader iDown = new ImageDownloader(this, iManager);

                PicTextAdapter adapter = new PicTextAdapter(this, ideas, iDown);

                ListView listview = (ListView) findViewById(R.id.ideas_list_listview);
                listview.setAdapter(adapter);
                listview.setOnItemClickListener(new IdeasListOnItemClickListener(this, comVO));

                new IdeasItemATDownloader(adapter, comManager, this, iDown).execute(comVO.getId());
            }
		}catch(Exception e){
			String eMessage = "Unknown Exception";
			AirbrakeHelper.sendException(e, eMessage);
		}
	}
}

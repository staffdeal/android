package com.einrichtungspartnerring.wirhabens.properties;

/**
 * String with all system property names.
 * 
 * @author Thomas Möller
 * 
 */
public interface PropertiesList {

	/**
	 * Last tab you have opened.
	 */
	String LAST_TAB_PROP = "lasttab";
	/**
	 * URLs
	 */
	String API_BASE_TEST_URL = "testbase.url";
	String API_BASE_URL = "base.url";
	String API_INFO_URL = "info.url";
	String API_NEWS_URL = "news.url";
    String API_NEWS_READ_URL = "news.read.url";
	String API_DEALS_URL = "deals.url";
    String API_DEALS_SYNC_URL = "deals_sync.url";
	String API_IDEAS_SUBMIT_URL = "ideas.submit.url";
	String API_IDEAS_COMPETITION_URL = "ideas.competitions.url";
	String API_IDEAS_LIST_URL = "ideas.list.url";
	String API_EVENTS_LIST_URL = "events.list.url";
	String API_REGISTRY_URL = "registry.url";
	String API_AUTHENTICAT_URL = "authenticat.url";
	String API_DEALER_URL = "dealer.url";
	String API_NOTIFICATION_DEVICE = "notification_device.url";
	String API_NOTIFICATION_UNREGIST_DEVICE = "notification_unregist_device.url";
	String API_CHANGE_PASSWORD = "password_change.url";
	String API_RESET_PASSWORD = "password_reset.url";

	/**
	 * Rights for access
	 */
	String NEWS_RIGHTS = "news.right";
	String IDEAS_RIGHTS = "ideas.right";
	String DEALS_RIGHTS = "deals.right";
	String EVENTS_RIGHTS = "events.right";

	/**
	 * User Informations
	 */
	String USER_E_MAIL = "user.email";
	String USER_PASS = "user.pass";
	String USER_TOKEN = "user.token";
	String USER_RIGHT = "user.right";

	/**
	 * Client
	 */
	String CLIENT_KEY = "client.key";

	/**
	 * Google Cloud Messaging
	 */
	String GCM_PROJECT_KEY = "gcm.projectkey"; // SENDER ID also named.
	String GMC_ACTIVE = "gcm.active";

	String LAST_UPDATE_TIME = "client.lastupdate";
	
	/**
	 * Airbrake API
	 */
	String AIRBRAKE_API_KEY = "airbrake.apikey";
	String AIRBRAKE_URL = "airbrake.url";
	
	/**
	 * API Infos
	 */
	String API_YOUTUBE_MEDIA_URL = "api.youtube.media.url";
    String API_FACEBOOK_MEDIA_URL = "api.facebook.media.url";

	String API_PRIVACY_POLICE_TEXT = "api.privacypolicy.text";
	String API_TERM_OF_USING_TEXT = "api.termOfUsing.text";
	String API_ABOUT_TEXT = "api.about.text";
	String API_IMPRESS_TEXT = "api.impress.text";
}

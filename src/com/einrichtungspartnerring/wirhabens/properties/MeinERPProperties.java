package com.einrichtungspartnerring.wirhabens.properties;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;

import android.content.Context;

public class MeinERPProperties {

	private Properties systemproperties;

	protected MeinERPProperties(InputStream iStream) throws IOException {
		systemproperties = new Properties();
		systemproperties.load(iStream);
	}

	public synchronized String get(String key) {
		return systemproperties.getProperty(key);
	}

	/**
	 * Remove the value behind given key. Return you the value.
	 * 
	 * @param key
	 *            of the object you want delete.
	 * @return the value behind the key.
	 */
	public String remove(String key) {
		return (String) systemproperties.remove(key);
	}

	public synchronized void set(String key, String value) {
		this.systemproperties.setProperty(key, value);
	}

	public synchronized void store(Context context) throws IOException {
		OutputStream oStream = context.openFileOutput(
				MeinERPPropertiesFactory.SAVE_LOCATION_PROPERTIES,
				Context.MODE_PRIVATE);
		this.systemproperties.store(oStream, "Test");
		oStream.close();
	}

}

package com.einrichtungspartnerring.wirhabens.properties;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.util.Log;

/**
 * Factory for the system intern properties. The only instance which can build
 * it.
 * 
 * @author Thomas Möller
 * 
 */
public class MeinERPPropertiesFactory {

	/**
	 * Save and load location of the properties.
	 */
	protected static final String SAVE_LOCATION_PROPERTIES = "current.properties";

	/**
	 * System properties of meinERP
	 */
	private static MeinERPProperties systemproperties;

	/**
	 * /** Return you the only instance of the system properties. If not exist
	 * currently it will builded.
	 * 
	 * @param context
	 *            - context of the application, only need and also maybe on
	 *            first time.
	 * @return the instance of the system properties
	 */
	public synchronized static MeinERPProperties getProperties(Context context) {
		if (systemproperties == null && context != null) {
			//--SAVE Data
			SharedPreferences preferences = context.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE);
			boolean updatedPropertyFile = preferences.getBoolean("PropertiesUpdated_1_3_2", false);

			boolean loaddefault = false;
			File file = context.getFileStreamPath(SAVE_LOCATION_PROPERTIES);
			InputStream iStream = null;
			if (file.exists() && updatedPropertyFile) {
				try {
					iStream = context.openFileInput(SAVE_LOCATION_PROPERTIES);
					systemproperties = new MeinERPProperties(iStream);
				} catch (FileNotFoundException e) {
					// should never reached. Is in the if impossible.
					e.printStackTrace();
				} catch (IOException e) {
					Log.e("MeinERPPropertiesFactory",
							"Problems by reading current. properties");
					loaddefault = true;
				} finally {
					try {
						iStream.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			} else {
				loaddefault = true;
			}
			// Load default properites
			if (loaddefault) {
				
				SharedPreferences.Editor editor = preferences.edit();
				editor.putBoolean("PropertiesUpdated_1_3_2", true);
				editor.commit();
				
				Resources resources = context.getResources();
				AssetManager assetManager = resources.getAssets();
				try {
					iStream = assetManager.open("default.properties");
					systemproperties = new MeinERPProperties(iStream);
				} catch (IOException e) {
					Log.e("MeinERPPropertiesFactoy",
							"Couldn't load the default properties this app is in trouble rigth now",
							e);
					throw new RuntimeException(
							"You need to load the default Properties, but I can't");
				} finally {
					try {
						if (iStream != null)
							iStream.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}

			}
		}
		return systemproperties;
	}
}

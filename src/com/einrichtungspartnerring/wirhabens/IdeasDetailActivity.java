package com.einrichtungspartnerring.wirhabens;

import java.text.SimpleDateFormat;
import java.util.Locale;

import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import com.einrichtungspartnerring.wirhabens.helper.AirbrakeHelper;
import com.einrichtungspartnerring.wirhabens.imagedownloader.ImageDownloader;
import com.einrichtungspartnerring.wirhabens.manager.CompetitionIdeasManager;
import com.einrichtungspartnerring.wirhabens.manager.ManagerFactory;
import com.einrichtungspartnerring.wirhabens.vo.IdeaVO;
import com.einrichtungspartnerring.wirhabens.vo.ImageVO;

public class IdeasDetailActivity extends Activity {

	public static final String EXTRAS_IDEAS_ID = "ideasId";
	public final static String EXTRAS_COMPETITION_ID = "competitionId";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		try{
			if (android.os.Build.VERSION.SDK_INT <= Build.VERSION_CODES.GINGERBREAD_MR1){
				MainActivity.customActionBar();
			}
			else{
				MainActivity.customActionBar(getActionBar(), this);
			}
			setContentView(R.layout.activity_ideas_detail);

			Bundle extras = getIntent().getExtras();
            if (extras == null)
                finish();
			String ideasId = extras.getString(EXTRAS_IDEAS_ID);
			String competitionId = extras.getString(EXTRAS_COMPETITION_ID);
			
			CompetitionIdeasManager comManager =  ManagerFactory.buildCompetitionIdeasManager(this);
			IdeaVO vo = comManager.getIdeasById(competitionId, ideasId, true);
			
        	String templateString = ((StaffsaleApplication) getApplication()).getHtmlTemplateForFile("competitionIdeaDetails.html");        	
            
        	templateString = templateString.replace("##META##","<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, maximum-scale=1.0\">");
        	templateString = templateString.replace("##AUTHOR##", vo.getUserFirstName() + vo.getUserLastName());
        	templateString = templateString.replace("##TITLE##", getString(R.string.ideas_detail_idea_headline));                	

            SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy", Locale.GERMANY);
            templateString = templateString.replace("##SUBMITTED##",df.format(vo.getSubmissionDate()));
            
                       
            String content = vo.getText();
			if (vo.getUrl() != null && !vo.getUrl().isEmpty()){
				content = content + "<h2>"+ getString(R.string.ideas_detail_idea_url_title)  +"</h2><a href=" + vo.getUrl() + ">" + vo.getUrl() + "</a>";
			}
			templateString = templateString.replace("##CONTENT##", content);  
			
			content = "";
			String imagesHeadStr = "";
			if (vo.getImages() != null && vo.getImages().size() > 0){
				
				for(ImageVO image : vo.getImages()) {
					content = content + "<img src=" + image.getPfad()  +" />";
				}
				
				imagesHeadStr =  getString(R.string.ideas_detail_idea_image_title);
			}
			templateString = templateString.replace("##CONTENTIMAGES##", content);			
			templateString = templateString.replace("##IMAGESTEXT##",imagesHeadStr);
            
            WebView text = (WebView) findViewById(R.id.ideas_details_content);
            WebSettings settings = text.getSettings();
            settings.setDefaultFontSize(20);
            settings.setDefaultTextEncodingName("utf-8");
            text.loadData(templateString, "text/html; charset=utf-8", "UTF-8");

			((StaffsaleApplication) getApplication()).trackEvent(getString(R.string.ga_category_idea),getString(R.string.ga_action_read),vo.getId());	
			
		}catch(Exception e){
			String eMessage = "Unknown Exception";
			AirbrakeHelper.sendException(e, eMessage);
		}
	}

	@Override
	protected void onStart() {
		super.onStart();
	}
}

package com.einrichtungspartnerring.wirhabens;

import android.annotation.TargetApi;
import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Switch;
import android.widget.TextView;

import com.einrichtungspartnerring.wirhabens.activityhelper.events.OnTaskLoginCompleteEvent;
import com.einrichtungspartnerring.wirhabens.gui.listener.settings.SettingGCMNotificationButton;
import com.einrichtungspartnerring.wirhabens.gui.listener.settings.SettingNewPasswordButtonListener;
import com.einrichtungspartnerring.wirhabens.gui.listener.settings.SettingsLogOffButtonListener;
import com.einrichtungspartnerring.wirhabens.gui.listener.settings.SettingsLoginButtonListener;
import com.einrichtungspartnerring.wirhabens.gui.listener.settings.SettingsResetButtonListener;
import com.einrichtungspartnerring.wirhabens.helper.AirbrakeHelper;
import com.einrichtungspartnerring.wirhabens.manager.ManagerFactory;
import com.einrichtungspartnerring.wirhabens.manager.NetworkManager;
import com.einrichtungspartnerring.wirhabens.properties.MeinERPProperties;
import com.einrichtungspartnerring.wirhabens.properties.MeinERPPropertiesFactory;
import com.einrichtungspartnerring.wirhabens.properties.PropertiesList;

/**
 * Activity for settings
 * @author Thomas Möller
 *
 */
public class SettingsActivity extends Activity implements OnTaskLoginCompleteEvent{

	private NetworkManager netManager;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		try{
			setContentView(R.layout.activity_settings);
			if (android.os.Build.VERSION.SDK_INT <= Build.VERSION_CODES.GINGERBREAD_MR1){
				MainActivity.customActionBar();
			}
			else{
				
				MainActivity.customActionBar(getActionBar(), this);
			}

			MeinERPProperties prop = MeinERPPropertiesFactory.getProperties(this);
			netManager = ManagerFactory.buildNetworkManager(this);
			boolean gcmActive = prop.get(PropertiesList.GMC_ACTIVE) != null ? true: false;
			setGCMButtonListener(new SettingGCMNotificationButton(prop, netManager,this), gcmActive);

			Button settingResetAllButtom = (Button) findViewById(R.id.settings_button_reset_all);
			settingResetAllButtom.setOnClickListener(new SettingsResetButtonListener(netManager, prop, this));

			Button login = (Button) findViewById(R.id.settings_button_login);
			login.setOnClickListener(new SettingsLoginButtonListener(this, netManager));
			Button logoff = (Button) findViewById(R.id.settings_button_logoff);
			logoff.setOnClickListener(new SettingsLogOffButtonListener(netManager, prop, this));
			Button settingNewPasswordButton = (Button) findViewById(R.id.settings_button_renew_password);
			settingNewPasswordButton.setOnClickListener(new SettingNewPasswordButtonListener(this, netManager));
			setButtons();
		}catch(Exception e){ 
			String eMessage = "Unknown Exception";
			AirbrakeHelper.sendException(e, eMessage);
		}

	}
	/**
	 * Set the Buttons after status of authentication.
	 */
	protected void setButtons(){
		Button login = (Button) findViewById(R.id.settings_button_login);
		Button logoff = (Button) findViewById(R.id.settings_button_logoff);
		Button settingNewPasswordButton = (Button) findViewById(R.id.settings_button_renew_password);
		TextView loginstatusView = (TextView) findViewById(R.id.settings_status_view);
		if (netManager.isAuthenticated()){
			login.setVisibility(View.GONE);
			logoff.setVisibility(View.VISIBLE);
			settingNewPasswordButton.setVisibility(View.VISIBLE);
			loginstatusView.setText(getString(R.string.settings_status_login));
		}else{
			login.setVisibility(View.VISIBLE);
			logoff.setVisibility(View.GONE);
			settingNewPasswordButton.setVisibility(View.GONE);
			loginstatusView.setText(getString(R.string.settings_status_logoff));
		}
	}
	/**
	 * Handling the setting for a listener on the GCM button.
	 * 
	 * @param listener
	 * @param checked
	 */
	private void setGCMButtonListener(SettingGCMNotificationButton listener,
			boolean checked) {
		if (android.os.Build.VERSION.SDK_INT <= Build.VERSION_CODES.HONEYCOMB_MR2)
			setGCMButtonListenerOnCheckBox(listener, checked);
		else
			setGCMButtonListenerOnSwitch(listener, checked);
	}

	/**
	 * Case you used a Android older then Ice Cream, so the switch will replace
	 * with a check box.
	 * 
	 * @param listener
	 * @param checked
	 */
	private void setGCMButtonListenerOnCheckBox(
			SettingGCMNotificationButton listener, boolean checked) {
		CheckBox checkBox = (CheckBox) findViewById(R.id.settings_gcm_checkbox);
		checkBox.setOnCheckedChangeListener(listener);
		checkBox.setChecked(checked);
	}

	/**
	 * @param listener
	 * @param checked
	 */
	@TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
	private void setGCMButtonListenerOnSwitch(
			SettingGCMNotificationButton listener, boolean checked) {
		Switch s = (Switch) findViewById(R.id.settings_gcm_switch);
		s.setOnCheckedChangeListener(listener);
		s.setChecked(checked);
	}

	public void onTaskLoginCompleteEvent() {
		setButtons();
	}

}

package com.einrichtungspartnerring.wirhabens;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.einrichtungspartnerring.wirhabens.helper.AirbrakeHelper;
import com.einrichtungspartnerring.wirhabens.manager.ManagerFactory;
import com.einrichtungspartnerring.wirhabens.manager.NetworkManager;
import com.einrichtungspartnerring.wirhabens.properties.MeinERPPropertiesFactory;
import com.einrichtungspartnerring.wirhabens.properties.PropertiesList;
import com.google.android.gcm.GCMBaseIntentService;

/**
 * Service for handling GCM.
 * 
 * @author Thomas Möller
 * 
 */
public class GCMIntentService extends GCMBaseIntentService {

	/**
	 * Log tag.
	 */
	private static final String TAG = "===GCMIntentService===";

	/**
	 * Default constructor
	 */
	public GCMIntentService() {
		super((String) MeinERPPropertiesFactory.getProperties(null).get(PropertiesList.GCM_PROJECT_KEY));

	}

	@Override
	protected void onError(Context context, String errorId) {
		Log.i(TAG, "Received error: " + errorId);
	}

	@Override
	protected void onMessage(Context context, Intent intent) {
		try{
            Intent startIntent = new Intent(context, MainActivity.class);
            PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, startIntent, 0);
			String message = intent.getExtras().getString("message");
			Log.i(TAG, "new message= " + message);
			NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
					this).setSmallIcon(R.drawable.erp_icon)
					.setContentTitle("wir haben's").setContentText(message);
            mBuilder.setContentText(message);
            mBuilder.setContentIntent(pendingIntent);

			NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
			// mId allows you to update the notification later on.
            Notification noti = mBuilder.build();
            noti.flags |= Notification.FLAG_AUTO_CANCEL;
			mNotificationManager.notify(3, noti);
		}catch(Exception e){
			String eMessage = "Unknown Exception";
			AirbrakeHelper.sendException(e, eMessage);
		}
	}

	@Override
	protected void onRegistered(Context context, String registrationId) {
		Log.i(TAG, "Device registered: regId = " + registrationId);
		NetworkManager netManager = ManagerFactory.buildNetworkManager(null);
		netManager.sendGCMIdToServer(registrationId, "news");

	}

	@Override
	protected void onUnregistered(Context context, String arg1) {
		Log.i(TAG, "unregistered = " + arg1);
	}

}

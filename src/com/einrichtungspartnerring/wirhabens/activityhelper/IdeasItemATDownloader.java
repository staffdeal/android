package com.einrichtungspartnerring.wirhabens.activityhelper;

import java.util.List;

import android.app.Activity;
import android.widget.ListView;

import com.einrichtungspartnerring.wirhabens.R;
import com.einrichtungspartnerring.wirhabens.gui.adapter.PicAdapterItem;
import com.einrichtungspartnerring.wirhabens.gui.adapter.PicTextAdapter;
import com.einrichtungspartnerring.wirhabens.helper.MeinERPDialogBuilder;
import com.einrichtungspartnerring.wirhabens.imagedownloader.ImageDownloader;
import com.einrichtungspartnerring.wirhabens.manager.CompetitionIdeasManager;
import com.einrichtungspartnerring.wirhabens.manager.MeinEPRSystemManager;

public class IdeasItemATDownloader extends PicTextXMLDownloaderAT {

	public IdeasItemATDownloader(PicTextAdapter adapter,
			MeinEPRSystemManager<?> manager, Activity activity,
			ImageDownloader iDown) {
		super(adapter, manager, activity, iDown);
	}

	@Override
	protected void onPreExecute() {
		proDialog = MeinERPDialogBuilder.buildProgressDialog(activity, R.string.dialog_loading_title, R.string.dialog_idea_loading_Text);
		proDialog.show();
		super.onPreExecute();
	}
	/**
	 * Use the given Manager to download all information about the ideas. Params
	 * need one item. The id of the competition you want to download the ideas.
	 */
	@Override
	protected List<PicAdapterItem> doInBackground(String... params) {
		if (manager instanceof CompetitionIdeasManager && params.length > 0) {
			CompetitionIdeasManager comManager = (CompetitionIdeasManager) manager;
			List<PicAdapterItem> items = comManager.getIdeasByCompetition(params[0]);
			return items;
		}
		return null;
	}


}

package com.einrichtungspartnerring.wirhabens.activityhelper;

import java.io.IOException;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.einrichtungspartnerring.wirhabens.R;
import com.einrichtungspartnerring.wirhabens.activityhelper.events.OnTaskLoginCompleteEvent;
import com.einrichtungspartnerring.wirhabens.fragments.StaffdealListFragment;
import com.einrichtungspartnerring.wirhabens.helper.MeinERPDialogBuilder;
import com.einrichtungspartnerring.wirhabens.manager.NetworkManager;
import com.einrichtungspartnerring.wirhabens.properties.MeinERPProperties;
import com.einrichtungspartnerring.wirhabens.properties.MeinERPPropertiesFactory;
import com.einrichtungspartnerring.wirhabens.properties.PropertiesList;
import com.einrichtungspartnerring.wirhabens.vo.AuthVO;

/**
 * Login task.
 * 
 * @author Thomas Möller
 * 
 */
public class LoginAT extends AsyncTask<String, Integer, AuthVO> implements
		PropertiesList {

	private Activity activity;
	private NetworkManager netManager;
	private String username;
	private String password;
	private OnTaskLoginCompleteEvent event;
	private AlertDialog currentDialog;
    private ProgressDialog progressDialog;
	/**
	 * Message view from the currentDialog.
	 */
	private TextView message;

	public LoginAT(Activity activity, NetworkManager netManager,
			String username, String password, OnTaskLoginCompleteEvent event, AlertDialog currentDialog, TextView message) {
		super();
		this.activity = activity;
		this.netManager = netManager;
		this.username = username;
		this.password = password;
		this.event = event;
		this.currentDialog = currentDialog;
		this.message = message;
	}
	
	@Override
	protected void onPreExecute() {
		message.setVisibility(View.GONE);
        progressDialog = MeinERPDialogBuilder.buildProgressDialog(activity, R.string.dialog_login_progress_title);
        progressDialog.show();

		super.onPreExecute();
	}

	@Override
	protected AuthVO doInBackground(String... params) {
		return netManager.authenticat(username, password);
	}

	@Override
	protected void onPostExecute(AuthVO result) {
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();

		if (result != null && result.getStatusMessag().equals("OK")) {
			MeinERPProperties prop = MeinERPPropertiesFactory.getProperties(activity);
			prop.set(USER_E_MAIL, username);
			prop.set(USER_PASS, password);
			prop.set(USER_RIGHT, "user");
			int duration = Toast.LENGTH_SHORT;
			Toast toast = Toast
					.makeText(activity, "Login successful", duration);
			toast.show();
			try {
				prop.store(activity);
			} catch (IOException e) {
				e.printStackTrace();
			}
			if (event !=null) {
			
				event.onTaskLoginCompleteEvent();
			
				for(StaffdealListFragment fragment : netManager.getLogInListeners()) {
					if(fragment != null) {
						fragment.logedIn();
					}					
				}
			
			}
		} else {
			String addMessage = activity.getResources().getString(R.string.dialog_login_gen_error)+ "\n";
			if (result != null)
				addMessage += result.getStatusMessag();
			message.setText(addMessage);
			message.setVisibility(View.VISIBLE);
			currentDialog.show();
		}
	}
}

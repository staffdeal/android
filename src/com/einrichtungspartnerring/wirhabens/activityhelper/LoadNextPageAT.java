package com.einrichtungspartnerring.wirhabens.activityhelper;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.BaseAdapter;
import android.widget.Toast;

import com.einrichtungspartnerring.wirhabens.MainActivity;
import com.einrichtungspartnerring.wirhabens.R;
import com.einrichtungspartnerring.wirhabens.gui.adapter.PicAdapterItem;
import com.einrichtungspartnerring.wirhabens.gui.adapter.PicTextAdapter;
import com.einrichtungspartnerring.wirhabens.manager.MeinEPRSystemManager;
import com.einrichtungspartnerring.wirhabens.vo.InfoVO;

import java.util.List;

/**
 * Created by loc on 24.07.14.
 */
public class LoadNextPageAT extends AsyncTask<String, String, List<PicAdapterItem>> {

    private Context context;
    private PicTextAdapter adapter;
    private MeinEPRSystemManager manager;


    public LoadNextPageAT(Context context, PicTextAdapter adapter, MeinEPRSystemManager manager){
        this.context = context;
        this.adapter = adapter;
        this.manager =manager;
    }

    @Override
    protected void onPreExecute() {

        if (context != null) {
            Toast toast = Toast.makeText(context, R.string.main_dynamic_loading_page, Toast.LENGTH_SHORT);
            toast.show();
        }
    }
    @Override
    protected List<PicAdapterItem> doInBackground(String... params) {
        return manager.loadPicAdapterItemFromNextPage();
    }

    @Override
    protected void onPostExecute(List<PicAdapterItem> result) {
        if (result != null) {
        	Log.d("MANAGER","NEw items " + result.size());
            adapter.setItems(result);
        }
    }
}

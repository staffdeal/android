package com.einrichtungspartnerring.wirhabens.activityhelper;

import java.util.List;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;

import com.einrichtungspartnerring.wirhabens.gui.adapter.PicAdapterItem;
import com.einrichtungspartnerring.wirhabens.gui.adapter.PicTextAdapter;
import com.einrichtungspartnerring.wirhabens.imagedownloader.ImageDownloader;
import com.einrichtungspartnerring.wirhabens.manager.MeinEPRSystemManager;

/**
 * Download in background information from the online site and add the
 * information to a list.
 * 
 * @author Thomas Möller
 * 
 */
public class PicTextXMLDownloaderAT extends
		AsyncTask<String, Integer, List<PicAdapterItem>> {

	/**
	 * view of the list where you want to show the new loaded items.
	 */
	protected PicTextAdapter adapter;
	
	/**
	 * Manager for the news
	 */
	protected MeinEPRSystemManager<?> manager;
	/**
	 * Activity from the list view.
	 */
	protected Activity activity;
	/**
	 * Downloader for the images.
	 */
	protected ImageDownloader iDown;
	
	protected ProgressDialog proDialog = null;

	/**
	 * Initialization constructor.
	 * 
	 * @param listView
	 * @param manager
	 * @param activity
	 * @param iDown
	 */
	public PicTextXMLDownloaderAT(PicTextAdapter adapter,
			MeinEPRSystemManager<?> manager, Activity activity,
			ImageDownloader iDown) {
		super();
		this.adapter = adapter;
		this.manager = manager;
		this.activity = activity;
		this.iDown = iDown;
	}
	
	/**
	 * Initialization constructor.
	 * 
	 * @param listView
	 * @param manager
	 * @param activity
	 * @param iDown
	 */
	public PicTextXMLDownloaderAT(PicTextAdapter adapter,
			MeinEPRSystemManager<?> manager, Activity activity,
			ImageDownloader iDown, ProgressDialog proDialog) {
		super();
		this.adapter = adapter;
		this.manager = manager;
		this.activity = activity;
		this.iDown = iDown;
		this.proDialog = proDialog;
	}

	@Override
	protected List<PicAdapterItem> doInBackground(String... params) {
		List<PicAdapterItem> items = manager.loadPicAdapterItem();
		return items;
	}

	@Override
	protected void onPostExecute(List<PicAdapterItem> result) {
		adapter.setItems(result);
		adapter.notifyDataSetChanged();
		if (proDialog != null && proDialog.isShowing())
			proDialog.dismiss();
		super.onPostExecute(result);
	}

}

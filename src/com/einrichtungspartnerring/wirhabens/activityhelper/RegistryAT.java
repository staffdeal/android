package com.einrichtungspartnerring.wirhabens.activityhelper;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.AsyncTask;

import com.einrichtungspartnerring.wirhabens.R;
import com.einrichtungspartnerring.wirhabens.helper.MeinERPDialogBuilder;
import com.einrichtungspartnerring.wirhabens.manager.NetworkManager;
import com.einrichtungspartnerring.wirhabens.manager.ServerResponse;

import java.util.List;

/**
 * Handling the registry process with network communication.
 * 
 * @author Thomas Möller
 * 
 */
public class RegistryAT extends AsyncTask<String, Integer, ServerResponse> {

	private NetworkManager netManager;
	private String email;
	private String password;
	private String firstname;
	private String lastname;
	private String location;
    private String dealterId;
	private Activity activity;
    private ProgressDialog progressDialog;

	public RegistryAT(NetworkManager netManager, String email, String password,
			String firstname, String lastname, String location, String dealterId, Activity activity) {
		super();
		this.netManager = netManager;
		this.email = email;
		this.password = password;
		this.firstname = firstname;
		this.lastname = lastname;
		this.location = location;
        this.dealterId = dealterId;
		this.activity = activity;
	}

    @Override
    protected void onPreExecute() {
        progressDialog = MeinERPDialogBuilder.buildProgressDialog(activity, R.string.registry_send_dia_title, R.string.registry_send_dia_text);
        progressDialog.show();
        super.onPreExecute();
    }

    @Override
	protected ServerResponse doInBackground(String... params) {
		ServerResponse response = netManager.registry(email, password, firstname, lastname, location,dealterId);
		return response;
	}
	
	@Override
	protected void onPostExecute(ServerResponse result) {
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();

        List<String> errrors = netManager.errorLoging;

		Dialog dialog = null;
		if (result.equals(ServerResponse.ok)){
			dialog = MeinERPDialogBuilder.buildInformationDialog(activity, R.string.registry_response_okay_title, R.string.registry_response_okay_text);
		}else if(result.equals(ServerResponse.passwort_not_fulfill_policies)){
			dialog = MeinERPDialogBuilder.buildInformationDialog(activity, R.string.registry_response_failed_title, R.string.registry_response_failed_password_text);
		}else if (result.equals(ServerResponse.incorrect_email_format)){
            dialog = MeinERPDialogBuilder.buildInformationDialog(activity, R.string.registry_response_failed_title, R.string.registry_response_failed_email_text);
        }else if (result.equals(ServerResponse.no_dealer)){
            dialog = MeinERPDialogBuilder.buildInformationDialog(activity, R.string.registry_response_failed_title, R.string.registry_response_failed_position_not_allow_text);
        }else if (result.equals(ServerResponse.email_duplicate)){
            dialog = MeinERPDialogBuilder.buildInformationDialog(activity, R.string.registry_response_failed_title, R.string.registry_response_failed_email_known_text);
        }else if (result.equals(ServerResponse.server_communication_error)){
            dialog = MeinERPDialogBuilder.buildInformationDialog(activity, R.string.registry_response_failed_title, R.string.registry_response_failed_server_com_text);
        }else{
			dialog = MeinERPDialogBuilder.buildInformationDialog(activity,R.string.registry_response_failed_title, R.string.registry_response_failed_unknown_text);
		}
		dialog.show();



		super.onPostExecute(result);
	}
	
	

}

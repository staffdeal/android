package com.einrichtungspartnerring.wirhabens.activityhelper;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.AsyncTask;

import com.einrichtungspartnerring.wirhabens.R;
import com.einrichtungspartnerring.wirhabens.helper.MeinERPDialogBuilder;
import com.einrichtungspartnerring.wirhabens.manager.NetworkManager;
import com.einrichtungspartnerring.wirhabens.manager.serverresponse.ResetPasswordServerResponse;

/**
 * Reset password AT and show result.
 * @author Thomas Möller
 *
 */
public class ResetPasswordAT extends AsyncTask<String, String, ResetPasswordServerResponse>{

	private Activity activity;
	private NetworkManager netManager;
	private String emailAddress;
	private ProgressDialog dialog;

	public ResetPasswordAT(Activity activity, NetworkManager netManager,
			String emailAddress) {
		this.activity = activity;
		this.netManager = netManager;
		this.emailAddress = emailAddress;
	}
	
	@Override
	protected void onPreExecute() {
		dialog = new ProgressDialog (activity);
		dialog.setTitle(R.string.dialog_loading_title);
		dialog.setMessage(activity.getString(R.string.dialog_loading_text));
		dialog.show();
		super.onPreExecute();
	}

	@Override
	protected ResetPasswordServerResponse doInBackground(String... params) {
		ResetPasswordServerResponse response = netManager.resetPassword(emailAddress);
		return response;
	}

	@Override
	protected void onPostExecute(ResetPasswordServerResponse result) {
		int title = 0;
		int message = 0;
		dialog.cancel();
		Dialog resultDia = null;
		switch (result) {
		case ok:
			title = R.string.password_reset_dialog_title_ok;
			message = R.string.password_reset_dialog_text_ok;
			break;
		case email_invalid_formated_error:
			title = R.string.password_reset_dialog_title_fail;
			message = R.string.password_reset_dialog_fail_invalid_email_format_error_text;
			break;
		case wrong_email_address_error:
			title = R.string.password_reset_dialog_title_fail;
			message = R.string.password_reset_dialog_fail_wrong_email_error_text;
			break;
		case server_error:
			title = R.string.password_reset_dialog_title_fail;
			message = R.string.password_reset_dialog_fail_server_error_text;
			break;
		default:
			title = R.string.password_reset_dialog_title_fail;
			message = R.string.password_reset_dialog_fail_unknown_error_text;
			break;
		}
		
		resultDia = MeinERPDialogBuilder.buildInformationDialog(activity, title, message);
		resultDia.show();
		
		super.onPostExecute(result);
	}
}

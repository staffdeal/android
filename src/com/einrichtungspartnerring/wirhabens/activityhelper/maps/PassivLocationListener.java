package com.einrichtungspartnerring.wirhabens.activityhelper.maps;

import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.util.Log;

public class PassivLocationListener implements LocationListener {

	private final static String TAG = "PassivLocationListener";
	
	public PassivLocationListener() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onLocationChanged(Location location) {
		Log.w(TAG, "!!!location change");
	}

	@Override
	public void onProviderDisabled(String provider) {
		Log.w(TAG, "!!!onProviderDisabled");
	}

	@Override
	public void onProviderEnabled(String provider) {
		Log.w(TAG, "!!!onProviderEnabled");
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		Log.w(TAG, "!!!onStatusChanged");
	}

}

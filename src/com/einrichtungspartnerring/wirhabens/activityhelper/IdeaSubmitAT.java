package com.einrichtungspartnerring.wirhabens.activityhelper;

import java.io.File;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;

import com.einrichtungspartnerring.wirhabens.R;
import com.einrichtungspartnerring.wirhabens.helper.MeinERPDialogBuilder;
import com.einrichtungspartnerring.wirhabens.manager.NetworkManager;
import com.einrichtungspartnerring.wirhabens.vo.CompetitionVO;

/**
 * AT for sending the ideas.
 * 
 * @author Thomas Möller
 * 
 */
public class IdeaSubmitAT extends AsyncTask<String, Integer, String> {

	private static final String TAG = "IdeaSubmitAT";

	private NetworkManager netManager;
	private String text;
	private CompetitionVO competition;
	private String link;
	private File imageFile;
	private ProgressDialog proDialog;
	private Activity activity;

	
	/**
	 * Init constructor
	 * 
	 * @param netManager
	 * @param competition
	 * @param text
	 * @param link
	 * @param imageFile
	 */
	public IdeaSubmitAT(NetworkManager netManager, CompetitionVO competition,
			String text, String link, File imageFile, Activity activity) {
		super();
		this.netManager = netManager;
		this.text = text;
		this.competition = competition;
		this.link = link;
		this.imageFile = imageFile;
		this.activity = activity;
	}
	
	@Override
	protected void onPreExecute() {
		proDialog =  MeinERPDialogBuilder.buildProgressDialog(activity, R.string.dialog_loading_title, R.string.dialog_idea_sending_Text);;
		proDialog.show();
		super.onPreExecute();
	}

	@Override
	protected String doInBackground(String... params) {
		Log.i(TAG, "sending idea in background.");
		netManager.submitIdea(competition.getId(), text, link, imageFile);
		return null;
	}

	@Override
	protected void onPostExecute(String result) {

		if (proDialog != null && proDialog.isShowing())
			proDialog.dismiss();
		
		super.onPostExecute(result);
	}

}

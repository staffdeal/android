package com.einrichtungspartnerring.wirhabens.activityhelper;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;

import com.einrichtungspartnerring.wirhabens.R;
import com.einrichtungspartnerring.wirhabens.helper.MeinERPDialogBuilder;
import com.einrichtungspartnerring.wirhabens.helper.StreamHelper;
import com.einrichtungspartnerring.wirhabens.manager.SystemPaths;
import com.einrichtungspartnerring.wirhabens.parser.builder.TagNames;
import com.einrichtungspartnerring.wirhabens.vo.DealsItemVO;


/**
 * Loading the offline bar code for the system and show it in a dialog.
 * @author Thomas Möller
 *
 */
public class BarcodeGrabberAT extends AsyncTask<String, Integer, Bitmap>{

	private static final String TAG = "BarcodeGrabberAT";

	/**
	 * Maybe you want to show a ProgressDialog by loading. By given it. The AT will cancel it for yoi
	 * Optional
	 */
	private ProgressDialog proDialog;
	/**
	 * Activity where the bar code should show.
	 */
	private Activity activity;
	/**
	 * Deal where the bar code is from.
	 */
	private String dealId;

    private String dealUrl;

    private int dealTitleStringId;

    private String dealDescriptionText;

    private String dealDescriptionUrl;

	public BarcodeGrabberAT(Activity activity,
			String dealId, String dealUrl, int dealTitleStringId, String dealDescriptionText, String dealDescriptionUrl) {
		this(activity, dealId, dealUrl, dealTitleStringId);
		this.dealDescriptionText = dealDescriptionText;
        this.dealDescriptionUrl = dealDescriptionUrl;
	}


    public BarcodeGrabberAT(Activity activity,
                            String dealId, String dealUrl, int dealTitleStringId) {
        super();
        this.proDialog = null;
        this.activity = activity;
        this.dealId = dealId;
        this.dealUrl = dealUrl;
        this.dealTitleStringId = dealTitleStringId;
    }

    @Override
    protected void onPreExecute() {
        this.proDialog = MeinERPDialogBuilder.buildProgressDialog(activity, R.string.dialog_loading_title, R.string.dialog_barcode_loading_dialog_message);
        this.proDialog.show();
        super.onPreExecute();
    }

    @Override
	protected Bitmap doInBackground(String... params) {
		String filepath = activity.getFilesDir().toString() + SystemPaths.BARCODE_FOLDER + dealId;
		File file = new File(filepath);
		Bitmap image = null;
		try {
			// Only download barcode if no file about this exist.
			if (!file.exists()){
				Log.d(TAG, "Barcode don't exist. Try to downloading it.");
				writeContentToFile(dealUrl, file);
			}else{
				Log.d(TAG, "Barcode exist.");
			}
			image = BitmapFactory.decodeFile(filepath);
		} catch (IOException e) {
			e.printStackTrace();
		}
        catch (Exception e){
            Log.d(TAG, "No URL available");
        }
		if (image == null && file.exists()){
			Log.w(TAG, "Image loading failed. Remove Picture for next try.");
			file.delete();
		}

		return image;
	}
	@Override
	protected void onPostExecute(Bitmap result) {
		// cancel loading dialog.
		if (proDialog != null &&  proDialog.isShowing())
			proDialog.dismiss();

		// Error case
		if (result == null){
			MeinERPDialogBuilder.buildInformationDialog(activity, 
					activity.getString(R.string.dialog_barcode_loading_error_title), 
					activity.getString(R.string.dialog_barcode_loading_error_text));
		}else{ // good case, show dialog
			Dialog dialog = MeinERPDialogBuilder.buildDialogWithImage(activity, 
					activity.getString(dealTitleStringId), null, result);
			dialog.show();
		}
	}

    /**
     * TODO remove network code from this part.
     * @param urlS
     * @param file
     * @throws Exception
     */
	protected void writeContentToFile(String urlS, File file) throws Exception{
        if (urlS == null && !urlS.equals(TagNames.NO_CODE))
            throw new Exception("URL is null");
		InputStream in = null;
		OutputStream out = null;
		try {
			URL url = new URL(urlS);
			in = url.openStream();
			out = new FileOutputStream(file);
			StreamHelper.copy(in, out);
			out.flush();
		} catch (IOException e) {
			e.printStackTrace();
			Log.e(TAG, "Could not load Bitmap from: " + urlS);
			throw e;
		} finally {
			try {
				if (in != null)
					in.close();
				if (out != null) {
					out.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}

}

package com.einrichtungspartnerring.wirhabens.activityhelper;

import java.io.IOException;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.einrichtungspartnerring.wirhabens.R;
import com.einrichtungspartnerring.wirhabens.manager.NetworkManager;
import com.einrichtungspartnerring.wirhabens.manager.ServerResponse;
import com.einrichtungspartnerring.wirhabens.properties.MeinERPProperties;
import com.einrichtungspartnerring.wirhabens.properties.MeinERPPropertiesFactory;
import com.einrichtungspartnerring.wirhabens.properties.PropertiesList;

/**
 * Login task.
 * 
 * @author Thomas Möller
 * 
 */
public class ChangePasswordAT extends AsyncTask<String, Integer, ServerResponse> implements
		PropertiesList {

	private Activity activity;
	private NetworkManager netManager;
	private String newPassword;
	private String newPassword2;
	private String oldPassword;
	private TextView message;
	private ProgressDialog waitDialog;
	private AlertDialog currentDialog;

	public ChangePasswordAT(Activity activity, NetworkManager netManager,
			String newPassword, String newPassword2, String oldPassword, TextView message, ProgressDialog waitDialog, AlertDialog currentDialog) {
		super();
		this.activity = activity;
		this.netManager = netManager;
		this.newPassword = newPassword;
		this.oldPassword = oldPassword;
		this.message = message;
		this.waitDialog = waitDialog;
		this.currentDialog = currentDialog;
		this.newPassword2 = newPassword2;
	}

	@Override
	protected void onPreExecute() {
		message.setVisibility(View.GONE);
		super.onPreExecute();
	}
	
	@Override
	protected ServerResponse doInBackground(String... params) {
		if (newPassword.equals(newPassword2))
			return netManager.changePassword(this.oldPassword, this.newPassword);
		return ServerResponse.pass1_to_pass2_is_different;
	}

	@Override
	protected void onPostExecute(ServerResponse result) {
		if (result != null && result.equals(ServerResponse.ok)) {
			MeinERPProperties prop = MeinERPPropertiesFactory
					.getProperties(activity);
			prop.set(USER_PASS, this.newPassword);
			int duration = Toast.LENGTH_SHORT;
			Toast toast = Toast.makeText(activity, "Password change successful", duration);
			toast.show();
			try {
				prop.store(activity);
			} catch (IOException e) {
				e.printStackTrace();
			}
			waitDialog.cancel();
		} else {
			String addMessage = activity.getResources().getString(R.string.dialog_login_gen_error)+ "\n";
			switch (result) {
			case old_pass_not_correct:
				addMessage += activity.getResources().getString(R.string.dialog_changepassword_error_old_pass_incorrect);
				break;
			case passwort_not_fulfill_policies:
				addMessage += activity.getResources().getString(R.string.dialog_changepassword_error_new_pass_not_sec);
				break;
			case pass1_to_pass2_is_different:
				addMessage += activity.getResources().getString(R.string.dialog_changepassword_pass_pass2_different);
				break;
			case unknown_error:
				addMessage += activity.getResources().getString(R.string.dialog_changepassword_error_unknown);
				break;
			default:
				addMessage += activity.getResources().getString(R.string.dialog_changepassword_error_unknown);
				break;
			}
			message.setText(addMessage);
			message.setVisibility(View.VISIBLE);
			waitDialog.cancel();
			currentDialog.show();
		}
		
	}
}

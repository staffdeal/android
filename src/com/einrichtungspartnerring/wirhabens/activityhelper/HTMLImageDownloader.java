package com.einrichtungspartnerring.wirhabens.activityhelper;

import java.io.File;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.widget.ImageView;

import com.einrichtungspartnerring.wirhabens.helper.ImageHelper;
import com.einrichtungspartnerring.wirhabens.manager.ImageManager;
import com.einrichtungspartnerring.wirhabens.manager.ManagerFactory;

/**
 * Download asynchron the picture and save it.
 * @author Thomas Möller
 *
 */
@Deprecated
public class HTMLImageDownloader extends AsyncTask<String, Integer, Bitmap> {

	private ImageView view;
	private Context context;
	private String itemId; 
	private String downloadURL;

	public HTMLImageDownloader(ImageView view, Context context, String itemId, String downloadURL) {
		this.view = view;
		this.context = context;
		this.itemId = itemId;
		this.downloadURL = downloadURL;
	}
 
	@Override
	protected Bitmap doInBackground(String... params) {
		ImageManager iManager = ManagerFactory.buildImageManager(context);
		File imageFile = iManager.get(context, itemId , downloadURL);

		Bitmap imageBitmap = ImageHelper.decodeAndScaleFile(imageFile, 200, 200);

		return imageBitmap;
	}

	@Override
	protected void onPostExecute(Bitmap result) {
		view.setImageBitmap(result);
		super.onPostExecute(result);
	}
}

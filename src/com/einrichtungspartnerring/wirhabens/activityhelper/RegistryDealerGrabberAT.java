package com.einrichtungspartnerring.wirhabens.activityhelper;

import android.app.Activity;
import android.os.AsyncTask;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ProgressBar;
import android.widget.Spinner;
import com.einrichtungspartnerring.wirhabens.R;
import com.einrichtungspartnerring.wirhabens.helper.MeinERPDialogBuilder;
import com.einrichtungspartnerring.wirhabens.manager.NetworkManager;
import com.einrichtungspartnerring.wirhabens.vo.DealLoadingDetails;
import com.einrichtungspartnerring.wirhabens.vo.DealerVO;
import com.einrichtungspartnerring.wirhabens.vo.InfoFieldVO;
import com.einrichtungspartnerring.wirhabens.vo.InfoVO;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by loc on 03.08.14.
 */
public class RegistryDealerGrabberAT extends AsyncTask<String, String, List<DealerVO>> {

    private Spinner dealerSpinner;
    private ProgressBar progressBar;
    private NetworkManager networkManager;
    private Activity activity;

    public RegistryDealerGrabberAT(Activity activity,  Spinner dealerSpinner, ProgressBar progressBar, NetworkManager networkManager) {
        this.dealerSpinner = dealerSpinner;
        this.progressBar = progressBar;
        this.networkManager = networkManager;
        this.activity = activity;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

    }

    @Override
    protected List<DealerVO> doInBackground(String... params) {
        InfoVO info = networkManager.getInfo();
        List<DealerVO> dealer = new ArrayList<DealerVO>();
        if (info == null)
            info = networkManager.loadInfos();
        if (info != null && info.getFields() != null) {
            for (InfoFieldVO field : info.getFields()){
                if (field.getName().equals("dealer")){
                    String url = field.getUrl();
                    dealer = networkManager.loadDealers(url);
                }
            }
        }

        return dealer;
    }

    @Override
    protected void onPostExecute(List<DealerVO> result) {
        ArrayList<String> content = new ArrayList<String>();
        if (result.size() > 0) {
            for (DealerVO dealer : result) {
                content.add(dealer.getName());
            }
        }else
            content.add("Keine Händler vorhanden");

        ArrayAdapter<String> ada = new ArrayAdapter<String>(activity ,android.R.layout.simple_spinner_item ,content);
        dealerSpinner.setAdapter(ada);

        progressBar.setVisibility(View.GONE);
        dealerSpinner.setVisibility(View.VISIBLE);

        super.onPostExecute(result);
    }

}

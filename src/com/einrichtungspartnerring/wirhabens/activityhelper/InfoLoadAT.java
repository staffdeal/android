package com.einrichtungspartnerring.wirhabens.activityhelper;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.AsyncTask;

import com.einrichtungspartnerring.wirhabens.R;
import com.einrichtungspartnerring.wirhabens.activityhelper.events.OnInfoLoadCompleteEvent;
import com.einrichtungspartnerring.wirhabens.manager.NetworkManager;
import com.einrichtungspartnerring.wirhabens.vo.InfoVO;

/**
 * Load the Backend information and done you tasks.
 * @author Thomas Möller
 *
 */
public class InfoLoadAT extends AsyncTask<String, String, InfoVO> {

	private OnInfoLoadCompleteEvent event;
	private NetworkManager netManager;
	private Object[] params;
	private Activity activity;
	private Dialog dialog;
	
	/**
	 * Load the backend informations without loading screen.
	 * @param event - method you want to call after informations are loaded.
	 * @param netManager - manager backend informations can loaded.
	 * @param params - you want to give in the event method at the end.
	 */
	public InfoLoadAT(OnInfoLoadCompleteEvent event, NetworkManager netManager, Object ...params) {
		this.event = event;
		this.netManager = netManager;
		this.params = params;
	}
	
	/**
	 * Load the backend informations without loading screen.
	 * @param event - method you want to call after informations are loaded.
	 * @param netManager - manager backend informations can loaded.
	 * @param params - you want to give in the event method at the end.
	 * @param activity where you want to show a loading screen.
	 */
	public InfoLoadAT(OnInfoLoadCompleteEvent event, NetworkManager netManager, Activity activity,Object ...params) {
		this.event = event;
		this.netManager = netManager;
		this.params = params;
		this.activity = activity;
	}
	
	@Override
	protected void onPreExecute() {
		if (activity != null){
			this.dialog = new ProgressDialog(this.activity);
			this.dialog.setTitle(R.string.dialog_loading_title);
			this.dialog.show();
		}
		super.onPreExecute();
	}

	@Override
	protected InfoVO doInBackground(String... params) {
		return netManager.loadInfos();
	}
	
	@Override
	protected void onPostExecute(InfoVO result) {
		super.onPostExecute(result);
		if (this.dialog != null)
			this.dialog.cancel();
		if (event != null)
			event.onInfoLoadComplete(params);
	}

}

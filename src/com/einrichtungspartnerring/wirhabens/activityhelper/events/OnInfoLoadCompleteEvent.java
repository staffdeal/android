package com.einrichtungspartnerring.wirhabens.activityhelper.events;

/**
 * Interface for Asyncron task handler InfoLoadAT
 * @author Thomas Möller
 *
 */
public interface OnInfoLoadCompleteEvent {

	/**
	 * Call after InfoLoadAT task is finish
	 * @param objects all want or need.
	 */
	void onInfoLoadComplete(Object ...objects );
	
}

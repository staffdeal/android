package com.einrichtungspartnerring.wirhabens.activityhelper.events;


/**
 * Event which will call of login asynchron task complete and successful.
 * @author Thomas Möller
 *
 */
public interface OnTaskLoginCompleteEvent {
	
	void onTaskLoginCompleteEvent();
}

package com.einrichtungspartnerring.wirhabens.activityhelper.events;

/**
 * Event which called from AsynchronTask if dialog accept.
 * @author Thomas Möller
 *
 */
public interface OnTaskDialogAcceptEvent {

	void onTaskCompleted(Object ...objects);
}

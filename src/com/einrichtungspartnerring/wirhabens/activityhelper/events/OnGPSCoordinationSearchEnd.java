package com.einrichtungspartnerring.wirhabens.activityhelper.events;

/**
 * Eventhandler after GPSCoordinationAT run is complete.
 * Created by loc on 08.01.14.
 */
public interface OnGPSCoordinationSearchEnd {
    /**
     * Handling information after GPSCoordinationAT is run complete.
     */
    void OnGPSCoordinationSearchEnd();
}

package com.einrichtungspartnerring.wirhabens.activityhelper.events;

/**
 * Created by loc on 24.08.14.
 */
public interface ReloadedInformation {

    void wasReloaded(boolean status);
}

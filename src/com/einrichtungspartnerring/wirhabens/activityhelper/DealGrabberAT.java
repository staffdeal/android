package com.einrichtungspartnerring.wirhabens.activityhelper;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import com.einrichtungspartnerring.wirhabens.R;
import com.einrichtungspartnerring.wirhabens.helper.MeinERPDialogBuilder;
import com.einrichtungspartnerring.wirhabens.manager.DealsManager;
import com.einrichtungspartnerring.wirhabens.manager.NetworkManager;
import com.einrichtungspartnerring.wirhabens.vo.DealLoadingDetails;
import com.einrichtungspartnerring.wirhabens.vo.DealsItemVO;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by loc on 31.07.14.
 */
public class DealGrabberAT extends AsyncTask<String, String, String> {

    /**
     * Maybe you want to show a ProgressDialog by loading. By given it. The AT will cancel it for yoi
     * Optional
     */
    private ProgressDialog proDialog;

    private NetworkManager networkManager;
    private DealsManager dealsManager;
    private DealsItemVO deal;
    private Activity activity;
    private String url;

    public DealGrabberAT(NetworkManager networkManager, DealsItemVO deal, Activity activity, String url, DealsManager dealsManager) {
        this.networkManager = networkManager;
        this.deal = deal;
        this.activity = activity;
        this.url = url;
        this.dealsManager = dealsManager;
    } 

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        this.proDialog = MeinERPDialogBuilder.buildProgressDialog(activity, R.string.dialog_loading_title, R.string.dialog_barcode_loading_dialog_message);
        this.proDialog.show();
    }

    @Override
    protected String doInBackground(String... params) {
        try {
            URL url = new URL(this.url);
            DealLoadingDetails dealDetails = networkManager.loadDealDetails(url.toString());
            if (dealDetails.getCode() != null && !dealDetails.getCode().isEmpty()) {
                deal.setPassOnlineCode(dealDetails.getCode());
                dealsManager.saveCodeToDeal(deal);
            }
            if (dealDetails.getBarcodeUrl() != null && !dealDetails.getBarcodeUrl().isEmpty())
                deal.setPassOnSiteUrl(dealDetails.getBarcodeUrl());
        }catch (MalformedURLException e){

        }
        return "";
    }

    @Override
    protected void onPostExecute(String result) {


        if (proDialog != null &&  proDialog.isShowing())
            proDialog.dismiss();


        super.onPostExecute(result);
    }
}

package com.einrichtungspartnerring.wirhabens.activityhelper;

import android.os.AsyncTask;
import android.util.Log;
import com.einrichtungspartnerring.wirhabens.R;
import com.einrichtungspartnerring.wirhabens.helper.MeinERPDialogBuilder;
import com.einrichtungspartnerring.wirhabens.manager.NetworkManager;

/**
 * Created by loc on 31.07.14.
 */
public class NewsReadAT extends AsyncTask<String, String, String> {

    private NetworkManager netManager;

    public NewsReadAT(NetworkManager netManager) {
        this.netManager = netManager;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(String... params) {
        if(params == null || params.length != 1)
            throw new UnsupportedOperationException("param should be one id");

        netManager.sendNewsRead(params[0]);
        return null;
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
    }
}

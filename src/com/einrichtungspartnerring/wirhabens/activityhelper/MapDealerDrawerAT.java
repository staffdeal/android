package com.einrichtungspartnerring.wirhabens.activityhelper;

import java.util.List;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;

import com.einrichtungspartnerring.wirhabens.R;
import com.einrichtungspartnerring.wirhabens.gui.maps.MeinEPRMapMarkers;
import com.einrichtungspartnerring.wirhabens.manager.DealsManager;
import com.einrichtungspartnerring.wirhabens.vo.DealerVO;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * Draw the position of the dealers on the map
 * @author Thomas Möller
 *
 */
public class MapDealerDrawerAT extends AsyncTask<String, Integer, List<DealerVO>> {


	private static final String TAG = "MapDealerDrawerAT";
	private GoogleMap mMap;
	private DealsManager dealManager;
	private String dealId;
	private Activity activity;
	private ProgressDialog pro;



	public MapDealerDrawerAT(GoogleMap mMap, DealsManager dealManager,
			String dealId, Activity activity) {
		super();
		this.mMap = mMap;
		this.dealManager = dealManager;
		this.dealId = dealId;
		this.activity = activity;
	}

	@Override
	protected void onPreExecute() {
		pro = new ProgressDialog(activity);
		pro.show();
		super.onPreExecute();
	}
	

	@Override
	protected List<DealerVO> doInBackground(String... params) {
		List<DealerVO> dealers = dealManager.getDealers(dealId);
		return dealers;
	}
	
	@Override
	protected void onPostExecute(List<DealerVO> result) {
		if (result != null){
			Log.d(TAG, "get size of dealser : " + result.size());
			mMap.animateCamera(CameraUpdateFactory.newCameraPosition
					(new CameraPosition(new LatLng(51.467697, 9.664306), 5, 0, 0)));
			mMap.setInfoWindowAdapter(new MeinEPRMapMarkers(activity));
			for (DealerVO dealer : result){
				mMap.addMarker(buildMarkerOptions(dealer));
			}
		}
		if (pro != null && pro.isShowing())
			pro.cancel();
		super.onPostExecute(result);
	}
	
	private MarkerOptions buildMarkerOptions(DealerVO dealer){
		MarkerOptions opt = new MarkerOptions();
		opt.position(new LatLng(Double.valueOf(dealer.getLocationLat()), Double.valueOf(dealer.getLocationLon())));
		opt.title(dealer.getName());
		StringBuilder sBuilder = new StringBuilder();
		String sInfo = dealer.getStreet();
		if (sInfo != null && !sInfo.isEmpty())
			sBuilder.append(activity.getString(R.string.maps_info_street)+ sInfo+ "\n");
		sInfo = dealer.getZip();
		if (sInfo != null && !sInfo.isEmpty())
			sBuilder.append(activity.getString(R.string.maps_info_zip)+ sInfo+ "\n");
		sInfo = dealer.getCity();
		if (sInfo != null && !sInfo.isEmpty())
			sBuilder.append(activity.getString(R.string.maps_info_city)+ sInfo+ "\n");
		sInfo = dealer.getCountry();
		if (sInfo != null && !sInfo.isEmpty())
			sBuilder.append(activity.getString(R.string.maps_info_country)+ sInfo+ "\n");
		
		opt.snippet(sBuilder.toString());
		return opt;
	}

}

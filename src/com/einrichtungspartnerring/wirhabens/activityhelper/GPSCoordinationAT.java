package com.einrichtungspartnerring.wirhabens.activityhelper;

import android.app.ProgressDialog;
import android.location.Location;
import android.os.AsyncTask;

import com.einrichtungspartnerring.wirhabens.R;
import com.einrichtungspartnerring.wirhabens.RegistryActivity;
import com.einrichtungspartnerring.wirhabens.activityhelper.events.OnGPSCoordinationSearchEnd;
import com.einrichtungspartnerring.wirhabens.helper.MeinERPDialogBuilder;

/**
 * Search for gps as AT.
 * Created by loc on 08.01.14.
 */
public class GPSCoordinationAT extends AsyncTask<String, Integer, Location> {

    private ProgressDialog dialog;
    private RegistryActivity activity;
    private OnGPSCoordinationSearchEnd event;

    /**
     * inti constructor
     * @param activity you want to show the dialog.
     * @param event call after process is complete.
     */
    public GPSCoordinationAT(RegistryActivity activity, OnGPSCoordinationSearchEnd event) {
        this.activity = activity;
        this.event = event;
    }

    @Override
    protected void onPreExecute() {
        dialog = MeinERPDialogBuilder.buildProgressDialog(activity, R.string.registry_gps_search_title, R.string.registry_gps_search_text);
        dialog.show();
        super.onPreExecute();
    }

    @Override
    protected Location doInBackground(String... params) {
        while(activity.getCurrentLocation() == null){
            try{
                Thread.sleep(500);
            }catch (InterruptedException e){
                e.printStackTrace();
            }
        }
        return activity.getCurrentLocation();
    }

    @Override
    protected void onPostExecute(Location s) {
        if (dialog != null && dialog.isShowing())
            dialog.dismiss();
        // eventhandler
        if (event != null)
            event.OnGPSCoordinationSearchEnd();
        super.onPostExecute(s);
    }

}

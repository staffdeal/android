package com.einrichtungspartnerring.wirhabens.gui.maps;

import android.app.Activity;
import android.view.View;
import android.widget.TextView;

import com.einrichtungspartnerring.wirhabens.R;
import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.google.android.gms.maps.model.Marker;

public class MeinEPRMapMarkers implements InfoWindowAdapter{

	private Activity activity;
	
	public MeinEPRMapMarkers(Activity activity) {
		this.activity = activity;
	}

	@Override
	public View getInfoContents(Marker marker) {
		View v = activity.getLayoutInflater().inflate(R.layout.map_custom_info, null);
		TextView title = (TextView) v.findViewById(R.id.maps_info_title);
		TextView snipped = (TextView) v.findViewById(R.id.maps_info_text);
		
		title.setText(marker.getTitle());
		snipped.setText(marker.getSnippet());
		return v;
	}

	@Override
	public View getInfoWindow(Marker marker) {
		return null;
	}

}

package com.einrichtungspartnerring.wirhabens.gui.adapter;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import android.app.Activity;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.einrichtungspartnerring.wirhabens.R;
import com.einrichtungspartnerring.wirhabens.imagedownloader.ImageDownloader;
import com.einrichtungspartnerring.wirhabens.vo.DealsItemVO;

/**
 * Universal view Adapter to show the items on the tab view.
 * 
 * @author Thomas Möller
 * 
 */
public class PicTextAdapter extends BaseAdapter{

	/**
	 * Optimizing! inflate should not done by every run.
	 * 
	 * @author Thomas Möller
	 * 
	 */

    public HashMap<String, View> tempView = new HashMap<String, View>();

	public static class ViewHolder {
		public TextView date;
		public TextView head;
		public TextView title;
		public ImageView image;
        public ImageView star;
        public ImageView soldOut;
		public LinearLayout layout;
		public String id;
	}

	/**
	 * Downloader for the images which maybe missed.
	 */
	private ImageDownloader iDown;

	/**
	 * All items for the list in the list view.
	 */
	private List<PicAdapterItem> items;
 
	/**
	 * Activity where the list view will showed.
	 */
	private Activity activity;
	/**
	 * Inflater for copy layouts.
	 */
	private LayoutInflater inflater;

	protected PicTextAdapter() {

	}

    public void setItems(List<PicAdapterItem> items) {
        this.items = items;
		Comparator<PicAdapterItem> comparator = Collections.reverseOrder();
		if (this.items != null) {
			Collections.sort(this.items, comparator);
		}
		activity.runOnUiThread(new Runnable() {         
	        public void run() {
	              notifyDataSetChanged();
	        }
	    });

		notifyDataSetChanged();
    }

    public PicTextAdapter(Activity activity, List<PicAdapterItem> items) {
		this.activity = activity;
		setItems(items);
		this.inflater = (LayoutInflater) activity.getLayoutInflater();
	}

	public PicTextAdapter(Activity activity, List<PicAdapterItem> items,
			ImageDownloader iDown) {
		this(activity, items);
		this.iDown = iDown;
	}

	@Override
	public int getCount() {
		if (items != null)
			return items.size();
		return 0;
	}

	@Override
	public Object getItem(int position) {
		if (items != null )
			return items.get(position);
		return null;
	} 

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
        View vi = convertView;
		// optimizing
		if (vi == null) {
			vi = inflater.inflate(R.layout.picadapter_item, null); 
			ViewHolder viewHolder = new ViewHolder();
			viewHolder.date = (TextView) vi.findViewById(R.id.date);
			viewHolder.head = (TextView) vi.findViewById(R.id.head);
			viewHolder.title = (TextView) vi.findViewById(R.id.title);
			viewHolder.image = (ImageView) vi.findViewById(R.id.picadapter_image);
            viewHolder.star = (ImageView) vi.findViewById(R.id.picadapter_starimage);
			viewHolder.layout = (LinearLayout) vi.findViewById(R.id.imagecontent);
			viewHolder.soldOut = (ImageView) vi.findViewById(R.id.deal_button_online_cash_ausverkauft);
			vi.setTag(viewHolder);
		}

		ViewHolder holder = (ViewHolder) vi.getTag();		
		
		holder.soldOut.setVisibility(View.GONE);
		PicAdapterItem item = items.get(position);
		holder.id = item.getId();
		holder.head.setText(item.getAdapterHead());

		holder.title.setText(item.getAdapterTitle());
		if (!item.isReaded())
			holder.title.setTypeface(null, Typeface.BOLD);
		else 
			holder.title.setTypeface(null, Typeface.NORMAL);
		
		holder.image.setBackgroundResource(R.color.white);
		holder.date.setText(item.getAdapterDate());

        if (item.isFeatured())
        	holder.star.setVisibility(View.VISIBLE);
        else
        	holder.star.setVisibility(View.GONE);

        switch (item.getType()) {
		case 0:
			holder.image.setImageResource(R.drawable.news_placeholder);
			break;
		case 1:
			holder.image.setImageResource(R.drawable.ideas_placeholder);
			break;
		case 2:
			holder.image.setImageResource(R.drawable.deals_placeholder);
			if(!((DealsItemVO) item).isPassOnlineAvaible() && ((DealsItemVO) item).getPassOnSiteUrl() == null){
				holder.soldOut.setVisibility(View.VISIBLE);
			}
			break;
		case 3:
			holder.image.setImageResource(R.drawable.events_placeholder);
			break;
		default:
			holder.image.setImageDrawable(null);
			break;
		}
      	
        if (iDown != null && item.getAdapterImagePath() != null && !item.getAdapterImagePath().isEmpty()) {
            iDown.add(holder, item.getId(),item.getAdapterImagePath());
        } 
        
		return vi;
	}

}

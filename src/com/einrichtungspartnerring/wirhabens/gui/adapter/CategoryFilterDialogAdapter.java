package com.einrichtungspartnerring.wirhabens.gui.adapter;

import java.util.List;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;

import com.einrichtungspartnerring.wirhabens.R;
import com.einrichtungspartnerring.wirhabens.gui.listener.dialogbuilder.DialogCategoryFilterBoxItemChange;
import com.einrichtungspartnerring.wirhabens.manager.CategoryFilter;

/**
 * Adapter for the filter list in the dialog.
 * 
 * @author Thomas Möller
 * 
 */
public class CategoryFilterDialogAdapter extends BaseAdapter {

	private static final String TAG = "CategoryFilterDialogAdapter";
	private List<CategoryFilter> filters;
	private Activity activity;

	public CategoryFilterDialogAdapter(List<CategoryFilter> filters,
			Activity activity) {
		super();
		Log.d(TAG, "build Adapter");
		this.filters = filters;
		this.activity = activity;
	}

	@Override
	public int getCount() {
		if (filters != null)
			return filters.size();
		return 0;
	}

	@Override
	public Object getItem(int position) {
		return filters.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View vi = convertView;
		if (vi == null) {
			LayoutInflater inflater = (LayoutInflater) activity.getLayoutInflater();
			vi = inflater.inflate(R.layout.category_filter_item, null);
		}
		CategoryFilter currentItem = filters.get(position);
		CheckBox box = (CheckBox) vi.findViewById(R.id.dialog_category_filter_chcekbox);
		box.setText(currentItem.getTitle());
		// Needed because if you set the checked value the listener react.
		box.setOnCheckedChangeListener(null);
		box.setChecked(currentItem.isVisible());
		box.setOnCheckedChangeListener(new DialogCategoryFilterBoxItemChange(
				currentItem));
        if (currentItem.isSticky())
            box.setVisibility(View.GONE);
        else
            box.setVisibility(View.VISIBLE);
		return vi;
	}

}

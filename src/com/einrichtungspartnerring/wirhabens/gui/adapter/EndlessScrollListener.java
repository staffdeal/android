package com.einrichtungspartnerring.wirhabens.gui.adapter;

import android.content.Context;
import android.util.Log;
import android.widget.AbsListView;
import com.einrichtungspartnerring.wirhabens.activityhelper.LoadNextPageAT;
import com.einrichtungspartnerring.wirhabens.manager.MeinEPRSystemManager;

/**
 * Created by loc on 24.07.14.
 */
public class EndlessScrollListener implements AbsListView.OnScrollListener {

    private int visibleThreshold = 1;
    private int previousTotal = 0;
    private boolean loading = true;
    private Context context;
    private PicTextAdapter adapter;
    private MeinEPRSystemManager manager;

    public EndlessScrollListener() {
    }

    public EndlessScrollListener(Context context, PicTextAdapter adapter, MeinEPRSystemManager manager) {
        this.context =  context;
        this.adapter = adapter;
        this.manager = manager;
    }
    public EndlessScrollListener(int visibleThreshold) {
        this.visibleThreshold = visibleThreshold;
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        if (loading) {
            if (totalItemCount > previousTotal) {
                loading = false;
                previousTotal = totalItemCount;
            } else if(previousTotal > totalItemCount) {
            	loading = false;
            	previousTotal = totalItemCount;
            }
        }
        if (!loading  && (totalItemCount - visibleItemCount) <= (firstVisibleItem + visibleThreshold)) {
            new LoadNextPageAT(context, adapter, manager).execute();
            loading = true;
        }
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
    }
}

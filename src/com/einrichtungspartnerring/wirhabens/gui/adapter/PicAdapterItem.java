package com.einrichtungspartnerring.wirhabens.gui.adapter;

/**
 * Adapter item, which can build the tab view.
 * 
 * @author Thomas Möller
 * 
 */
public interface PicAdapterItem extends Comparable<PicAdapterItem>{

	/**
	 * 
	 * @return the color of the background. By news for example is it the
	 *         category color.
	 */
	int getAdapterColor();

	/**
	 * @return the String you want to set in the date field.
	 */
	String getAdapterDate();

	/**
	 * @return the String you want to set in the head field.
	 */
	String getAdapterHead();

	/**
	 * @return the path/name of the image.
	 */
	String getAdapterImagePath();

	/**
	 * @return the String you want to set in the title field.
	 */
	String getAdapterTitle();

	/**
	 * @return the id of the item
	 */
	String getId();

	/**
	 * @return if the element readed from the user before.
	 */
	boolean isReaded();

	/**
	 * @return if the element readed from the user before.
	 */
	void setReaded(boolean readed);
	
	/**
	 * 
	 * @return the id of the category from this object or null if no category set.
	 */
	String getCategoryId();
	
	/**
	 * Value which the element should sorted.
	 * @return
	 */
	Long getSortingValue();

	/**
	 * Type of item to display (new,deals...)
	 * @return
	 */
	int getType();
	
    boolean isFeatured();

}

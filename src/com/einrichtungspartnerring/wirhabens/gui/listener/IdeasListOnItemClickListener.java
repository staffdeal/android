package com.einrichtungspartnerring.wirhabens.gui.listener;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.einrichtungspartnerring.wirhabens.IdeasDetailActivity;
import com.einrichtungspartnerring.wirhabens.gui.adapter.PicAdapterItem;
import com.einrichtungspartnerring.wirhabens.gui.adapter.PicTextAdapter;
import com.einrichtungspartnerring.wirhabens.vo.CompetitionVO;


/**
 * Listener for click events in the ideas list view.
 * @author Thomas Möller
 *
 */
public class IdeasListOnItemClickListener implements OnItemClickListener{


	/**
	 * Context of the activity
	 */
	private Context context;
	private CompetitionVO comVO;
	
	
	public IdeasListOnItemClickListener(Context context, CompetitionVO comVO) {
		super();
		this.context = context;
		this.comVO = comVO;
	}



	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		Intent intent = new Intent(context, IdeasDetailActivity.class);
		ListView listView = (ListView) parent;
		PicTextAdapter adapter = (PicTextAdapter) listView.getAdapter();
		PicAdapterItem item = (PicAdapterItem) adapter.getItem(position);
		intent.putExtra( IdeasDetailActivity.EXTRAS_IDEAS_ID , item.getId());
		intent.putExtra( IdeasDetailActivity.EXTRAS_COMPETITION_ID,  comVO.getId());
		context.startActivity(intent);
	}

}

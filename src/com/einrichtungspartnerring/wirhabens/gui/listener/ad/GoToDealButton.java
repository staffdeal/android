package com.einrichtungspartnerring.wirhabens.gui.listener.ad;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import com.einrichtungspartnerring.wirhabens.DealActivity;
import com.einrichtungspartnerring.wirhabens.manager.DealsManager;
import com.einrichtungspartnerring.wirhabens.vo.DealsItemVO;

/**
 * Created by loc on 17.08.14.
 */
public class GoToDealButton implements View.OnClickListener{

    private Activity activity;
    private DealsItemVO vo;
    private DealsManager manager;
    private View dispView;

    public GoToDealButton(View displayView,Activity activity, DealsItemVO vo, DealsManager manager) {
        this.activity = activity;
        this.vo = vo;
        this.manager = manager;
        this.dispView = displayView;
    }

    @Override
    public void onClick(View v) {
    	dispView.setVisibility(View.GONE);
        Intent intent = new Intent(activity, DealActivity.class);
        intent.putExtra("item", manager.getPositionByItem(vo));
        activity.startActivity(intent);
    }
}

package com.einrichtungspartnerring.wirhabens.gui.listener.ad;

import android.app.Activity;
import android.view.View;

/**
 * Created by loc on 17.08.14.
 */
public class CloseButton implements View.OnClickListener{

    private Activity activity;

    public CloseButton(Activity activity) {
        this.activity = activity;
    }

    @Override
    public void onClick(View v) {
        this.activity.finish();
    }
}

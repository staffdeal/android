package com.einrichtungspartnerring.wirhabens.gui.listener;

import com.einrichtungspartnerring.wirhabens.DealActivity;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

/**
 * Click listener for the third main tab.
 * 
 * @author Thomas Möller
 * 
 */
public class ThirdTabOnItemClickListener implements OnItemClickListener {
	/**
	 * Context of the activity
	 */
	private Context context;

	/**
	 * Initialization constructor
	 * 
	 * @param context
	 *            of the activity
	 */
	public ThirdTabOnItemClickListener(Context context) {
		this.context = context;
	}

	/**
	 * Implementation of the item click listener.
	 */
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,long id) {
		Intent intent = new Intent(context, DealActivity.class);
		intent.putExtra("item", position);
		context.startActivity(intent);
	}

}

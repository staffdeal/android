package com.einrichtungspartnerring.wirhabens.gui.listener.registry;

import android.app.Activity;
import android.app.Dialog;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CheckBox;

import com.einrichtungspartnerring.wirhabens.R;
import com.einrichtungspartnerring.wirhabens.helper.MeinERPDialogBuilder;

public class CheckBoxVerificationListener implements OnClickListener{

	private CheckBox box;
	private String text;
	private Activity activity;



	public CheckBoxVerificationListener(CheckBox box, String text,
			Activity activity) {
		super();
		this.box = box;
		this.text = text;
		this.activity = activity;
	}



	@Override
	public void onClick(View v) {
		if (box.isChecked()){// only show dialog if activate
			CheckBoxAcceptListener acceptListener = new CheckBoxAcceptListener(box);
			CheckBoxResetListener notacceptListener = new CheckBoxResetListener(box);
			Dialog dia = MeinERPDialogBuilder.buildDialogYesNoActionWithHTMLMessage(activity, R.string.registry_dialog_verification, text,
					acceptListener, notacceptListener, R.string.dialog_button_ok);
			acceptListener.setDia(dia);
			notacceptListener.setDia(dia);
			dia.show();
		}
	}
}

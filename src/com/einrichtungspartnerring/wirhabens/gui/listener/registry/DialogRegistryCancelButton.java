package com.einrichtungspartnerring.wirhabens.gui.listener.registry;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;

/**
 * Dialog action button for canceling button.
 * 
 * @author Thomas Möller
 * 
 */
public class DialogRegistryCancelButton implements OnClickListener {

    private Activity activity;

    public DialogRegistryCancelButton(Activity activity) {
        this.activity = activity;
    }

    @Override
	public void onClick(DialogInterface dialog, int which) {
		dialog.cancel();
        this.activity.finish();
	}

}

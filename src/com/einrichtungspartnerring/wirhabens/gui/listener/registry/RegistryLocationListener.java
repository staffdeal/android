package com.einrichtungspartnerring.wirhabens.gui.listener.registry;

import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;

import com.einrichtungspartnerring.wirhabens.RegistryActivity;

public class RegistryLocationListener implements LocationListener{

	private static final String TAG = "RegistryLocationListener";

    private RegistryActivity registryActivity;
    private LocationManager locationManager;

    public RegistryLocationListener(RegistryActivity registryActivity, LocationManager locationManager) {
        this.registryActivity = registryActivity;
        this.locationManager = locationManager;
    }

    @Override
	public void onLocationChanged(Location location) {
		Log.i(TAG, "GPS location change");
        this.registryActivity.setCurrentLocation(location);
	}

	@Override
	public void onProviderDisabled(String provider) {
		Log.i(TAG, "GPS provide disabled");
	}

	@Override
	public void onProviderEnabled(String provider) {
		Log.i(TAG, "GPS provide enabled");
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		Log.i(TAG, "GPS status change");
	}

}

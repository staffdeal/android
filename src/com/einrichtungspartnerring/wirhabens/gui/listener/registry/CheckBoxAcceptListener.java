package com.einrichtungspartnerring.wirhabens.gui.listener.registry;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.widget.CheckBox;
/**
 * Helper for CheckBoxVerificationListener - Accept the box if verification denied. 
 * @author Thomas Möller
 *
 */
public class CheckBoxAcceptListener implements OnClickListener{

	private CheckBox box;
	private Dialog dia;
	
	

	protected CheckBoxAcceptListener(CheckBox box) {
		super();
		this.box = box;
	}



	public void setDia(Dialog dia) {
		this.dia = dia;
	}



	@Override
	public void onClick(DialogInterface dialog, int which) {
		box.setChecked(true);
		dia.cancel();
		
	}

}

package com.einrichtungspartnerring.wirhabens.gui.listener.registry;

import java.util.ArrayList;
import java.util.List;

import android.app.AlertDialog;
import android.location.Location;
import android.location.LocationManager;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CheckBox;
import android.widget.EditText;

import android.widget.Spinner;
import android.widget.TextView;
import com.einrichtungspartnerring.wirhabens.R;
import com.einrichtungspartnerring.wirhabens.RegistryActivity;
import com.einrichtungspartnerring.wirhabens.activityhelper.GPSCoordinationAT;
import com.einrichtungspartnerring.wirhabens.activityhelper.RegistryAT;
import com.einrichtungspartnerring.wirhabens.activityhelper.events.OnGPSCoordinationSearchEnd;
import com.einrichtungspartnerring.wirhabens.helper.GPSHelper;
import com.einrichtungspartnerring.wirhabens.helper.MeinERPDialogBuilder;
import com.einrichtungspartnerring.wirhabens.manager.NetworkManager;

/**
 * Button for registing user. And handling event after new GPS coordination are found.
 * @author Thomas Möller
 *
 */
public class RegistryButtonListener implements OnClickListener, OnGPSCoordinationSearchEnd {

	private static final String TAG = "RegistryButtonListener";

    /**
     * Layout where the registration information are behind.
     */
	private View layout;
	private NetworkManager netManager;
	private RegistryActivity activity;
	private LocationManager locationManager;
    private GPSCoordinationAT at;

	public RegistryButtonListener(View layout, NetworkManager netManager,
                                  RegistryActivity activity, LocationManager locationManager) {
		super();
		this.layout = layout;
		this.netManager = netManager;
		this.activity = activity;
		this.locationManager = locationManager;

	}
    /**
     * Clean errors on the view.
     */
	private void cleanErrors(){
		List<View> errorViews = new ArrayList<View>();
        errorViews.add(layout.findViewById(R.id.registy_condition_error_privacy_policy));
        errorViews.add(layout.findViewById(R.id.registy_condition_terms_of_use));
        errorViews.add(layout.findViewById(R.id.registy_condition_email_require));
        errorViews.add(layout.findViewById(R.id.registy_condition_password_require));
        errorViews.add(layout.findViewById(R.id.registy_condition_firstname_require));
        errorViews.add(layout.findViewById(R.id.registy_condition_lastname_require));

		for (View v : errorViews){
			v.setVisibility(View.GONE);
		}

		
	}

	@Override
	public void onClick(View v) {
		cleanErrors();
		if (isPreConditionSuccessful()){

			if (GPSHelper.isGPSOn(locationManager)) {
                at = new GPSCoordinationAT(activity, this);
                at.execute();

			} else {
				AlertDialog dialog = MeinERPDialogBuilder.buildActivateGPSDialog(activity);
				dialog.show();
			}
		}

	}

    /**
     * Check if pre conditions of sending registration informations are fulfilled.
     * @return true if fulfilled,else false.
     */
	private boolean isPreConditionSuccessful() {
		List<View> errorViews = new ArrayList<View>();
		CheckBox box = (CheckBox)layout.findViewById(R.id.registy_privacy_policy_checkbox);
		if (!box.isChecked()){
            errorViews.add(layout.findViewById(R.id.registy_condition_error_privacy_policy));
		}

		box = (CheckBox)layout.findViewById(R.id.registy_terms_of_use_checkbox);
		if (!box.isChecked()){
            errorViews.add(layout.findViewById(R.id.registy_condition_terms_of_use));
		}

        TextView element = (TextView) layout.findViewById(R.id.registy_firstname_input);
        if (element.getText().toString().isEmpty())
            errorViews.add(layout.findViewById(R.id.registy_condition_firstname_require));

        element = (TextView) layout.findViewById(R.id.registy_lastname_input);
        if (element.getText().toString().isEmpty())
            errorViews.add(layout.findViewById(R.id.registy_condition_lastname_require));

        element = (TextView) layout.findViewById(R.id.registy_email_input);
        if (element.getText().toString().isEmpty())
            errorViews.add(layout.findViewById(R.id.registy_condition_email_require));

        element = (TextView) layout.findViewById(R.id.registy_password_input);
        if (element.getText().toString().isEmpty())
            errorViews.add(layout.findViewById(R.id.registy_condition_password_require));
		
		
		for (View v : errorViews){
			v.setVisibility(View.VISIBLE);
		}
		
		return errorViews.size() > 0 ? false : true;
	}

    @Override
    public void OnGPSCoordinationSearchEnd() {
        Location location = activity.getCurrentLocation();

        if (location != null) {
            Log.i(TAG, "start registing user");
            String latLon = location.getLatitude() + ","
                    + location.getLongitude();
            EditText email = (EditText) layout.findViewById(R.id.registy_email_input);
            EditText password = (EditText) layout.findViewById(R.id.registy_password_input);
            EditText firstname = (EditText) layout.findViewById(R.id.registy_firstname_input);
            EditText lastname = (EditText) layout.findViewById(R.id.registy_lastname_input);
            Spinner dealerSpinner = (Spinner) layout.findViewById(R.id.registy_dealer_spinner);

            int position = dealerSpinner.getSelectedItemPosition();
            String dealerId = "unknown";
            if (netManager != null && netManager.loadCachedDealers() != null && netManager.loadCachedDealers().get(position) != null)
                 dealerId = netManager.loadCachedDealers().get(position).getId();


            new RegistryAT(netManager, email.getText().toString(), password
                    .getText().toString(), firstname.getText().toString(),
                    lastname.getText().toString(), latLon,dealerId, activity).execute();
        } else {
            Log.i(TAG, "no gps position found.");
            AlertDialog dialog = MeinERPDialogBuilder.buildInformationDialog(activity, R.string.registry_gps_nofound_title,R.string.registry_gps_nofound_text);
            dialog.show();
        }
    }
}

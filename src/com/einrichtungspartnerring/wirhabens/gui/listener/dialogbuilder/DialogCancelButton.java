package com.einrichtungspartnerring.wirhabens.gui.listener.dialogbuilder;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;

/**
 * Dialog action button for canceling button.
 * 
 * @author Thomas Möller
 * 
 */
public class DialogCancelButton implements OnClickListener {

	@Override
	public void onClick(DialogInterface dialog, int which) {
		dialog.cancel();
	}

}

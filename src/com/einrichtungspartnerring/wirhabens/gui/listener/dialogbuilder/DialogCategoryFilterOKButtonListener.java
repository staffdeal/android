package com.einrichtungspartnerring.wirhabens.gui.listener.dialogbuilder;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;

import com.einrichtungspartnerring.wirhabens.MainActivity;
import com.einrichtungspartnerring.wirhabens.manager.MeinEPRSystemManager;

/**
 * Listener for clicking event on filters.
 * 
 * @author Thomas Möller
 * 
 */
public class DialogCategoryFilterOKButtonListener implements OnClickListener {

	private MainActivity activity;
	private MeinEPRSystemManager<?> manager;
	private int reloadTab;
	
	
	
	public DialogCategoryFilterOKButtonListener(MainActivity activity,
			MeinEPRSystemManager<?> manager, int reloadTab) {
		super();
		this.activity = activity;
		this.manager = manager;
		this.reloadTab = reloadTab;
	}



	@Override
	public void onClick(DialogInterface dialog, int which) {
		manager.setFilters(manager.getCategories());
		activity.reload(reloadTab, false);
	}

}

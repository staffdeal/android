package com.einrichtungspartnerring.wirhabens.gui.listener.dialogbuilder;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.provider.Settings;

import com.einrichtungspartnerring.wirhabens.RegistryActivity;

/**
 * Button which intent to setting menu for GPS.
 * 
 * @author Thomas Möller
 * 
 */
public class DialogActivateGPSActiveButton implements OnClickListener {

	private Activity activity;

	public DialogActivateGPSActiveButton(Activity activity) {
		super();
		this.activity = activity;
	}

	@Override
	public void onClick(DialogInterface dialog, int which) {
		Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
		activity.startActivityForResult(intent, RegistryActivity.REQUEST_GPS_SETTING_CODE);
	}

}

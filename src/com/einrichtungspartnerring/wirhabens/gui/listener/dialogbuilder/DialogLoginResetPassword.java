package com.einrichtungspartnerring.wirhabens.gui.listener.dialogbuilder;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;

import com.einrichtungspartnerring.wirhabens.ResetPasswordActivity;

/**
 * Event for forget passwords.
 * @author Thomas Möller
 *
 */
public class DialogLoginResetPassword implements OnClickListener{

	private Activity activity;
	
	public DialogLoginResetPassword(Activity activity) {
		this.activity = activity;
	}

	@Override
	public void onClick(View v) {
		Intent intent = null;
		intent = new Intent(activity, ResetPasswordActivity.class);
		activity.startActivity(intent);
		
	}

}

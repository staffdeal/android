package com.einrichtungspartnerring.wirhabens.gui.listener.dialogbuilder;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.util.Log;

/**
 * Listener for the dialog filter, when click on one item.
 * 
 * @author Thomas Möller
 * 
 */
public class DialogCategoryFilterItemListListener implements OnClickListener {

	private static final String TAG = "DialogCategoryFilterItemListListener";

	@Override
	public void onClick(DialogInterface dialog, int which) {
		Log.i(TAG, "get which : " + which);
	}

}

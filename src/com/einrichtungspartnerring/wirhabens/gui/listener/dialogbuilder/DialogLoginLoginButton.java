package com.einrichtungspartnerring.wirhabens.gui.listener.dialogbuilder;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.widget.EditText;
import android.widget.TextView;

import com.einrichtungspartnerring.wirhabens.R;
import com.einrichtungspartnerring.wirhabens.activityhelper.LoginAT;
import com.einrichtungspartnerring.wirhabens.activityhelper.events.OnTaskLoginCompleteEvent;
import com.einrichtungspartnerring.wirhabens.manager.NetworkManager;

/**
 * Action for login in the login dialog.
 * 
 * @author Thomas Möller
 * 
 */
public class DialogLoginLoginButton implements OnClickListener {

	private Activity activity;
	private NetworkManager netManager;
	private OnTaskLoginCompleteEvent eventHandler;
	private TextView message;

	public DialogLoginLoginButton(Activity activity, NetworkManager netManager, OnTaskLoginCompleteEvent eventHandler
			,TextView message) {
		this.activity = activity;
		this.netManager = netManager;
		this.eventHandler = eventHandler;
		this.message = message;
	}

	@Override
	public void onClick(DialogInterface dialog, int which) {
		AlertDialog altDialog = (AlertDialog) dialog;
		
		EditText username = (EditText) ((AlertDialog) dialog)
				.findViewById(R.id.dialog_login_username);
		EditText password = (EditText) ((AlertDialog) dialog)
				.findViewById(R.id.dialog_login_password);
		new LoginAT(activity, netManager, username.getText().toString(),
				password.getText().toString(), eventHandler, altDialog, message).execute();

	}

}

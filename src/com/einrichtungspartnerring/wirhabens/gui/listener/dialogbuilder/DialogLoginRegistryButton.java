package com.einrichtungspartnerring.wirhabens.gui.listener.dialogbuilder;

import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;

import com.einrichtungspartnerring.wirhabens.RegistryActivity;

/**
 * Action for the registry button in login dialog.
 * 
 * @author Thomas Möller
 * 
 */
public class DialogLoginRegistryButton implements OnClickListener {

	/**
	 * Context of the activity you call from.
	 */
	private Context context;

	public DialogLoginRegistryButton(Context context) {
		super();
		this.context = context;
	}

	@Override
	public void onClick(DialogInterface dialog, int which) {
		Intent intent = new Intent(context, RegistryActivity.class);
		context.startActivity(intent);
	}

}

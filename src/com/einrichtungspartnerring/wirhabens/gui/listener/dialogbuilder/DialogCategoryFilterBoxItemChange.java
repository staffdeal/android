package com.einrichtungspartnerring.wirhabens.gui.listener.dialogbuilder;

import android.util.Log;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;

import com.einrichtungspartnerring.wirhabens.manager.CategoryFilter;

/**
 * Reaction of a change in the CategoryFilterDialogAdapter
 * 
 * @author Thomas Möller
 * 
 */
public class DialogCategoryFilterBoxItemChange implements
		OnCheckedChangeListener {

	private static final String TAG = "DialogCategoryFilterBoxItemChange";

	private CategoryFilter currentItem;

	public DialogCategoryFilterBoxItemChange(CategoryFilter currentItem) {
		super();
		this.currentItem = currentItem;
	}

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		Log.d(TAG, "Change item " + currentItem.getTitle() + " to " + isChecked);
		currentItem.setVisible(isChecked);

	}

}

package com.einrichtungspartnerring.wirhabens.gui.listener;


import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;

import com.einrichtungspartnerring.wirhabens.MainActivity;
import com.einrichtungspartnerring.wirhabens.R;
import com.einrichtungspartnerring.wirhabens.StaffsaleApplication;
import com.einrichtungspartnerring.wirhabens.helper.MeinERPDialogBuilder;
import com.einrichtungspartnerring.wirhabens.properties.MeinERPProperties;
import com.einrichtungspartnerring.wirhabens.properties.PropertiesList;


/**
 * Handling by clicking on the media button in the main view. 
 * @author Thomas Möller
 *
 */
public class MediaButtonListener implements ActionBar.TabListener {

	private MeinERPProperties properties;
	private Activity activity;
	private android.content.DialogInterface.OnClickListener youtubeButton = new android.content.DialogInterface.OnClickListener() {
		
		@Override
		public void onClick(DialogInterface dialog, int which) {
			((StaffsaleApplication) activity.getApplication()).trackScreen(activity.getString(R.string.ga_screen_youtube));
			String url = properties.get(PropertiesList.API_YOUTUBE_MEDIA_URL);
			if(url != null) {
				Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(Uri.parse(url));
				changeTab(activity);
				activity.startActivity(i);
			}else{
				 AlertDialog.Builder builder = new AlertDialog.Builder(activity);
			        builder.setMessage(R.string.media_not_available)
			               .setPositiveButton(R.string.dialog_button_ok, new DialogInterface.OnClickListener() {
			                   public void onClick(DialogInterface dialog, int id) {
			                       // FIRE ZE MISSILES!
			                   }
			               });
			        // Create the AlertDialog object and return it
			        builder.create().show();
			}			
		}
	};

    private android.content.DialogInterface.OnClickListener facebookButton = new android.content.DialogInterface.OnClickListener() {

        @Override
        public void onClick(DialogInterface dialog, int which) {
        	((StaffsaleApplication) activity.getApplication()).trackScreen(activity.getString(R.string.ga_screen_youtube));
            String url = properties.get(PropertiesList.API_FACEBOOK_MEDIA_URL);
            if(url != null) {
            	Intent i = new Intent(Intent.ACTION_VIEW);
            	i.setData(Uri.parse(url));
            	changeTab(activity);
            	activity.startActivity(i);	
            }else{
            	AlertDialog.Builder builder = new AlertDialog.Builder(activity);
		        builder.setMessage(R.string.media_not_available)
		               .setPositiveButton(R.string.dialog_button_ok, new DialogInterface.OnClickListener() {
		                   public void onClick(DialogInterface dialog, int id) {
		                       // FIRE ZE MISSILES!
		                   }
		               });
		        // Create the AlertDialog object and return it
		        builder.create().show();
            }	
        }
    };
    
    private android.content.DialogInterface.OnClickListener cancelButton = new android.content.DialogInterface.OnClickListener() {

        @Override
        public void onClick(DialogInterface dialog, int which) {
        	dialog.cancel();
        	changeTab(activity);
        }
    };
	
	/**
	 * Init constructor
	 * @param properties for your system.
	 * @param activity you want to show the dialog.
	 */
	public MediaButtonListener(MeinERPProperties properties, Activity activity) {
		this.properties = properties;
		this.activity = activity;
	}

	@Override
	public void onTabReselected(Tab tab, FragmentTransaction ft) {
		Dialog dia = MeinERPDialogBuilder.buildDialogYesNoCancelActionWithText(activity, R.string.media_button_title, R.string.media_button_text,
                R.string.media_button_youtube_text, R.string._media_button_facebook_text, facebookButton, youtubeButton,cancelButton);
		dia.show();
	}

	@Override
	public void onTabSelected(Tab tab, FragmentTransaction ft) {
		Dialog dia = MeinERPDialogBuilder.buildDialogYesNoCancelActionWithText(activity, R.string.media_button_title, R.string.media_button_text,
                R.string.media_button_youtube_text, R.string._media_button_facebook_text, facebookButton, youtubeButton,cancelButton);
		dia.show();
		
	}

	@Override
	public void onTabUnselected(Tab tab, FragmentTransaction ft) {
	}
	
    public static void changeTab(Activity activity) {
    	((MainActivity) activity).changeTab(((MainActivity) activity).getmCurrentTab());
    	Log.d("TAB","Changing tab " + ((MainActivity) activity).getmCurrentTab());
    }

}

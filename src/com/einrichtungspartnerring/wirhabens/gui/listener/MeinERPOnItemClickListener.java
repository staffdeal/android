package com.einrichtungspartnerring.wirhabens.gui.listener;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

import com.einrichtungspartnerring.wirhabens.NewsDetailActivity;

/**
 * Click listener for the first main tab.
 * 
 * @author Thomas Möller
 * 
 */
public class MeinERPOnItemClickListener implements OnItemClickListener {

	/**
	 * Context of the activity
	 */
	private Context context;

	/**
	 * Initialization constructor
	 * 
	 * @param context
	 *            of the activity 
	 */
	public MeinERPOnItemClickListener(Context context) {
		this.context = context;
	}

	/**
	 * Implementation of the item click listener.
	 */
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		Log.d("TAG","Clicked position " +  position);
		Intent intent = new Intent(context, NewsDetailActivity.class);
		intent.putExtra("newsitempos", position);
		context.startActivity(intent);

	}

}

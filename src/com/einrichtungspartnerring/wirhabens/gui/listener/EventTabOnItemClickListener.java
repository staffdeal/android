package com.einrichtungspartnerring.wirhabens.gui.listener;

import com.einrichtungspartnerring.wirhabens.EventDetailActivity;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

public class EventTabOnItemClickListener implements OnItemClickListener {
	/**
	 * Context of the activity
	 */
	private Context context;

	/**
	 * Initialization constructor
	 * 
	 * @param context
	 *            of the activity
	 */
	public EventTabOnItemClickListener(Context context) {
		this.context = context;
	}
	
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,long id) {
		// TODO Auto-generated method stub
		Intent intent = new Intent(context, EventDetailActivity.class);
		intent.putExtra("eventitempos", position);
		context.startActivity(intent);
	}

}

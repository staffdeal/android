package com.einrichtungspartnerring.wirhabens.gui.listener.settings;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.TextView;

import com.einrichtungspartnerring.wirhabens.R;
import com.einrichtungspartnerring.wirhabens.manager.NetworkManager;

public class SettingNewPasswordButtonListener implements OnClickListener {

	private Activity activity;
	private NetworkManager netManager;
	private AlertDialog dialog;

	public SettingNewPasswordButtonListener( Activity activity, NetworkManager netManager) {
		super();
		this.activity = activity;
		this.netManager = netManager;
	}

	private android.content.DialogInterface.OnClickListener cancelButton= new android.content.DialogInterface.OnClickListener() {

		@Override
		public void onClick(DialogInterface dialog, int which) {
			dialog.cancel();
		}
	};

	@Override
	public void onClick(View v) {
		AlertDialog.Builder builder = new AlertDialog.Builder(activity);
		LayoutInflater inflater = activity.getLayoutInflater();
		View view = inflater.inflate(R.layout.dialog_change_password, null);
		EditText oldpassText = (EditText) view.findViewById(R.id.dialog_changepassword_oldpass);
		EditText newpassText = (EditText )view.findViewById(R.id.dialog_changepassword_newpass);
		EditText newpassText2 = (EditText )view.findViewById(R.id.dialog_changepassword_newpass2);
		TextView messageField = (TextView) view.findViewById(R.id.dialog_changepassword_message);
		
		builder.setView(view);
		builder.setMessage(R.string.dialog_changepassword_body_messages);
		builder.setTitle(R.string.dialog_changepassword_tile);
		builder.setPositiveButton(R.string.dialog_button_ok, new ChangePasswordSubmitDialogButtonListener(activity, netManager, oldpassText,
				newpassText,newpassText2, messageField));
		builder.setNegativeButton(R.string.dialog_button_cancel, cancelButton);
		builder.setCancelable(false);
		dialog = builder.create();
		dialog.show();

	}
}

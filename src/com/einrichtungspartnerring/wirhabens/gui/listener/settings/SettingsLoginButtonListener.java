package com.einrichtungspartnerring.wirhabens.gui.listener.settings;

import android.app.Dialog;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;

import com.einrichtungspartnerring.wirhabens.SettingsActivity;
import com.einrichtungspartnerring.wirhabens.helper.MeinERPDialogBuilder;
import com.einrichtungspartnerring.wirhabens.manager.NetworkManager;

public class SettingsLoginButtonListener implements OnClickListener{

	private SettingsActivity activity;
	private NetworkManager netManager;
	
	private static final String TAG = "SettingsLoginButton";
	
	public SettingsLoginButtonListener(SettingsActivity activity, NetworkManager netManager){
		this.activity = activity;
		this.netManager = netManager;
	}
	
	@Override
	public void onClick(View v) {
		if (!netManager.isAuthenticated()) {
			Log.i(TAG, "User is not authenticat.");
			Dialog loginDialog = MeinERPDialogBuilder.buildAuthenticatDialog(activity, netManager, activity);
			loginDialog.show();
		}
		
	}

	
}

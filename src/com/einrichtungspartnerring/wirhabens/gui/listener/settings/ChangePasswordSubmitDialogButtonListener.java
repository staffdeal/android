package com.einrichtungspartnerring.wirhabens.gui.listener.settings;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.widget.EditText;
import android.widget.TextView;

import com.einrichtungspartnerring.wirhabens.R;
import com.einrichtungspartnerring.wirhabens.activityhelper.ChangePasswordAT;
import com.einrichtungspartnerring.wirhabens.manager.NetworkManager;


/**
 * Listener for sending data to change the password.
 * @author Thomas Möller
 *
 */
public class ChangePasswordSubmitDialogButtonListener implements OnClickListener{

	private Activity activity;
	private NetworkManager netManager;
	private EditText oldPasswordText;
	private EditText newPasswordText;
	private EditText newPasswordText2;
	private TextView message;
	
	
	

	protected ChangePasswordSubmitDialogButtonListener(Activity activity,
			NetworkManager netManager, EditText oldPasswordText,
			EditText newPasswordText, EditText newPasswordText2, TextView message) {
		super();
		this.activity = activity;
		this.netManager = netManager;
		this.oldPasswordText = oldPasswordText;
		this.newPasswordText = newPasswordText;
		this.message = message;
		this.newPasswordText2 = newPasswordText2;
	}




	@Override
	public void onClick(DialogInterface dialog, int which) {
		AlertDialog aDialog =  (AlertDialog) dialog;
		
		ProgressDialog waitDialog = ProgressDialog.show(activity, activity.getResources().getString(R.string.dialog_loading_title),
				activity.getResources().getString(R.string.dialog_loading_text), true);
		waitDialog.show();
		String oldPass = oldPasswordText.getText().toString();
		String newPass = newPasswordText.getText().toString();
		String newPass2 = newPasswordText2.getText().toString();
		new ChangePasswordAT(activity, netManager, newPass, newPass2, oldPass, message, waitDialog, aDialog).execute();
		
	}

}

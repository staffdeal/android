package com.einrichtungspartnerring.wirhabens.gui.listener.settings;

import android.app.Activity;
import android.util.Log;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;

import com.einrichtungspartnerring.wirhabens.R;
import com.einrichtungspartnerring.wirhabens.helper.MeinERPDialogBuilder;
import com.einrichtungspartnerring.wirhabens.manager.NetworkManager;
import com.einrichtungspartnerring.wirhabens.properties.MeinERPProperties;
import com.einrichtungspartnerring.wirhabens.properties.PropertiesList;

/**
 * Action for the GCM button in the setting menu.
 * 
 * @author Thomas Möller
 * 
 */
public class SettingGCMNotificationButton implements OnCheckedChangeListener {

	private MeinERPProperties prop;
	private NetworkManager netManager;
	private Activity activity;

	private static final String TAG = "SettingGCMNotificationButton";

	public SettingGCMNotificationButton(MeinERPProperties prop,
			NetworkManager netManager, Activity activity) {
		this.netManager = netManager;
		this.prop = prop;
		this.activity = activity;
	}

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		String gcmid = null;
		gcmid = netManager.getGMCId();

		if (gcmid != null && !isChecked) { // Case system is registered
			Log.i(TAG, "unregistration on GCM");
			netManager.unregistGCM(activity);
			prop.remove(PropertiesList.GMC_ACTIVE);
		} else { // Case system is unregistered
			Log.i(TAG, "registration on GCM");
			try {
				netManager.registGCM(activity, prop);
			} catch (UnsupportedOperationException e) {
				Log.w(TAG, "This device didn't support GCM.");
				MeinERPDialogBuilder
						.buildInformationDialog(
								activity,
								activity.getString(R.string.settings_notifcation_error_dialog_title),
								activity.getString(R.string.settings_notifcation_error_dialog_text));
			}
			prop.set(PropertiesList.GMC_ACTIVE, "true");
		}

	}

}

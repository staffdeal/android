package com.einrichtungspartnerring.wirhabens.gui.listener.settings;

import android.app.Dialog;
import android.content.DialogInterface;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Toast;

import com.einrichtungspartnerring.wirhabens.R;
import com.einrichtungspartnerring.wirhabens.SettingsActivity;
import com.einrichtungspartnerring.wirhabens.helper.MeinERPDialogBuilder;
import com.einrichtungspartnerring.wirhabens.helper.Rights.Level;
import com.einrichtungspartnerring.wirhabens.manager.NetworkManager;
import com.einrichtungspartnerring.wirhabens.properties.MeinERPProperties;
import com.einrichtungspartnerring.wirhabens.properties.PropertiesList;

/**
 * Button action for deleting all user data from system.
 * @author Thomas Möller
 *
 */
public class SettingsResetButtonListener implements OnClickListener, PropertiesList {

	private NetworkManager netManager;
	private MeinERPProperties prop;
	private SettingsActivity actitity;

	public SettingsResetButtonListener(NetworkManager netManager,
			MeinERPProperties prop,SettingsActivity actitity ) {
		super();
		this.netManager = netManager;
		this.prop = prop;
		this.actitity = actitity;
	}


	private android.content.DialogInterface.OnClickListener delete = new android.content.DialogInterface.OnClickListener() {

		@Override
		public void onClick(DialogInterface dialog, int which) {
			
			prop.set(NEWS_RIGHTS, Level.guest.toString());
			prop.set(IDEAS_RIGHTS, Level.guest.toString());
			prop.set(DEALS_RIGHTS, Level.guest.toString());
			prop.set(USER_RIGHT, Level.guest.toString());
			netManager.deleteAuthentication();
			prop.remove(USER_TOKEN);
			prop.remove(USER_E_MAIL);
			prop.remove(USER_PASS);
			int duration = Toast.LENGTH_SHORT;
			Toast toast = Toast.makeText(actitity, R.string.settings_dialog_confirm_delete_all_data_endtoast, duration);
			toast.show();
			//TODO delete all other datas
			actitity.onTaskLoginCompleteEvent();
		}
	};
	

	@Override
	public void onClick(View v) {
		Dialog dialog = MeinERPDialogBuilder.buildDialogGivenYesAction(actitity, actitity.getString(R.string.settings_dialog_confirm_delete_all_data_title)
				, actitity.getString(R.string.settings_dialog_confirm_delete_all_data_text), delete, 
				actitity.getString(R.string.dialog_button_ok));
		dialog.show();
		
	}
}

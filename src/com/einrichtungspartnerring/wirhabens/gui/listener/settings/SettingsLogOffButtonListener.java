package com.einrichtungspartnerring.wirhabens.gui.listener.settings;

import android.view.View;
import android.view.View.OnClickListener;

import com.einrichtungspartnerring.wirhabens.activityhelper.events.OnTaskLoginCompleteEvent;
import com.einrichtungspartnerring.wirhabens.helper.Rights.Level;
import com.einrichtungspartnerring.wirhabens.manager.NetworkManager;
import com.einrichtungspartnerring.wirhabens.properties.MeinERPProperties;
import com.einrichtungspartnerring.wirhabens.properties.PropertiesList;

/**
 * Log off the user from the system.
 * @author Thomas Möller
 *
 */
public class SettingsLogOffButtonListener implements OnClickListener, PropertiesList{

	
	private NetworkManager netManager;
	private MeinERPProperties prop;
	private OnTaskLoginCompleteEvent eventHandler;

	public SettingsLogOffButtonListener(NetworkManager netManager,
			MeinERPProperties prop, OnTaskLoginCompleteEvent eventHandler) {
		super();
		this.netManager = netManager;
		this.prop = prop;
		this.eventHandler = eventHandler;
	}

	@Override
	public void onClick(View v) {		
		prop.set(USER_RIGHT, Level.guest.toString());
		prop.remove(USER_TOKEN);
		prop.remove(USER_PASS);
		netManager.deleteAuthentication();
		if (eventHandler != null){
			eventHandler.onTaskLoginCompleteEvent();
		}
	}
}

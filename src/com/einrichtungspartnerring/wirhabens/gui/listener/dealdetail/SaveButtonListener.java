package com.einrichtungspartnerring.wirhabens.gui.listener.dealdetail;

import android.app.AlertDialog;
import android.view.View;
import android.view.View.OnClickListener;

import com.einrichtungspartnerring.wirhabens.DealActivity;
import com.einrichtungspartnerring.wirhabens.R;
import com.einrichtungspartnerring.wirhabens.StaffsaleApplication;
import com.einrichtungspartnerring.wirhabens.helper.MeinERPDialogBuilder;
import com.einrichtungspartnerring.wirhabens.manager.DealsManager;
import com.einrichtungspartnerring.wirhabens.manager.NetworkManager;
import com.einrichtungspartnerring.wirhabens.vo.DealsItemVO;

/**
 * Listener for the save button on the deals view.
 * @author Thomas Möller
 *
 */
public class SaveButtonListener implements OnClickListener{

    public enum ListenerSaveSource{
        onsite, online
    }

	private NetworkManager netManager;
	private DealActivity activity;
	private android.content.DialogInterface.OnClickListener yesButton;
	

	public SaveButtonListener(NetworkManager netManager, DealActivity activity, DealsItemVO vo, DealsManager dealManager, ListenerSaveSource source) {
        super();
        this.netManager = netManager;
        this.activity = activity;
        if (source == ListenerSaveSource.online) {
            yesButton = new SaveOnlineButtonAcceptButtonDialogListener(vo, activity, dealManager, activity, netManager);
        } else {
            yesButton = new SaveOnSiteButtonAcceptButtonDialogListener(vo, activity, dealManager, activity, netManager);
        }
        ((StaffsaleApplication) activity.getApplication()).trackEvent(activity.getString(R.string.ga_category_deals),activity.getString(R.string.ga_action_save),vo.getId());	

    }

	@Override
	public void onClick(View v) {
		String message = "No information";
		if (netManager.getInfo() != null && netManager.getInfo().getRegistration() != null)
			message = netManager.getInfo().getRegistration().getTermsOfUse();
		String title = activity.getResources().getString(R.string.deal_save_dialog_police_title);
		String buttonName = activity.getResources().getString(R.string.deal_save_dialog_police_accecpt_button);
		AlertDialog dialog = MeinERPDialogBuilder.buildDialogGivenYesActionHTMLMessage(activity,title, message, yesButton, buttonName);
		dialog.show();
	}
	

}

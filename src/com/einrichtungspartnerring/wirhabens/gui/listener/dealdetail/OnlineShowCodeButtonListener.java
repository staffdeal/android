package com.einrichtungspartnerring.wirhabens.gui.listener.dealdetail;

import android.app.Activity;
import android.app.Dialog;
import android.view.View;
import com.einrichtungspartnerring.wirhabens.helper.MeinERPDialogBuilder;
import com.einrichtungspartnerring.wirhabens.vo.DealsItemVO;

/**
 * Created by loc on 31.07.14.
 */
public class OnlineShowCodeButtonListener implements View.OnClickListener {

    private Activity activity;
    private DealsItemVO vo;

    public OnlineShowCodeButtonListener(Activity activity, DealsItemVO vo) {
        this.activity = activity;
        this.vo = vo;
    }

    @Override
    public void onClick(View v) {
        Dialog dia = MeinERPDialogBuilder.buildInformationDialog(activity, null, vo.getPassOnlineCode());
        dia.show();

    }
}

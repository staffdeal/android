package com.einrichtungspartnerring.wirhabens.gui.listener.dealdetail;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;

import com.einrichtungspartnerring.wirhabens.activityhelper.DealGrabberAT;
import com.einrichtungspartnerring.wirhabens.activityhelper.events.OnTaskDialogAcceptEvent;
import com.einrichtungspartnerring.wirhabens.manager.DealsManager;
import com.einrichtungspartnerring.wirhabens.manager.NetworkManager;
import com.einrichtungspartnerring.wirhabens.vo.DealsItemVO;

/**
 * Listener for acception the privacy police on saving deals.
 * @author Thomas Möller
 *
 */
public class SaveOnlineButtonAcceptButtonDialogListener implements OnClickListener{

	private DealsItemVO vo;
	private OnTaskDialogAcceptEvent event;
	private DealsManager dealManager;
    private Activity activity;
    private NetworkManager networkManager;


    public SaveOnlineButtonAcceptButtonDialogListener(DealsItemVO vo, OnTaskDialogAcceptEvent event, DealsManager dealManager, Activity activity, NetworkManager networkManager) {
        this.vo = vo;
        this.event = event;
        this.dealManager = dealManager;
        this.activity = activity;
        this.networkManager = networkManager;
    }

    @Override
	public void onClick(DialogInterface dialog, int which) {
		dealManager.setToSaveOnlineDeal(vo);
		if (event != null)
			event.onTaskCompleted(vo);
        new DealGrabberAT(networkManager,vo, activity, vo.getPassOnlineUrl(), dealManager ).execute();
	}

}

package com.einrichtungspartnerring.wirhabens.gui.listener.dealdetail;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;

import com.einrichtungspartnerring.wirhabens.GeneralDetailsActivity;
import com.einrichtungspartnerring.wirhabens.R;
import com.einrichtungspartnerring.wirhabens.vo.DealsItemVO;

/**
 * Description button for deal view
 * @author Thomas Möller
 *
 */
public class DescButtonListener implements OnClickListener{


	/**
	 * Context of the activity
	 */
	private Context context;
	
	/**
	 * Item of deal you want description showing.
	 */
	private DealsItemVO dealVO;
	
	
	/**
	 * Init constructor.
	 * @param context
	 * @param dealVO
	 */
	public DescButtonListener(Context context, DealsItemVO dealVO) {
		super();
		this.context = context;
		this.dealVO = dealVO;
	}


	@Override
	public void onClick(View v) {
		SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy", Locale.GERMANY);
		
		Intent intent = new Intent(context, GeneralDetailsActivity.class);
		intent.putExtra(GeneralDetailsActivity.FILENAME,"dealDescription.html");
		intent.putExtra(GeneralDetailsActivity.DESC_TITLE, dealVO.getTitle());
		intent.putExtra(GeneralDetailsActivity.DESC_CONTENT, dealVO.getText());
		intent.putExtra(GeneralDetailsActivity.DESC_COMPANY, dealVO.getProvider());
		
		Date date = dealVO.getStartDate();
		if (date != null){
			intent.putExtra(GeneralDetailsActivity.DESC_DATE_FROM, df.format(date));
		}
		date = dealVO.getEndDate();
		if (date != null){
			intent.putExtra(GeneralDetailsActivity.DESC_DATE_UNTIL, " - "+df.format(date));
		}
		intent.putExtra(GeneralDetailsActivity.TITLE, context.getString(R.string.description_text));
		context.startActivity(intent);
		
	}

}

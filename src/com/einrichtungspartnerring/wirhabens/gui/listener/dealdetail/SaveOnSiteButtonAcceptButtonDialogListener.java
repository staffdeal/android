package com.einrichtungspartnerring.wirhabens.gui.listener.dealdetail;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;

import com.einrichtungspartnerring.wirhabens.activityhelper.DealGrabberAT;
import com.einrichtungspartnerring.wirhabens.activityhelper.events.OnTaskDialogAcceptEvent;
import com.einrichtungspartnerring.wirhabens.manager.DealsManager;
import com.einrichtungspartnerring.wirhabens.manager.NetworkManager;
import com.einrichtungspartnerring.wirhabens.vo.DealsItemVO;

/**
 * Created by loc on 28.07.14.
 */
public class SaveOnSiteButtonAcceptButtonDialogListener implements OnClickListener {

    private DealsItemVO vo;
    private OnTaskDialogAcceptEvent event;
    private DealsManager dealManager;
    private Activity activity;
    private NetworkManager networkManager;


    public SaveOnSiteButtonAcceptButtonDialogListener(DealsItemVO vo, OnTaskDialogAcceptEvent event, DealsManager dealManager, Activity activity, NetworkManager networkManager) {
        this.vo = vo;
        this.event = event;
        this.dealManager = dealManager;
        this.activity = activity;
        this.networkManager = networkManager;
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        dealManager.setToSaveOnSiteDeal(vo);
        if (event != null)
            event.onTaskCompleted(vo);
        new DealGrabberAT(networkManager,vo, activity, vo.getPassOnlineUrl() , dealManager).execute();
    }
}

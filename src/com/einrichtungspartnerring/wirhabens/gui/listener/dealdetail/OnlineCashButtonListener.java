package com.einrichtungspartnerring.wirhabens.gui.listener.dealdetail;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.view.View.OnClickListener;

import com.einrichtungspartnerring.wirhabens.properties.PropertiesList;
import com.einrichtungspartnerring.wirhabens.vo.DealsItemVO;

/**
 * Listener for the description button
 * 
 * @author Thomas Möller
 * 
 */
public class OnlineCashButtonListener implements OnClickListener {

	/**
	 * Context of the activity
	 */
	private Activity activity;

	/**
	 * Competition you add the description.
	 */
	private DealsItemVO deal;

	public OnlineCashButtonListener(Activity activity, DealsItemVO deal) {
		this.activity = activity;
		this.deal = deal;
	}

	@Override
	public void onClick(View v) {
        String url = deal.getOnlineUrl();
        if (deal.getPassOnlineCode() != null && !deal.getPassOnlineCode().isEmpty())
            url = url.replace("__CODE__", deal.getPassOnlineCode());
        if(url != null && !url.isEmpty()) {
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(url));
            activity.startActivity(i);
        }
	}

}

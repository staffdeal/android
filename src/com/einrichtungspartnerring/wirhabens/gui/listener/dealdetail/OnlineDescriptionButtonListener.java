package com.einrichtungspartnerring.wirhabens.gui.listener.dealdetail;

import android.app.Activity;
import android.app.Dialog;
import android.view.View;
import com.einrichtungspartnerring.wirhabens.R;
import com.einrichtungspartnerring.wirhabens.helper.MeinERPDialogBuilder;

/**
 * Created by loc on 31.07.14.
 */
public class OnlineDescriptionButtonListener implements View.OnClickListener {

    private String html;
    private Activity activity;

    public OnlineDescriptionButtonListener(String html, Activity activity) {
        this.html = html;
        this.activity = activity;
    }

    @Override
    public void onClick(View v) {
        Dialog dia = MeinERPDialogBuilder.buildInformationDialogWithHTMLMessage(activity, R.string.deal_online_description_title, html);
        dia.show();
    }
}

package com.einrichtungspartnerring.wirhabens.gui.listener.dealdetail;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;

import com.einrichtungspartnerring.wirhabens.MapActivity;
import com.einrichtungspartnerring.wirhabens.vo.DealsItemVO;

/**
 * Listener for the description button
 * 
 * @author Thomas Möller
 * 
 */
public class SearchStoreButtonListener implements OnClickListener {

	private static final String TAG = "SearchStoreButtonListener";

	/**
	 * Context of the activity
	 */
	private Activity activity;

	/**
	 * Competition you add the description.
	 */
	private DealsItemVO deal;

	/**
	 * Init constructor.
	 * 
	 * @param context
	 * @param competition
	 */
	public SearchStoreButtonListener(Activity activity, DealsItemVO deal) {
		this.activity = activity;
		this.deal = deal;
	}

	@Override
	public void onClick(View v) {
		Log.d(TAG, "start the map view");
		Intent intent = new Intent(activity, MapActivity.class);
		intent.putExtra(MapActivity.EXTRA_DEAL_ID, deal.getId());
		activity.startActivity(intent);
	}

}

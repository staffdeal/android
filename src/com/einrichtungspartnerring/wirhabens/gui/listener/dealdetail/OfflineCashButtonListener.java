package com.einrichtungspartnerring.wirhabens.gui.listener.dealdetail;

import android.app.Activity;
import android.view.View;
import android.view.View.OnClickListener;

import com.einrichtungspartnerring.wirhabens.R;
import com.einrichtungspartnerring.wirhabens.activityhelper.BarcodeGrabberAT;
import com.einrichtungspartnerring.wirhabens.vo.DealsItemVO;

/**
 * Listener for the online cash button
 * 
 * @author Thomas Möller
 * 
 */
public class OfflineCashButtonListener implements OnClickListener {

	/**
	 * Context of the activity
	 */
	private Activity activity;

	/**
	 * Competition you add the description.
	 */
	private DealsItemVO deal;

	public OfflineCashButtonListener(Activity activity, DealsItemVO deal) {
		this.activity = activity;
		this.deal = deal;
	}

	@Override
	public void onClick(View v) {
        int titletext =  R.string.dialog_barcode_barcodeImage_title;
        new BarcodeGrabberAT(activity, deal.getId(), deal.getPassOnSiteUrl(), titletext).execute();

	}

}

package com.einrichtungspartnerring.wirhabens.gui.listener.maps;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;

public class NoMapsFinishButtonListener implements OnClickListener {

	private Activity activity;
	
	

	public NoMapsFinishButtonListener(Activity activity) {
		super();
		this.activity = activity;
	}



	@Override
	public void onClick(DialogInterface dialog, int which) {
		activity.finish();
		
	}

}

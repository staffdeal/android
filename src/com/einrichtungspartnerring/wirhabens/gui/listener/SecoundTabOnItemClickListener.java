package com.einrichtungspartnerring.wirhabens.gui.listener;

import com.einrichtungspartnerring.wirhabens.CompetitionDetailActivity;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

/**
 * Listener for click events of the ideas/competition tab.
 * 
 * @author Thomas Möller
 * 
 */
public class SecoundTabOnItemClickListener implements OnItemClickListener {

	/**
	 * Context of the activity
	 */
	private Context context;

	public SecoundTabOnItemClickListener(Context context) {
		super();
		this.context = context;
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		Intent intent = new Intent(context, CompetitionDetailActivity.class);
		intent.putExtra("itempos", position);
		context.startActivity(intent);
	}

}

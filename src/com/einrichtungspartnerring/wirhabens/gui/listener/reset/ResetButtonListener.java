package com.einrichtungspartnerring.wirhabens.gui.listener.reset;

import com.einrichtungspartnerring.wirhabens.activityhelper.ResetPasswordAT;
import com.einrichtungspartnerring.wirhabens.manager.ManagerFactory;
import com.einrichtungspartnerring.wirhabens.manager.NetworkManager;

import android.app.Activity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;

/**
 * Listener for clicking button by the reset view.
 * @author Thomas Möller
 *
 */
public class ResetButtonListener implements OnClickListener{

	private Activity activity;
	private EditText email;
	
	

	public ResetButtonListener(Activity activity, EditText email) {
		super();
		this.activity = activity;
		this.email = email;
	}



	@Override
	public void onClick(View v) {
		String emailAddress = email.getText().toString();
		NetworkManager netManager = ManagerFactory.buildNetworkManager(activity);
		
		new ResetPasswordAT(activity, netManager, emailAddress).execute();
		
	}

}

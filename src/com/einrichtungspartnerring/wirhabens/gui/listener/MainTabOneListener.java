package com.einrichtungspartnerring.wirhabens.gui.listener;

import android.app.Activity;
import android.app.AlertDialog;
import android.widget.TabHost;
import android.widget.TabHost.OnTabChangeListener;

import com.einrichtungspartnerring.wirhabens.R;
import com.einrichtungspartnerring.wirhabens.activityhelper.events.OnTaskLoginCompleteEvent;
import com.einrichtungspartnerring.wirhabens.helper.MeinERPDialogBuilder;
import com.einrichtungspartnerring.wirhabens.helper.Rights;
import com.einrichtungspartnerring.wirhabens.manager.NetworkManager;
import com.einrichtungspartnerring.wirhabens.properties.MeinERPProperties;
import com.einrichtungspartnerring.wirhabens.properties.PropertiesList;

/**
 * Tab listener implementation.
 * 
 * @author Thomas Möller
 * 
 */
public class MainTabOneListener implements OnTabChangeListener, PropertiesList, OnTaskLoginCompleteEvent {

	/**
	 * Activity of the current Android Activity.
	 */
	private Activity activity;

	/**
	 * Current system properties
	 */
	private MeinERPProperties properties;

	private NetworkManager netManager;
	
	/**
	 * Tab you want to jump
	 */
	private int currentTab;

	/**
	 * Initialization constructor.
	 * 
	 * @param activity
	 *            of the current android activity.
	 */
	public MainTabOneListener(Activity activity, MeinERPProperties properties,
			NetworkManager netManager) {
		this.activity = activity;
		this.properties = properties;
		this.netManager = netManager;
	}

	@Override
	public void onTabChanged(String tabId) {
		TabHost tabHost = null;
		currentTab = tabHost.getCurrentTab();
		String searchedProp = null;
		switch (currentTab) {
		case 0:
			searchedProp = NEWS_RIGHTS;
			break;
		case 1:
			searchedProp = IDEAS_RIGHTS;
			break;
		case 2:
			searchedProp = DEALS_RIGHTS;
			break;
		case 3:
			searchedProp = EVENTS_RIGHTS;
			break;
		default:
			searchedProp = null;
		}

		if (!Rights.check((String) properties.get(searchedProp),
				(String) properties.get(USER_RIGHT))) {
			int lasttab = geLastTab();
			tabHost.setCurrentTab(lasttab);
			AlertDialog dialog = MeinERPDialogBuilder.buildAuthenticatDialog(
					activity, netManager, this);
			dialog.show();
			
		} else {
			properties.set(LAST_TAB_PROP, String.valueOf(currentTab));
		}

	}
	
	private int geLastTab(){
		int lasttab = 0;
		try{
			lasttab = Integer.valueOf(properties.get(LAST_TAB_PROP));
		}catch(NumberFormatException e){
			e.printStackTrace();
			return 0;
		}
		return lasttab;
		
	}

	@Override
	public void onTaskLoginCompleteEvent() {
		TabHost tabHost = null;
		properties.set(LAST_TAB_PROP, String.valueOf(currentTab));
		tabHost.setCurrentTab(currentTab);
	}

}

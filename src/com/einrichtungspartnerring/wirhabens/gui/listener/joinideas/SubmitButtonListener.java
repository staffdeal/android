package com.einrichtungspartnerring.wirhabens.gui.listener.joinideas;

import java.io.File;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import android.app.Activity;
import android.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.Toast;

import com.einrichtungspartnerring.wirhabens.R;
import com.einrichtungspartnerring.wirhabens.activityhelper.IdeaSubmitAT;
import com.einrichtungspartnerring.wirhabens.helper.AirbrakeHelper;
import com.einrichtungspartnerring.wirhabens.helper.MeinERPDialogBuilder;
import com.einrichtungspartnerring.wirhabens.manager.NetworkManager;
import com.einrichtungspartnerring.wirhabens.vo.CompetitionVO;

public class SubmitButtonListener implements OnClickListener{

	private Activity activity;
	private CompetitionVO competition;
	private NetworkManager netManager;
	private EditText text;
	private EditText link;
	private File imagePath;

	private static String TAG = "SubmitButton";

	public SubmitButtonListener(Activity activity, CompetitionVO competition,
			NetworkManager netManager, EditText text, EditText link) {
		super();
		this.activity = activity;
		this.competition = competition;
		this.netManager = netManager;
		this.text = text;
		this.link = link;
	}

	public void setImagePath(File imagePath) {
		this.imagePath = imagePath;
	}


	@Override
	public void onClick(View v) {
		int aException = 0;
		try {
			Log.i(TAG,"send idea to competition with id: "+ competition.getId());
			new IdeaSubmitAT(netManager, competition, text.getText()
					.toString(), link.getText().toString(), imagePath, activity).execute().get(20, TimeUnit.SECONDS);
		} catch (InterruptedException e) {
			String message = "Thread was interrupted";
			Log.e(TAG, message);
			AirbrakeHelper.sendException(e, message);
			aException = 1;
		} catch (ExecutionException e) {
			String message = "Error on execution";
			Log.e(TAG, message);
			AirbrakeHelper.sendException(e, message);
			aException = 2;
		} catch (TimeoutException e) {
			String message = "Timeout";
			Log.e(TAG, message);
			AirbrakeHelper.sendException(e, message);
			aException = 3;
		}finally{
			if (aException != 0){
				AlertDialog dialog = null;
				switch (aException) {
				case 1:
					dialog = MeinERPDialogBuilder.buildInformationDialog(activity, activity.getString(R.string.dialog_idea_sending_fail_title)
							, activity.getString(R.string.dialog_idea_sending_fail_text_interrupt)+ "\n"
									+ activity.getString(R.string.dialog_idea_sending_fail_text_ending));
					break;
				case 2:
					dialog = MeinERPDialogBuilder.buildInformationDialog(activity, activity.getString(R.string.dialog_idea_sending_fail_title)
							, activity.getString(R.string.dialog_idea_sending_fail_text_exception)+ "\n"
									+ activity.getString(R.string.dialog_idea_sending_fail_text_ending));
					break;
				case 3:
					dialog = MeinERPDialogBuilder.buildInformationDialog(activity, activity.getString(R.string.dialog_idea_sending_fail_title)
							, activity.getString(R.string.dialog_idea_sending_fail_text_timeout)+ "\n"
									+ activity.getString(R.string.dialog_idea_sending_fail_text_ending));
					break;

				default:
					dialog = MeinERPDialogBuilder.buildInformationDialog(activity, activity.getString(R.string.dialog_idea_sending_fail_title)
							, activity.getString(R.string.dialog_idea_sending_fail_text_exception)+ "\n"
									+ activity.getString(R.string.dialog_idea_sending_fail_text_ending));
					break;

				}
				dialog.show();
			}else{
				int duration = Toast.LENGTH_LONG;
				Toast toast = Toast.makeText(activity, R.string.dialog_idea_sending_successfull_Text, duration);
				toast.show();
				activity.finish();
			}
		}
	}
}

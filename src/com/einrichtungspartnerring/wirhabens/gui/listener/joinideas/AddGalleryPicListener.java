package com.einrichtungspartnerring.wirhabens.gui.listener.joinideas;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;

public class AddGalleryPicListener implements OnClickListener{

	
	private int responseId;
	private Activity activity;
	
	public AddGalleryPicListener(int responseId, Activity activity){
		this.responseId =responseId;
		this.activity = activity;
	}

	@Override
	public void onClick(View v) {
		Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
		//intent.setType("image/*");
		//intent.setAction(Intent.ACTION_GET_CONTENT);
		activity.startActivityForResult(intent, responseId);
		
	}

}

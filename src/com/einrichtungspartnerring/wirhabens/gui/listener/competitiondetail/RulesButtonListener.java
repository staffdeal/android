package com.einrichtungspartnerring.wirhabens.gui.listener.competitiondetail;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;

import com.einrichtungspartnerring.wirhabens.GeneralDetailsActivity;
import com.einrichtungspartnerring.wirhabens.R;
import com.einrichtungspartnerring.wirhabens.vo.CompetitionVO;

/**
 * Listener for the rules button in the competition details view.
 * 
 * @author Thomas Möller
 * 
 */
public class RulesButtonListener implements OnClickListener {

	/**
	 * Context of the activity
	 */
	private Context context;

	/**
	 * Competition you add the rules.
	 */
	private CompetitionVO competition;

	/**
	 * Init constructor.
	 * 
	 * @param context
	 * @param competition
	 */
	public RulesButtonListener(Context context, CompetitionVO competition) {
		super();
		this.context = context;
		this.competition = competition;
	} 

	@Override
	public void onClick(View v) {
		SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy", Locale.GERMANY);
		
		Intent intent = new Intent(context, GeneralDetailsActivity.class);
		intent.putExtra(GeneralDetailsActivity.FILENAME,"competitionRules.html");
		intent.putExtra(GeneralDetailsActivity.DESC_TITLE,
				competition.getTitle());
		intent.putExtra(GeneralDetailsActivity.DESC_CONTENT,
				competition.getRules());
		intent.putExtra(GeneralDetailsActivity.TITLE,context.getString(R.string.rules_text));
		
		intent.putExtra(GeneralDetailsActivity.DESC_AUTHOR, competition.getCompany());
		Date date = competition.getEndDate();
		if (date != null)
			intent.putExtra(GeneralDetailsActivity.DESC_DATE_UNTIL, "läuft bis "+ df.format(date));
		context.startActivity(intent);
	}

}

package com.einrichtungspartnerring.wirhabens.gui.listener.competitiondetail;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;

import com.einrichtungspartnerring.wirhabens.IdeasListActivity;
import com.einrichtungspartnerring.wirhabens.activityhelper.events.OnTaskLoginCompleteEvent;
import com.einrichtungspartnerring.wirhabens.helper.MeinERPDialogBuilder;
import com.einrichtungspartnerring.wirhabens.helper.Rights;
import com.einrichtungspartnerring.wirhabens.manager.NetworkManager;
import com.einrichtungspartnerring.wirhabens.properties.MeinERPProperties;
import com.einrichtungspartnerring.wirhabens.properties.MeinERPPropertiesFactory;
import com.einrichtungspartnerring.wirhabens.properties.PropertiesList;
import com.einrichtungspartnerring.wirhabens.vo.CompetitionVO;

public class ShowIdeasButtonListener implements OnClickListener, OnTaskLoginCompleteEvent {

	private static final String TAG = "ShowIdeasButtonListener";
	
	/**
	 * last activity
	 */
	private Activity activity;

	/**
	 * Competition you add the description.
	 */
	private CompetitionVO competition;
	
	private NetworkManager netManager;

	/**
	 * Init constructor.
	 * 
	 * @param context
	 * @param competition
	 */
	public ShowIdeasButtonListener(Activity activity, CompetitionVO competition, NetworkManager netManager) {
		super();
		this.activity = activity;
		this.competition = competition;
		this.netManager = netManager;
	}
	
	public void startShowCommitedIdeas(){
		Intent intent = new Intent(activity, IdeasListActivity.class);
		intent.putExtra(IdeasListActivity.IDEAS_LIST_COMPETITION_ID,competition.getId());
		activity.startActivity(intent);
	}

	@Override
	public void onClick(View v) {
		MeinERPProperties properties = MeinERPPropertiesFactory.getProperties(this.activity);
		if (!Rights.check((String) properties.get(PropertiesList.IDEAS_RIGHTS),
				(String) properties.get(PropertiesList.USER_RIGHT))) {
			Log.i(TAG, "User is not authenticat.");
			AlertDialog dialog = MeinERPDialogBuilder.buildAuthenticatDialog(activity, netManager, this);
			dialog.show();
		} else {
			startShowCommitedIdeas();
		}
	}

	@Override
	public void onTaskLoginCompleteEvent() {
		startShowCommitedIdeas();
	}

}

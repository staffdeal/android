package com.einrichtungspartnerring.wirhabens.gui.listener.competitiondetail;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;

import com.einrichtungspartnerring.wirhabens.JoinIdeasActivity;
import com.einrichtungspartnerring.wirhabens.activityhelper.events.OnTaskLoginCompleteEvent;
import com.einrichtungspartnerring.wirhabens.helper.MeinERPDialogBuilder;
import com.einrichtungspartnerring.wirhabens.manager.NetworkManager;
import com.einrichtungspartnerring.wirhabens.vo.CompetitionVO;

/**
 * Listener for the button where the you want to join a idea.
 * 
 * @author Thomas Möller
 * 
 */
public class JoinIdeasButtonListener implements OnClickListener, OnTaskLoginCompleteEvent {

	private static final String TAG = "JoinIdeasButtonListener";

	private Activity activity;
	private CompetitionVO competition;
	private NetworkManager netManager;

	/**
	 * Init constructor
	 * 
	 * @param context
	 *            - android context
	 * @param competitionId
	 *            - id of the competition you want to add a idea.
	 */
	public JoinIdeasButtonListener(Activity activity,
			CompetitionVO competition, NetworkManager netManager) {
		this.competition = competition;
		this.activity = activity;
		this.netManager = netManager;
	}

	private void startJoinIdeas(){
		Intent intent = new Intent(activity, JoinIdeasActivity.class);
		intent.putExtra(JoinIdeasActivity.COMPETITON_ID, competition.getId());
		activity.startActivity(intent);
	}
	
	@Override
	public void onClick(View v) {
		if (!netManager.isAuthenticated()) {
			Log.i(TAG, "User is not authenticat.");
			Dialog loginDialog = MeinERPDialogBuilder.buildAuthenticatDialog(activity, netManager, this);
			loginDialog.show();
		} else {
			startJoinIdeas();
		}
	}

	@Override
	public void onTaskLoginCompleteEvent() {
		startJoinIdeas();
	}
	
	

}

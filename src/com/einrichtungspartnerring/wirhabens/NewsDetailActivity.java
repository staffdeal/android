package com.einrichtungspartnerring.wirhabens;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.text.SimpleDateFormat;
import java.util.Locale;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.TextView;
import android.widget.Toast;

import com.einrichtungspartnerring.wirhabens.activityhelper.NewsReadAT;
import com.einrichtungspartnerring.wirhabens.helper.AirbrakeHelper;
import com.einrichtungspartnerring.wirhabens.helper.HTMLProjectHelper;
import com.einrichtungspartnerring.wirhabens.manager.ManagerFactory;
import com.einrichtungspartnerring.wirhabens.manager.MeinEPRSystemManager;
import com.einrichtungspartnerring.wirhabens.manager.NewsManager;
import com.einrichtungspartnerring.wirhabens.vo.NewsItemVO;

/**
 * Detail view for news
 * 
 * @author Thomas Möller
 * 
 */
public class NewsDetailActivity extends Activity implements OnTouchListener {
	private static final String TAG = "NewsDetailActivity";

	private String shareURL;
	private String newsId;
	private float startX;
	private float endX;
	private static final float THRESHOLD = 300.0f;
	private int currentNewsId;
	private NewsManager newsmanager;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		try{
			setContentView(R.layout.activity_news_detail);
			if (android.os.Build.VERSION.SDK_INT <= Build.VERSION_CODES.GINGERBREAD_MR1){
				//TODO Android 2.3.3. Action bar coloring.
			}
			else{
				MainActivity.customActionBar(getActionBar(), this);

			}

			Bundle extras = getIntent().getExtras();
            if (extras == null)
                finish();

			int newsitempos = extras.getInt("newsitempos");
			currentNewsId = newsitempos;
			newsmanager = ManagerFactory.buildNewsManager(this);

			NewsItemVO vo = newsmanager.getItemByPosition(newsitempos, true);

			shareURL = vo.getUrl();
			
			setNewsData(vo);
			
			((StaffsaleApplication) getApplication()).trackEvent(getString(R.string.ga_category_news),getString(R.string.ga_action_read),vo.getId());	
		}catch(Exception e){
			String eMessage = "Unknown Exception";
			AirbrakeHelper.sendException(e, eMessage);
		}

	}

	/**
	 * Menu button
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.news_detail, menu);
		return true;
	}

	protected void onLeft() {
		if (newsmanager.loadOfflinePicAdapterItem().size() - 1 > currentNewsId) {
			currentNewsId++;
			setNewsData(newsmanager.getItemByPosition(currentNewsId, true));
		}
	}
 
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int duration = Toast.LENGTH_SHORT;
		Toast toast = null;
		int itemId = item.getItemId();
		if (itemId == R.id.action_news_sharing) {
			if (shareURL != null)
				sharing();
			else
				toast = Toast.makeText(this, R.string.toast_no_url_to_sharing, duration);
			((StaffsaleApplication) getApplication()).trackEvent(getString(R.string.ga_category_news),getString(R.string.ga_action_send),newsId);
		} else if (itemId == R.id.action_news_read_more) {
			if (shareURL != null){
				Log.d(TAG, "open url: "+shareURL );
				Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(Uri.parse(shareURL));
				startActivity(i);
			}
			else
				toast = Toast.makeText(this, R.string.toast_no_url_to_sharing, duration);
			((StaffsaleApplication) getApplication()).trackEvent(getString(R.string.ga_category_news),getString(R.string.ga_action_open_source),newsId);
		} else {
			return super.onOptionsItemSelected(item);
		}
		if (toast != null)
			toast.show();
		return super.onOptionsItemSelected(item);
	}

	protected void onRight() {
		if (0 < currentNewsId) {
			currentNewsId--;
			setNewsData(newsmanager.getItemByPosition(currentNewsId, true));
		}
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		int action = event.getAction();
		if (action == MotionEvent.ACTION_DOWN) {
			startX = event.getX();
			return true;
		} else if (action == MotionEvent.ACTION_UP) {
			endX = event.getX();
			float diff = startX - endX;
			Log.i(TAG, "Touch event diff : " + diff);
			if (Math.abs(diff) >= THRESHOLD)
				if (diff < 0) {// right
					Log.i(TAG, "right Event");
					onRight();
				} else if (diff > 0) {// left
					Log.i(TAG, "left Event");
					onLeft();
				}
			return true;
		}
		return false;
	}

	public void setNewsData(NewsItemVO vo) {
		
            try {
                if (vo != null) {
                	
                	String templateString = ((StaffsaleApplication) getApplication()).getHtmlTemplateForFile("newsDetails.html");        	
                    
                	templateString = templateString.replace("##META##","<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, maximum-scale=1.0\">");
                	templateString = templateString.replace("##CATEGORY##", vo.getCategory().getTitle());
                	templateString = templateString.replace("##TITLE##", vo.getTitle());                	

                    SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy", Locale.GERMANY);
                    templateString = templateString.replace("##PUBLISHED##",df.format(vo.getPublished()));
                    templateString = templateString.replace("##SOURCE##",vo.getSource());
                    templateString = templateString.replace("##CONTENT##",vo.getText());
                	
                    WebView text = (WebView) findViewById(R.id.details_content);
                    WebSettings settings = text.getSettings();
                    settings.setDefaultFontSize(20);
                    settings.setDefaultTextEncodingName("utf-8");
                    text.loadData(templateString, "text/html; charset=utf-8", "UTF-8");
                }
                else {
                    Log.w(TAG, "A deleted news was called.");
                    finish();
                }
            } catch (Exception e) {
                String eMessage = "Unknown Exception";
                AirbrakeHelper.sendException(e, eMessage);
            }
        new NewsReadAT(ManagerFactory.buildNetworkManager(this)).execute(vo.getId());

	}

	/**
	 * Method for sharing with intents to other apps.
	 * 
	 * @param v
	 */
	protected void sharing() {
		Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
		// set the type

		shareIntent.setType("text/plain");

		// add a subject
		// shareIntent.putExtra(android.content.Intent.EXTRA_SUBJECT,
		// "Subject");

		// build the body of the message to be shared
		String shareMessage = shareURL;

		// add the message
		shareIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareMessage);

		// start the chooser for sharing
		startActivity(Intent.createChooser(shareIntent, getString(R.string.news_chooser_text)));
	}

}

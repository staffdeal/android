package com.einrichtungspartnerring.wirhabens.manager;

public enum ServerResponse {

    ok, passwort_not_fulfill_policies,
    wrong_username_passwort, unknown_error, not_authenticated, old_pass_not_correct, pass1_to_pass2_is_different, incorrect_email_format, no_dealer, email_duplicate,
    server_communication_error;

    public static ServerResponse mappingResponse(int code) {
        ServerResponse response = ServerResponse.unknown_error;
        switch (code) {
            case 0:
                response = ServerResponse.ok;
                break;
            case 200:
                response = ServerResponse.ok;
                break;
            case 403:
                response = ServerResponse.old_pass_not_correct;
                break;
            case 405:
                response = ServerResponse.passwort_not_fulfill_policies;
                break;
            case 101:
                response = ServerResponse.incorrect_email_format;
                break;
            case 102:
                response = ServerResponse.passwort_not_fulfill_policies;
                break;
            case 303:
                response = ServerResponse.no_dealer;
            case 302:
                // Koodinaten sind unsinnig
            case 301:
                // Location ist unsinnig
            case 1:
                // Email ist schon vorhanden
                response = ServerResponse.email_duplicate;
            default:
                break;
        }
        return response;
    }
}

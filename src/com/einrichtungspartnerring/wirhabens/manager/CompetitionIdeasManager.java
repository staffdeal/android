package com.einrichtungspartnerring.wirhabens.manager;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.content.Context;

import com.einrichtungspartnerring.wirhabens.gui.adapter.PicAdapterItem;
import com.einrichtungspartnerring.wirhabens.parser.ParserVO;
import com.einrichtungspartnerring.wirhabens.parser.builder.CompetionsConstructBuilder;
import com.einrichtungspartnerring.wirhabens.properties.MeinERPProperties;
import com.einrichtungspartnerring.wirhabens.properties.MeinERPPropertiesFactory;
import com.einrichtungspartnerring.wirhabens.properties.PropertiesList;
import com.einrichtungspartnerring.wirhabens.vo.CompetitionCategoryVO;
import com.einrichtungspartnerring.wirhabens.vo.CompetitionVO;
import com.einrichtungspartnerring.wirhabens.vo.IdeaVO;

/**
 * Manager for the competitions and ideas of the system.
 * 
 * @author Thomas Möller
 * 
 */
public class CompetitionIdeasManager extends MeinEPRSystemManager<CompetitionVO> implements SystemPaths {

	private Map<String, IdeasManager> ideaManagers;
	private Context context;
	
	/**
	 * Initialization constructor.
	 * 
	 * @param netManager
	 *            - instance if the network manager.
	 */
	protected CompetitionIdeasManager(NetworkManager netManager, Context context) {
		super(context, COMPETITIONS_READED_PATH, COMPETITIONS_FILDERED_PATH, netManager);
		MeinERPProperties prop = MeinERPPropertiesFactory.getProperties(context);
		if (xmlSavePath == null)
			xmlSavePath = context.getFilesDir().toString()
			+ COMPETITIONS_XML_PATH;
		if (loadURL == null) {
			loadURL = (String) prop.get(PropertiesList.API_BASE_URL) + (String) prop.get(PropertiesList.API_IDEAS_COMPETITION_URL);
		}
		ideaManagers = new HashMap<String, IdeasManager>();
		this.context = context; 
	}

	private IdeasManager getIdeasManager(String competitionId){
		IdeasManager manager = null;
		manager = ideaManagers.get(competitionId);
		if (manager == null){
			manager = new IdeasManager(context, IDEAS_READED_PATH+competitionId, netManager);
			ideaManagers.put(competitionId, manager);
		}
		return manager;
	}
	
	/**
	 * 
	 * @param vo
	 * @return
	 */
	public List<PicAdapterItem> getIdeasByCompetition(CompetitionVO vo) {
		IdeasManager ideasManager = getIdeasManager(vo.getId());
		return ideasManager.getIdeasByCompetition(vo);
	}

	/**
	 * Return all ideas of the given competition.
	 * @param competitionId id of the comptition you want all ideas.
	 * @return all founded ideas.
	 */
	public List<PicAdapterItem> getIdeasByCompetition(String competitionId) {
		CompetitionVO competition = getItemById(competitionId);
		return getIdeasByCompetition(competition);
	}

	/**
	 * Search in all ideas for the idea with the same id you give and return it.
	 * @param readed should idea set readed.
	 * @return you searched id by given id, or null if not found.
	 */
	public IdeaVO getIdeasById(String competitionId, String ideasId, boolean readed){
		IdeasManager ideasManager = getIdeasManager(competitionId);
		return ideasManager.getItemById(ideasId, readed);
	}

	@Override
	protected List<CompetitionVO> parsedCached(File file) {
		List<CompetitionVO> competitions = null;
		List<CompetitionCategoryVO> comCategory = new ArrayList<CompetitionCategoryVO>();
		if (file.exists()) {
			ParserVO firstElement = netManager.parsingFile(file);
			if (firstElement != null) {
				competitions = CompetionsConstructBuilder.buildCategories(firstElement, comCategory);
			}
		}
		setCategories(comCategory);
		return competitions;
	}

    @Override
    protected List<CompetitionVO> parseString(String content, List<CategoryFilter> filters) {
        throw new  UnsupportedOperationException();
    }
}

package com.einrichtungspartnerring.wirhabens.manager;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.util.*;

import android.content.Context;
import android.util.Log;

import com.einrichtungspartnerring.wirhabens.activityhelper.events.ReloadedInformation;
import com.einrichtungspartnerring.wirhabens.helper.StreamHelper;

/**
 * Manager for images
 * 
 * @author Thomas Möller
 * 
 */
public class ImageManager {

	private static final String TAG = "ImageManager";

	/**
	 * All cached image files on the app.
	 */
	private List<File> images = null;
    private Map<String, String> idUrl = new HashMap<String, String>();

	/**
	 * Constructor of the image with context
	 * 
	 * @param context
	 *            - app context
	 */
	protected ImageManager(Context context) {
		images = Collections.synchronizedList(new ArrayList<File>());
		File imgDir = new File(context.getFilesDir().getAbsolutePath()
				+ SystemPaths.IMAGE_Folder);
		images.addAll(Arrays.asList(imgDir.listFiles()));
	}

	private File downlaod(Context context, String id, String urlS) {
		File file = new File(context.getFilesDir().getAbsolutePath()
				+ SystemPaths.IMAGE_Folder + id);
		InputStream in = null;
		OutputStream out = null;
		try {
			URL url = new URL(urlS);
			in = url.openStream();
			out = new FileOutputStream(file);
			StreamHelper.copy(in, out);
			out.flush();
            idUrl.put(id, urlS);
		} catch (IOException e) {
			e.printStackTrace();
			Log.e(TAG, "Could not load Bitmap from: " + urlS);
		} finally {
			try {
				if (in != null)
					in.close();
				if (out != null) {
					out.close();
					images.add(file);
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return file;
	}

	public synchronized File get(Context context, String id, String url) {
		File searched = getIfExist(id);
		if (searched == null || idUrl.get(id) == null || idUrl.get(id) != url) {
			searched = downlaod(context, id, url);
		}
		return searched;
	}

    public synchronized File get(Context context, String id, String url, ReloadedInformation fileWasReloaded) {
        boolean reloaded = false;
        File searched = getIfExist(id);
        if (searched == null || idUrl.get(id) == null || idUrl.get(id) != url) {
            searched = downlaod(context, id, url);
            reloaded = true;
        }
        if (fileWasReloaded != null)
            fileWasReloaded.wasReloaded(reloaded);
        return searched;
    }

	/**
	 * Search if a file exist under the given id.
	 * 
	 * @param id
	 *            of the file.
	 * @return a file if it exist. Null of not.
	 */
	public synchronized File getIfExist(String id) {
		File searched = null;
		for (File image : images) {
			if (image.getName().equals(id)) {
				searched = image;
				break;
			}
		}
		return searched;
	}
}

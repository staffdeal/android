package com.einrichtungspartnerring.wirhabens.manager;

public interface SystemPaths {

	/**
	 * Path where image should saved.
	 */
	String IMAGE_Folder = "/img/";

	/**
	 * Path where all xml files should saved.
	 */
	String XML_Folder = "/xml/";
	/**
	 * Folder for id which are readed.
	 */
	String READED_FOLDER = "/readed/";
	/**
	 * Folder where all bar codes should saved.
	 */
	String BARCODE_FOLDER = "/barcode/";
	

	/**
	 * Name of the xml save Paths
	 */
	String NEWS_XML_PATH = XML_Folder + "news.xml";
	String COMPETITIONS_XML_PATH = XML_Folder + "competitions.xml";
	String IDEAS_XML_PATH = XML_Folder + "ideas.xml";
	String DEALS_XML_PATH = XML_Folder + "deals.xml";
	String IDEAS_PART_XML_PATH = XML_Folder + "ideas_%s.xml";
	String EVENTS_XML_PATH = XML_Folder + "events.xml";

	/**
	 * Name of the list of readed item ids.
	 */
	String NEWS_READED_PATH = READED_FOLDER + "news.readed";
	String DEALS_READED_PATH = READED_FOLDER + "deals.readed";
	String COMPETITIONS_READED_PATH = READED_FOLDER + "competitions.readed";
	String IDEAS_READED_PATH = READED_FOLDER + "ideas.readed";
	String EVENTS_READED_PATH = READED_FOLDER + "events.readed";
	
	/**
	 * Saved deals
	 */
	String DEALS_ONLINE_SAVE_PATH = "deals_online.saved";
    String DEALS_ONSITE_SAVE_PATH = "deals_onsite.saved";
    String DEALS_SHOW_AS_AD_SAVE_PATH = "deals_ad.saved";
    String DEALS_SHOW_REMINDER_SAVE_PATH = "deals_reminder.saved";
    String DEALS_CODE_SAVE_PATH = "deals_code.saved";
	
	/**
	 * Saves object they will NOT shown in the user.
	 */
	String NEWS_FILDERED_PATH = READED_FOLDER + "news.filtered";
	String COMPETITIONS_FILDERED_PATH = READED_FOLDER + "competitions.filtered";
	String DEALS_FILDERED_PATH = READED_FOLDER + "deals.filtered";
}

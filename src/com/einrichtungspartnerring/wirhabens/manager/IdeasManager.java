package com.einrichtungspartnerring.wirhabens.manager;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.util.Log;

import com.einrichtungspartnerring.wirhabens.gui.adapter.PicAdapterItem;
import com.einrichtungspartnerring.wirhabens.parser.ParserVO;
import com.einrichtungspartnerring.wirhabens.parser.builder.IdeasBuilder;
import com.einrichtungspartnerring.wirhabens.properties.MeinERPProperties;
import com.einrichtungspartnerring.wirhabens.properties.MeinERPPropertiesFactory;
import com.einrichtungspartnerring.wirhabens.properties.PropertiesList;
import com.einrichtungspartnerring.wirhabens.vo.CompetitionVO;
import com.einrichtungspartnerring.wirhabens.vo.IdeaVO;

/**
 * Manager for the Ideas.
 * @author Thomas Möller
 *
 */
class IdeasManager extends MeinEPRSystemManager<IdeaVO>{

	private static final String TAG = "MeinERPSystemManager";

	public IdeasManager(Context context, String readedPath, NetworkManager manager) {
		super(context, readedPath, null, manager);
		MeinERPProperties prop = MeinERPPropertiesFactory.getProperties(context);
		if (xmlSavePath == null)
			xmlSavePath = context.getFilesDir().toString() + IDEAS_PART_XML_PATH ;
		if (loadURL == null) {
			loadURL = (String) prop.get(PropertiesList.API_BASE_URL) + (String) prop.get(PropertiesList.API_IDEAS_LIST_URL);
		}
	}

	public List<PicAdapterItem> getIdeasByCompetition(CompetitionVO vo){
		storeReadedElements(convert(this.items));

		String path = String.format(this.xmlSavePath, vo.getId());
		String urlS = this.loadURL + vo.getId();
		List<IdeaVO> newItems = refresh(path, urlS);
		if (this.items == null)
			this.items = new ArrayList<IdeaVO>();
		if (newItems != null)
			this.items.addAll(newItems);

		List<PicAdapterItem> searched = convert(newItems);
		restoreReadedElement(searched);
		return searched;
	}

	@Override
	protected List<IdeaVO> parsedCached(File file) {
		List<IdeaVO> items = null;
		if (file.exists()){
			ParserVO firstElement = netManager.parsingFile(file);
			if (firstElement != null) {
				items = IdeasBuilder.buildideas(firstElement);
			}
		}
		
		return items;
	}

	/**
	 * For this manager don't use this method
	 */
	public List<PicAdapterItem> loadOfflinePicAdapterItem(){
		Log.e(TAG, "Call unallowed method");
		return  null;
	}

	/**
	 * For this manager don't use this method
	 */
	public List<PicAdapterItem> loadPicAdapterItem(){
		Log.e(TAG, "Call unallowed method");
		return  null;
	}

	public IdeaVO getItemById(String id, boolean readed) {
		IdeaVO idea = super.getItemById(id);
		if (idea != null && readed){
			idea.setReaded(readed);
			storeReadedElements(convert(this.items));
		}
		return idea;
	}

    @Override
    protected List<IdeaVO> parseString(String content, List<CategoryFilter> filters) {
        throw new UnsupportedOperationException();
    }
}

package com.einrichtungspartnerring.wirhabens.manager;

public interface CategoryFilter {

	/**
	 * @return the id of the category.
	 */
	String getId();

	/**
	 * 
	 * @return the title of the category.
	 */
	String getTitle();

	/**
	 * 
	 * @return if this category visible for the user.
	 */
	boolean isVisible();

	/**
	 * Set the visible of this category.
	 * 
	 * @param visible
	 */
	void setVisible(boolean visible);

    boolean isSticky();
}

package com.einrichtungspartnerring.wirhabens.manager;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import android.util.Log;
import android.util.Xml;
import android.widget.Toast;

import com.einrichtungspartnerring.wirhabens.StaffsaleApplication;
import com.einrichtungspartnerring.wirhabens.parser.builder.*;
import com.einrichtungspartnerring.wirhabens.vo.*;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;

import com.einrichtungspartnerring.wirhabens.fragments.StaffdealListFragment;
import com.einrichtungspartnerring.wirhabens.helper.AirbrakeHelper;
import com.einrichtungspartnerring.wirhabens.helper.AndroidInformationHelper;
import com.einrichtungspartnerring.wirhabens.helper.StreamHelper;
import com.einrichtungspartnerring.wirhabens.manager.exception.NotAuthenticatException;
import com.einrichtungspartnerring.wirhabens.manager.serverresponse.ResetPasswordServerResponse;
import com.einrichtungspartnerring.wirhabens.parser.MeinERPParserException;
import com.einrichtungspartnerring.wirhabens.parser.Parser;
import com.einrichtungspartnerring.wirhabens.parser.ParserVO;
import com.einrichtungspartnerring.wirhabens.properties.MeinERPProperties;
import com.einrichtungspartnerring.wirhabens.properties.MeinERPPropertiesFactory;
import com.einrichtungspartnerring.wirhabens.properties.PropertiesList;
import com.google.android.gcm.GCMRegistrar;
import org.xmlpull.v1.XmlSerializer;

/**
 * Network manager for the system
 * 
 * @author Thomas Möller
 */
public class NetworkManager extends StreamHelper {

	private static final String TAG = "NetworkManager";

	/**
	 * Info object from backend.
	 */
	private InfoVO info;

	private ArrayList<StaffdealListFragment> logInListeners;
	
	/**
	 * Token for authentication from backend.
	 */
	private String token;

	/**
	 * Google cloud message id.
	 */
	private static String gcmId = null;

	/**
	 * URL where to authenticate the client.
	 */
	private static String authenticatePartURL = null;

	/**
	 * URL for changing password. 
	 */
	private static String passwordChangePartURL = null;
	
	/**
	 * URL for resetting  password. 
	 */
	private static String passwordResetTemplateURL = null;

	/**
	 * URL where new ideas should send.
	 */
	private static String ideaSubmitURL = null;

	/**
	 * URL where to registry new user.
	 */
	private static String registryURL = null;

	/**
	 * Identifier for this software client.
	 */
	private static String clientKey = null;
	/**
	 * URL where the deals are online can found.
	 */
	private static String infoURL = null;

	/**
	 * Version name in the manifest.
	 */
	private String currentVersionName = null;

    private String dealsSyncUrl = null;

	/**
	 * User agent which should send to the backend.
	 */
	private String userAgent = null;

	private static final String USER_AGENT_TEMPLATE = "wir haben's/%s (Android %s)";

	private static String notificationDeviceTemplateUrl = null;

	private static String notificationUnregistDeviceTemplateUrl = null;

	private String notificationServerToken = null;

	private MeinERPProperties prop = null;

    private String newsReadUrl = null;

    private List<DealerVO> cachedDealers;
    
    private Context mContext;
    
	private HashMap<String,String> mCurrTemplates = new HashMap<String,String>();

	public HashMap<String, String> getmCurrTemplates() {
		return mCurrTemplates;
	}

	/**
	 * Protected constructor for singleton construction. No singleton Pattern,
	 * ManagerFactory have the controlling.
	 */
	protected NetworkManager(Context context) {
			
		mContext = context;
		
		logInListeners = new ArrayList<StaffdealListFragment>();
		
		prop = MeinERPPropertiesFactory.getProperties(context);
		if (infoURL == null) {
			infoURL = (String) prop.get(PropertiesList.API_BASE_URL) + (String) prop.get(PropertiesList.API_INFO_URL);
		}
		if (authenticatePartURL == null) {
			authenticatePartURL = (String) prop.get(PropertiesList.API_BASE_URL) + (String) prop.get(PropertiesList.API_AUTHENTICAT_URL);
		}
		if (clientKey == null) {
			clientKey = (String) prop.get(PropertiesList.CLIENT_KEY);
		}
		if (ideaSubmitURL == null) {
			ideaSubmitURL = (String) prop.get(PropertiesList.API_BASE_URL) + (String) prop.get(PropertiesList.API_IDEAS_SUBMIT_URL);
		}
		if (passwordChangePartURL == null) {
			passwordChangePartURL = (String) prop.get(PropertiesList.API_BASE_URL) + (String) prop.get(PropertiesList.API_CHANGE_PASSWORD);
		}
		if (passwordResetTemplateURL == null){
			passwordResetTemplateURL = (String) prop.get(PropertiesList.API_BASE_URL) + (String) prop.get(PropertiesList.API_RESET_PASSWORD);
		}
		if (registryURL == null) {
			registryURL = (String) prop.get(PropertiesList.API_BASE_URL) + (String) prop.get(PropertiesList.API_REGISTRY_URL);
		}
		try {
			currentVersionName = AndroidInformationHelper.getAPKVersionName(context);
			userAgent = String.format(USER_AGENT_TEMPLATE, currentVersionName, AndroidInformationHelper.getAndroidReleaseVersion());
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}
		if (notificationDeviceTemplateUrl == null)
			notificationDeviceTemplateUrl = (String) prop.get(PropertiesList.API_BASE_URL) + (String) prop.get(PropertiesList.API_NOTIFICATION_DEVICE);
		if (notificationUnregistDeviceTemplateUrl == null)
			notificationUnregistDeviceTemplateUrl = (String) prop.get(PropertiesList.API_BASE_URL) + (String) prop.get(PropertiesList.API_NOTIFICATION_UNREGIST_DEVICE);
        if (dealsSyncUrl == null)
            dealsSyncUrl = (String) prop.get(PropertiesList.API_BASE_URL) + (String) prop.get(PropertiesList.API_DEALS_SYNC_URL);
        if (newsReadUrl == null)
            newsReadUrl = (String) prop.get(PropertiesList.API_BASE_URL) + (String) prop.get(PropertiesList.API_NEWS_READ_URL);
	}

	public ServerResponse changePassword(String oldPassword, String newPassword){
		HttpResponse response = null;
		ServerResponse contentResponse = ServerResponse.unknown_error;
		if (isAuthenticated()){
			String urlS  = String.format(passwordChangePartURL, this.token, oldPassword, newPassword);

			HttpGet request = new HttpGet(urlS);

			try{
				response = sendGet(request);
				String content = loadContent(response.getEntity().getContent());
				Parser parser = new Parser();
				ParserVO vo= parser.parseContent(content);
				contentResponse = ServerResponseBuilder.buildChangePasswordResponse(vo);
			}catch(ClientProtocolException e){
				String eMessage = "send POST request error";
				AirbrakeHelper.sendException(e, eMessage);
			} catch (IOException e) {
				String eMessage = "loadContent Error";
				AirbrakeHelper.sendException(e, eMessage);
			} catch (MeinERPParserException e) {
				String eMessage = "parsing server response error";
				AirbrakeHelper.sendException(e, eMessage);
			}
		}else
			return ServerResponse.not_authenticated;

		return contentResponse;
	}

	/**
	 * Try to Authenticate the system with the backend.
	 * @param username you want to authenticate.
	 * @param password you want to authenticate
	 * @return A Auth-VO with information about you authentication.
	 */
	public AuthVO authenticat(String username, String password) {
		String urlS = String.format(authenticatePartURL, username, password,
				clientKey);
		HttpPost request = new HttpPost(urlS);
		HttpResponse response = null;
		String content = null;
		AuthVO auth = null;
		try {
			response = sendPost(request);
			content = loadContent(response.getEntity().getContent());
			Parser parser = new Parser();
			ParserVO staffSale = parser.parseContent(content);
			auth = AuthBuilder.buildAuthentication(staffSale);
			token = auth.getToken();
		}catch(ClientProtocolException e){
			String eMessage = "send POST request error";
			AirbrakeHelper.sendException(e, eMessage);
		} catch (IOException e) {
			String eMessage = "loadContent Error";
			AirbrakeHelper.sendException(e, eMessage);
		} catch (MeinERPParserException e) {
			String eMessage = "parsing server response error";
			AirbrakeHelper.sendException(e, eMessage);
		}
		return auth;
	}

	public String getGMCId() {
		return gcmId;
	}

	public InfoVO getInfo() {
		return info;
	}

	/**
	 * Check if a token exist.
	 * 
	 * @return true if token exist, else false.
	 */
	public boolean isAuthenticated() {
		if (token == null)
			return false;
		return true;
	}

	public void deleteAuthentication(){
		token = null;
		
		for(StaffdealListFragment fragment : logInListeners) {
			
			if(fragment != null){
				fragment.logedIn();
			}			
		}
	}

	/**
	 * Load the informations from the online info site.
     * TODO overwork loading from website
	 */
	public InfoVO loadInfos() {
		InputStream iStream = null;
		URL url;
		InfoVO info = null;
		try {
			String urlS = infoURL;
			if (token != null)
				urlS = urlS + "?token=" + token;
			url = new URL(urlS);
			iStream = url.openStream();		
			Parser parser = new Parser();
			ParserVO firstElement = parser.parse(iStream);
			info = InfoBuilder.buildInfo(firstElement, mContext);

		} catch (MalformedURLException e) {
			String eMessage = "url is wrong";
			AirbrakeHelper.sendException(e, eMessage);
		} catch (IOException e) {
			String eMessage = "loading content";
			AirbrakeHelper.sendException(e, eMessage);
		} catch (MeinERPParserException e) {
			String eMessage = "parsing server response error";
			AirbrakeHelper.sendException(e, eMessage);
		}
		if (info != null) {
			this.info = info;
            if (info.getYouTubeMediaUrl() != null)
			    prop.set(PropertiesList.API_YOUTUBE_MEDIA_URL, info.getYouTubeMediaUrl());
            if (info.getFacebookMediaUrl() != null)
                prop.set(PropertiesList.API_FACEBOOK_MEDIA_URL, info.getFacebookMediaUrl());
            if (info.getClientAbout() != null)
			    prop.set(PropertiesList.API_ABOUT_TEXT, info.getClientAbout());
            if (info.getClientImpressum() != null)
			    prop.set(PropertiesList.API_IMPRESS_TEXT, info.getClientImpressum());
			if (info.getRegistration() != null){
				RegistrationVO reg = info.getRegistration(); 
				prop.set(PropertiesList.API_PRIVACY_POLICE_TEXT, reg.getPrivacyPolicy());
				prop.set(PropertiesList.API_TERM_OF_USING_TEXT, reg.getTermsOfUse());	
			}
			for (InfoModul module : info.getModules()){
				if (module.getId().equals("deals")){
					prop.set(PropertiesList.DEALS_RIGHTS, module.getAuthLevel().name());					
					downloadTemplates(module.getTemplates());
				} else if (module.getId().equals("ideas")){
					prop.set(PropertiesList.IDEAS_RIGHTS, module.getAuthLevel().name());					
					downloadTemplates(module.getTemplates());
				} else if (module.getId().equals("news")){
					prop.set(PropertiesList.NEWS_RIGHTS, module.getAuthLevel().name());
					downloadTemplates(module.getTemplates());
				} else if (module.getId().equals("events")) {
					prop.set(PropertiesList.EVENTS_RIGHTS, module.getAuthLevel().name());
					downloadTemplates(module.getTemplates());
				}
			}
		}
		return info;
	}

	private void downloadTemplates(ArrayList<TemplateVO> templates) {		
		if(templates != null && templates.size() > 0) {
			for(TemplateVO template : templates) {
				if(template.getUrl() != null && !mCurrTemplates.containsValue((template.getHash()))) {
					
					try
			        {
			            URL url = new URL(template.getUrl());

			            URLConnection ucon = url.openConnection();
			            ucon.setReadTimeout(5000);
			            ucon.setConnectTimeout(10000);

			            InputStream is = ucon.getInputStream();
			            BufferedInputStream inStream = new BufferedInputStream(is, 1024 * 5);

			            String path = mContext.getDir("filesdir", Context.MODE_PRIVATE) + "/" + template.getUseFor() + ".html";
			            File file = new File(path);

			            if (file.exists())
			            {
			                file.delete();
			            }
			            file.createNewFile();

			            FileOutputStream outStream = new FileOutputStream(file);
			            byte[] buff = new byte[5 * 1024];

			            int len;
			            while ((len = inStream.read(buff)) != -1)
			            {
			                outStream.write(buff, 0, len);
			            }

			            outStream.flush();
			            outStream.close();
			            inStream.close();

			            mCurrTemplates.put(template.getUseFor(), template.getHash());
			        }
			        catch (Exception e)
			        {
			            e.printStackTrace();
			        }				
				}					
			}							
		}
	}

	/**
	 * 
	 * Load the content of the given URL as String into the given file.
	 * 
	 * @param localfile
	 *            - file local you want to write the content behind the url.
	 * @param urlS
	 *            you want to download the content.
	 */
	public void loadToXML(File localfile, String urlS) {
		reinfomated();
        HttpGet get = new HttpGet(urlS);
        HttpResponse response = null;
        InputStream iStream = null;
        FileOutputStream oStream = null;
		try {
            response = sendGet(get);
            iStream = response.getEntity().getContent();

			oStream = new FileOutputStream(localfile);
			copy(iStream, oStream);
		} catch (MalformedURLException e) {
			String eMessage = "url is wrong";
			AirbrakeHelper.sendException(e, eMessage);
		} catch (IOException e) {
			String eMessage = "loading content";
			AirbrakeHelper.sendException(e, eMessage);
		} finally {
			close(iStream);
			close(oStream);
		}
	}

    public String loadNewPage(String urlS){
        reinfomated();
        HttpGet get = new HttpGet(urlS);
        HttpResponse response = null;
        InputStream iStream = null;
        String body = null;
        try {
            response = sendGet(get);
            iStream = response.getEntity().getContent();

            body = loadContent(iStream);
        } catch (MalformedURLException e) {
            String eMessage = "url is wrong";
            AirbrakeHelper.sendException(e, eMessage);
        } catch (IOException e) {
            String eMessage = "loading content";
            AirbrakeHelper.sendException(e, eMessage);
        } finally {
            close(iStream);
        }
        return body;
    }

	/**
	 * Parsed the xml content of the given file.
	 * 
	 * @param parsingFile
	 *            - file which should have a xml structure and should parsed.
	 * @return the parsed xml structure in a ParserVO object. Or null if
	 *         something go wrong.
	 */
	public ParserVO parsingFile(File parsingFile) {
		ParserVO firstElement = null;
		InputStream iStream = null;
		if (parsingFile.exists()){
			try {
				Parser parser = new Parser();
				iStream = new FileInputStream(parsingFile);
				firstElement = parser.parse(iStream);
			} catch (IOException e) {
				String eMessage = "loading content";
				AirbrakeHelper.sendException(e, eMessage);
			} catch (MeinERPParserException e) {
				String eMessage = "parsing server response error";
				AirbrakeHelper.sendException(e, eMessage);
			} finally {
				close(iStream);
			}
		}
		return firstElement;
	}


    public ParserVO parsingFile(String parsingFile) {
        ParserVO firstElement = null;
        try {
            Parser parser = new Parser();
            firstElement = parser.parseContent(parsingFile);
        } catch (MeinERPParserException e) {
            String eMessage = "parsing server response error";
            AirbrakeHelper.sendException(e, eMessage);
        } finally {
        }
        return firstElement;
    }

	public void registGCM(Context context, MeinERPProperties properties) {
		GCMRegistrar.checkDevice(context);
		GCMRegistrar.checkManifest(context);
		final String regId = GCMRegistrar.getRegistrationId(context);
		if (regId.equals("")) {
			GCMRegistrar.register(context,
					(String) properties.get(PropertiesList.GCM_PROJECT_KEY));
		} else {
		}
		gcmId = regId;
	}

	/**
	 * Registry the given GMC id to the backend on the given topic.
	 * @param gcmId - id of the GCM you get.
	 * @param topic you want to registry the client.
	 * @return true if sending with token responsed, else false.
	 */
	public boolean sendGCMIdToServer(String gcmId, String topic){
		HttpClient hClient = new DefaultHttpClient();
		String urlS = String.format(notificationDeviceTemplateUrl, topic, gcmId);
		HttpPost request = new HttpPost(urlS);
		request.setHeader("User-Agent", userAgent);
		HttpResponse response = null;

		String content = null;
		try {
			response = hClient.execute(request);
			content = loadContent(response.getEntity().getContent());
			Parser parser = new Parser();
			ParserVO staffSale = parser.parseContent(content);
			this.notificationServerToken = ServerResponseBuilder.buildSubScripteNotificationResponse(staffSale);;
		}catch(ClientProtocolException e){
			String eMessage = "send POST request error";
			AirbrakeHelper.sendException(e, eMessage);
		} catch (IOException e) {
			String eMessage = "loadContent Error";
			AirbrakeHelper.sendException(e, eMessage);
		} catch (MeinERPParserException e) {
			String eMessage = "parsing server response error";
			AirbrakeHelper.sendException(e, eMessage);
		}
		if (this.notificationServerToken == null)
			return false;
		return true;
	}

	public void sendUnregistGCMIdTobackend(String backendNotificationToken, String topic){
		HttpClient hClient = new DefaultHttpClient();
		String urlS = String.format(notificationUnregistDeviceTemplateUrl, topic, backendNotificationToken);
		HttpPost request = new HttpPost(urlS);
		request.setHeader("User-Agent", userAgent);

		HttpResponse response = null;
		String content = null;
		try {
			response = hClient.execute(request);
			content = loadContent(response.getEntity().getContent());
			Parser parser = new Parser();
			ParserVO staffSale = parser.parseContent(content);
			System.out.println(staffSale);
		}catch(ClientProtocolException e){
			String eMessage = "send POST request error";
			AirbrakeHelper.sendException(e, eMessage);
		} catch (IOException e) {
			String eMessage = "loadContent Error";
			AirbrakeHelper.sendException(e, eMessage);
		} catch (MeinERPParserException e) {
			String eMessage = "parsing server response error";
			AirbrakeHelper.sendException(e, eMessage);
		}
		//TODO Analyse response!
		//TODO Dry to analyseResetPasswordResponse()
	}

    public static List<String> errorLoging = new ArrayList<String>();

	/**
	 * Registry the client with the given information.
	 * @param email
	 * @param password
	 * @param firstname
	 * @param lastname
	 * @param location
	 * @return A Response of the server answer.
	 */
	public ServerResponse registry(String email, String password, String firstname,
			String lastname, String location, String dealerId) {
		HttpPost request = new HttpPost(registryURL);
		MultipartEntity mEntity = new MultipartEntity();
		HttpResponse response = null;

        ServerResponse responseCode = ServerResponse.unknown_error;

		try {
			mEntity.addPart("email", new StringBody(email));
			mEntity.addPart("password", new StringBody(password));
			mEntity.addPart("firstname", new StringBody(firstname));
			mEntity.addPart("lastname", new StringBody(lastname));
			mEntity.addPart("location", new StringBody(location));
            mEntity.addPart("dealer", new StringBody(dealerId));
			// TODO remove if you go productive
			//mEntity.addPart("dryRun", new StringBody("true")); // Test run.

			request.setEntity(mEntity);
            errorLoging.add("Vor dem Senden der Nachricht");
			response = sendPost(request);
            errorLoging.add("Nachricht erfolgreich gesendet");
		} catch (UnsupportedEncodingException e) {
			String eMessage = "encoding not supportet";
			AirbrakeHelper.sendException(e, eMessage);
            responseCode = ServerResponse.server_communication_error;
            errorLoging.add("Sendefehler: Nachrichencoding");
            errorLoging.add(e.getMessage());
		}catch(ClientProtocolException e){
			String eMessage = "send POST request error";
			AirbrakeHelper.sendException(e, eMessage);
            responseCode = ServerResponse.server_communication_error;
            errorLoging.add("Sendefehler: ClienProtokoll");
            errorLoging.add(e.getMessage());
		} catch (IOException e) {
			String eMessage = "loadContent Error";
			AirbrakeHelper.sendException(e, eMessage);
            responseCode = ServerResponse.server_communication_error;
            errorLoging.add("Sendefehler: Allgemeiner Kommunikationsfehler");
            errorLoging.add(e.getMessage());
		}

		//TODO Dry to analyseResetPasswordResponse()
		if (response != null) {
			BufferedReader rd = null;
			try {
				rd = new BufferedReader(new InputStreamReader(response
						.getEntity().getContent()));
				String line = "";
				StringBuffer sBuffer = new StringBuffer();
				while ((line = rd.readLine()) != null) {
					sBuffer.append(line + "\n");
				}
                errorLoging.add(sBuffer.toString());
                if (response.getStatusLine().getStatusCode() == 200)
                    responseCode = ServerResponse.ok;
                else{
                    Parser parser = new Parser();
                    ParserVO firstelement = parser.parseContent(sBuffer.toString());
                    responseCode = ServerResponseBuilder.buildAuthenticationResponse(firstelement);
                }
			} catch (IllegalStateException e) {
				String eMessage = "So you can not access this method.";
				AirbrakeHelper.sendException(e, eMessage);
			} catch (IOException e) {
				String eMessage = "loadContent Error";
				AirbrakeHelper.sendException(e, eMessage);
			} catch (MeinERPParserException e) {
				String eMessage = "parsing server response error";
				AirbrakeHelper.sendException(e, eMessage);
			}finally{
				close(rd);
			}
		}
		return responseCode;
	}

	/**
	 * Reinformate the app and get a token if no exist.
	 */
	protected void reinfomated() {
		InfoVO info = loadInfos();
		if (info != null && info.getUser() == null) {
			MeinERPProperties prop = MeinERPPropertiesFactory
					.getProperties(null);
			String username = prop.get(PropertiesList.USER_E_MAIL);
			String password = prop.get(PropertiesList.USER_PASS);
			if (username != null && password != null) {
				AuthVO authVo = authenticat(username, password);
				if (authVo.getToken() != null)
					this.token = authVo.getToken();
			}
		}
	}

	public void submitIdea(String competitionId, String text, String link,
			File imageFile) {
		String urlS = String.format(ideaSubmitURL, competitionId);

		if (!isAuthenticated())
			throw new NotAuthenticatException("Submit is not possible.");
		HttpPost request = new HttpPost(urlS);
		MultipartEntity mEntity = new MultipartEntity();
		HttpResponse response = null;
		try {
			mEntity.addPart("token", new StringBody(token));
			if (imageFile != null)
				mEntity.addPart("image", new FileBody(imageFile));
			mEntity.addPart("text",
					new StringBody(text, Charset.forName("UTF-8")));
			if (link != null) {
				mEntity.addPart("url",
						new StringBody(link, Charset.forName("UTF-8")));
			}
			request.setEntity(mEntity);
			response = sendPost(request);

		}catch(ClientProtocolException e){
			String eMessage = "send POST request error";
			AirbrakeHelper.sendException(e, eMessage);
		}catch (IOException e) {
			String eMessage = "loadContent Error";
			AirbrakeHelper.sendException(e, eMessage);
		} 

		// TODO : response analyzing
		//TODO Dry to analyseResetPasswordResponse()
		if (response != null) {
			BufferedReader rd;
			try {
				rd = new BufferedReader(new InputStreamReader(response
						.getEntity().getContent()));
				System.out.println(response);
				String line = "";
				StringBuffer sBuffer = new StringBuffer();
				while ((line = rd.readLine()) != null) {
					sBuffer.append(line + "\n");
				}

			} catch (IllegalStateException e) {
				String eMessage = "loadContent Error";
				AirbrakeHelper.sendException(e, eMessage);
			} catch (IOException e) {
				String eMessage = "loadContent Error";
				AirbrakeHelper.sendException(e, eMessage);
			}
		}
	}

	public void unregistGCM(Context context) {
		Intent unregIntent = new Intent(
				"com.google.android.c2dm.intent.UNREGISTER");
		unregIntent.putExtra("app",
				PendingIntent.getBroadcast(context, 0, new Intent(), 0));
		context.startService(unregIntent);
		if (gcmId != null && !gcmId.isEmpty() && this.notificationServerToken != null){
			Thread thread = new Thread(new Runnable(){
				@Override
				public void run() {
				    try {
				       sendUnregistGCMIdTobackend(notificationServerToken, "news");
				    } catch (Exception e) {
				       Log.e(TAG, e.getMessage());
				    }				}
				});
				thread.start();
			
		}
		this.notificationServerToken = null;
		gcmId = null;
	}
	
	/**
	 * Reset your password for the given email address.
	 * @param emailAddress you want to reset.
	 * @return Server response 
	 */
	public ResetPasswordServerResponse resetPassword(String emailAddress){
		String urlS = String.format(passwordResetTemplateURL, emailAddress);
		HttpGet request = new HttpGet( urlS);
		HttpResponse response = null;
		try {
			response = sendGet(request);
		} catch (ClientProtocolException e) {
			String eMessage = "send GET request to reset password; failed";
			AirbrakeHelper.sendException(e, eMessage);
		} catch (IOException e) {
			String eMessage = "loadContent Error";
			AirbrakeHelper.sendException(e, eMessage);
		}
		
		ResetPasswordServerResponse responseCode = ResetPasswordServerResponse.unknown_error;
		
		if (response != null) {
			responseCode = analyseResetPasswordResponse(response);
		}
		
		return responseCode;
	}
	
	private ResetPasswordServerResponse analyseResetPasswordResponse(HttpResponse response){
		ResetPasswordServerResponse responseCode = ResetPasswordServerResponse.unknown_error;
		BufferedReader rd = null;
		try {
			rd = new BufferedReader(new InputStreamReader(response
					.getEntity().getContent()));
			String line = "";
			StringBuffer sBuffer = new StringBuffer();
			while ((line = rd.readLine()) != null) {
				sBuffer.append(line + "\n");
			}
			Parser parser = new Parser();
			ParserVO firstelement = parser.parseContent(sBuffer.toString());
			responseCode = ServerResponseBuilder.buildResetPasswordResponse(firstelement);
		} catch (IllegalStateException e) {
			String eMessage = "So you can not access this method.";
			AirbrakeHelper.sendException(e, eMessage);
		} catch (IOException e) {
			String eMessage = "loadContent Error";
			AirbrakeHelper.sendException(e, eMessage);
		} catch (MeinERPParserException e) {
			String eMessage = "parsing server response error";
			AirbrakeHelper.sendException(e, eMessage);
		}finally{
			close(rd);
		}
		return responseCode;
	}

    private String buildSyncBody(List<DealsItemVO> items){
        if (items == null)
            throw new IllegalStateException("items are null");
        XmlSerializer xmlSerializer = Xml.newSerializer();
        StringWriter writer = new StringWriter();
        try {
            xmlSerializer.setOutput(writer);
            // start DOCUMENT
            xmlSerializer.startDocument("UTF-8", true);

            // open tag
            xmlSerializer.startTag("", "staffsale");
            // open tag
            xmlSerializer.startTag("", "deals");
            for (DealsItemVO vo : items){
                if (vo.isOnlineSaved()){
                    xmlSerializer.startTag("", "deal");
                    xmlSerializer.attribute("", "id", vo.getId());
                    xmlSerializer.startTag("", "online");
                    if (vo.getPassOnlineUrl() != null && !vo.getPassOnlineUrl().isEmpty())
                        xmlSerializer.text(vo.getPassOnlineUrl());
                    else
                        xmlSerializer.text(TagNames.NO_CODE);
                    xmlSerializer.endTag("", "online");
                    xmlSerializer.endTag("", "deal");
                }
            }
            xmlSerializer.endTag("", "deals");
            xmlSerializer.endTag("", "staffsale");
            xmlSerializer.endDocument();
        }
        catch (IOException e){
            e.printStackTrace();
        }

        return writer.toString();
    }

    public HttpResponse sendGet(HttpGet get) throws IOException {
        if(get == null)
            throw  new IllegalStateException("get is null");
        HttpClient hClient = new DefaultHttpClient();
        get.setHeader("User-Agent", userAgent);
        get.setHeader("X-API-Version", "2");
        if (isAuthenticated())
            get.addHeader("X-Auth-Token", token);
        HttpResponse response = hClient.execute(get);
        return response;
    }

    protected HttpResponse sendPost(HttpPost post) throws IOException{
        if (post == null)
            throw new IllegalArgumentException("post is null");

        HttpClient hClient = new DefaultHttpClient();
        post.setHeader("User-Agent", userAgent);
        post.setHeader("X-API-Version", "2");
        if (isAuthenticated())
            post.addHeader("X-Auth-Token", token);
        HttpResponse response = hClient.execute(post);
        return response;
    }

    public HttpURLConnection addAuthDataToConnection(HttpURLConnection inputConnection) {
    	reinfomated();
    	inputConnection.setRequestProperty("User-Agent", userAgent);
    	inputConnection.setRequestProperty("X-API-Version", "2");
    	if (isAuthenticated())
    		inputConnection.setRequestProperty("X-Auth-Token", token);    	
    	return inputConnection;
    }
    
    
    public DealLoadingDetails loadDealDetails(String url){
        HttpGet get = new HttpGet(url);
        InputStream iStream = null;
        DealLoadingDetails vo = null;
        try {
            iStream = sendGet(get).getEntity().getContent();
            String content = loadContent(iStream);
            ParserVO firstElement = parsingFile(content);
            vo = DealsLoadingDetailsBuilder.buildLoadingDetails(firstElement);
        }catch (IOException e){
            e.printStackTrace();
        }finally {
            if (iStream != null)
                try {
                    iStream.close();
                }catch(Exception e){}
        }
        return vo;
    }

    public List<SyncedDealsVO> syncDeals(List<DealsItemVO> items){
        List<SyncedDealsVO> sync = new ArrayList<SyncedDealsVO>();
        if (items != null && isAuthenticated()) {
            System.out.println("!!!!!!!!!!!!!!!!!!!!!!! CALL SYNC");
            String xml = buildSyncBody(items);
            System.out.println("!!!!!!!!!!!!!!!!!!!!!!! XML:");
            System.out.println(xml);
            System.out.println("!!!!!!!!!!!!!!!!!!!!!!! NACH XML");

            HttpPost post = new HttpPost(dealsSyncUrl);

            InputStream iStream = null;
            String body = null;
            try {
                StringEntity mEntity = new StringEntity(xml);
                post.setEntity(mEntity);
                System.out.println("!!!!!!!!!!!!!!!!!!!!!!! NACH XML - EXECUTE");
                HttpResponse response = sendPost(post);
                iStream = response.getEntity().getContent();
                String responseText = loadContent(iStream);
                System.out.println("!!!!!!!!!!!!!!!!!!!!!!! XML:");
                System.out.println(responseText);


                ParserVO paserVo = parsingFile(responseText);
                sync = SyncDealsBuilder.buildSyncDeals(paserVo);

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    if (iStream != null)
                        iStream.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }
        return sync;
    }

    public void sendNewsRead(String newsId){
        String urlS = String.format(newsReadUrl, newsId);
        HttpGet get = new HttpGet(urlS);
        try {
            sendGet(get);

        }catch (Exception e){
            e.printStackTrace();
        }
    }


    public List<DealerVO> loadDealers(String url){
        HttpGet get = new HttpGet(url);
        List<DealerVO> dealers = new ArrayList<DealerVO>();
        try {
            InputStream iSteam = sendGet(get).getEntity().getContent();
            String body = loadContent(iSteam);
            ParserVO parserVO = parsingFile(body);
            dealers = DealerBuilder.buildDealers(parserVO);
            iSteam.close();
        }catch (Exception e){
            e.printStackTrace();
        }
        this.cachedDealers = dealers;
       return dealers;
    }


    public List<DealerVO> loadCachedDealers(){
        return  this.cachedDealers;
    }

	public ArrayList<StaffdealListFragment> getLogInListeners() {
		return logInListeners;
	}

	public void setLogInListeners(ArrayList<StaffdealListFragment> logInListeners) {
		this.logInListeners = logInListeners;
	}
	
	public void addLogInListener(StaffdealListFragment listener) {
		this.logInListeners.add(listener);
	}
	
}

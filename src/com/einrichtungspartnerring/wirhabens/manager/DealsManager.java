package com.einrichtungspartnerring.wirhabens.manager;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;

import android.content.Context;
import android.util.Log;

import com.einrichtungspartnerring.wirhabens.gui.adapter.PicAdapterItem;
import com.einrichtungspartnerring.wirhabens.helper.AirbrakeHelper;
import com.einrichtungspartnerring.wirhabens.parser.MeinERPParserException;
import com.einrichtungspartnerring.wirhabens.parser.Parser;
import com.einrichtungspartnerring.wirhabens.parser.ParserVO;
import com.einrichtungspartnerring.wirhabens.parser.builder.DealerBuilder;
import com.einrichtungspartnerring.wirhabens.parser.builder.DealsBuilder;
import com.einrichtungspartnerring.wirhabens.properties.MeinERPProperties;
import com.einrichtungspartnerring.wirhabens.properties.MeinERPPropertiesFactory;
import com.einrichtungspartnerring.wirhabens.properties.PropertiesList;
import com.einrichtungspartnerring.wirhabens.vo.DealerVO;
import com.einrichtungspartnerring.wirhabens.vo.DealsCategory;
import com.einrichtungspartnerring.wirhabens.vo.DealsItemVO;
import com.einrichtungspartnerring.wirhabens.vo.SyncedDealsVO;

/**
 * Manager for the deals items.
 * 
 * @author Thomas Möller
 * 
 */
public class DealsManager extends MeinEPRSystemManager<DealsItemVO> implements
SystemPaths {

	private static final String TAG = "DealsManager";

	private String dealerPartURL = null;

	/**
	 * Category for unsaved deals
	 */
	private DealsCategory unsavedCategory;
	/**
	 * Category for saved deals
	 */
	private DealsCategory savedCategory;

	/**
	 * List of ids from all saved deals
	 */
	private List<String> idsOfOnlineSavedCategories;
    private List<String> idsOfOnSiteSavedCategories;
    private List<String> idsOfAdSavedCategories;
    private List<String> idsOfReminderSavedCategories;
    /**
     * Hold a list in formatn
     * <id>:<code>
     */
    private List<String> idsToCodeList;
	/**
	 * Path where all saved deals id should store.
	 */
	private String onlineSavedDealsPath;
    private String onsiteSavedDealsPath;
    private String adSavedDealsPath;
    private String reminderSavedDealsPath;
    private String idSToCodeSavePath;

	/**
	 * Initialization constructor.
	 * 
	 * @param netManager
	 *            - instance if the network manager.
	 */
	public DealsManager(NetworkManager netManager, Context context) {
		super(context, DEALS_READED_PATH, DEALS_FILDERED_PATH, netManager);
		MeinERPProperties prop = MeinERPPropertiesFactory.getProperties(context);
		if (xmlSavePath == null)
			xmlSavePath = context.getFilesDir().toString() + DEALS_XML_PATH;
		if (loadURL == null) {
			loadURL = (String) prop.get(PropertiesList.API_BASE_URL) + (String) prop.get(PropertiesList.API_DEALS_URL);
		}
		if (dealerPartURL == null){
			dealerPartURL = prop.get(PropertiesList.API_BASE_URL) + prop.get(PropertiesList.API_DEALER_URL);
		}
		if (onlineSavedDealsPath == null){
            onlineSavedDealsPath = context.getFilesDir().toString() + DEALS_ONLINE_SAVE_PATH;
		}
        if (onsiteSavedDealsPath == null){
            onsiteSavedDealsPath = context.getFilesDir().toString() + DEALS_ONSITE_SAVE_PATH;
        }
        if (adSavedDealsPath == null){
            adSavedDealsPath = context.getFilesDir().toString() + DEALS_SHOW_AS_AD_SAVE_PATH;
        }
        if (reminderSavedDealsPath == null) {
            reminderSavedDealsPath = context.getFilesDir().toString() + DEALS_SHOW_REMINDER_SAVE_PATH;
        }
        if (idSToCodeSavePath == null){
            idSToCodeSavePath = context.getFilesDir().toString() + DEALS_CODE_SAVE_PATH;
        }
		unsavedCategory = new DealsCategory(DealsCategory.IDs.special_unsaved);
        savedCategory = new DealsCategory(DealsCategory.IDs.special_saved);
		this.idsOfOnlineSavedCategories = restoreSavedDeals(onlineSavedDealsPath);
        this.idsOfOnSiteSavedCategories = restoreSavedDeals(onsiteSavedDealsPath);
        this.idsOfAdSavedCategories = restoreSavedDeals(adSavedDealsPath);
        this.idsOfReminderSavedCategories = restoreSavedDeals(reminderSavedDealsPath);
        this.idsToCodeList = restoreSavedDeals(idSToCodeSavePath);
	}

	/**
	 * Set the given vo to a saved deal.
	 * @param vo
	 */
	public void setToSaveOnlineDeal(DealsItemVO vo){
		vo.setCategory(savedCategory);
        vo.setOnlineSaved(true);
        idsOfOnlineSavedCategories.add(vo.getId());
		storeSavedDeals(idsOfOnlineSavedCategories, onlineSavedDealsPath);
	}

    public void setToSaveOnSiteDeal(DealsItemVO vo){
        vo.setCategory(savedCategory);
        vo.setOnSiteSaved(true);
        idsOfOnSiteSavedCategories.add(vo.getId());
        storeSavedDeals(idsOfOnSiteSavedCategories, onsiteSavedDealsPath);
    }

    public void saveCodeToDeal(DealsItemVO vo){
        String idCode = vo.getId() + ":" +vo.getPassOnlineCode();
        idsToCodeList.add(idCode);
        storeSavedDeals(idsToCodeList, idSToCodeSavePath);
    }

    public boolean hasNexAdDeal(){
        return nextAdDeal() == null ? false : true;
    }

    public boolean hasNextReminderDeal(){
        return nextReminderDeal() == null ? false : true;
    }

    public DealsItemVO getNextAdDeal(){
        DealsItemVO vo = nextAdDeal();
        if (vo != null){
            idsOfAdSavedCategories.add(vo.getId());
            vo.setAdvertising(true);
            storeSavedDeals(idsOfAdSavedCategories, adSavedDealsPath);
        }
        return vo;

    }

    public DealsItemVO getNextReminderDeal(){
        DealsItemVO vo = nextReminderDeal();
        if (vo != null){
            idsOfReminderSavedCategories.add(vo.getId());
            vo.setReminded(true);
            storeSavedDeals(idsOfReminderSavedCategories, reminderSavedDealsPath);
        }
        return vo;

    }

    protected DealsItemVO nextAdDeal(){
        DealsItemVO result = null;

        if (this.items != null) {
            Collections.sort(this.items);
            for (DealsItemVO value : this.items) {
                if (!value.isAdvertising()) {
                    result = value;
                    break;
                }
            }
        }
        return result;
    }

    protected DealsItemVO nextReminderDeal(){

        DealsItemVO result = null;
        if (this.items != null) {
            Collections.sort(this.items);
            for (DealsItemVO value : this.items) {
                if (!value.isReminded()) {
                    result = value;
                    break;
                }
            }
        }
        return result;
    }

	/**
	 * load all id of stored saved ids from the given path.
	 * @param savePath - path where the stored saved ids are.
	 * @return list if saved deals ids.
	 */
	private List<String>  restoreSavedDeals(String savePath) {
		List<String> idsOfSavedCategories = new ArrayList<String>();
		File file = new File(savePath);
		if (file.exists()){
			try {
				idsOfSavedCategories = readLines(file);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return idsOfSavedCategories;
	}

	/**
	 * Store all given ids in the idsOfSavedCategories into the given savePath
	 * @param idsOfSavedCategories - ids if saved deals.
	 * @param savePath - path where you want to save the ids.
	 */
	private void storeSavedDeals(List<String> idsOfSavedCategories,String savePath) {
		File file = new File(savePath);
		try {
			writeLines(idsOfSavedCategories, file);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

    protected Map<String,String> splitListFirstChar(List<String> items, String sign){
        Map<String,String> idCodeMap = new HashMap<String, String>();
        for (String item : items){
            if (item.contains(sign)){
                String []splitted = item.split(sign);
                if (splitted.length > 1){
                    idCodeMap.put(splitted[0], splitted[1]);
                }
            }
        }
        return  idCodeMap;
    }

	@Override
	protected List<DealsItemVO> parsedCached(File file) {
		List<CategoryFilter> categories = new ArrayList<CategoryFilter>();
		categories.add(savedCategory);
		categories.add(unsavedCategory);
		List<DealsItemVO> deals = null;
		if (file.exists()){
			ParserVO firstElement = netManager.parsingFile(file);
			if (firstElement != null)
				deals = DealsBuilder.buildDeals(firstElement);
		}
        Map<String,String> tempListOfCodeIds = splitListFirstChar(this.idsToCodeList, ":");

		if (deals != null){
			for (DealsItemVO vo : deals){
				vo.setCategory(unsavedCategory);
                vo.setOnSiteSaved(false);
                vo.setOnlineSaved(false);
                if (idsOfOnlineSavedCategories.contains(vo.getId()))  {
                    vo.setCategory(savedCategory);
                    vo.setOnlineSaved(true);
                }
                if (idsOfOnSiteSavedCategories.contains(vo.getId()))  {
                    vo.setCategory(savedCategory);
                    vo.setOnSiteSaved(true);
                }
                if (idsOfAdSavedCategories.contains(vo.getId()))  {
                    vo.setAdvertising(true);
                }

                if (idsOfReminderSavedCategories.contains(vo.getId()))  {
                    vo.setReminded(true);
                }
                if (tempListOfCodeIds.keySet().contains(vo.getId())){
                    vo.setPassOnlineCode(tempListOfCodeIds.get(vo.getId()));
                }

			}
			setCategories(categories);
		}
		return deals;
	}

	/**
	 * Load the dealers from the information of the given vo and return all dealers.
	 * @param vo with information about the dealers.
	 * @return list of dealers.
	 */
	public List<DealerVO> getDealers(DealsItemVO vo){
		List<DealerVO> dealers = null;
		String urlS = dealerPartURL + vo.getId();
		try {
			URL url = new URL(urlS);
			Log.d(TAG, "load from URL dealers : " + urlS);
			String content = netManager.loadContent(url);
			Parser parser = new Parser();
			ParserVO firstElement = parser.parseContent(content);
			dealers  = DealerBuilder.buildDealers(firstElement);

		} catch(MalformedURLException e){
			String m ="You used url is malformated! Your URL" + urlS;
			Log.e(TAG, m , e);
			AirbrakeHelper.sendException(e, m);
		}catch (IOException e) {
			String m ="Loading from url is not poosible. Your URL" + urlS;
			Log.e(TAG, m, e);
			AirbrakeHelper.sendException(e, m);
		} catch (MeinERPParserException e) {
			String m ="Parsing content is not possible";
			Log.e(TAG, m, e);
			AirbrakeHelper.sendException(e, m);
		}

		return dealers;
	}

	/**
	 * Load the dealers from the information of the given vo and return all dealers. 
	 * @param dealId
	 * @return
	 */
	public List<DealerVO> getDealers(String dealId) {
		DealsItemVO vo = getItemById(dealId);
		if (vo != null)
			return getDealers(vo);
		return null;
	}

    @Override
    protected List<DealsItemVO> parseString(String content,List<CategoryFilter> filters) {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<PicAdapterItem> loadPicAdapterItem() {
        List<SyncedDealsVO> syncDeals = netManager.syncDeals(this.items);
        for (SyncedDealsVO sync :syncDeals){
            DealsItemVO vo = getItemById(sync.getId());
            if (vo != null){
                if(sync.getOnline() != null  && !sync.getOnline().isEmpty()){
                    vo.setPassOnlineCode(sync.getOnline());
                    setToSaveOnlineDeal(vo);
                }
                if(sync.getOnSite() != null && !sync.getOnSite().isEmpty()){
                    vo.setPassOnSiteUrl(sync.getOnSite());
                    setToSaveOnSiteDeal(vo);
                }
            }
        }

        return super.loadPicAdapterItem();
    }

    public int getPositionByItem(DealsItemVO vo){
        for (int i = 0 ; i < this.items.size(); i++){
            if (vo.getId().equals(this.items.get(i).getId()))
                return 1;
        }
        return -1;

    }
}

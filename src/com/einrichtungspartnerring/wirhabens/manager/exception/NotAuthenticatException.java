package com.einrichtungspartnerring.wirhabens.manager.exception;

/**
 * Throw if the user not authenticated.
 * 
 * @author Thomas Möller
 * 
 */
public class NotAuthenticatException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5735916285155291123L;

	public NotAuthenticatException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public NotAuthenticatException(String detailMessage) {
		super(detailMessage);
		// TODO Auto-generated constructor stub
	}

	public NotAuthenticatException(String detailMessage, Throwable throwable) {
		super(detailMessage, throwable);
		// TODO Auto-generated constructor stub
	}

	public NotAuthenticatException(Throwable throwable) {
		super(throwable);
		// TODO Auto-generated constructor stub
	}

}

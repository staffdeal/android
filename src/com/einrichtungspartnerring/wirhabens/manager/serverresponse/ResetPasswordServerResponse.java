package com.einrichtungspartnerring.wirhabens.manager.serverresponse;


public enum ResetPasswordServerResponse {
	ok, wrong_email_address_error, email_invalid_formated_error, server_error, unknown_error;
	
	public static ResetPasswordServerResponse mappingServerResponseCode(int code){
		ResetPasswordServerResponse value = ResetPasswordServerResponse.unknown_error;
		switch (code) {
		case 0:
			value = ResetPasswordServerResponse.ok;
			break;
		case -1:
			value = ResetPasswordServerResponse.ok;
			break;
		case 200:
			value = ResetPasswordServerResponse.ok;
			break;
		case 400:
			value = ResetPasswordServerResponse.email_invalid_formated_error;
			break;
		case 403:
			value = ResetPasswordServerResponse.wrong_email_address_error;
			break;
		case 500:
			value = ResetPasswordServerResponse.server_error;
			break;
		default:
			break;
		}
		return value;
	}
}

package com.einrichtungspartnerring.wirhabens.manager;

import android.content.Context;

/**
 * Factory Manager for all other manager.
 * 
 * @author Thomas Möller
 * 
 */
public class ManagerFactory {

	/**
	 * Manager for the news.
	 */
	private static NewsManager newsManager = null;
	/**
	 * Manager for the network operations.
	 */
	private static NetworkManager networkManager = null;
	/**
	 * Manager for the images.
	 */
	private static ImageManager imageManager = null;

	/**
	 * Manager for the deals items.
	 */
	private static DealsManager dealsManager = null;

	/**
	 * Manager for the events items.
	 */
	private static EventsManager eventsManager = null;

	
	/**
	 * Instance if the competition manager.
	 */
	private static CompetitionIdeasManager competitionManager = null;

	/**
	 * Factory method for the news manager.
	 * 
	 * @param context
	 *            - app context.
	 * @return a instance of the news manager.
	 */
	public synchronized static CompetitionIdeasManager buildCompetitionIdeasManager(
			Context context) {
		if (competitionManager == null)
			competitionManager = new CompetitionIdeasManager(
					buildNetworkManager(context), context);
		return competitionManager;

	}

	/**
	 * Factory method for the deal manager.
	 * 
	 * @param context
	 *            - app context.
	 * @return a instance of the deal manager.
	 */
	public synchronized static DealsManager buildDealsManager(Context context) {
		if (dealsManager == null)
			dealsManager = new DealsManager(buildNetworkManager(context),
					context);
		return dealsManager;
	}

	/**
	 * Factory method for the image manager.
	 * 
	 * @param context
	 *            of the app.
	 * @return a instance of the image manager.
	 */
	public synchronized static ImageManager buildImageManager(Context context) {
		if (imageManager == null)
			imageManager = new ImageManager(context);
		return imageManager;
	}

	/**
	 * Factory method for the network manager.
	 * 
	 * @return a instance of the network manager.
	 */
	public synchronized static NetworkManager buildNetworkManager(
			Context context) {
		if (networkManager == null)
			networkManager = new NetworkManager(context);
		return networkManager;
	}

	/**
	 * Factory method for the events manager.
	 * 
	 * @param context
	 *            - app context.
	 * @return a instance of the news manager.
	 */
	public synchronized static EventsManager buildEventsManager (Context context) {
		if (eventsManager == null)
			eventsManager = new EventsManager(buildNetworkManager(context), context);
		return eventsManager;

	}
	
	/**
	 * Factory method for the news manager.
	 * 
	 * @param context
	 *            - app context.
	 * @return a instance of the news manager.
	 */
	public synchronized static NewsManager buildNewsManager(Context context) {
		if (newsManager == null)
			newsManager = new NewsManager(buildNetworkManager(context), context);
		return newsManager;

	}
}

package com.einrichtungspartnerring.wirhabens.manager;

import java.io.File;
import java.util.List;

import android.content.Context;
import android.util.Log;

import com.einrichtungspartnerring.wirhabens.parser.ParserVO;
import com.einrichtungspartnerring.wirhabens.parser.builder.EventBuilder;
import com.einrichtungspartnerring.wirhabens.properties.MeinERPProperties;
import com.einrichtungspartnerring.wirhabens.properties.MeinERPPropertiesFactory;
import com.einrichtungspartnerring.wirhabens.properties.PropertiesList;
import com.einrichtungspartnerring.wirhabens.vo.EventVO;

public class EventsManager extends MeinEPRSystemManager<EventVO> {

	public EventsManager(NetworkManager buildNetworkManager, Context context) {
		super(context, EVENTS_READED_PATH, null, buildNetworkManager);
		MeinERPProperties prop = MeinERPPropertiesFactory.getProperties(context);
		if (xmlSavePath == null)
			xmlSavePath = context.getFilesDir().toString() + EVENTS_XML_PATH;
		if (loadURL == null) {
			loadURL = (String) prop.get(PropertiesList.API_BASE_URL) + (String) prop.get(PropertiesList.API_EVENTS_LIST_URL);
			Log.d("EVENTS","downloading events from " + loadURL);
		}
	}
	
	@Override
	protected List<EventVO> parsedCached(File file) {
		List<EventVO> items = null;
		if (file.exists()){
			ParserVO firstElement = netManager.parsingFile(file);
			if (firstElement != null) {
				items = EventBuilder.buildevents(firstElement);
			}
		}
		
		return items;
	}

    @Override
    protected List<EventVO> parseString(String content, List<CategoryFilter> filters) {
        throw new UnsupportedOperationException();
    }
}

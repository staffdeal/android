package com.einrichtungspartnerring.wirhabens.manager;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.util.Log;

import com.einrichtungspartnerring.wirhabens.gui.adapter.PicAdapterItem;
import com.einrichtungspartnerring.wirhabens.helper.StreamHelper;

/**
 * Top class for interaction between UI and manager level.
 * 
 * @author Thomas Möller
 * 
 */
public abstract class MeinEPRSystemManager<T extends PicAdapterItem> extends StreamHelper implements
SystemPaths {

	private static final String TAG = "MeinERPSystemManager";

	/**
	 * Path where the read element of the system should stored.
	 */
	protected String readedPath;
	/**
	 * Ids if items which the user have read.
	 */
	protected List<String> readedIds = null;
	/**
	 * Items of one type in the system.
	 */
	protected List<T> items = null;
	/**
	 * List of categories of the items.
	 */ 
	protected List<CategoryFilter> categories = null;

	/**
	 * list if filtered items by the categories filter.
	 */
	protected List<T> filteredItems = null;

	/**
	 * URL where the specific manager can load the new content.
	 */
	protected String loadURL = null;

	/**
	 * where the online loaded items should saved.
	 */
	protected String xmlSavePath = null;

	/**
	 * Network  manager
	 */
	protected NetworkManager netManager;

    protected String nextURL;

	/**
	 * In this path will all ids from categories stored which should NOT shown the user.
	 */
	protected String filteredPath;


	public MeinEPRSystemManager(Context context, String readedPath, String filteredPath, NetworkManager manager) {
		this.readedPath = context.getFilesDir().toString() + readedPath;
		if (filteredPath != null)
			this.filteredPath = context.getFilesDir().toString() + filteredPath;
		this.netManager = manager;
	}

	@SuppressWarnings("unchecked")
	protected List<PicAdapterItem> convert(List<T> items) {
		List<PicAdapterItem> picItems = null;
		if (items != null && items.size() > 0
				&& items.get(0) instanceof PicAdapterItem) {
			picItems = (List<PicAdapterItem>) (List<?>) items;
		}
		return picItems;
	}

	public List<CategoryFilter> getCategories() {
		return this.categories;
	}

	/**
	 * Check if the given id in the given readedIds
	 * 
	 * @param id
	 *            you want to check if is in the list.
	 * @param readedIds
	 *            list of ids for checking
	 * @return true, if given id in given readedIds, else false.
	 */
	protected boolean isInReaded(String id, List<String> readedIds) {
		if (id == null || readedIds == null || readedIds.isEmpty())
			return false;
		for (String readedId : readedIds) {
			if (id.equals(readedId))
				return true;
		}
		return false;
	}

	/**
	 * 
	 * @return the items from the manager which should show for the
	 *         PicTextAdapter. But without using the Internet connection.
	 */
	public List<PicAdapterItem> loadOfflinePicAdapterItem(){
		List<PicAdapterItem> converteItems = null;
		if (items == null){
			items = parsedCached(new File(this.xmlSavePath));
			restoreFilteredCategories(this.categories, this.filteredPath);
			this.filteredItems = filterList(this.categories, items);
			converteItems = convert(this.filteredItems);
			restoreReadedElement(converteItems);			
		}else{
			converteItems = convert(this.filteredItems);
		}
		return converteItems;
	}

	/**
	 * Parse the given file specific for T.
	 * @param file where should parsed.
	 * @return a List of parsed T objects. 
	 */
	protected abstract List<T> parsedCached(File file) ;

    protected abstract List<T> parseString(String content, List<CategoryFilter> filters);

	/**
	 * @return the items from the manager which should show for the
	 *         PicTextAdapter.
	 */
	public List<PicAdapterItem> loadPicAdapterItem(){
		storeReadedElements(convert(this.items)); 
		this.items = refresh(this.xmlSavePath, this.loadURL);
		restoreReadedElement(convert(this.items));
		
		restoreFilteredCategories(this.categories, this.filteredPath);
		this.filteredItems = filterList(this.categories, this.items);
		
		List<PicAdapterItem> items = convert(this.filteredItems);
		return items;
	}

    public List<PicAdapterItem> loadPicAdapterItemFromNextPage(){
        if(this.nextURL != null) {
            storeReadedElements(convert(this.items));
            this.items.addAll(loadNextPage(this.nextURL));
            restoreReadedElement(convert(this.items));

            restoreFilteredCategories(this.categories, this.filteredPath);
            this.filteredItems = filterList(this.categories, this.items);
        }
        List<PicAdapterItem> items = convert(this.filteredItems);
        return items;
    }
  
	/**
	 * Store the categories which the user didn't want to see.
	 * So new categories will showed.
	 * @param categories - you want to store. Will only store category elements where visible is false.
	 * @param path where you want to store the ids.
	 */
	private void storeFilteredCategories(List<CategoryFilter> categories, String path){
		if (path == null)
			return;
		List<String> filteredCategoryIds = new ArrayList<String>();
		for (CategoryFilter category : categories){
			if (!category.isVisible())
				filteredCategoryIds.add(category.getId());
		}
		File file = new File(path);
		try {
			writeLines(filteredCategoryIds, file);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Save all give items into the path you have set in the constructor.
	 * 
	 * @param items
	 */
	protected void storeReadedElements(List<PicAdapterItem> items) {
		if (items == null)
			return;
		this.readedIds = scanForReadedElements(items);
		Log.d(TAG, "have follor readedIds size here : " + readedIds.size());
		File file = new File(readedPath);
		try {
			writeLines(readedIds, file);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Scan in the given items for all read items and return a list of ids from
	 * them.
	 * 
	 * @param items you want to scan.
	 * @return a list of all ids of given read items.
	 */
	protected List<String> scanForReadedElements(List<PicAdapterItem> items) {
		List<String> readedIds = new ArrayList<String>();
		if (items != null){
			for (PicAdapterItem item : items) {
				if (item.isReaded())
					readedIds.add(item.getId());
			}
		}
		return readedIds;
	}

	/**
	 * Abstract Setter for the categories.
	 * @param categories
	 */
	@SuppressWarnings("unchecked")
	protected void setCategories(List<?> categories) {
		if (categories != null && categories.size() > 0 && categories.get(0) instanceof CategoryFilter)
			this.categories = (List<CategoryFilter>) categories;
	}

	/**
	 * Set the visible of the given categories "false" if the id of the category in the given file.
	 * @param categories you want to set the visible.
	 * @param path where the file with ids should be.
	 */
	private void restoreFilteredCategories(List<CategoryFilter> categories, String path){
		if (path == null || categories == null)
			return;
		File file = new File(path);
		try {
			List<String> filteredCategoriesId = readLines(file);
			if (filteredCategoriesId != null && !filteredCategoriesId.isEmpty()){
				for (String id : filteredCategoriesId){
					for (CategoryFilter category : categories){
						if (id.equals(category.getId())){
							category.setVisible(false);
							break;
						}
					}
				}
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Set all given items to read if a id to them saved.
	 * 
	 * @param items
	 *            you want set to read.
	 */
	protected void restoreReadedElement(List<PicAdapterItem> items) {
		if (items == null)
			return;
		Log.d(TAG, "settings readed element for elements size : " + items.size());
		if (readedIds == null || readedIds.isEmpty()){
			File file = new File(readedPath);
			try {
				this.readedIds = readLines(file);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		if (readedIds != null) {
			Log.d(TAG, "have follow readed ids here : " + readedIds.size());
			for (PicAdapterItem item : items) {
				if (isInReaded(item.getId(), this.readedIds))
					item.setReaded(true);

			}
		}
	}
	
	/**
	 * Search after the given id an return a founded object.
	 * @param id you search from the object.
	 * @return the searched object or null if not found.
	 */
	public T getItemById(String id){
		T searched = null;
		if (items != null) {
			for (T item : items) {
				if (item.getId().equals(id)) {
					searched = item;
					break;
				}
			}
		}
		return searched;
	}

	/**
	 * Return the item by given position.
	 * @return the item.
	 */
	protected T getItemByPosition(int i){
		if (filteredItems != null && i >= 0 && i < filteredItems.size())
			return filteredItems.get(i);
		return null;
	}

	/**
	 * Return the item by given position. And set read if you want.
	 * @param  i position of the item.
	 * @param setReaded should the item set read.
	 * @return the item you want. Null if not found.
	 */
	public T getItemByPosition(int i, boolean setReaded){
		T item = getItemByPosition(i);
		if (setReaded && item != null){
			item.setReaded(true);
			storeReadedElements(convert(this.items));
		}
		return item;
	}


	/**
	 * Filter all items after the given filters by the category id. Return a list with all filtered items.
	 * @param filters you searched for.
	 * @param items you want filtered.
	 * @return a list of filtered items.
	 */
	public List<T> filterList(List<CategoryFilter> filters, List<T> items){
		List<T> filteredItems = new ArrayList<T>();
		if (filters != null && !filters.isEmpty()){
			for (CategoryFilter filter : filters){
				for (T item : items){
					if (filter.getId().equals(item.getCategoryId()) && filter.isVisible())
						filteredItems.add(item);
				}
			}
		}else
			filteredItems = items;
		return filteredItems;

	}

	/**
	 * Set the given filters visible to the visible of the class categories.
	 * @param filters
	 */
	public void setFilters(List<CategoryFilter> filters){
		if (filters != null){
			for (CategoryFilter filter : filters){
				for (CategoryFilter category : this.categories){
					if (category.getId().equals(filter.getId())){
						category.setVisible(filter.isVisible());
						break;
					}
				}
			}
		}
		this.filteredItems = filterList(this.categories, this.items);
		storeFilteredCategories(filters, this.filteredPath);
	}

	/**
	 * Refresh the xml given xml file from the online system behind the given URL.
	 * @param xmlSavepath where data from online system should store.
	 * @param loadURL URL to the online system.
	 * @return parsed elements.
	 */
	protected synchronized List<T> refresh(String xmlSavepath, String loadURL){
		List<T> items = null;
		File file = new File(xmlSavepath);
		netManager.loadToXML(file, loadURL);
		items = this.parsedCached(file);
		return items;
	}

    /**
     * Refresh the xml given xml file from the online system behind the given URL.
     * @param loadURL URL to the online system.
     * @return parsed elements.
     */
    protected synchronized List<T> loadNextPage(String loadURL){
        List<T> items = null;
        String page = netManager.loadNewPage(loadURL);
        items = this.parseString(page, categories);
        return items;
    }
	
	/**
	 * Set all items readed.
	 */
	public void setAllItemsReaded(){
		for (T item : items)
			item.setReaded(true);
		storeReadedElements(convert(this.items));
	}
}

package com.einrichtungspartnerring.wirhabens.manager;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;

import com.einrichtungspartnerring.wirhabens.parser.ParserVO;
import com.einrichtungspartnerring.wirhabens.parser.builder.NewsListBuilder;
import com.einrichtungspartnerring.wirhabens.properties.MeinERPProperties;
import com.einrichtungspartnerring.wirhabens.properties.MeinERPPropertiesFactory;
import com.einrichtungspartnerring.wirhabens.properties.PropertiesList;
import com.einrichtungspartnerring.wirhabens.vo.NewsCategoryVO;
import com.einrichtungspartnerring.wirhabens.vo.NewsItemVO;

/**
 * Manager for news items in the system.
 * 
 * @author Thomas Möller
 * 
 */
public class NewsManager extends MeinEPRSystemManager<NewsItemVO> {

	/**
	 * Initialization constructor.
	 * 
	 * @param netManager
	 */
	protected NewsManager(NetworkManager netManager, Context context) {
		super(context, NEWS_READED_PATH, NEWS_FILDERED_PATH , netManager);
		MeinERPProperties prop = MeinERPPropertiesFactory
				.getProperties(context);
		if (xmlSavePath == null) {
			xmlSavePath = context.getFilesDir().toString() + NEWS_XML_PATH;
		}
		if (loadURL == null) {
			loadURL = (String) prop.get(PropertiesList.API_BASE_URL) + (String) prop.get(PropertiesList.API_NEWS_URL);
		}

	}

	@Override
	protected List<NewsItemVO> parsedCached(File newsFile) {
		List<NewsItemVO> items = null;
        List<NewsCategoryVO>  categories = new ArrayList<NewsCategoryVO>();
		if (newsFile.exists()) {
			ParserVO firstElement = netManager.parsingFile(newsFile);
			if (firstElement != null) {
				items = new ArrayList<NewsItemVO>();
                this.nextURL = NewsListBuilder.buildNews(items, categories, firstElement);
			}
		}
		setCategories(categories);
		return items;
	}

    @Override
    protected List<NewsItemVO> parseString(String content, List<CategoryFilter> filters) {
        List<NewsItemVO> items = new ArrayList<NewsItemVO>();
        List<NewsCategoryVO> categories = (List<NewsCategoryVO>)(List<?>) filters;
        ParserVO firstElement = netManager.parsingFile(content);
        if (firstElement != null) {
            items = new ArrayList<NewsItemVO>();
            this.nextURL = NewsListBuilder.buildNews(items, categories, firstElement);
        }
        return items;
    }


}

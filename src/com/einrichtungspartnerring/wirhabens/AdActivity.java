package com.einrichtungspartnerring.wirhabens;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Paint;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.einrichtungspartnerring.wirhabens.gui.adapter.PicTextAdapter.ViewHolder;
import com.einrichtungspartnerring.wirhabens.gui.listener.ad.CloseButton;
import com.einrichtungspartnerring.wirhabens.gui.listener.ad.GoToDealButton;
import com.einrichtungspartnerring.wirhabens.helper.AirbrakeHelper;
import com.einrichtungspartnerring.wirhabens.helper.ImageHelper;
import com.einrichtungspartnerring.wirhabens.imagedownloader.ImageDownloader;
import com.einrichtungspartnerring.wirhabens.manager.DealsManager;
import com.einrichtungspartnerring.wirhabens.manager.ImageManager;
import com.einrichtungspartnerring.wirhabens.manager.ManagerFactory;
import com.einrichtungspartnerring.wirhabens.vo.DealsItemVO;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Locale;

/**
 * Created by loc on 17.08.14.
 */
public class AdActivity extends Activity {

    public static final  String BUNDLE_ID = "deal_id";
    private static final String TAG = "AdActivity";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try {
            if (!MainActivity.wasStarted) {
                Intent i = new Intent(getBaseContext(), MainActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
            } else {
                setContentView(R.layout.actitivty_ad);

                if (android.os.Build.VERSION.SDK_INT <= Build.VERSION_CODES.GINGERBREAD_MR1){
                    MainActivity.customActionBar();
                }
                else{
                    MainActivity.customActionBar(getActionBar(), this);
                }

                Bundle extras = getIntent().getExtras();
                String itemId = null;
                if(extras.containsKey(BUNDLE_ID)){
                    itemId = extras.getString(BUNDLE_ID);
                }

                DealsManager dealsmanager = ManagerFactory.buildDealsManager(this);
                DealsItemVO vo = dealsmanager.getItemById(itemId);


                TextView date = (TextView) findViewById(R.id.ad_date);
                TextView title = (TextView) findViewById(R.id.ad_title);
                TextView enterprise = (TextView) findViewById(R.id.ad_enterprise);

                ImageManager iManager = ManagerFactory.buildImageManager(this);
                File imageFile = iManager.getIfExist(vo.getAdapterImagePath());
                ImageView iView = (ImageView) findViewById(R.id.ad_image);
                if (imageFile != null && imageFile.exists()) {
                    Bitmap imageBitmap = ImageHelper.decodeAndScaleFile(imageFile, 100, 100);
                    iView.setImageBitmap(imageBitmap);
                } else {
                    ImageDownloader iDown = new ImageDownloader(this, iManager);
                    if (iDown != null && vo.getAdapterImagePath() != null && !vo.getAdapterImagePath().isEmpty()) {
                    	ViewHolder viewHolder = new ViewHolder();
                    	viewHolder.image = iView;
                    	viewHolder.id = vo.getId();
                        iDown.add(viewHolder, vo.getId(), vo.getAdapterImagePath());
                    }
                }

                SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy", Locale.GERMANY);
                date.setText(getString(R.string.deal_date_head_text) + " "+ df.format(vo.getEndDate()));    //todo

                enterprise.setText(vo.getProvider());

                title.setText(vo.getTitle());

                TextView teaser = (TextView) findViewById(R.id.ad_teaser_text);
                teaser.setText(vo.getTeaser());

                TextView price =  (TextView) findViewById(R.id.ad_price);
                if (vo.getPrice() != null && !vo.getPrice().isEmpty()){
                    price.setVisibility(View.VISIBLE);
                    price.setText(vo.getPrice());
                } else{
                    price.setVisibility(View.GONE);
                }

                TextView oldPrice =  (TextView) findViewById(R.id.ad_old_price);
                if (vo.getOldPrice() != null && !vo.getOldPrice().isEmpty()){
                    oldPrice.setVisibility(View.VISIBLE);
                    oldPrice.setText(vo.getOldPrice());
                    oldPrice.setPaintFlags(oldPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                } else{
                    oldPrice.setVisibility(View.GONE);
                }

                Button exitButton = (Button) findViewById(R.id.ad_exit_button) ;
                exitButton.setOnClickListener(new CloseButton(this));
                Button goToDealButton = (Button) findViewById(R.id.ad_go_to_deal_button) ;
                goToDealButton.setOnClickListener(new GoToDealButton(null,this, vo, dealsmanager));
            }
        }catch(Exception e){
            String eMessage = "Unknown Exception";
            AirbrakeHelper.sendException(e, eMessage);
        }
    }
}
package com.einrichtungspartnerring.wirhabens;

import java.text.SimpleDateFormat;
import java.util.Locale;

import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.TextView;

import com.einrichtungspartnerring.wirhabens.helper.AirbrakeHelper;

public class GeneralDetailsActivity extends Activity {

	public final static String TITLE = "title";
	public final static String FILENAME = "filename";
	public final static String DESC_TITLE = "desc_title";
	public final static String DESC_DATE_FROM = "desc_data_from";
	public final static String DESC_DATE_UNTIL = "desc_data_until";
	public final static String DESC_AUTHOR = "desc_data_author";
	public final static String DESC_CONTENT = "desc_content";
	public final static String DESC_COMPANY = "desc_company";
	public final static String DESC_CATEGORY = "desc_category";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		try{
			super.onCreate(savedInstanceState);
			setContentView(R.layout.activity_general_details);
		
			if (android.os.Build.VERSION.SDK_INT <= Build.VERSION_CODES.GINGERBREAD_MR1){
				MainActivity.customActionBar();
			}
			else{
				MainActivity.customActionBar(getActionBar(), this);
			}

			Bundle extras = getIntent().getExtras();
            if (extras == null)
                finish();

            if (extras.getString(TITLE) != null) {
				setTitle(extras.getString(TITLE));
            }
            
        	String templateString = ((StaffsaleApplication) getApplication()).getHtmlTemplateForFile(extras.getString(FILENAME));        	
        	templateString = templateString.replace("##META##","<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, maximum-scale=1.0\">");

        	String tempStr;
            if (extras.getString(DESC_TITLE) != null) {
            	tempStr = extras.getString(DESC_TITLE);
			} else {
				tempStr = "";
			}			
            templateString = templateString.replace("##TITLE##", tempStr);

            if (extras.getString(DESC_CATEGORY) != null) {
            	tempStr = extras.getString(DESC_CATEGORY);
			} else {
				tempStr = "";
			}			
            templateString = templateString.replace("##CATEGORY##", tempStr);
            
            if (extras.getString(DESC_COMPANY) != null) {
            	tempStr = extras.getString(DESC_COMPANY);
			} else {
				tempStr = "";
			}			
            templateString = templateString.replace("##COMPANY##", tempStr);
            
			String date = "";
			if (extras.getString(DESC_DATE_FROM) != null){
				date = extras.getString(DESC_DATE_FROM);
			}

			if (extras.getString(DESC_DATE_UNTIL) != null){
				date = date + extras.getString(DESC_DATE_UNTIL);
			}
			
			templateString = templateString.replace("##DATE##",date);
			
			if (extras.getString(DESC_CONTENT) != null) {				
				tempStr = extras.getString(DESC_CONTENT);
			} else {
				tempStr = "";
			}			
            templateString = templateString.replace("##CONTENT##", tempStr);

			
			if (extras.getString(DESC_AUTHOR) != null){
				tempStr = extras.getString(DESC_AUTHOR);
			} else {
				tempStr = "";
			}	
			templateString = templateString.replace("##AUTHOR##", tempStr);

			WebView content = (WebView) findViewById(R.id.general_detail_content);
			WebSettings settings = content.getSettings();
			settings.setDefaultFontSize(20);
			settings.setDefaultTextEncodingName("utf-8");
			content.loadData(templateString, "text/html; charset=UTF-8", null);


		
		}catch(Exception e){
			String eMessage = "Unknown Exception";
			AirbrakeHelper.sendException(e, eMessage);
		}
	}
}

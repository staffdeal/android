package com.einrichtungspartnerring.wirhabens;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;

import com.einrichtungspartnerring.wirhabens.helper.HTMLProjectHelper;
import com.einrichtungspartnerring.wirhabens.manager.ManagerFactory;
import com.einrichtungspartnerring.wirhabens.properties.MeinERPProperties;
import com.einrichtungspartnerring.wirhabens.vo.InfoVO;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import android.app.Application;
import android.content.Context;
import android.content.res.AssetManager;

public class StaffsaleApplication extends Application {

	/**
	   * Enum used to identify the tracker that needs to be used for tracking.
	   *
	   * A single tracker is usually enough for most purposes. In case you do need multiple trackers,
	   * storing them all in Application object helps ensure that they are created only once per
	   * application instance.
	   */
	  public enum TrackerName {
	    APP_TRACKER // Tracker used only in this app.
	  }

	  HashMap<TrackerName, Tracker> mTrackers = new HashMap<TrackerName, Tracker>();

	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();
	}

	public String getHtmlTemplateForFile(String filename) {
	
		File retFile;
		String retString;
		String path = this.getDir("filesdir", Context.MODE_PRIVATE) + "/" + filename;
        retFile = new File(path);
        
        if(!retFile.exists()) {
           	try {
        	    InputStream is = getAssets().open(filename);
        	    int size = is.available();
        	    byte[] buffer = new byte[size];
        	    is.read(buffer);
        	    is.close();


        	    FileOutputStream fos = new FileOutputStream(retFile);
        	    fos.write(buffer);
        	    fos.close();
        	} catch (Exception e) { throw new RuntimeException(e); }

        } 		
		
    	String str;
    	StringBuffer finalStringBuffer = new StringBuffer();

    	BufferedReader inputBuf;
    	try {
			inputBuf = new BufferedReader(new InputStreamReader(new FileInputStream(retFile), "UTF-8"));
			 
		    while (null != ((str = inputBuf.readLine()))) {  
		      	finalStringBuffer.append(str);  
		    }  
		        
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return HTMLProjectHelper.handlingURLEncoding(finalStringBuffer.toString());
	}
	
	
	
	synchronized Tracker getTracker(TrackerName trackerId) {
	    if (!mTrackers.containsKey(trackerId)) {

	      GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
	      Tracker t = analytics.newTracker(R.xml.analytics_tracker);
	      t.setAnonymizeIp(true);
	      InfoVO info = ManagerFactory.buildNetworkManager(this).getInfo();
	      if(info != null) {
		      if(info.getGoogleAnalyticsID() != null) {
		    	  t.set("ga_trackingId", info.getGoogleAnalyticsID());
		      }
		      mTrackers.put(trackerId, t);
		  }
	    }
	    return mTrackers.get(trackerId);
	  }
	
	public void setTrackingID(String id) {
		Tracker t = mTrackers.get(TrackerName.APP_TRACKER);
		if(t != null) {
			t.set("ga_trackingId", id);
		}
	}
	
	public synchronized void trackScreen(String screenName) { 		// Get tracker.
	    Tracker t = getTracker(TrackerName.APP_TRACKER);

	    if(t != null) {
	    	// Set screen name.
	    	// Where path is a String representing the screen name.
	    	t.setScreenName(screenName);

	    	// Send a screen view.
	    	t.send(new HitBuilders.AppViewBuilder().build());
	    }
	}
	
	public synchronized void trackEvent(String categoryId, String actionId, String labelId) { 		// Get tracker.
	    Tracker t = getTracker(TrackerName.APP_TRACKER);

        // Build and send an Event.
        t.send(new HitBuilders.EventBuilder()
            .setCategory(categoryId)
            .setAction(actionId)
            .setLabel(labelId)
            .build());
	}
	
}

package com.einrichtungspartnerring.wirhabens;

import java.util.Date;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.einrichtungspartnerring.wirhabens.gui.adapter.PicTextAdapter.ViewHolder;
import com.einrichtungspartnerring.wirhabens.gui.listener.competitiondetail.DescButtonListener;
import com.einrichtungspartnerring.wirhabens.gui.listener.competitiondetail.JoinIdeasButtonListener;
import com.einrichtungspartnerring.wirhabens.gui.listener.competitiondetail.RulesButtonListener;
import com.einrichtungspartnerring.wirhabens.gui.listener.competitiondetail.ShowIdeasButtonListener;
import com.einrichtungspartnerring.wirhabens.helper.AirbrakeHelper;
import com.einrichtungspartnerring.wirhabens.helper.HTMLProjectHelper;
import com.einrichtungspartnerring.wirhabens.imagedownloader.ImageDownloader;
import com.einrichtungspartnerring.wirhabens.manager.CompetitionIdeasManager;
import com.einrichtungspartnerring.wirhabens.manager.ManagerFactory;
import com.einrichtungspartnerring.wirhabens.vo.CompetitionVO;

/**
 * Details for the competitions.
 * 
 * @author Thomas Möller
 * 
 */
public class CompetitionDetailActivity extends Activity {

    protected boolean isCompetitionOver(Date competitionDate){
        Date current = new Date();
        long dayInMilli = competitionDate.getTime() - current.getTime();
        if (dayInMilli >= 0)
            return false;
        return true;
    }

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		try{
			
			if (!MainActivity.wasStarted){
				Intent i = new Intent(getBaseContext(), MainActivity.class);
				i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(i);
			}
			if (android.os.Build.VERSION.SDK_INT <= Build.VERSION_CODES.GINGERBREAD_MR1){
				MainActivity.customActionBar();
			}
			else{
				MainActivity.customActionBar(getActionBar(), this);
			}
			setContentView(R.layout.activity_competition_detail);

			Bundle extras = getIntent().getExtras();
            if (extras == null)
                finish();

			int itempos = extras.getInt("itempos");

			CompetitionIdeasManager comManager = ManagerFactory
					.buildCompetitionIdeasManager(this);

			CompetitionVO competition = comManager.getItemByPosition(itempos, true);
			competition.setReaded(true);

			String tempStr = "";
			TextView title = (TextView) findViewById(R.id.competition_detail_title);
			
			if(competition.getTitle() != null) {
				title.setText(competition.getTitle());
			} else {
				title.setText(tempStr);
			}

			TextView teasertext = (TextView) findViewById(R.id.competition_detail_teasertext);
			
			if(competition.getTeasertext() != null) {
				teasertext.setText(Html.fromHtml(competition.getTeasertext()));
			} else {
				teasertext.setText(tempStr);
			}

			Button descButton = (Button) findViewById(R.id.competition_detail_button_desc);
			descButton.setOnClickListener(new DescButtonListener(this, competition));

			Button rulesButton = (Button) findViewById(R.id.competition_detail_button_rules);
			rulesButton.setOnClickListener(new RulesButtonListener(this,
					competition));

			Button showIdeasButton = (Button) findViewById(R.id.competition_detail_button_show_ideas);
			showIdeasButton.setOnClickListener(new ShowIdeasButtonListener(this,
					competition, ManagerFactory.buildNetworkManager(this)));

			Button joinButton = (Button) findViewById(R.id.competition_detail_button_join);
			joinButton.setOnClickListener(new JoinIdeasButtonListener(this,
					competition, ManagerFactory.buildNetworkManager(this)));

            if (isCompetitionOver(competition.getEndDate())){
                joinButton.setVisibility(View.GONE);
                if (competition.getEndText() != null)
                    teasertext.setText(Html.fromHtml(competition.getEndText()));
            }

			ImageView imageView = (ImageView) findViewById(R.id.competition_detail_image);

			if (competition.getAdapterImagePath() != null){
				ImageDownloader iDown = new ImageDownloader(this,ManagerFactory.buildImageManager(this));
				ViewHolder viewHolder = new ViewHolder();
            	viewHolder.image = imageView;
            	viewHolder.id = competition.getId();
				iDown.add(viewHolder, competition.getId(), competition.getAdapterImagePath());
			}else
				imageView.setVisibility(View.INVISIBLE);
			
			((StaffsaleApplication) getApplication()).trackEvent(getString(R.string.ga_category_competition),getString(R.string.ga_action_read),competition.getId());	
			
		}catch(Exception e){
			String eMessage = "Unknown Exception";
			AirbrakeHelper.sendException(e, eMessage);
		}
	}
}

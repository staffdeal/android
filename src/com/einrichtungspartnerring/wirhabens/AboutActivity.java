package com.einrichtungspartnerring.wirhabens;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.TextView;

import com.einrichtungspartnerring.wirhabens.activityhelper.InfoLoadAT;
import com.einrichtungspartnerring.wirhabens.activityhelper.events.OnInfoLoadCompleteEvent;
import com.einrichtungspartnerring.wirhabens.helper.AirbrakeHelper;
import com.einrichtungspartnerring.wirhabens.helper.AndroidInformationHelper;
import com.einrichtungspartnerring.wirhabens.manager.ManagerFactory;
import com.einrichtungspartnerring.wirhabens.manager.NetworkManager;
import com.einrichtungspartnerring.wirhabens.vo.InfoVO;

public class AboutActivity extends Activity implements OnInfoLoadCompleteEvent {

	private NetworkManager netManager;
	
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		try{
			if (android.os.Build.VERSION.SDK_INT <= Build.VERSION_CODES.GINGERBREAD_MR1){
				MainActivity.customActionBar();
			}
			else{
				MainActivity.customActionBar(getActionBar(), this);
			}
			setContentView(R.layout.activity_about);

			netManager = ManagerFactory.buildNetworkManager(this);

			if (netManager.getInfo() == null ){
				new InfoLoadAT(this, netManager, this).execute();
			}else
				onInfoLoadComplete();
			
		}catch(Exception e){
			String eMessage = "Unknown Exception";
			AirbrakeHelper.sendException(e, eMessage);
		}

		((StaffsaleApplication) getApplication()).trackScreen(getString(R.string.ga_screen_legal));
		((StaffsaleApplication) getApplication()).trackScreen(getString(R.string.ga_screen_about));
	}

	@Override
	public void onInfoLoadComplete(Object... objects) {
		WebView aboutView = (WebView) findViewById(R.id.about_webview_content);
		WebView impressView = (WebView) findViewById(R.id.impress_webview_content);
		TextView versionInfoText = (TextView) findViewById(R.id.about_versioninfo_versiontext);
		InfoVO info = netManager.getInfo();
		
		if (info != null){
			if (info.getClientAbout() != null){
				WebSettings settings = aboutView.getSettings();
				settings.setDefaultFontSize(20);
				Spanned spannedText = Html.fromHtml(info.getClientAbout());
				TextView textSp = new TextView(this);
				textSp.setText(spannedText);
				aboutView.loadData(textSp.getText().toString(), "text/html", "utf-8");
			}
			if (info.getClientImpressum() != null){
				WebSettings settings = impressView.getSettings();
				settings.setDefaultFontSize(20);
				Spanned spannedText = Html.fromHtml(info.getClientImpressum());
				TextView textSp = new TextView(this);
				textSp.setText(spannedText);
				impressView.loadData(textSp.getText().toString(), "text/html", "utf-8");
			}
		}
		String versionText = "Version: %s \n Version-Code: %s \n Android-Device: %s";
		try {
			versionText = String.format(versionText, AndroidInformationHelper.getAPKVersionName(this), 
					AndroidInformationHelper.getAPKVersionCode(this),
					AndroidInformationHelper.getAndroidReleaseVersion());
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}
		versionInfoText.setText(versionText);
	}
	
	@Override
	protected void onResume(){

		super.onResume();
		((StaffsaleApplication) getApplication()).trackScreen(getString(R.string.ga_screen_legal));
		((StaffsaleApplication) getApplication()).trackScreen(getString(R.string.ga_screen_about));
	
	}

	
}

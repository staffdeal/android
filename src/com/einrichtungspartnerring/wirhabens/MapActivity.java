package com.einrichtungspartnerring.wirhabens;

import android.app.Dialog;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;

import com.einrichtungspartnerring.wirhabens.activityhelper.MapDealerDrawerAT;
import com.einrichtungspartnerring.wirhabens.gui.listener.maps.NoMapsFinishButtonListener;
import com.einrichtungspartnerring.wirhabens.helper.AirbrakeHelper;
import com.einrichtungspartnerring.wirhabens.helper.MeinERPDialogBuilder;
import com.einrichtungspartnerring.wirhabens.manager.DealsManager;
import com.einrichtungspartnerring.wirhabens.manager.ManagerFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;

/**
 * Activity for the google maps
 * @author Thomas Möller
 *
 */
public class MapActivity extends FragmentActivity {

	public static final String EXTRA_DEAL_ID = "dealid";

	public boolean isGoogleMapsInstalled()
	{
		try
		{
			getPackageManager().getApplicationInfo("com.google.android.apps.maps", 0 );
			return true;
		} 
		catch(PackageManager.NameNotFoundException e)
		{
			return false;
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		try{
			if (isGoogleMapsInstalled()){
				GoogleMap mMap;
				SupportMapFragment mMapFragment;
				setContentView(R.layout.activity_map);
				if (android.os.Build.VERSION.SDK_INT <= Build.VERSION_CODES.GINGERBREAD_MR1){
					MainActivity.customActionBar();
				}
				else{
					MainActivity.customActionBar(getActionBar(), this);

				}


				DealsManager dealerManager = ManagerFactory.buildDealsManager(this);
				Bundle extra = getIntent().getExtras();

				FragmentManager fManager = getSupportFragmentManager();
				if (fManager != null){
					mMapFragment = ((SupportMapFragment)fManager.findFragmentById(R.id.activity_map_map));
					mMap =   mMapFragment.getMap();
					if (mMap != null)
						new MapDealerDrawerAT(mMap, dealerManager, extra.getString(EXTRA_DEAL_ID), this).execute();
					else{
						Dialog error = MeinERPDialogBuilder.buildInformationDialog(this, 
								getString(R.string.maps_not_exist_error_title), 
								getString(R.string.maps_not_exist_error_text)
								,new NoMapsFinishButtonListener(this));
						error.show();
					}
				}else{
					Dialog error = MeinERPDialogBuilder.buildInformationDialog(this, 
							getString(R.string.maps_not_exist_error_title), 
							getString(R.string.maps_not_exist_error_text)
							,new NoMapsFinishButtonListener(this));
					error.show();
				}
			}else{
				Dialog error = MeinERPDialogBuilder.buildInformationDialog(this, 
						getString(R.string.maps_not_exist_error_title), 
						getString(R.string.maps_not_exist_error_text)
						,new NoMapsFinishButtonListener(this));
				error.show();
			}
		}catch(Exception e){
			String eMessage = "Unknown Exception";
			AirbrakeHelper.sendException(e, eMessage);
		}
	}
}

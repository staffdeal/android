package com.einrichtungspartnerring.wirhabens;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Locale;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Paint;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.einrichtungspartnerring.wirhabens.activityhelper.events.OnTaskDialogAcceptEvent;
import com.einrichtungspartnerring.wirhabens.gui.adapter.PicTextAdapter.ViewHolder;
import com.einrichtungspartnerring.wirhabens.gui.listener.dealdetail.*;
import com.einrichtungspartnerring.wirhabens.helper.AirbrakeHelper;
import com.einrichtungspartnerring.wirhabens.helper.ImageHelper;
import com.einrichtungspartnerring.wirhabens.imagedownloader.ImageDownloader;
import com.einrichtungspartnerring.wirhabens.manager.DealsManager;
import com.einrichtungspartnerring.wirhabens.manager.ImageManager;
import com.einrichtungspartnerring.wirhabens.manager.ManagerFactory;
import com.einrichtungspartnerring.wirhabens.manager.NetworkManager;
import com.einrichtungspartnerring.wirhabens.vo.DealsItemVO;

/**
 * Activity for the deals
 * 
 * @author Thomas Möller
 * 
 */
public class DealActivity extends Activity implements OnTaskDialogAcceptEvent{

	private static final String TAG = "DealActivity";
	private static int lastItem = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		try{
			if (!MainActivity.wasStarted){
				Intent i = new Intent(getBaseContext(), MainActivity.class);
				i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(i);
			}else{

				setContentView(R.layout.activity_deal);

				if (android.os.Build.VERSION.SDK_INT <= Build.VERSION_CODES.GINGERBREAD_MR1){
					MainActivity.customActionBar();
				}
				else{
					MainActivity.customActionBar(getActionBar(), this);
				}

				Bundle extras = getIntent().getExtras();
                if (extras == null)
                    finish();
				int itempos = lastItem;
				if(extras.containsKey("item")){
					itempos = extras.getInt("item");
					lastItem = itempos;
				}

				DealsManager dealsmanager = ManagerFactory.buildDealsManager(this);
				DealsItemVO vo = dealsmanager.getItemByPosition(itempos, true);
                if (vo == null)                // In Case of item in background is deleted.
                    finish();

				vo.setReaded(true);

				TextView date = (TextView) findViewById(R.id.deal_date);
				TextView title = (TextView) findViewById(R.id.deal_title);
				TextView enterprise = (TextView) findViewById(R.id.deal_enterprise);

				ImageManager iManager = ManagerFactory.buildImageManager(this);
				File imageFile = iManager.getIfExist(vo.getAdapterImagePath());
				ImageView iView = (ImageView) findViewById(R.id.deal_image);
				if (imageFile != null && imageFile.exists()) {
					Bitmap imageBitmap = ImageHelper.decodeAndScaleFile(imageFile, 100,100);
					iView.setImageBitmap(imageBitmap);
				} else {
					ImageDownloader iDown = new ImageDownloader(this, iManager);
					if (iDown != null && vo.getAdapterImagePath() != null && !vo.getAdapterImagePath().isEmpty()) {
						ViewHolder viewHolder = new ViewHolder();
	                	viewHolder.image = iView;
	                	viewHolder.id = vo.getId();
						iDown.add(viewHolder, vo.getId(), vo.getAdapterImagePath());
					}
				}

				SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy", Locale.GERMANY);
				date.setText(getString(R.string.deal_date_head_text) + " "+ df.format(vo.getEndDate()));

				enterprise.setText(vo.getProvider());

				title.setText(vo.getTitle());

				TextView teaser = (TextView) findViewById(R.id.deal_teaser_text);
				teaser.setText(vo.getTeaser());
				setButtonVisible(vo);

                TextView price =  (TextView) findViewById(R.id.deal_price);
                if (vo.getPrice() != null && !vo.getPrice().isEmpty()){
                    price.setVisibility(View.VISIBLE);
                    price.setText(vo.getPrice());
                } else{
                    price.setVisibility(View.GONE);
                }

                TextView oldPrice =  (TextView) findViewById(R.id.deal_old_price);
                if (vo.getOldPrice() != null && !vo.getOldPrice().isEmpty()){
                    oldPrice.setVisibility(View.VISIBLE);
                    oldPrice.setText(vo.getOldPrice());
                    oldPrice.setPaintFlags(oldPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                } else{
                    oldPrice.setVisibility(View.GONE);
                }

                ((StaffsaleApplication) getApplication()).trackEvent(getString(R.string.ga_category_deals),getString(R.string.ga_action_read),vo.getId());	

			}
		}catch(Exception e){
			String eMessage = "Unknown Exception";
			AirbrakeHelper.sendException(e, eMessage);
		}
	}

	protected void setButtonVisible (DealsItemVO vo){

		Button descButton = (Button) findViewById(R.id.deal_button_desc);
		descButton.setOnClickListener(new DescButtonListener(this, vo));

        NetworkManager netManager = ManagerFactory.buildNetworkManager(this);
        DealsManager dealsManager = ManagerFactory.buildDealsManager(this);

        Button saveOnlineButton = (Button) findViewById(R.id.deal_button_saved_online);
        saveOnlineButton.setOnClickListener(new SaveButtonListener(netManager, this, vo, dealsManager, SaveButtonListener.ListenerSaveSource.online));

        Button onlineCashButton = (Button) findViewById(R.id.deal_button_online_cash);
        Button onlineDescriptionButton =  (Button) findViewById(R.id.deal_button_online_description);
        Button onlineShowCode =  (Button) findViewById(R.id.deal_button_online_show_code);

        onlineCashButton.setOnClickListener(new OnlineCashButtonListener(this, vo));
        onlineDescriptionButton.setOnClickListener(new OnlineDescriptionButtonListener(vo.getOnlineDescription(), this));
        onlineShowCode.setOnClickListener(new OnlineShowCodeButtonListener(this, vo));

        ImageView siteImageView = (ImageView) findViewById(R.id.deal_layout_ausverkauft);

		if (vo.getOnlineUrl() == null) {

            onlineCashButton.setVisibility(View.GONE);
            onlineDescriptionButton.setVisibility(View.GONE);
            onlineShowCode.setVisibility(View.GONE);

            saveOnlineButton.setVisibility(View.GONE);

        }
        else if (vo.getOnlineUrl() != null && !vo.isOnlineSaved() ) {
            onlineCashButton.setVisibility(View.GONE);
            onlineDescriptionButton.setVisibility(View.GONE);
            onlineShowCode.setVisibility(View.GONE);

            saveOnlineButton.setVisibility(View.VISIBLE);

            if (vo.isPassOnlineAvaible() == false) {
                saveOnlineButton.setClickable(false);
                siteImageView.setVisibility(View.VISIBLE);
            } else {
                siteImageView.setVisibility(View.GONE);
            }
        }
		else{
            saveOnlineButton.setVisibility(View.GONE);
			onlineCashButton.setVisibility(View.VISIBLE);
            if (vo.getOnlineDescription() != null && !vo.getOnlineDescription().isEmpty())
                onlineDescriptionButton.setVisibility(View.VISIBLE);
            else
                onlineDescriptionButton.setVisibility(View.GONE);
            if (vo.getOnlineUrl() != null && vo.getOnlineUrl().trim().isEmpty() ==  false && vo.getPassOnlineUrl() != null && vo.getPassOnlineUrl().trim().isEmpty() == false )
                onlineShowCode.setVisibility(View.VISIBLE);
            else
                onlineShowCode.setVisibility(View.GONE);
		}

        Button saveOnSiteButton = (Button) findViewById(R.id.deal_button_saved_onsite);
        saveOnSiteButton.setOnClickListener(new SaveButtonListener(netManager, this, vo, dealsManager, SaveButtonListener.ListenerSaveSource.onsite));
		Button offlineButton = (Button) findViewById(R.id.deal_button_local_cash);
		offlineButton.setOnClickListener(new OfflineCashButtonListener(this, vo));
		if (vo.getPassOnSiteUrl() == null) {
            offlineButton.setVisibility(View.GONE);
            saveOnSiteButton.setVisibility(View.GONE);
        }
        else if (vo.getPassOnSiteUrl() != null && !vo.isOnSiteSaved() ) {
            offlineButton.setVisibility(View.GONE);
            saveOnSiteButton.setVisibility(View.VISIBLE);
            siteImageView.setVisibility(View.GONE);
        }
		else{
            siteImageView.setVisibility(View.GONE);
			offlineButton.setVisibility(View.VISIBLE);
            saveOnSiteButton.setVisibility(View.GONE);
		}

		Button searchStoreButton = (Button) findViewById(R.id.deal_button_search_store);
		searchStoreButton.setOnClickListener(new SearchStoreButtonListener(this, vo));
		if (vo.isOnlineSaved() || vo.isOnSiteSaved())
			searchStoreButton.setVisibility(View.VISIBLE);
		else{
			searchStoreButton.setVisibility(View.GONE);
		}

	}

	@Override
	public void onTaskCompleted(Object... objects) {
		if (objects != null && objects.length > 0 && objects[0] instanceof DealsItemVO)
			setButtonVisible ((DealsItemVO) objects[0]);
		else
			Log.w(TAG, "Callback get wrong params");
	}

}

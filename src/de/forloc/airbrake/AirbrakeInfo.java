package de.forloc.airbrake;

/**
 * Information about Airbrake
 * @author Thomas Möller
 *
 */
public interface AirbrakeInfo {

	
	// Basic settings
	String AIRBRAKE_API_VERSION = "2.3";
	String NOTIFIER_NAME = "Android Airbrake Notifier";
	String NOTIFIER_VERSION = "0.1.0";
    String NOTIFIER_URL = "https://www.wir-habens.com";
    
    String ENVIRONMENT_PRODUCTION = "production";
    String ENVIRONMENT_DEFAULT = ENVIRONMENT_PRODUCTION;
	
}

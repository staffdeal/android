package de.forloc.airbrake;

import java.io.IOException;
import java.io.StringWriter;
import java.util.Map;

import org.xmlpull.v1.XmlSerializer;

import android.util.Log;
import android.util.Xml;

/**
 * Builder for the Airbrake notice.
 * @author Thomas Möller
 *
 */
public class AirbrakeNoticeBuilder implements AirbrakeInfo{

	/**
	 * TAG for logging.
	 */
	private final static String TAG = "AirbrakeNoticeBuilder";
	
	/**
	 * Key from the API.
	 */
	private String apiKey;
	/**
	 * Exception you want send.
	 */
	private Exception e;
	/**
	 * Version of the program.
	 */
	private String versionName;
	/**
	 * Model of the Phone you want to use
	 */
	private static String phoneModel = android.os.Build.MODEL;
	/**
	 * Version of android which is used.
	 */
	private static String androidVersion = android.os.Build.VERSION.RELEASE;
	/**
	 * You current environment.
	 */
	private static String environmentName = ENVIRONMENT_DEFAULT;

	/**
	 * Default constructor
	 */
	private AirbrakeNoticeBuilder() {
		super();
		versionName = "not set";
	}

	/**
	 * Minimal init constructor
	 * @param apiKey - key of the API
	 * @param e - you exception
	 */
	public AirbrakeNoticeBuilder(String apiKey, Exception e) {
		this();
		this.apiKey = apiKey;
		this.e = e;
	}

	/**
	 * init constructor
	 * @param apiKey - key of the API
	 * @param e - you exception
	 * @param versionName - current program version
	 */
	public AirbrakeNoticeBuilder(String apiKey, Exception e, String versionName) {
		this(apiKey, e);
		this.versionName = versionName;
	}

	/**
	 * Build a new notice and return it.
	 * @return a new notice.
	 */
	public AirbrakeNotice newNotice(){
		String xml = buildXMLError(null);
		return new AirbrakeNotice(xml);
	}
	
	/**
	 * Build a new notice with extra informations.
	 * @param extraData - map with extra informations.
	 * @return the new notice.
	 */
	public AirbrakeNotice newNotice(Map<String,String> extraData){
		String xml = buildXMLError(extraData);
		return new AirbrakeNotice(xml);
	}

	/**
	 * Build the xml from the exception informations.
	 * @param extraData - set extra data into the notice.
	 * @return the xml from the exception in Airbrake format.
	 */
	private String buildXMLError(Map<String,String> extraData){
		XmlSerializer s = Xml.newSerializer();
		StringWriter sWriter = new StringWriter();
		try {
			s.setOutput(sWriter);
			s.startDocument("UTF-8", true);
			// Top level tag
			s.startTag("", "notice");
			s.attribute("", "version", AIRBRAKE_API_VERSION);

			// Fill in the api key
			s.startTag("", "api-key");
			s.text(apiKey);
			s.endTag("", "api-key");

			// Fill in the notifier data
			s.startTag("", "notifier");
			s.startTag("", "name");
			s.text(NOTIFIER_NAME);
			s.endTag("", "name");
			s.startTag("", "version");
			s.text(NOTIFIER_VERSION);
			s.endTag("", "version");
			s.startTag("", "url");
			s.text(NOTIFIER_URL);
			s.endTag("", "url");
			s.endTag("", "notifier");

			// Fill in the error info
			s.startTag("", "error");
			s.startTag("", "class");
			s.text(e.getClass().getName());
			s.endTag("", "class");
			s.startTag("", "message");
			s.text("[" + versionName + "] " + e.toString());
			s.endTag("", "message");

			// Extract the stack traces
			s.startTag("", "backtrace");
			Throwable currentEx = e;
			while(currentEx != null) {
				// Catch some inner exceptions without discarding the entire report
				try {
					StackTraceElement[] stackTrace = currentEx.getStackTrace();
					for(StackTraceElement el : stackTrace) {
						s.startTag("", "line");
						try{
							s.attribute("", "method", el.getClassName() + "." + el.getMethodName());
							s.attribute("", "file", el.getFileName() == null ? "Unknown" : el.getFileName());
							s.attribute("", "number", String.valueOf(el.getLineNumber()));
						}catch(Throwable ex){
							Log.v(TAG, "Exception caught:",ex);
						}
						s.endTag("", "line");
					}

					currentEx = currentEx.getCause();
					if(currentEx != null) {
						s.startTag("", "line");
						try{
							s.attribute("", "file", "### CAUSED BY ###: " + currentEx.toString());
							s.attribute("", "number", "");
						}catch(Throwable ex){
							Log.v(TAG, "Exception caught:",ex);
						}
						s.endTag("", "line");
					}
				} catch(Throwable innerException) {
					Log.v(TAG, "Exception caught:",e);
					break;
				}
			}
			s.endTag("", "backtrace");
			s.endTag("", "error");
			
			// Additional request info
            s.startTag("", "request");

            s.startTag("", "url");
            s.endTag("", "url");
            s.startTag("", "component");
            s.endTag("", "component");
            s.startTag("", "action");
            s.endTag("", "action");
            s.startTag("", "cgi-data");
            s.startTag("", "var");
            s.attribute("", "key", "Device");
            s.text(phoneModel);
            s.endTag("", "var");
            s.startTag("", "var");
            s.attribute("", "key", "Android Version");
            s.text(androidVersion);
            s.endTag("", "var");
            s.startTag("", "var");
            s.attribute("", "key", "App Version");
            s.text(versionName);
            s.endTag("", "var");
         // Extra info, if present
            if (extraData != null && !extraData.isEmpty()) {
                for (Map.Entry<String,String> extra : extraData.entrySet()) {
                    s.startTag("", "var");
                    s.attribute("", "key", extra.getKey());
                    s.text(extra.getValue());
                    s.endTag("", "var");
                }
            }
            
            s.endTag("", "cgi-data");
            s.endTag("", "request");
            
            // Production/development mode flag and app version
            s.startTag("", "server-environment");
            s.startTag("", "environment-name");
            s.text(environmentName);
            s.endTag("", "environment-name");
            s.startTag("", "app-version");
            s.text(versionName);
            s.endTag("", "app-version");
            s.endTag("", "server-environment");

            // Close document
            s.endTag("", "notice");
            s.endDocument();
			
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return sWriter.toString();
	}

}

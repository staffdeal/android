package de.forloc.airbrake;

/**
 * A Notice VO
 * @author Thomas Möller
 *
 */
public class AirbrakeNotice {

	private String xml;
	
	protected AirbrakeNotice(String xml) {
		this.setXml(xml);
	}

	public String getXml() {
		return xml;
	}

	public void setXml(String xml) {
		this.xml = xml;
	}

}

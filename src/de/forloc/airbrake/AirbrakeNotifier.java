package de.forloc.airbrake;

import java.io.IOException;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;

import android.os.AsyncTask;
import android.util.Log;

/**
 * Notifier for Airbrake. Send data to the server n a AsyncTask
 * @author Thomas Möller
 *
 */
public class AirbrakeNotifier extends AsyncTask<String, String, HttpResponse>{

	private static final String TAG = "AirbrakeNotifier";
	/**
	 * where to send the error message.
	 */
	private String url;
	/**
	 * message you want to send.
	 */
	private AirbrakeNotice notice;

	/**
	 * Init constructor
	 * @param url - where to send the error message.
	 * @param notice - message you want to send.
	 */
	public AirbrakeNotifier(String url, AirbrakeNotice notice) {
		this.url = url;
		this.notice = notice;
	}

	@Override
	protected HttpResponse doInBackground(String... params) {
		HttpClient client = new DefaultHttpClient();
		HttpPost httppost = new HttpPost(this.url);
		HttpResponse response = null;

		try {
			httppost.setEntity(new ByteArrayEntity(notice.getXml().getBytes()));
			httppost.setHeader("Content-type", "text/xml");

			response = client.execute(httppost);
		} catch (ClientProtocolException e) {
			Log.e(TAG, "exception by sending exception", e);
		} catch (IOException e) {
			Log.e(TAG, "exception by sending exception", e);
		} catch (Exception e){
			Log.e(TAG, "exception by sending exception", e);
		}
		return response;
	}

	@Override
	protected void onPostExecute(HttpResponse result) {
		if (result != null && result.getStatusLine() != null)
			Log.i(TAG, "send Airbrake message. Response-Code: "+result.getStatusLine().getStatusCode() );
		super.onPostExecute(result);
	}
}
